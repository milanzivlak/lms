-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: lms
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `administrator` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `registrovani_korisnik_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK615ujs81obaqlp9p6uvcokwtw` (`registrovani_korisnik_id`),
  CONSTRAINT `FK615ujs81obaqlp9p6uvcokwtw` FOREIGN KEY (`registrovani_korisnik_id`) REFERENCES `registrovani_korisnik` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (1,1);
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adresa`
--

DROP TABLE IF EXISTS `adresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adresa` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `broj` longtext,
  `ulica` longtext,
  `fakultet_id` bigint DEFAULT NULL,
  `mesto_id` bigint NOT NULL,
  `univerzitet_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4tj63cl0b0la9lgtdxkc9v5jb` (`fakultet_id`),
  KEY `FKs3cl12kvk7ugqu3wi2u9y7vd0` (`mesto_id`),
  KEY `FKbiukt6ogofbooohtq6ukauan0` (`univerzitet_id`),
  CONSTRAINT `FK4tj63cl0b0la9lgtdxkc9v5jb` FOREIGN KEY (`fakultet_id`) REFERENCES `fakultet` (`id`),
  CONSTRAINT `FKbiukt6ogofbooohtq6ukauan0` FOREIGN KEY (`univerzitet_id`) REFERENCES `univerzitet` (`id`),
  CONSTRAINT `FKs3cl12kvk7ugqu3wi2u9y7vd0` FOREIGN KEY (`mesto_id`) REFERENCES `mesto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2096665370 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adresa`
--

LOCK TABLES `adresa` WRITE;
/*!40000 ALTER TABLE `adresa` DISABLE KEYS */;
INSERT INTO `adresa` VALUES (1,'7','Dositeja',1,1,1),(2,'15','dada',2,2,2);
/*!40000 ALTER TABLE `adresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autor`
--

DROP TABLE IF EXISTS `autor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `autor` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `ime` longtext,
  `prezime` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autor`
--

LOCK TABLES `autor` WRITE;
/*!40000 ALTER TABLE `autor` DISABLE KEYS */;
INSERT INTO `autor` VALUES (1,'Dragan','Dragic'),(2,'Milos','Milosevic');
/*!40000 ALTER TABLE `autor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autor_nastavni_materijali`
--

DROP TABLE IF EXISTS `autor_nastavni_materijali`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `autor_nastavni_materijali` (
  `autor_id` bigint NOT NULL,
  `nastavni_materijali_id` bigint NOT NULL,
  PRIMARY KEY (`autor_id`,`nastavni_materijali_id`),
  KEY `FK24b4ialuuoggox42y3qwc5hil` (`nastavni_materijali_id`),
  CONSTRAINT `FK24b4ialuuoggox42y3qwc5hil` FOREIGN KEY (`nastavni_materijali_id`) REFERENCES `nastavni_materijal` (`id`),
  CONSTRAINT `FKbthbpycsncjp0rmi3fbld70un` FOREIGN KEY (`autor_id`) REFERENCES `autor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autor_nastavni_materijali`
--

LOCK TABLES `autor_nastavni_materijali` WRITE;
/*!40000 ALTER TABLE `autor_nastavni_materijali` DISABLE KEYS */;
INSERT INTO `autor_nastavni_materijali` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `autor_nastavni_materijali` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caretaker`
--

DROP TABLE IF EXISTS `caretaker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `caretaker` (
  `id` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caretaker`
--

LOCK TABLES `caretaker` WRITE;
/*!40000 ALTER TABLE `caretaker` DISABLE KEYS */;
INSERT INTO `caretaker` VALUES (1),(2);
/*!40000 ALTER TABLE `caretaker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drzava`
--

DROP TABLE IF EXISTS `drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `drzava` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1879393776 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava`
--

LOCK TABLES `drzava` WRITE;
/*!40000 ALTER TABLE `drzava` DISABLE KEYS */;
INSERT INTO `drzava` VALUES (1,'Nemacka'),(2,'Srbija');
/*!40000 ALTER TABLE `drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluacija_znanja`
--

DROP TABLE IF EXISTS `evaluacija_znanja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evaluacija_znanja` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bodovi` int NOT NULL,
  `vreme_pocetka` datetime DEFAULT NULL,
  `vreme_zavrsetka` datetime DEFAULT NULL,
  `ishod_id` bigint DEFAULT NULL,
  `polaganje_id` bigint DEFAULT NULL,
  `realizacija_predmeta_id` bigint DEFAULT NULL,
  `tip_evaluacije_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKop318lluohc1b7fifg30hwbex` (`ishod_id`),
  KEY `FKc1cfnttkiqpfhl2363g3elpd1` (`polaganje_id`),
  KEY `FKmicppx3e9v4gl9wn4i6l46v7p` (`realizacija_predmeta_id`),
  KEY `FKrld48go4m6p9x3s0baevu51un` (`tip_evaluacije_id`),
  CONSTRAINT `FKc1cfnttkiqpfhl2363g3elpd1` FOREIGN KEY (`polaganje_id`) REFERENCES `polaganje` (`id`),
  CONSTRAINT `FKmicppx3e9v4gl9wn4i6l46v7p` FOREIGN KEY (`realizacija_predmeta_id`) REFERENCES `realizacija_predmeta` (`id`),
  CONSTRAINT `FKop318lluohc1b7fifg30hwbex` FOREIGN KEY (`ishod_id`) REFERENCES `ishod` (`id`),
  CONSTRAINT `FKrld48go4m6p9x3s0baevu51un` FOREIGN KEY (`tip_evaluacije_id`) REFERENCES `tip_evaluacije` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluacija_znanja`
--

LOCK TABLES `evaluacija_znanja` WRITE;
/*!40000 ALTER TABLE `evaluacija_znanja` DISABLE KEYS */;
INSERT INTO `evaluacija_znanja` VALUES (1,12,'2020-12-12 00:00:00','2020-12-12 00:00:00',1,1,1,1),(2,16,'2020-10-10 00:00:00','2020-10-10 00:00:00',2,2,2,2);
/*!40000 ALTER TABLE `evaluacija_znanja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fajl`
--

DROP TABLE IF EXISTS `fajl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fajl` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `opis` longtext,
  `url` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fajl`
--

LOCK TABLES `fajl` WRITE;
/*!40000 ALTER TABLE `fajl` DISABLE KEYS */;
INSERT INTO `fajl` VALUES (1,'materijal','www.google.com'),(2,'nastava','www.youtube.com');
/*!40000 ALTER TABLE `fajl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fajl_instrumenti_evaluacije`
--

DROP TABLE IF EXISTS `fajl_instrumenti_evaluacije`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fajl_instrumenti_evaluacije` (
  `fajl_id` bigint NOT NULL,
  `instrumenti_evaluacije_id` bigint NOT NULL,
  PRIMARY KEY (`fajl_id`,`instrumenti_evaluacije_id`),
  KEY `FKhyw1pkerw1ekb8306qk6c2lyp` (`instrumenti_evaluacije_id`),
  CONSTRAINT `FKfvgcuxl2x9e2glg9v5u7vhhed` FOREIGN KEY (`fajl_id`) REFERENCES `fajl` (`id`),
  CONSTRAINT `FKhyw1pkerw1ekb8306qk6c2lyp` FOREIGN KEY (`instrumenti_evaluacije_id`) REFERENCES `instrument_evaluacije` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fajl_instrumenti_evaluacije`
--

LOCK TABLES `fajl_instrumenti_evaluacije` WRITE;
/*!40000 ALTER TABLE `fajl_instrumenti_evaluacije` DISABLE KEYS */;
INSERT INTO `fajl_instrumenti_evaluacije` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `fajl_instrumenti_evaluacije` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fajl_nastavni_materijali`
--

DROP TABLE IF EXISTS `fajl_nastavni_materijali`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fajl_nastavni_materijali` (
  `fajl_id` bigint NOT NULL,
  `nastavni_materijali_id` bigint NOT NULL,
  PRIMARY KEY (`fajl_id`,`nastavni_materijali_id`),
  KEY `FKb6cdg5hn2r9r0occj146ysxik` (`nastavni_materijali_id`),
  CONSTRAINT `FKb6cdg5hn2r9r0occj146ysxik` FOREIGN KEY (`nastavni_materijali_id`) REFERENCES `nastavni_materijal` (`id`),
  CONSTRAINT `FKpabp39o7co7we8p41yuekwkiv` FOREIGN KEY (`fajl_id`) REFERENCES `fajl` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fajl_nastavni_materijali`
--

LOCK TABLES `fajl_nastavni_materijali` WRITE;
/*!40000 ALTER TABLE `fajl_nastavni_materijali` DISABLE KEYS */;
INSERT INTO `fajl_nastavni_materijali` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `fajl_nastavni_materijali` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fakultet`
--

DROP TABLE IF EXISTS `fakultet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fakultet` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` longtext,
  `univerzitet_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKc9lc5sekwpb19qpobc7eegjpn` (`univerzitet_id`),
  CONSTRAINT `FKc9lc5sekwpb19qpobc7eegjpn` FOREIGN KEY (`univerzitet_id`) REFERENCES `univerzitet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1848779144 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fakultet`
--

LOCK TABLES `fakultet` WRITE;
/*!40000 ALTER TABLE `fakultet` DISABLE KEYS */;
INSERT INTO `fakultet` VALUES (1,'Singidunum',1),(2,'PMF',2);
/*!40000 ALTER TABLE `fakultet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `godina_studija`
--

DROP TABLE IF EXISTS `godina_studija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `godina_studija` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `godina` date DEFAULT NULL,
  `studijski_program_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK365x07l7paf40u1xlbabq84gq` (`studijski_program_id`),
  CONSTRAINT `FK365x07l7paf40u1xlbabq84gq` FOREIGN KEY (`studijski_program_id`) REFERENCES `studijski_program` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `godina_studija`
--

LOCK TABLES `godina_studija` WRITE;
/*!40000 ALTER TABLE `godina_studija` DISABLE KEYS */;
INSERT INTO `godina_studija` VALUES (1,'2020-01-01',1),(2,'2020-01-01',2);
/*!40000 ALTER TABLE `godina_studija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrument_evaluacije`
--

DROP TABLE IF EXISTS `instrument_evaluacije`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `instrument_evaluacije` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instrument_evaluacije`
--

LOCK TABLES `instrument_evaluacije` WRITE;
/*!40000 ALTER TABLE `instrument_evaluacije` DISABLE KEYS */;
INSERT INTO `instrument_evaluacije` VALUES (1),(2);
/*!40000 ALTER TABLE `instrument_evaluacije` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrument_evaluacije_fajlovi`
--

DROP TABLE IF EXISTS `instrument_evaluacije_fajlovi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `instrument_evaluacije_fajlovi` (
  `instrument_evaluacije_id` bigint NOT NULL,
  `fajlovi_id` bigint NOT NULL,
  PRIMARY KEY (`instrument_evaluacije_id`,`fajlovi_id`),
  KEY `FKf0ly611o2d6i9hwuu3eeb7ll` (`fajlovi_id`),
  CONSTRAINT `FKf0ly611o2d6i9hwuu3eeb7ll` FOREIGN KEY (`fajlovi_id`) REFERENCES `fajl` (`id`),
  CONSTRAINT `FKfe9l4prbemkisft8wg5rbf6n7` FOREIGN KEY (`instrument_evaluacije_id`) REFERENCES `instrument_evaluacije` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instrument_evaluacije_fajlovi`
--

LOCK TABLES `instrument_evaluacije_fajlovi` WRITE;
/*!40000 ALTER TABLE `instrument_evaluacije_fajlovi` DISABLE KEYS */;
INSERT INTO `instrument_evaluacije_fajlovi` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `instrument_evaluacije_fajlovi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ishod`
--

DROP TABLE IF EXISTS `ishod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ishod` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `opis` longtext,
  `nastavni_materijal_id` bigint DEFAULT NULL,
  `obrazovni_cilj_id` bigint DEFAULT NULL,
  `predmet_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKek1cbqc5cv36q9kbufsj52ldp` (`nastavni_materijal_id`),
  KEY `FKbbs7ut8a93q1ipta559tx2tk9` (`obrazovni_cilj_id`),
  KEY `FKshna04judhuvrpku4w60urgfv` (`predmet_id`),
  CONSTRAINT `FKbbs7ut8a93q1ipta559tx2tk9` FOREIGN KEY (`obrazovni_cilj_id`) REFERENCES `obrazovni_cilj` (`id`),
  CONSTRAINT `FKek1cbqc5cv36q9kbufsj52ldp` FOREIGN KEY (`nastavni_materijal_id`) REFERENCES `nastavni_materijal` (`id`),
  CONSTRAINT `FKshna04judhuvrpku4w60urgfv` FOREIGN KEY (`predmet_id`) REFERENCES `predmet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ishod`
--

LOCK TABLES `ishod` WRITE;
/*!40000 ALTER TABLE `ishod` DISABLE KEYS */;
INSERT INTO `ishod` VALUES (1,'Zavrsen',1,1,1),(2,'Skoro gotov',2,2,2);
/*!40000 ALTER TABLE `ishod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memento`
--

DROP TABLE IF EXISTS `memento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `memento` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `caretaker_id` bigint DEFAULT NULL,
  `state_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKondrixims42xxtaegxyjbx82n` (`caretaker_id`),
  KEY `FKq5ayx7ks9vtu60yuqt1laqddt` (`state_id`),
  CONSTRAINT `FKondrixims42xxtaegxyjbx82n` FOREIGN KEY (`caretaker_id`) REFERENCES `caretaker` (`id`),
  CONSTRAINT `FKq5ayx7ks9vtu60yuqt1laqddt` FOREIGN KEY (`state_id`) REFERENCES `nastavni_materijal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memento`
--

LOCK TABLES `memento` WRITE;
/*!40000 ALTER TABLE `memento` DISABLE KEYS */;
INSERT INTO `memento` VALUES (1,1,1),(2,2,2);
/*!40000 ALTER TABLE `memento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mesto`
--

DROP TABLE IF EXISTS `mesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mesto` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` longtext,
  `drzava_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKs8rcbu6hgv8df9mee76jih079` (`drzava_id`),
  CONSTRAINT `FKs8rcbu6hgv8df9mee76jih079` FOREIGN KEY (`drzava_id`) REFERENCES `drzava` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1695854595 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mesto`
--

LOCK TABLES `mesto` WRITE;
/*!40000 ALTER TABLE `mesto` DISABLE KEYS */;
INSERT INTO `mesto` VALUES (1,'Beograd',1),(2,'Novi Sad',1),(3,'Nis',1);
/*!40000 ALTER TABLE `mesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nastavni_materijal`
--

DROP TABLE IF EXISTS `nastavni_materijal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nastavni_materijal` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `datum_azuriranja` datetime DEFAULT NULL,
  `datum_uklanjanja` datetime DEFAULT NULL,
  `godina_izdanja` date DEFAULT NULL,
  `naziv` longtext,
  `version` bigint DEFAULT NULL,
  `istorija_id` bigint DEFAULT NULL,
  `stanje_id` bigint DEFAULT NULL,
  `caretaker_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkvt22a36eq9qe7igfxrswf13u` (`istorija_id`),
  KEY `FKdu7jd2nx0w7irfcq3drhcfg68` (`stanje_id`),
  KEY `FK2919uvagl743by2f8dogbg62v` (`caretaker_id`),
  CONSTRAINT `FK2919uvagl743by2f8dogbg62v` FOREIGN KEY (`caretaker_id`) REFERENCES `caretaker` (`id`),
  CONSTRAINT `FKdu7jd2nx0w7irfcq3drhcfg68` FOREIGN KEY (`stanje_id`) REFERENCES `stanje` (`id`),
  CONSTRAINT `FKkvt22a36eq9qe7igfxrswf13u` FOREIGN KEY (`istorija_id`) REFERENCES `originator` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nastavni_materijal`
--

LOCK TABLES `nastavni_materijal` WRITE;
/*!40000 ALTER TABLE `nastavni_materijal` DISABLE KEYS */;
INSERT INTO `nastavni_materijal` VALUES (1,'2020-10-10 00:00:00','2020-12-12 00:00:00','2015-10-10','Prvi materijal',1,1,1,NULL),(2,'2018-12-10 00:00:00','2019-09-09 00:00:00','2010-05-05','Drugi',2,2,2,NULL);
/*!40000 ALTER TABLE `nastavni_materijal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nastavni_materijal_autori`
--

DROP TABLE IF EXISTS `nastavni_materijal_autori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nastavni_materijal_autori` (
  `nastavni_materijal_id` bigint NOT NULL,
  `autori_id` bigint NOT NULL,
  PRIMARY KEY (`nastavni_materijal_id`,`autori_id`),
  KEY `FKoorhoo7k0jfergjc3axb3l8jn` (`autori_id`),
  CONSTRAINT `FK3vr6kj0wl5i35x3ryo4ode1b` FOREIGN KEY (`nastavni_materijal_id`) REFERENCES `nastavni_materijal` (`id`),
  CONSTRAINT `FKoorhoo7k0jfergjc3axb3l8jn` FOREIGN KEY (`autori_id`) REFERENCES `autor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nastavni_materijal_autori`
--

LOCK TABLES `nastavni_materijal_autori` WRITE;
/*!40000 ALTER TABLE `nastavni_materijal_autori` DISABLE KEYS */;
INSERT INTO `nastavni_materijal_autori` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `nastavni_materijal_autori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nastavni_materijal_fajlovi`
--

DROP TABLE IF EXISTS `nastavni_materijal_fajlovi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nastavni_materijal_fajlovi` (
  `nastavni_materijal_id` bigint NOT NULL,
  `fajlovi_id` bigint NOT NULL,
  PRIMARY KEY (`nastavni_materijal_id`,`fajlovi_id`),
  KEY `FKgvgcapvtabg321mvpen9h7kce` (`fajlovi_id`),
  CONSTRAINT `FK6e3nlr6u6ii9ah7v752a8kfi7` FOREIGN KEY (`nastavni_materijal_id`) REFERENCES `nastavni_materijal` (`id`),
  CONSTRAINT `FKgvgcapvtabg321mvpen9h7kce` FOREIGN KEY (`fajlovi_id`) REFERENCES `fajl` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nastavni_materijal_fajlovi`
--

LOCK TABLES `nastavni_materijal_fajlovi` WRITE;
/*!40000 ALTER TABLE `nastavni_materijal_fajlovi` DISABLE KEYS */;
INSERT INTO `nastavni_materijal_fajlovi` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `nastavni_materijal_fajlovi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nastavnik`
--

DROP TABLE IF EXISTS `nastavnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nastavnik` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `biografija` longtext,
  `ime` longtext,
  `jmbg` longtext,
  `fakultet_id` bigint DEFAULT NULL,
  `registrovani_korisnik_id` bigint DEFAULT NULL,
  `studijski_program_id` bigint DEFAULT NULL,
  `univerzitet_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1mc9n5u8xb65qqa89dxoym998` (`fakultet_id`),
  KEY `FK7e87rubfxrrlfcup81jcu5109` (`registrovani_korisnik_id`),
  KEY `FKngyj0c536ty0oa3qlmfc7pcei` (`studijski_program_id`),
  KEY `FK24bsarms946taf03w15dfe8ne` (`univerzitet_id`),
  CONSTRAINT `FK1mc9n5u8xb65qqa89dxoym998` FOREIGN KEY (`fakultet_id`) REFERENCES `fakultet` (`id`),
  CONSTRAINT `FK24bsarms946taf03w15dfe8ne` FOREIGN KEY (`univerzitet_id`) REFERENCES `univerzitet` (`id`),
  CONSTRAINT `FK7e87rubfxrrlfcup81jcu5109` FOREIGN KEY (`registrovani_korisnik_id`) REFERENCES `registrovani_korisnik` (`id`),
  CONSTRAINT `FKngyj0c536ty0oa3qlmfc7pcei` FOREIGN KEY (`studijski_program_id`) REFERENCES `studijski_program` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2057486666 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nastavnik`
--

LOCK TABLES `nastavnik` WRITE;
/*!40000 ALTER TABLE `nastavnik` DISABLE KEYS */;
INSERT INTO `nastavnik` VALUES (1,'tri fakulteta zavrsio','Milan','232424',1,5,1,1),(2,'dva fakulteta','Dragisa','22222',2,4,2,2);
/*!40000 ALTER TABLE `nastavnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nastavnik_na_realizaciji`
--

DROP TABLE IF EXISTS `nastavnik_na_realizaciji`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nastavnik_na_realizaciji` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `broj_casova` int NOT NULL,
  `nastavnik_id` bigint DEFAULT NULL,
  `tip_nastave_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKnevmktxs4agqf99pqqqlaeuhg` (`nastavnik_id`),
  KEY `FKo7x2h84alaxk70bvj5s7to3iu` (`tip_nastave_id`),
  CONSTRAINT `FKnevmktxs4agqf99pqqqlaeuhg` FOREIGN KEY (`nastavnik_id`) REFERENCES `nastavnik` (`id`),
  CONSTRAINT `FKo7x2h84alaxk70bvj5s7to3iu` FOREIGN KEY (`tip_nastave_id`) REFERENCES `tip_nastave` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nastavnik_na_realizaciji`
--

LOCK TABLES `nastavnik_na_realizaciji` WRITE;
/*!40000 ALTER TABLE `nastavnik_na_realizaciji` DISABLE KEYS */;
INSERT INTO `nastavnik_na_realizaciji` VALUES (1,15,1,1),(2,20,2,2);
/*!40000 ALTER TABLE `nastavnik_na_realizaciji` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `naucna_oblast`
--

DROP TABLE IF EXISTS `naucna_oblast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `naucna_oblast` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `naucna_oblast`
--

LOCK TABLES `naucna_oblast` WRITE;
/*!40000 ALTER TABLE `naucna_oblast` DISABLE KEYS */;
INSERT INTO `naucna_oblast` VALUES (1,'Geo'),(2,'Neo');
/*!40000 ALTER TABLE `naucna_oblast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obrazovni_cilj`
--

DROP TABLE IF EXISTS `obrazovni_cilj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `obrazovni_cilj` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `opis` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obrazovni_cilj`
--

LOCK TABLES `obrazovni_cilj` WRITE;
/*!40000 ALTER TABLE `obrazovni_cilj` DISABLE KEYS */;
INSERT INTO `obrazovni_cilj` VALUES (1,'Doktor'),(2,'Naucnik');
/*!40000 ALTER TABLE `obrazovni_cilj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `originator`
--

DROP TABLE IF EXISTS `originator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `originator` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `originator`
--

LOCK TABLES `originator` WRITE;
/*!40000 ALTER TABLE `originator` DISABLE KEYS */;
INSERT INTO `originator` VALUES (1),(2);
/*!40000 ALTER TABLE `originator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  `permission_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (1,'ROLE_NASTAVNIK',NULL),(2,'ROLE_STUDENT',NULL);
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pohadjanje_predmeta`
--

DROP TABLE IF EXISTS `pohadjanje_predmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pohadjanje_predmeta` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `konacna_ocena` int NOT NULL,
  `realizacija_predmeta_id` bigint DEFAULT NULL,
  `student_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKskigfrejfgiob4xr8ai7w65xu` (`realizacija_predmeta_id`),
  KEY `FK1p0y5mpl8odif2pkofb88rogl` (`student_id`),
  CONSTRAINT `FK1p0y5mpl8odif2pkofb88rogl` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`),
  CONSTRAINT `FKskigfrejfgiob4xr8ai7w65xu` FOREIGN KEY (`realizacija_predmeta_id`) REFERENCES `realizacija_predmeta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pohadjanje_predmeta`
--

LOCK TABLES `pohadjanje_predmeta` WRITE;
/*!40000 ALTER TABLE `pohadjanje_predmeta` DISABLE KEYS */;
INSERT INTO `pohadjanje_predmeta` VALUES (1,9,1,1),(2,10,2,2);
/*!40000 ALTER TABLE `pohadjanje_predmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polaganje`
--

DROP TABLE IF EXISTS `polaganje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `polaganje` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bodovi` int NOT NULL,
  `napomena` longtext,
  `instrument_evaluacije_id` bigint NOT NULL,
  `student_na_godini_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKaj6rmv7yki1q3yjh809am23ha` (`instrument_evaluacije_id`),
  KEY `FKk3is5klk83tliwxm5yy2yvqgg` (`student_na_godini_id`),
  CONSTRAINT `FKaj6rmv7yki1q3yjh809am23ha` FOREIGN KEY (`instrument_evaluacije_id`) REFERENCES `instrument_evaluacije` (`id`),
  CONSTRAINT `FKk3is5klk83tliwxm5yy2yvqgg` FOREIGN KEY (`student_na_godini_id`) REFERENCES `student_na_godini` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polaganje`
--

LOCK TABLES `polaganje` WRITE;
/*!40000 ALTER TABLE `polaganje` DISABLE KEYS */;
INSERT INTO `polaganje` VALUES (1,60,'Odlican',1,1),(2,60,'Super',2,2);
/*!40000 ALTER TABLE `polaganje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predmet`
--

DROP TABLE IF EXISTS `predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `predmet` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `broj_predavanja` int NOT NULL,
  `broj_semestara` int NOT NULL,
  `broj_vezbi` int NOT NULL,
  `drugi_oblici_nastave` int NOT NULL,
  `espb` int NOT NULL,
  `istrazivacki_rad` int NOT NULL,
  `naziv` longtext,
  `obavezan` bit(1) NOT NULL,
  `ostali_casovi` int NOT NULL,
  `godina_studija_id` bigint NOT NULL,
  `predmet_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK37thl4qshgxy3wornrfek2ji3` (`godina_studija_id`),
  KEY `FKf8ilmgm8rphh00c7iuasu0ehp` (`predmet_id`),
  CONSTRAINT `FK37thl4qshgxy3wornrfek2ji3` FOREIGN KEY (`godina_studija_id`) REFERENCES `godina_studija` (`id`),
  CONSTRAINT `FKf8ilmgm8rphh00c7iuasu0ehp` FOREIGN KEY (`predmet_id`) REFERENCES `predmet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predmet`
--

LOCK TABLES `predmet` WRITE;
/*!40000 ALTER TABLE `predmet` DISABLE KEYS */;
INSERT INTO `predmet` VALUES (1,12,1,12,0,8,0,'BP',_binary '',1,1,1),(2,12,1,12,0,8,0,'MR',_binary '',1,1,2);
/*!40000 ALTER TABLE `predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realizacija_predmeta`
--

DROP TABLE IF EXISTS `realizacija_predmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `realizacija_predmeta` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nastavnik_na_realizaciji_id` bigint DEFAULT NULL,
  `predmet_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKswpedesc75wxrgn9jms3sxqov` (`nastavnik_na_realizaciji_id`),
  KEY `FKrw6tx0pbaevbvs89psuqfqijx` (`predmet_id`),
  CONSTRAINT `FKrw6tx0pbaevbvs89psuqfqijx` FOREIGN KEY (`predmet_id`) REFERENCES `predmet` (`id`),
  CONSTRAINT `FKswpedesc75wxrgn9jms3sxqov` FOREIGN KEY (`nastavnik_na_realizaciji_id`) REFERENCES `nastavnik_na_realizaciji` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realizacija_predmeta`
--

LOCK TABLES `realizacija_predmeta` WRITE;
/*!40000 ALTER TABLE `realizacija_predmeta` DISABLE KEYS */;
INSERT INTO `realizacija_predmeta` VALUES (1,1,1),(2,2,2);
/*!40000 ALTER TABLE `realizacija_predmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registrovani_korisnik`
--

DROP TABLE IF EXISTS `registrovani_korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registrovani_korisnik` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` longtext,
  `korisnicko_ime` longtext,
  `lozinka` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1442794939 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registrovani_korisnik`
--

LOCK TABLES `registrovani_korisnik` WRITE;
/*!40000 ALTER TABLE `registrovani_korisnik` DISABLE KEYS */;
INSERT INTO `registrovani_korisnik` VALUES (1,'pavle@gmail.com','Pavle','123'),(2,'milan@gmail.com','Milan','321'),(3,'ana@gmail.com','Ana','222'),(4,'ivan@gmail.com','Ivan','333'),(5,'djordje@gmail.com','Djordje','444');
/*!40000 ALTER TABLE `registrovani_korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stanje`
--

DROP TABLE IF EXISTS `stanje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stanje` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stanje`
--

LOCK TABLES `stanje` WRITE;
/*!40000 ALTER TABLE `stanje` DISABLE KEYS */;
INSERT INTO `stanje` VALUES (1,'Super'),(2,'Dobro');
/*!40000 ALTER TABLE `stanje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `ime` longtext,
  `jmbg` longtext,
  `adresa_id` bigint NOT NULL,
  `registrovani_korisnik_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKm6xun08ncl8vw4kosdqsbj4im` (`adresa_id`),
  KEY `FKdweiwc9c75nnr7jdcvsr2avu` (`registrovani_korisnik_id`),
  CONSTRAINT `FKdweiwc9c75nnr7jdcvsr2avu` FOREIGN KEY (`registrovani_korisnik_id`) REFERENCES `registrovani_korisnik` (`id`),
  CONSTRAINT `FKm6xun08ncl8vw4kosdqsbj4im` FOREIGN KEY (`adresa_id`) REFERENCES `adresa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1821173913 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'Pavle','1111',1,1),(2,'Milan','2222',2,2);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_na_godini`
--

DROP TABLE IF EXISTS `student_na_godini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_na_godini` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `broj_indeksa` longtext,
  `datum_upisa` datetime DEFAULT NULL,
  `godina_studija_id` bigint NOT NULL,
  `student_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa0qj2cperexvshppcmuh1a1s7` (`godina_studija_id`),
  KEY `FKmu7rrfp6rrd3ds2i229h46d8l` (`student_id`),
  CONSTRAINT `FKa0qj2cperexvshppcmuh1a1s7` FOREIGN KEY (`godina_studija_id`) REFERENCES `godina_studija` (`id`),
  CONSTRAINT `FKmu7rrfp6rrd3ds2i229h46d8l` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_na_godini`
--

LOCK TABLES `student_na_godini` WRITE;
/*!40000 ALTER TABLE `student_na_godini` DISABLE KEYS */;
INSERT INTO `student_na_godini` VALUES (1,'20010','2018-03-03 00:00:00',1,1),(2,'21234','2018-06-06 00:00:00',2,2);
/*!40000 ALTER TABLE `student_na_godini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studijski_program`
--

DROP TABLE IF EXISTS `studijski_program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `studijski_program` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` longtext,
  `fakultet_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK303wuewako66hti92rcv4a9mh` (`fakultet_id`),
  CONSTRAINT `FK303wuewako66hti92rcv4a9mh` FOREIGN KEY (`fakultet_id`) REFERENCES `fakultet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studijski_program`
--

LOCK TABLES `studijski_program` WRITE;
/*!40000 ALTER TABLE `studijski_program` DISABLE KEYS */;
INSERT INTO `studijski_program` VALUES (1,'SII',1),(2,'IT',2);
/*!40000 ALTER TABLE `studijski_program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `termin_nastave`
--

DROP TABLE IF EXISTS `termin_nastave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `termin_nastave` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `vreme_kraja` datetime DEFAULT NULL,
  `vreme_pocetka` datetime DEFAULT NULL,
  `ishod_id` bigint DEFAULT NULL,
  `realizacija_predmeta_id` bigint DEFAULT NULL,
  `tip_nastave_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKou85kss19sqvi8qoxgeby8i18` (`ishod_id`),
  KEY `FKasrq9uj8dmfa2dtyycxclwwwu` (`realizacija_predmeta_id`),
  KEY `FKbpd7rs6wsdq5aoxutgqhqm93f` (`tip_nastave_id`),
  CONSTRAINT `FKasrq9uj8dmfa2dtyycxclwwwu` FOREIGN KEY (`realizacija_predmeta_id`) REFERENCES `realizacija_predmeta` (`id`),
  CONSTRAINT `FKbpd7rs6wsdq5aoxutgqhqm93f` FOREIGN KEY (`tip_nastave_id`) REFERENCES `tip_nastave` (`id`),
  CONSTRAINT `FKou85kss19sqvi8qoxgeby8i18` FOREIGN KEY (`ishod_id`) REFERENCES `ishod` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `termin_nastave`
--

LOCK TABLES `termin_nastave` WRITE;
/*!40000 ALTER TABLE `termin_nastave` DISABLE KEYS */;
INSERT INTO `termin_nastave` VALUES (1,'2020-11-11 00:00:00','2020-12-12 00:00:00',1,1,1),(2,'2021-01-01 00:00:00','2021-02-02 00:00:00',2,2,2);
/*!40000 ALTER TABLE `termin_nastave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_evaluacije`
--

DROP TABLE IF EXISTS `tip_evaluacije`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tip_evaluacije` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_evaluacije`
--

LOCK TABLES `tip_evaluacije` WRITE;
/*!40000 ALTER TABLE `tip_evaluacije` DISABLE KEYS */;
INSERT INTO `tip_evaluacije` VALUES (1,'Uspesan'),(2,'Neuspesan');
/*!40000 ALTER TABLE `tip_evaluacije` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_nastave`
--

DROP TABLE IF EXISTS `tip_nastave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tip_nastave` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_nastave`
--

LOCK TABLES `tip_nastave` WRITE;
/*!40000 ALTER TABLE `tip_nastave` DISABLE KEYS */;
INSERT INTO `tip_nastave` VALUES (1,'Dobar'),(2,'Los');
/*!40000 ALTER TABLE `tip_nastave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_zvanja`
--

DROP TABLE IF EXISTS `tip_zvanja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tip_zvanja` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_zvanja`
--

LOCK TABLES `tip_zvanja` WRITE;
/*!40000 ALTER TABLE `tip_zvanja` DISABLE KEYS */;
INSERT INTO `tip_zvanja` VALUES (1,'Profesor'),(2,'Asistent'),(3,'Direktor');
/*!40000 ALTER TABLE `tip_zvanja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `univerzitet`
--

DROP TABLE IF EXISTS `univerzitet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `univerzitet` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `broj` longtext,
  `ulica` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `univerzitet`
--

LOCK TABLES `univerzitet` WRITE;
/*!40000 ALTER TABLE `univerzitet` DISABLE KEYS */;
INSERT INTO `univerzitet` VALUES (1,'10','Novosadska'),(2,'22','Hadzi Ruvimova');
/*!40000 ALTER TABLE `univerzitet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_permission`
--

DROP TABLE IF EXISTS `user_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_permission` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `permission_id` bigint DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbklmo9kchans5u3e4va0ouo1s` (`permission_id`),
  KEY `FK4qwk1v8fcv89bsscppb8ndsk3` (`user_id`),
  CONSTRAINT `FK4qwk1v8fcv89bsscppb8ndsk3` FOREIGN KEY (`user_id`) REFERENCES `registrovani_korisnik` (`id`),
  CONSTRAINT `FKbklmo9kchans5u3e4va0ouo1s` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_permission`
--

LOCK TABLES `user_permission` WRITE;
/*!40000 ALTER TABLE `user_permission` DISABLE KEYS */;
INSERT INTO `user_permission` VALUES (1,1,5),(2,2,2);
/*!40000 ALTER TABLE `user_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zvanje`
--

DROP TABLE IF EXISTS `zvanje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zvanje` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `datum_izbora` datetime DEFAULT NULL,
  `datum_prestanka` datetime DEFAULT NULL,
  `nastavnik_id` bigint DEFAULT NULL,
  `naucna_oblast_id` bigint NOT NULL,
  `tip_zvanja_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3p9bn3s3p2mhemnx59sfvxwc3` (`nastavnik_id`),
  KEY `FKl5hpqogqfdtev1q9rric2ifok` (`naucna_oblast_id`),
  KEY `FKkpu227epeh3exwr26w5ryql3m` (`tip_zvanja_id`),
  CONSTRAINT `FK3p9bn3s3p2mhemnx59sfvxwc3` FOREIGN KEY (`nastavnik_id`) REFERENCES `nastavnik` (`id`),
  CONSTRAINT `FKkpu227epeh3exwr26w5ryql3m` FOREIGN KEY (`tip_zvanja_id`) REFERENCES `tip_zvanja` (`id`),
  CONSTRAINT `FKl5hpqogqfdtev1q9rric2ifok` FOREIGN KEY (`naucna_oblast_id`) REFERENCES `naucna_oblast` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zvanje`
--

LOCK TABLES `zvanje` WRITE;
/*!40000 ALTER TABLE `zvanje` DISABLE KEYS */;
INSERT INTO `zvanje` VALUES (1,'2019-12-12 00:00:00','2021-10-10 00:00:00',1,1,1),(2,'2018-10-10 00:00:00','2021-09-09 00:00:00',2,2,2);
/*!40000 ALTER TABLE `zvanje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'lms'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-07 15:27:13
