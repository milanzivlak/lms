package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.StudentNaGodiniDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.model.GodinaStudija;
import rs.ac.singidunum.isa.app.model.Predmet;
import rs.ac.singidunum.isa.app.model.StudentNaGodini;
import rs.ac.singidunum.isa.app.service.GodinaStudijaService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/api/godinastudija")
@CrossOrigin
public class GodinaStudijaController {
	
	@Autowired
	private GodinaStudijaService godinaStudijaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<GodinaStudijaDTO>> getAllGodineStudija() {
		
		
		ArrayList<GodinaStudijaDTO> godinaStudija = new ArrayList<GodinaStudijaDTO>();
		
		for(GodinaStudija godina : godinaStudijaService.findAll()) {
			ArrayList<StudentNaGodiniDTO> studentNaGodini = new ArrayList<StudentNaGodiniDTO>();
			ArrayList<PredmetDTO> predmeti = new ArrayList<PredmetDTO>();
			
			for(StudentNaGodini sng : godina.getStudentiNaGodini()) {
				studentNaGodini.add(new StudentNaGodiniDTO(sng.getId(), sng.getDatumUpisa(), sng.getBrojIndeksa()));
			}
			
			for(Predmet predmet : godina.getPredmeti()) {
				predmeti.add(new PredmetDTO(predmet.getId(), predmet.getNaziv(), predmet.getEspb(), predmet.isObavezan(), predmet.getBrojPredavanja(), predmet.getBrojVezbi(), predmet.getDrugiObliciNastave(), predmet.getIstrazivackiRad(), predmet.getOstaliCasovi(), predmet.getBrojSemestara()));
			}
			
			StudijskiProgramDTO spd = new StudijskiProgramDTO(godina.getStudijskiProgram().getId(), godina.getStudijskiProgram().getNaziv());
			
			godinaStudija.add(new GodinaStudijaDTO(godina.getId(), godina.getGodina(), studentNaGodini, predmeti, spd));
		}
		
		return new ResponseEntity<Iterable<GodinaStudijaDTO>>(godinaStudija, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{godinaStudijaId}", method = RequestMethod.GET)
	public ResponseEntity<GodinaStudijaDTO> getGodinaStudija(@PathVariable("godinaStudijaId") Long godinaStudijaId) {
		
		Optional<GodinaStudija> godinaStudija = godinaStudijaService.findOne(godinaStudijaId);
		
		if(godinaStudija.isPresent()) {
			GodinaStudijaDTO godinaStudijaDTO = new GodinaStudijaDTO(godinaStudija.get().getId(), godinaStudija.get().getGodina());
			return new ResponseEntity<GodinaStudijaDTO>(godinaStudijaDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<GodinaStudijaDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<GodinaStudija> createGodinaStudija(@RequestBody GodinaStudija godinaStudija){
		
		try {
			
			godinaStudijaService.save(godinaStudija);
			
			return new ResponseEntity<GodinaStudija>(godinaStudija, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<GodinaStudija>(HttpStatus.BAD_REQUEST);

		
	}
	
	@RequestMapping(path = "/{godinaStudijaId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<GodinaStudija> updateGodinaStudija(@PathVariable("godinaStudijaId") Long godinaStudijaId, @RequestBody GodinaStudija izmenjenaGodinaStudija) {
		
		GodinaStudija godinaStudija = godinaStudijaService.findOne(godinaStudijaId).orElse(null);
		
		if( godinaStudija != null) {
			izmenjenaGodinaStudija.setId(godinaStudijaId);
			
			godinaStudijaService.save(izmenjenaGodinaStudija);
			return new ResponseEntity<GodinaStudija>(izmenjenaGodinaStudija, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<GodinaStudija>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{godinaStudijaId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<GodinaStudija> deleteGodinaStudija(@PathVariable("godinaStudijaId") Long godinaStudijaId) {
	
		if(godinaStudijaService.findOne(godinaStudijaId).isPresent()) {
			godinaStudijaService.delete(godinaStudijaId);
			return new ResponseEntity<GodinaStudija>(HttpStatus.OK);
		}
		
		return new ResponseEntity<GodinaStudija>(HttpStatus.NOT_FOUND);
	}
	
}
