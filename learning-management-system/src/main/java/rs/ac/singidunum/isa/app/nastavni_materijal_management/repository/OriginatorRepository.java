package rs.ac.singidunum.isa.app.nastavni_materijal_management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Originator;

@Repository
public interface OriginatorRepository extends PagingAndSortingRepository<Originator, Long>{

}
