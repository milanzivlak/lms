package rs.ac.singidunum.isa.app.nastavni_materijal_management.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import rs.ac.singidunum.isa.app.model.StudentNaGodini;

@Entity
public class Polaganje {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private int bodovi;
	
	@Lob
	private String napomena;

	@ManyToOne(optional = false)
	private InstrumentEvaluacije instrumentEvaluacije;
	
	@ManyToOne
	private StudentNaGodini studentNaGodini;
	
	@OneToOne(mappedBy = "polaganje")
	private EvaluacijaZnanja evaluacijaZnanja;
	
	public InstrumentEvaluacije getInstrumentEvaluacije() {
		return instrumentEvaluacije;
	}

	public void setInstrumentEvaluacije(InstrumentEvaluacije instrumentEvaluacije) {
		this.instrumentEvaluacije = instrumentEvaluacije;
	}

	public Polaganje() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Polaganje(Long id, int bodovi, String napomena) {
		super();
		this.id = id;
		this.bodovi = bodovi;
		this.napomena = napomena;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBodovi() {
		return bodovi;
	}

	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}

	public String getNapomena() {
		return napomena;
	}

	public void setNapomena(String napomena) {
		this.napomena = napomena;
	}

	public StudentNaGodini getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(StudentNaGodini studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}

	public EvaluacijaZnanja getEvaluacijaZnanja() {
		return evaluacijaZnanja;
	}

	public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
		this.evaluacijaZnanja = evaluacijaZnanja;
	}
	
	
	
	
	
}
