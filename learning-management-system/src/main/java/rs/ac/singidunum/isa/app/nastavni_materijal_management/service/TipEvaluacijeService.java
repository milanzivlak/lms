package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TipEvaluacije;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.TipEvaluacijeRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class TipEvaluacijeService extends GenerateService<TipEvaluacijeRepository, TipEvaluacije, Long>{

	public Page<TipEvaluacije> findAll(Pageable pageable) {
		return ((TipEvaluacijeRepository) this.getRepository()).findAll(pageable);
	}

}
