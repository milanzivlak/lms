package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;

@Entity
public class Nastavnik {
	
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String ime;
	
	@Lob
	private String biografija;
	@Lob
	private String jmbg;
	
	@OneToMany(mappedBy = "nastavnik")
	private Set<Zvanje> zvanja = new HashSet<Zvanje>();
	
	@OneToOne()
	private Fakultet fakultet;
	
	@OneToOne()
	private Univerzitet univerzitet;
	
	// Da li nastavnik moze da bude rukovodilac samo jednog studijskog programa ili moze na vise njih?
	@OneToOne()
	private StudijskiProgram studijskiProgram;
	
	@OneToOne(mappedBy = "nastavnik", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private NastavnikNaRealizaciji realizacija;
	
	@OneToOne()
	private RegistrovaniKorisnik registrovaniKorisnik;
	
	
	
	
	public RegistrovaniKorisnik getRegistrovaniKorisnik() {
		return registrovaniKorisnik;
	}

	public void setRegistrovaniKorisnik(RegistrovaniKorisnik registrovaniKorisnik) {
		this.registrovaniKorisnik = registrovaniKorisnik;
	}

	public Nastavnik() {
		super();
	}
	
	public Nastavnik(Long id, String ime, String biografija, String jmbg) {
		super();
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getBiografija() {
		return biografija;
	}
	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}
	public String getJmbg() {
		return jmbg;
	}
	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public Set<Zvanje> getZvanja() {
		return zvanja;
	}

	public void setZvanja(Set<Zvanje> zvanja) {
		this.zvanja = zvanja;
	}

	public Fakultet getFakultet() {
		return fakultet;
	}

	public void setFakultet(Fakultet fakultet) {
		this.fakultet = fakultet;
	}

	public Univerzitet getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(Univerzitet univerzitet) {
		this.univerzitet = univerzitet;
	}

	public StudijskiProgram getStudijskiProgram() {
		return studijskiProgram;
	}

	public void setStudijskiProgram(StudijskiProgram studijskiProgram) {
		this.studijskiProgram = studijskiProgram;
	}

	public NastavnikNaRealizaciji getRealizacija() {
		return realizacija;
	}

	public void setRealizacija(NastavnikNaRealizaciji realizacija) {
		this.realizacija = realizacija;
	}
	
}
