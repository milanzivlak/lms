package rs.ac.singidunum.isa.app.parsers;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.TipZvanjaDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.dto.ZvanjeDTO;

public class UniverzitetParser {
	
	private static final String SCHEMANAME = "univerzitet.xsd";
	public ArrayList<UniverzitetDTO> spisakUniverziteta;
	public ArrayList<UniverzitetDTO> importovaniUniverziteti;
	
	public UniverzitetParser(ArrayList<UniverzitetDTO> spisakUniverziteta) {
		this.spisakUniverziteta = spisakUniverziteta;
	}
	
	public UniverzitetParser() {
		
	}
	
	public void export() throws IOException, ParserConfigurationException, TransformerException {
		//root
		
		DocumentBuilderFactory dbFactory =
		         DocumentBuilderFactory.newInstance();
		         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		         Document doc = (Document) dBuilder.newDocument();
		
         Element univerziteti = doc.createElement("univerziteti");
        doc.appendChild(univerziteti);
        for(UniverzitetDTO d : this.spisakUniverziteta) {
        	Element univerzitet = doc.createElement("univerzitet");
        	
        	Attr attribute = doc.createAttribute("id");
        	attribute.setValue(d.getId().toString());
        	univerzitet.setAttributeNode(attribute);
        	
        	Element ulica = doc.createElement("ulica");
        	ulica.appendChild(doc.createTextNode(d.getUlica()));
        	univerzitet.appendChild(ulica);
        	
        	Element broj = doc.createElement("broj");
        	broj.appendChild(doc.createTextNode(d.getBroj()));
        	univerzitet.appendChild(broj);
        	
        	// ADRESA ////////////////
        	
        	Element adresa = doc.createElement("adresa");
        	AdresaDTO adresaDTO = d.getAdresa();
        	
        	Attr adresaAttr = doc.createAttribute("id");
        	adresaAttr.setValue(adresaDTO.getId().toString());
        	adresa.setAttributeNode(adresaAttr);
        	
        	Element adresa_ulica = doc.createElement("ulica");
        	adresa_ulica.appendChild(doc.createTextNode(adresaDTO.getUlica()));
        	adresa.appendChild(adresa_ulica);
        	
        	Element adresa_broj = doc.createElement("broj");
        	adresa_broj.appendChild(doc.createTextNode(adresaDTO.getBroj()));
        	adresa.appendChild(adresa_broj);
        	
        	Element adresa_mesto = doc.createElement("mesto");
        	MestoDTO mestoDTO = d.getAdresa().getMesto();
        	
        	Attr mestoAttr = doc.createAttribute("id");
        	mestoAttr.setValue(mestoDTO.getId().toString());
        	adresa_mesto.setAttributeNode(mestoAttr);
        	
        	Element mesto_naziv = doc.createElement("naziv");
        	mesto_naziv.appendChild(doc.createTextNode(mestoDTO.getNaziv()));
        	adresa_mesto.appendChild(mesto_naziv);
        	
        	Element mesto_drzava = doc.createElement("drzava");
        	DrzavaDTO drzavaDTO = d.getAdresa().getMesto().getDrzava();
        	
        	Attr drzavaAttr = doc.createAttribute("id");
        	drzavaAttr.setValue(drzavaDTO.getId().toString());
        	mesto_drzava.setAttributeNode(drzavaAttr);
        	
        	Element drzava_naziv = doc.createElement("naziv");
        	drzava_naziv.appendChild(doc.createTextNode(drzavaDTO.getNaziv()));
        	mesto_drzava.appendChild(drzava_naziv);
        	
        	adresa_mesto.appendChild(mesto_drzava);
        	adresa.appendChild(adresa_mesto);
        	
        	univerzitet.appendChild(adresa);
        	
        	// REKTOR ////////////////
        	
        	Element rektor = doc.createElement("rektor");
        	NastavnikDTO nastavnikDTO = d.getRektor();
        	
        	Attr rektorAttr = doc.createAttribute("id");
        	rektorAttr.setValue(nastavnikDTO.getId().toString());
        	rektor.setAttributeNode(rektorAttr);
        	
        	Element rektor_ime = doc.createElement("ime");
        	rektor_ime.appendChild(doc.createTextNode(nastavnikDTO.getIme()));
        	rektor.appendChild(rektor_ime);
        	
        	Element rektor_bio = doc.createElement("biografija");
        	rektor_bio.appendChild(doc.createTextNode(nastavnikDTO.getBiografija()));
        	rektor.appendChild(rektor_bio);
        	
        	Element rektor_jmbg = doc.createElement("jmbg");
        	rektor_jmbg.appendChild(doc.createTextNode(nastavnikDTO.getJmbg()));
        	rektor.appendChild(rektor_jmbg);
        	
        	
        	univerzitet.appendChild(rektor);
        	// ZVANJA ////////////////
        	
//        	Element zvanja = doc.createElement("zvanja");
//        	for(ZvanjeDTO m : d.getRektor().getZvanja()) {
//        		
//        		Element zvanje = doc.createElement("zvanje");
//        		
//        		Attr zvanjeAttr = doc.createAttribute("id");
//        		zvanjeAttr.setValue(m.getId().toString());
//        		zvanje.setAttributeNode(zvanjeAttr);
//        		
//        		Element datum_izbora = doc.createElement("datumIzbora");
//        		datum_izbora.appendChild(doc.createTextNode(m.getDatumIzbora().toString()));
//        		zvanje.appendChild(datum_izbora);
//        		
//        		Element datum_prestanka = doc.createElement("datumPrestanka");
//        		datum_prestanka.appendChild(doc.createTextNode(m.getDatumPrestanka().toString()));
//        		zvanje.appendChild(datum_prestanka);
//        		
//        		Element tipZvanja = doc.createElement("tipZvanja");
//        		
//        		//TipZvanjaDTO tz = new TipZvanjaDTO();
//        		
//        		Attr tipZvanjaAttr = doc.createAttribute("id");
//        		tipZvanjaAttr.setValue(m.getTipZvanja().getId().toString());
//        		tipZvanja.setAttributeNode(tipZvanjaAttr);
//        		
//        		Element tipZvanjaNaziv = doc.createElement("naziv");
//        		tipZvanjaNaziv.appendChild(doc.createTextNode(m.getTipZvanja().getNaziv()));
//        		tipZvanja.appendChild(tipZvanjaNaziv);
//        		
//        		zvanje.appendChild(tipZvanja);
//        		zvanja.appendChild(zvanje);
//        	}
//        	univerzitet.appendChild(zvanja);
        	
        	// FAKULTETI //////////////////
        	
        	Element fakulteti = doc.createElement("fakulteti");
        	for(FakultetDTO f : d.getFakulteti()) {
        		Element fakultet = doc.createElement("fakultet");
        		
        		Attr faksAttr = doc.createAttribute("id");
        		faksAttr.setValue(f.getId().toString());
        		fakultet.setAttributeNode(faksAttr);
        		
        		Element naziv = doc.createElement("naziv");
    			naziv.appendChild(doc.createTextNode(f.getNaziv()));
    			fakultet.appendChild(naziv);
    			
    			Element dekan = doc.createElement("dekan");
    			
    			Attr dekanAttr = doc.createAttribute("id");
    			dekanAttr.setValue(nastavnikDTO.getId().toString());
    			dekan.setAttributeNode(dekanAttr);
    			
    			Element dekan_ime = doc.createElement("ime");
    			dekan_ime.appendChild(doc.createTextNode(nastavnikDTO.getIme()));
            	dekan.appendChild(dekan_ime);
            	
            	Element dekan_bio = doc.createElement("biografija");
            	dekan_bio.appendChild(doc.createTextNode(nastavnikDTO.getBiografija()));
            	dekan.appendChild(dekan_bio);
            	
            	Element dekan_jmbg = doc.createElement("jmbg");
            	dekan_jmbg.appendChild(doc.createTextNode(nastavnikDTO.getJmbg()));
            	dekan.appendChild(dekan_jmbg);
    			
//            	Element dekan_zvanja = doc.createElement("zvanja");
//            	for(ZvanjeDTO zd : f.getDekan().getZvanja()) {
//            		
//            		Element dekan_zvanje = doc.createElement("zvanje");
//            		
//            		Attr dekan_zvanjeAttr = doc.createAttribute("id");
//            		dekan_zvanjeAttr.setValue(zd.getId().toString());
//            		dekan_zvanje.setAttributeNode(dekan_zvanjeAttr);
//            		
//            		Element dekan_datum_izbora = doc.createElement("datumIzbora");
//            		dekan_datum_izbora.appendChild(doc.createTextNode(zd.getDatumIzbora().toString()));
//            		dekan_zvanje.appendChild(dekan_datum_izbora);
//            		
//            		Element dekan_datum_prestanka = doc.createElement("datumPrestanka");
//            		dekan_datum_prestanka.appendChild(doc.createTextNode(zd.getDatumPrestanka().toString()));
//            		dekan_zvanje.appendChild(dekan_datum_prestanka);
//            		
//            		
//            		Element dekan_tipZvanja = doc.createElement("tipZvanja");
//            		
//            		//TipZvanjaDTO tz = new TipZvanjaDTO();
//            		
//            		Attr dekan_tipZvanjaAttr = doc.createAttribute("id");
//            		dekan_tipZvanjaAttr.setValue(zd.getTipZvanja().getId().toString());
//            		dekan_tipZvanja.setAttributeNode(dekan_tipZvanjaAttr);
//            		
//            		Element dekan_tipZvanjaNaziv = doc.createElement("naziv");
//            		dekan_tipZvanjaNaziv.appendChild(doc.createTextNode(zd.getTipZvanja().getNaziv()));
//            		dekan_tipZvanja.appendChild(dekan_tipZvanjaNaziv);
//            		
//            		
//            		dekan_zvanje.appendChild(dekan_tipZvanja);
//            		
//            		dekan_zvanja.appendChild(dekan_zvanje);
//            		dekan.appendChild(dekan_zvanja);
//            	}
            	
            	
    			
    			fakultet.appendChild(dekan);
    			fakulteti.appendChild(fakultet);
    			univerzitet.appendChild(fakulteti);
        	}
        	
        	
        	
        	
        	
        	
        	univerziteti.appendChild(univerzitet);
        }
        
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Schema schema = loadSchema(SCHEMANAME);
		
		if(validateXml(schema, doc)) {
			DOMSource source = new DOMSource((Node) doc);
			String home = System.getProperty("user.home");
			File file = new File(home+"/Downloads/" + "univerzitet" + ".xml");
			StreamResult result = new StreamResult(file);
			//StreamResult result =  new StreamResult(new File("C:\\Users\\Korisnik\\Desktop\\xml\\drzave.xml"));
			transformer.transform(source, result);
			StreamResult consoleResult = new StreamResult(System.out);
	        transformer.transform(source, consoleResult);
		}
	else {
			System.out.println("Neispravan format za export!");
		}
	}
	
	public ArrayList<UniverzitetDTO> importXml(String s){
		
		this.importovaniUniverziteti = new ArrayList<UniverzitetDTO>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			Document doc = convertStringToXMLDocument(s);
			Schema schema = loadSchema(SCHEMANAME);
			
			//if(validateXml(schema, doc)) {
				doc.getDocumentElement().normalize();
				
				System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
				
				NodeList list = doc.getElementsByTagName("univerzitet");
				
				for(int temp = 0; temp < list.getLength(); temp++) {
					Node node = list.item(temp);
					
					if(node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						
						String univerzitetId = element.getAttribute("id");
						String univerzitetUlica = element.getElementsByTagName("ulica").item(0).getTextContent();
						String univerzitetBroj = element.getElementsByTagName("broj").item(0).getTextContent();
						
						UniverzitetDTO uniDTO = new UniverzitetDTO();
						uniDTO.setId(Long.parseLong(univerzitetId));
						uniDTO.setUlica(univerzitetUlica);
						uniDTO.setBroj(univerzitetBroj);
						
						Element adresa = (Element)element.getElementsByTagName("adresa").item(0);
						
						System.out.println("Element: " + node.getNodeName());
						System.out.println("Univerzitet ID: " + univerzitetId);
						System.out.println("Ulica univerziteta: " + univerzitetUlica);
						
						String adresaId = adresa.getAttribute("id");
						String adresaUlica = adresa.getElementsByTagName("ulica").item(0).getTextContent();
						String adresaBroj = adresa.getElementsByTagName("broj").item(0).getTextContent();

						Element adresa_mesto = (Element)adresa.getElementsByTagName("mesto").item(0);
						
						String mestoId = adresa_mesto.getAttribute("id");
						String mestoNaziv = adresa_mesto.getElementsByTagName("naziv").item(0).getTextContent();
						
						Element mesto_drzava = (Element)adresa_mesto.getElementsByTagName("drzava").item(0);
						
						String drzavaId = mesto_drzava.getAttribute("id");
						String drzavaNaziv = mesto_drzava.getElementsByTagName("naziv").item(0).getTextContent();
						
						DrzavaDTO drzavaDTO = new DrzavaDTO(Long.parseLong(drzavaId), drzavaNaziv);
						
						MestoDTO mestoDTO = new MestoDTO(Long.parseLong(mestoId), mestoNaziv);
						mestoDTO.setDrzava(drzavaDTO);

						
						AdresaDTO adresaDTO = new AdresaDTO(Long.parseLong(adresaId), adresaUlica, adresaBroj);
						adresaDTO.setMesto(mestoDTO);
						uniDTO.setAdresa(adresaDTO);
						
						Element rektor = (Element)element.getElementsByTagName("rektor").item(0);
						
						String rektorID = rektor.getAttribute("id");
						String rektorIme = rektor.getElementsByTagName("ime").item(0).getTextContent();
						String rektorBiografija = rektor.getElementsByTagName("biografija").item(0).getTextContent();
						String rektorJmbg = rektor.getElementsByTagName("jmbg").item(0).getTextContent();
						
						NastavnikDTO rektorDTO = new NastavnikDTO(Long.parseLong(rektorID), rektorIme, rektorBiografija, rektorJmbg);
						uniDTO.setRektor(rektorDTO);
						
						NodeList lista_fakulteta = doc.getElementsByTagName("fakultet");
						ArrayList<FakultetDTO> fakulteti_lista = new ArrayList<FakultetDTO>();
						for(int temp1 = 0; temp1 < lista_fakulteta.getLength(); temp1++) {
							Node node1 = lista_fakulteta.item(temp1);
							
							if(node.getNodeType() == Node.ELEMENT_NODE) {
								Element fakultet = (Element) node1;
								
								String faksId = fakultet.getAttribute("id");
								String faksNaziv = fakultet.getElementsByTagName("naziv").item(0).getTextContent();
								
								FakultetDTO faks = new FakultetDTO(Long.parseLong(faksId), faksNaziv);
								
								fakulteti_lista.add(faks);
						
							}
						}
						uniDTO.setFakulteti(fakulteti_lista);
						this.importovaniUniverziteti.add(uniDTO);
					}
					
				}
		//	}
			return this.importovaniUniverziteti;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static Schema loadSchema(String schemaFileName) {
		Schema schema = null;
		try {
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			
			schema = factory.newSchema(new File(schemaFileName));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return schema;
	}
	
	public static Document parseXmlDom(String xmlName) {
		
		Document document = null;
		try {
		   DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		   DocumentBuilder builder = factory.newDocumentBuilder();
		   document = builder.parse(new File(xmlName));
		} catch (Exception e) {
		   System.out.println(e.toString());
		}
		return document;
	}
	
	public static boolean validateXml(Schema schema, Document document) {
		try {
			Validator validator = schema.newValidator();
			
			validator.validate(new DOMSource(document));
			System.out.println("Validacija uspjesna!");
			return true;
			
		} catch (Exception e) {
			System.out.println("Validacije neuspjesna!");
			return false;
		}
		
	}
	
	public static Document convertStringToXMLDocument(String xmlString) 
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
             
            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return null;
    }

}
