package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TerminNastave;


@Entity
public class TipNastave {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String naziv;
	
	@OneToMany(mappedBy = "tipNastave")
	private Set<NastavnikNaRealizaciji> realizacije = new HashSet<NastavnikNaRealizaciji>();

	@OneToMany(mappedBy = "tipNastave")
	private Set<TerminNastave> terminiNastave = new HashSet<TerminNastave>();
	
	
	public Set<TerminNastave> getTerminiNastave() {
		return terminiNastave;
	}

	public void setTerminiNastave(Set<TerminNastave> terminiNastave) {
		this.terminiNastave = terminiNastave;
	}

	public TipNastave() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipNastave(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<NastavnikNaRealizaciji> getRealizacije() {
		return realizacije;
	}

	public void setRealizacije(Set<NastavnikNaRealizaciji> realizacije) {
		this.realizacije = realizacije;
	}

}
