package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.InstrumentEvaluacije;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.InstrumentEvaluacijeRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service

public class InstrumentEvaluacijeService extends GenerateService<InstrumentEvaluacijeRepository, InstrumentEvaluacije, Long>{

	public Page<InstrumentEvaluacije> findAll(Pageable pageable) {
		return ((InstrumentEvaluacijeRepository) this.getRepository()).findAll(pageable);
	}

}
