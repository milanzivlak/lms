package rs.ac.singidunum.isa.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.dto.TipZvanjaDTO;
import rs.ac.singidunum.isa.app.dto.ZvanjeDTO;
import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Mesto;
import rs.ac.singidunum.isa.app.model.TipZvanja;
import rs.ac.singidunum.isa.app.model.Zvanje;
import rs.ac.singidunum.isa.app.parsers.TipZvanjaParser;
import rs.ac.singidunum.isa.app.service.TipZvanjaService;

@Controller
@RequestMapping(path = "/api/tipoviZvanja")
@CrossOrigin
public class TipZvanjaController {
	
	@Autowired
	private TipZvanjaService tipZvanjaService;
	
	TipZvanjaParser parser = new TipZvanjaParser();
	
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<TipZvanjaDTO>> getAllTipoviZvanja() {
		
		
		ArrayList<TipZvanjaDTO> tipoviZvanja = new ArrayList<TipZvanjaDTO>();
		
		for(TipZvanja tipZvanja : tipZvanjaService.findAll()) {
			
			ArrayList<ZvanjeDTO> zvanja = new ArrayList<ZvanjeDTO>();
			
			for(Zvanje zvanje : tipZvanja.getZvanja()) {
				zvanja.add(
						new ZvanjeDTO(zvanje.getId(), zvanje.getDatumIzbora(), zvanje.getDatumPrestanka()));
			}
			
			tipoviZvanja.add(new TipZvanjaDTO(tipZvanja.getId(), tipZvanja.getNaziv(), zvanja));
		}
		
		return new ResponseEntity<Iterable<TipZvanjaDTO>>(tipoviZvanja, HttpStatus.OK);

	}
	
	
	@RequestMapping(path = "/import", method = RequestMethod.GET)
	public ResponseEntity<Iterable<TipZvanjaDTO>> tipZvanjaImport(@Param(value = "url") String url) throws UnsupportedEncodingException {

		
		ArrayList<TipZvanjaDTO> importovane = this.parser.importXml(url);
		
		for(TipZvanjaDTO d : importovane) {
			TipZvanja tipZvanja = new TipZvanja(d.getId(), d.getNaziv());
			tipZvanjaService.save(tipZvanja);
			
		}
		return new ResponseEntity<Iterable<TipZvanjaDTO>>(this.parser.importXml(url), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/export", method = RequestMethod.GET)
	public void tipZvanjaExport() {
		System.out.println("Exporting...");
		ArrayList<TipZvanjaDTO> tipovi = new ArrayList<TipZvanjaDTO>();
		
		for(TipZvanja tipZvanja : tipZvanjaService.findAll()) {
			tipovi.add(TipZvanjaDTO.fromTipZvanja(tipZvanja));
		}
		
		this.parser.spisakTipova = tipovi;
		try {
			this.parser.export();
		} catch (IOException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(path = "/{tipZvanjaId}", method = RequestMethod.GET)
	public ResponseEntity<TipZvanja> getTipZvanja(@PathVariable("tipZvanjaId") Long tipZvanjaId) {
		
		Optional<TipZvanja> tipZvanja = tipZvanjaService.findOne(tipZvanjaId);
		
		if(tipZvanja.isPresent()) {
			return new ResponseEntity<TipZvanja>(tipZvanja.get(), HttpStatus.OK);

		}
		
		return new ResponseEntity<TipZvanja>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<TipZvanja> createTipZvanja(@RequestBody TipZvanja tipZvanja){
		
		try {
			
			tipZvanjaService.save(tipZvanja);
			
			return new ResponseEntity<TipZvanja>(tipZvanja, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<TipZvanja>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{tipZvanjaId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<TipZvanja> updateTipZvanja(@PathVariable("tipZvanjaId") Long tipZvanjaId, @RequestBody TipZvanja izmenjeniTipZvanja) {
		
		TipZvanja tipZvanja = tipZvanjaService.findOne(tipZvanjaId).orElse(null);
		
		if( tipZvanja != null) {
			izmenjeniTipZvanja.setId(tipZvanjaId);
			
			tipZvanjaService.save(izmenjeniTipZvanja);
			return new ResponseEntity<TipZvanja>(izmenjeniTipZvanja, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<TipZvanja>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{tipZvanjaId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<TipZvanja> deleteTipZvanja(@PathVariable("tipZvanjaId") Long tipZvanjaId) {
	
		if(tipZvanjaService.findOne(tipZvanjaId).isPresent()) {
			tipZvanjaService.delete(tipZvanjaId);
			return new ResponseEntity<TipZvanja>(HttpStatus.OK);
		}
		
		return new ResponseEntity<TipZvanja>(HttpStatus.NOT_FOUND);
	}
}
