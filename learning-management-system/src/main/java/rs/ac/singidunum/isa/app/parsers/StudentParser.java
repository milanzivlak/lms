package rs.ac.singidunum.isa.app.parsers;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;

import org.w3c.dom.Document;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.RegistrovaniKorisnikDTO;
import rs.ac.singidunum.isa.app.dto.StudentDTO;
import rs.ac.singidunum.isa.app.dto.StudentNaGodiniDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;

import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class StudentParser {
	//private static final String FILENAME = "C:\\Users\\Korisnik\\Downloads\\student.xml";
		private static final String SCHEMANAME = "student.xsd";
		public ArrayList<StudentDTO> spisakStudenata;
		public ArrayList<StudentDTO> importovaniStudenti;
		
		public StudentParser(ArrayList<StudentDTO> spisakStudenata) {
			this.spisakStudenata = spisakStudenata;
		}
		
		public StudentParser() {
			
		}
		public void export() throws IOException, ParserConfigurationException, TransformerException {
			//root
			
			DocumentBuilderFactory dbFactory =
			         DocumentBuilderFactory.newInstance();
			         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			         Document doc = (Document) dBuilder.newDocument();
			
	        Element studenti = doc.createElement("studenti");
			doc.appendChild(studenti);
			
			for(StudentDTO s : this.spisakStudenata) {
				Element student = doc.createElement("student");
				
				Attr attribute = doc.createAttribute("id");
				attribute.setValue(s.getId().toString());
				student.setAttributeNode(attribute);
				
				//Jmbg
				Element jmbg = doc.createElement("jmbg");
				jmbg.appendChild(doc.createTextNode(s.getJmbg()));
				student.appendChild(jmbg);
				
				//Ime
				Element ime = doc.createElement("ime");
				ime.appendChild(doc.createTextNode(s.getIme()));
				student.appendChild(ime);

				
				
				//-----------------------------------------------------------------
				
				//Adresa
				Element adresa = doc.createElement("adresa");
				AdresaDTO adresaDTO = s.getAdresa();
				Attr adresaID = doc.createAttribute("id");
				adresaID.setValue(adresaDTO.getId().toString());
				adresa.setAttributeNode(adresaID);
				
				//Adresa-ulica
				Element adresaUlica = doc.createElement("ulica");
				adresaUlica.appendChild(doc.createTextNode(adresaDTO.getUlica()));
				adresa.appendChild(adresaUlica);
				
				//Adresa-broj
				Element adresBroj = doc.createElement("broj");
				adresBroj.appendChild(doc.createTextNode(adresaDTO.getBroj()));
				adresa.appendChild(adresBroj);
				
				//Adresa-mesto
				Element mesto = doc.createElement("mesto");
				MestoDTO mestoDTO = adresaDTO.getMesto();
				Attr mestoID = doc.createAttribute("id");
				mestoID.setValue(mestoDTO.getId().toString());
				mesto.setAttributeNode(mestoID);
				
				Element mestoNaziv = doc.createElement("naziv");
				mestoNaziv.appendChild(doc.createTextNode(mestoDTO.getNaziv()));
				mesto.appendChild(mestoNaziv);
				
				//Mesto-drzava
				Element drzava = doc.createElement("drzava");
				DrzavaDTO drzavaDTO = mestoDTO.getDrzava();
				Attr drzavaID = doc.createAttribute("id");
				drzavaID.setValue(drzavaDTO.getId().toString());
				drzava.setAttributeNode(drzavaID);
				
				//Drzava-naziv
				Element drzavaNaziv = doc.createElement("naziv");
				drzavaNaziv.appendChild(doc.createTextNode(drzavaDTO.getNaziv()));
				drzava.appendChild(drzavaNaziv);
				
				mesto.appendChild(drzava);
				adresa.appendChild(mesto);
				student.appendChild(adresa);
				
				
				Element registrovaniKorisnik = doc.createElement("registrovaniKorisnik");
				RegistrovaniKorisnikDTO registrovaniKorisnikDTO = s.getRegistrovaniKorisnik();
				Attr rkID = doc.createAttribute("id");
				rkID.setValue(registrovaniKorisnikDTO.getId().toString());
				registrovaniKorisnik.setAttributeNode(rkID);
				
				Element rkEmail = doc.createElement("email");
				rkEmail.appendChild(doc.createTextNode(registrovaniKorisnikDTO.getEmail()));
				registrovaniKorisnik.appendChild(rkEmail);
				
				Element rkKorisnicko = doc.createElement("korisnickoIme");
				rkKorisnicko.appendChild(doc.createTextNode(registrovaniKorisnikDTO.getKorisnickoIme()));
				registrovaniKorisnik.appendChild(rkKorisnicko);
				
				Element rkLozinka = doc.createElement("lozinka");
				rkLozinka.appendChild(doc.createTextNode(registrovaniKorisnikDTO.getLozinka()));
				registrovaniKorisnik.appendChild(rkLozinka);
				
				
				student.appendChild(registrovaniKorisnik);
				
			
				studenti.appendChild(student);
			}
			
			
			
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			Schema schema = loadSchema(SCHEMANAME);
			
			if(validateXml(schema, doc)) {
					DOMSource source = new DOMSource((Node) doc);
					String home = System.getProperty("user.home");
					File file = new File(home+"/Downloads/" + "studenti" + ".xml");
					StreamResult result = new StreamResult(file);
					//StreamResult result =  new StreamResult(new File("C:\\Users\\Korisnik\\Desktop\\xml\\drzave.xml"));
					transformer.transform(source, result);
					StreamResult consoleResult = new StreamResult(System.out);
			        transformer.transform(source, consoleResult);
			}else {
				System.out.println("Neispravan format za export!");
			}
				
		}
		
		public ArrayList<StudentDTO> importXml(String s){
			
			this.importovaniStudenti = new ArrayList<StudentDTO>();
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			
			try {
				
				dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
				
				DocumentBuilder db = dbf.newDocumentBuilder();
				
				Document doc = convertStringToXMLDocument(s);
				Schema schema = loadSchema(SCHEMANAME);
				
				if(validateXml(schema, doc)) {
					doc.getDocumentElement().normalize();
					
					System.out.println("Root element: " +doc.getDocumentElement().getNodeName());
					
					NodeList list = doc.getElementsByTagName("student");
					
					for(int temp = 0; temp < list.getLength(); temp++) {
						Node node = list.item(temp);
						
						if(node.getNodeType() == Node.ELEMENT_NODE) {
							
							Element student = (Element) node;
							
							String studentID = student.getAttribute("id");
							String studentJmbg = student.getElementsByTagName("jmbg").item(0).getTextContent();
							String studentIme = student.getElementsByTagName("ime").item(0).getTextContent();
							
							// Adresa u studentu
							
							Element adresa = (Element)student.getElementsByTagName("adresa").item(0);
							
							String adresaID = adresa.getAttribute("id");
							String adresaUlica = adresa.getElementsByTagName("ulica").item(0).getTextContent();
							String adresaBroj = adresa.getElementsByTagName("broj").item(0).getTextContent();
							
							// Mesto u adresi
							
							Element adresa_mesto = (Element)adresa.getElementsByTagName("mesto").item(0);
							
							String adresa_mestoID = adresa_mesto.getAttribute("id");
							String adresa_mestoNaziv = adresa_mesto.getElementsByTagName("naziv").item(0).getTextContent();
							
							// Drzava u mestu
							
							Element mesto_drzava = (Element)adresa_mesto.getElementsByTagName("drzava").item(0);
							
							String mesto_drzavaID = mesto_drzava.getAttribute("id");
							String mesto_drzavaNaziv = mesto_drzava.getElementsByTagName("naziv").item(0).getTextContent();
							
							// Registrovani korisnik
							
							Element reg_korisnik = (Element)student.getElementsByTagName("registrovaniKorisnik").item(0);
							
							String korisnikID = reg_korisnik.getAttribute("id");
							String korisnikEmail = reg_korisnik.getElementsByTagName("email").item(0).getTextContent();
							String korisnikIme = reg_korisnik.getElementsByTagName("korisnickoIme").item(0).getTextContent();
							String korisnikLozinka = reg_korisnik.getElementsByTagName("lozinka").item(0).getTextContent();
							
							
							// Dodavanje u DTO
							
							DrzavaDTO drzavaDTO = new DrzavaDTO();
							drzavaDTO.setId(Long.parseLong(mesto_drzavaID));
							drzavaDTO.setNaziv(mesto_drzavaNaziv);
							
							MestoDTO mestoDTO = new MestoDTO();
							mestoDTO.setId(Long.parseLong(adresa_mestoID));
							mestoDTO.setNaziv(adresa_mestoNaziv);
							mestoDTO.setDrzava(drzavaDTO);
							
							AdresaDTO adresaDTO = new AdresaDTO();
							adresaDTO.setId(Long.parseLong(adresaID));
							adresaDTO.setUlica(adresaUlica);
							adresaDTO.setBroj(adresaBroj);
							adresaDTO.setMesto(mestoDTO);
							
							RegistrovaniKorisnikDTO reg_korisnikDTO = new RegistrovaniKorisnikDTO();
							reg_korisnikDTO.setId(Long.parseLong(korisnikID));
							reg_korisnikDTO.setEmail(korisnikEmail);
							reg_korisnikDTO.setKorisnickoIme(korisnikIme);
							reg_korisnikDTO.setLozinka(korisnikLozinka);
							
							StudentDTO studentDTO = new StudentDTO();
							studentDTO.setId(Long.parseLong(studentID));
							studentDTO.setJmbg(studentJmbg);
							studentDTO.setIme(studentIme);
							studentDTO.setAdresa(adresaDTO);
							studentDTO.setRegistrovaniKorisnik(reg_korisnikDTO);
							
							this.importovaniStudenti.add(studentDTO);
						}
					}
					return this.importovaniStudenti;
				}else {
					System.out.println("XML dokument nije ispravan!");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}
		
		
		public static Schema loadSchema(String schemaFileName) {
			Schema schema = null;
			try {
				String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
				SchemaFactory factory = SchemaFactory.newInstance(language);
				
				schema = factory.newSchema(new File(schemaFileName));
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return schema;
		}
		
		public static Document parseXmlDom(String xmlName) {
			
			Document document = null;
			try {
			   DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			   DocumentBuilder builder = factory.newDocumentBuilder();
			   document = builder.parse(new File(xmlName));
			} catch (Exception e) {
			   System.out.println(e.toString());
			}
			return document;
		}
		
		public static boolean validateXml(Schema schema, Document document) {
			try {
				Validator validator = schema.newValidator();
				
				validator.validate(new DOMSource(document));
				System.out.println("Validacija uspjesna!");
				return true;
				
			} catch (Exception e) {
				System.out.println("Validacije neuspjesna!");
				return false;
			}
			
		}
		
		public static Document convertStringToXMLDocument(String xmlString) 
	    {
	        //Parser that produces DOM object trees from XML content
	        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	         
	        //API to obtain DOM Document instance
	        DocumentBuilder builder = null;
	        try
	        {
	            //Create DocumentBuilder with default configuration
	            builder = factory.newDocumentBuilder();
	             
	            //Parse the content to Document object
	            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
	            return doc;
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	        return null;
	    }
}
