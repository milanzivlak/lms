package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Fakultet {
	
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String naziv;
	
	// TODO: Ovo treba dodati u konstruktre i ostale stvari gde bude falio
//	@Lob
//	private String opis;
//	
	// TODO: plus treba napraviti klasu kontakt i povezati je ovde
//	private Kontakt kontakt;
	
	@OneToOne(mappedBy = "fakultet")
	private Adresa adresa;
	
	@ManyToOne
	private Univerzitet univerzitet;
	
	@OneToOne(mappedBy = "fakultet", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private Nastavnik dekan;
	
	@OneToMany(mappedBy = "fakultet", cascade = CascadeType.REMOVE)
	private Set<StudijskiProgram> studijskiProgrami = new HashSet<StudijskiProgram>();

	public Fakultet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Fakultet(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public Univerzitet getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(Univerzitet univerzitet) {
		this.univerzitet = univerzitet;
	}

	public Nastavnik getDekan() {
		return dekan;
	}

	public void setDekan(Nastavnik dekan) {
		this.dekan = dekan;
	}

	public Set<StudijskiProgram> getStudijskiProgrami() {
		return studijskiProgrami;
	}

	public void setStudijskiProgrami(Set<StudijskiProgram> studijskiProgrami) {
		this.studijskiProgrami = studijskiProgrami;
	}
	
	
}
