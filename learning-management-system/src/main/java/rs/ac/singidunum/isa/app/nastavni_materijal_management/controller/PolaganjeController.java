package rs.ac.singidunum.isa.app.nastavni_materijal_management.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.PolaganjeDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Polaganje;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.PolaganjeService;

@Controller
@RequestMapping(path = "/api/nastavni-materijal/polaganje")
public class PolaganjeController {
	@Autowired
	private PolaganjeService polaganjeService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Page<PolaganjeDTO>> getAllPolaganjei(Pageable pageable) {
		return new ResponseEntity<Page<PolaganjeDTO>>(
				polaganjeService.findAll(pageable).map(a -> new PolaganjeDTO(a)), HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{polaganjeId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<PolaganjeDTO> getPolaganje(@PathVariable("polaganjeId") Long polaganjeId) {
		
		Optional<Polaganje> polaganje = polaganjeService.findOne(polaganjeId);
		
		if(polaganje.isPresent()) {
			PolaganjeDTO PolaganjeDTO = new PolaganjeDTO(polaganje.get());
			return new ResponseEntity<PolaganjeDTO>(PolaganjeDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<PolaganjeDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Polaganje> createPolaganje(@RequestBody Polaganje polaganje){
		
		try {
			
			polaganjeService.save(polaganje);
			
			return new ResponseEntity<Polaganje>(polaganje, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Polaganje>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{polaganjeId}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Polaganje> updatePolaganje(@PathVariable("polaganjeId") Long polaganjeId, @RequestBody Polaganje izmenjenPolaganje) {
		
		Polaganje adresa = polaganjeService.findOne(polaganjeId).orElse(null);
		
		if( adresa != null) {
			izmenjenPolaganje.setId(polaganjeId);
			
			polaganjeService.save(izmenjenPolaganje);
			return new ResponseEntity<Polaganje>(izmenjenPolaganje, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Polaganje>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{polaganjeId}", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Polaganje> deletePolaganje(@PathVariable("polaganjeId") Long polaganjeId) {
	
		if(polaganjeService.findOne(polaganjeId).isPresent()) {
			polaganjeService.delete(polaganjeId);
			return new ResponseEntity<Polaganje>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Polaganje>(HttpStatus.NOT_FOUND);
	}

}
