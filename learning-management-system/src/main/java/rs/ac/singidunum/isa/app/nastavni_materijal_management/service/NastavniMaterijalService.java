package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.NastavniMaterijalRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class NastavniMaterijalService extends GenerateService<NastavniMaterijalRepository, NastavniMaterijal, Long>{

	public Page<NastavniMaterijal> findAll(Pageable pageable) {

		return ((NastavniMaterijalRepository) this.getRepository()).findAll(pageable);
	}

}
