package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;



public class MementoDTO {
	
	
	private Long id;
	
	private NastavniMaterijalDTO state;
	
	private CaretakerDTO caretaker;
	
	
	public MementoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MementoDTO(Long id) {
		this(id, null, null);
	}
	
	

	public MementoDTO(Long id, NastavniMaterijalDTO state, CaretakerDTO caretaker) {
		super();
		this.id = id;
		this.state = state;
		this.caretaker = caretaker;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NastavniMaterijalDTO getState() {
		return state;
	}

	public void setState(NastavniMaterijalDTO state) {
		this.state = state;
	}

	public CaretakerDTO getCaretaker() {
		return caretaker;
	}

	public void setCaretaker(CaretakerDTO caretaker) {
		this.caretaker = caretaker;
	}
}
