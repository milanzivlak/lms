package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class NastavnikNaRealizaciji {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private int brojCasova;
	
	// Nisam siguran da li ide OneToOne ili ManyToOne
	
	@OneToOne()
	private Nastavnik nastavnik;
	
	@ManyToOne(optional = false)
	private TipNastave tipNastave;

	@OneToMany(mappedBy = "nastavnikNaRealizaciji")
	private Set<RealizacijaPredmeta> realizacijePredmeta = new HashSet<RealizacijaPredmeta>();
	
	
	
	public NastavnikNaRealizaciji() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NastavnikNaRealizaciji(Long id, int brojCasova) {
		super();
		this.id = id;
		this.brojCasova = brojCasova;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBrojCasova() {
		return brojCasova;
	}

	public void setBrojCasova(int brojCasova) {
		this.brojCasova = brojCasova;
	}

	public Nastavnik getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}

	public TipNastave getTipNastave() {
		return tipNastave;
	}

	public void setTipNastave(TipNastave tipNastave) {
		this.tipNastave = tipNastave;
	}

	public Set<RealizacijaPredmeta> getRealizacijePredmeta() {
		return realizacijePredmeta;
	}

	public void setRealizacijePredmeta(Set<RealizacijaPredmeta> realizacijePredmeta) {
		this.realizacijePredmeta = realizacijePredmeta;
	}
	
	
	
}
