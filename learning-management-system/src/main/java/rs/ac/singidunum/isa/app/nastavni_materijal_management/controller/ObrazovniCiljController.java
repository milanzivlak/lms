package rs.ac.singidunum.isa.app.nastavni_materijal_management.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.ObrazovniCiljDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.ObrazovniCilj;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.ObrazovniCiljService;

@Controller
@RequestMapping(path = "/api/nastavni-materijal/obrazovni-cilj")
public class ObrazovniCiljController {
	@Autowired
	private ObrazovniCiljService obrazovniCiljService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Page<ObrazovniCiljDTO>> getAllObrazovniCilji(Pageable pageable) {
		return new ResponseEntity<Page<ObrazovniCiljDTO>>(
				obrazovniCiljService.findAll(pageable).map(a -> new ObrazovniCiljDTO(a)), HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{obrazovniCiljId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<ObrazovniCiljDTO> getObrazovniCilj(@PathVariable("obrazovniCiljId") Long obrazovniCiljId) {
		
		Optional<ObrazovniCilj> obrazovniCilj = obrazovniCiljService.findOne(obrazovniCiljId);
		
		if(obrazovniCilj.isPresent()) {
			ObrazovniCiljDTO ObrazovniCiljDTO = new ObrazovniCiljDTO(obrazovniCilj.get());
			return new ResponseEntity<ObrazovniCiljDTO>(ObrazovniCiljDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<ObrazovniCiljDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<ObrazovniCilj> createObrazovniCilj(@RequestBody ObrazovniCilj obrazovniCilj){
		
		try {
			
			obrazovniCiljService.save(obrazovniCilj);
			
			return new ResponseEntity<ObrazovniCilj>(obrazovniCilj, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<ObrazovniCilj>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{obrazovniCiljId}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<ObrazovniCilj> updateObrazovniCilj(@PathVariable("obrazovniCiljId") Long obrazovniCiljId, @RequestBody ObrazovniCilj izmenjenObrazovniCilj) {
		
		ObrazovniCilj adresa = obrazovniCiljService.findOne(obrazovniCiljId).orElse(null);
		
		if( adresa != null) {
			izmenjenObrazovniCilj.setId(obrazovniCiljId);
			
			obrazovniCiljService.save(izmenjenObrazovniCilj);
			return new ResponseEntity<ObrazovniCilj>(izmenjenObrazovniCilj, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<ObrazovniCilj>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{obrazovniCiljId}", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<ObrazovniCilj> deleteObrazovniCilj(@PathVariable("obrazovniCiljId") Long obrazovniCiljId) {
	
		if(obrazovniCiljService.findOne(obrazovniCiljId).isPresent()) {
			obrazovniCiljService.delete(obrazovniCiljId);
			return new ResponseEntity<ObrazovniCilj>(HttpStatus.OK);
		}
		
		return new ResponseEntity<ObrazovniCilj>(HttpStatus.NOT_FOUND);
	}



}
