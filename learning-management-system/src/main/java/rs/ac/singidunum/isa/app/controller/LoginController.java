package rs.ac.singidunum.isa.app.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;
import rs.ac.singidunum.isa.app.dto.RegistrovaniKorisnikDTO;
import rs.ac.singidunum.isa.app.dto.TokenDTO;

import rs.ac.singidunum.isa.app.service.RegistrovaniKorisnikService;
import rs.ac.singidunum.isa.app.utils.TokenUtils;

@Controller
@RequestMapping("/api")
public class LoginController {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private TokenUtils tokenUtils;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private RegistrovaniKorisnikService registrovaniKorisnikService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public ResponseEntity<TokenDTO> login(@RequestBody RegistrovaniKorisnikDTO korisnik) {
		
		try {
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(korisnik.getKorisnickoIme(), korisnik.getLozinka());
		Authentication authentication = authenticationManager.authenticate(token);
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		UserDetails userDetails = userDetailsService.loadUserByUsername(korisnik.getKorisnickoIme());
		String jwt = tokenUtils.generateToken(userDetails);
		
		TokenDTO jwtDTO = new TokenDTO(jwt);
		
		return new ResponseEntity<TokenDTO>(jwtDTO, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<TokenDTO>(HttpStatus.UNAUTHORIZED);
		}
		
	}
	
	@RequestMapping(path = "/register", method = RequestMethod.POST)
	public ResponseEntity<RegistrovaniKorisnikDTO> register(@RequestBody RegistrovaniKorisnikDTO korisnik) {
		
		RegistrovaniKorisnik noviKorisnik = new RegistrovaniKorisnik(korisnik.getId(), korisnik.getKorisnickoIme(), passwordEncoder.encode(korisnik.getLozinka()), korisnik.getEmail());
		
		registrovaniKorisnikService.save(noviKorisnik);
		
		return new ResponseEntity<RegistrovaniKorisnikDTO>(new RegistrovaniKorisnikDTO(noviKorisnik.getId(), noviKorisnik.getKorisnickoIme(), null, noviKorisnik.getEmail()), HttpStatus.OK);
	}
}
