package rs.ac.singidunum.isa.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Mesto;
import rs.ac.singidunum.isa.app.parsers.MestoParser;
import rs.ac.singidunum.isa.app.service.DrzavaService;
import rs.ac.singidunum.isa.app.service.MestoService;

@Controller
@RequestMapping(path = "/api/mesta")
@CrossOrigin
public class MestoController {
	
	@Autowired
	private MestoService mestoService;
	
	@Autowired
	private DrzavaService drzavaService;
	
	MestoParser parser = new MestoParser();
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<MestoDTO>> getAllMesta() {
		
		
		ArrayList<MestoDTO> mesta = new ArrayList<MestoDTO>();
		
		for(Mesto mesto : mestoService.findAll()) {
			MestoDTO dto = MestoDTO.fromMesto(mesto);
			dto.addDrzava(mesto.getDrzava());
			dto.addAdressses(mesto.getAdrese());
			mesta.add(dto);
			//mesta.add(MestoDTO.fromMesto(mesto).addAdressses(mesto.getAdrese()));
		}
	
		return new ResponseEntity<Iterable<MestoDTO>>(mesta, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/import", method = RequestMethod.GET)
	public ResponseEntity<Iterable<MestoDTO>> mestaImport(@Param(value = "url") String url) throws UnsupportedEncodingException {
		
		ArrayList<MestoDTO> importovane = this.parser.importXml(url);
		System.out.println(importovane.size());
		for(MestoDTO d : importovane) {
			Drzava drzava = new Drzava(d.getDrzava().getId(), d.getDrzava().getNaziv());
			Mesto mesto = new Mesto(d.getId(), d.getNaziv());
			drzavaService.save(drzava);
			mesto.setDrzava(drzava);
			
			mestoService.save(mesto);
			
			
			
			
			
		}
		return new ResponseEntity<Iterable<MestoDTO>>(this.parser.importXml(url), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/export", method = RequestMethod.GET)
	public void mestaExport() {
		System.out.println("Exporting...");
		ArrayList<MestoDTO> mesta = new ArrayList<MestoDTO>();
		
		for(Mesto mesto : mestoService.findAll()) {
			MestoDTO mesto1 = MestoDTO.fromMesto(mesto);
			//mesta.add(MestoDTO.fromMesto(mesto));
			
			DrzavaDTO drzava = new DrzavaDTO(mesto.getDrzava().getId(), mesto.getDrzava().getNaziv());
			mesto1.setDrzava(drzava);
			
			mesta.add(mesto1);
		}
		
		this.parser.spisakMesta = mesta;
		try {
			this.parser.export();
		} catch (IOException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(path = "/{mestoId}", method = RequestMethod.GET)
	public ResponseEntity<MestoDTO> getMesto(@PathVariable("mestoId") Long mestoId) {
		
		Optional<Mesto> mesta = mestoService.findOne(mestoId);
		
		if(mesta.isPresent()) {
			MestoDTO mestoDTO = new MestoDTO(mesta.get().getId(), mesta.get().getNaziv());
			mestoDTO.addDrzava(mesta.get().getDrzava());
			return new ResponseEntity<MestoDTO>(mestoDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<MestoDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Mesto> createMesto(@RequestBody Mesto mesto){
		
		try {
			
			mestoService.save(mesto);
			
			return new ResponseEntity<Mesto>(mesto, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Mesto>(HttpStatus.BAD_REQUEST);

		
	}
	
	@RequestMapping(path = "/{mestoId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Mesto> updateMesto(@PathVariable("mestoId") Long mestoId, @RequestBody Mesto izmenjenoMesto) {
		
		Mesto mesto = mestoService.findOne(mestoId).orElse(null);
		
		if( mesto != null) {
			izmenjenoMesto.setId(mestoId);
			
			mestoService.save(izmenjenoMesto);
			return new ResponseEntity<Mesto>(izmenjenoMesto, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Mesto>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{mestoId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Mesto> deleteMesto(@PathVariable("mestoId") Long mestoId) {
	
		if(mestoService.findOne(mestoId).isPresent()) {
			mestoService.delete(mestoId);
			return new ResponseEntity<Mesto>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Mesto>(HttpStatus.NOT_FOUND);
	}
}
