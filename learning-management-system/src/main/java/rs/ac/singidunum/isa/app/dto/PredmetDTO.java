package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.model.Predmet;

public class PredmetDTO {
	
	private Long id;
	private String naziv;
	
	private int espb;
	private boolean obavezan;
	private int brojPredavanja;
	private int brojVezbi;
	private int drugiObliciNastave;
	private int istrazivackiRad;
	private int ostaliCasovi;
	private int brojSemestara;
	
	private GodinaStudijaDTO godinaStudija;
	private ArrayList<PredmetDTO> preduslov = new ArrayList<PredmetDTO>();
	private IshodDTO silabus;
	private RealizacijaPredmetaDTO realizacijaPredmeta;
	
	public PredmetDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public PredmetDTO(Long id, String naziv, int espb, boolean obavezan, int brojPredavanja, int brojVezbi,
			int drugiObliciNastave, int istrazivackiRad, int ostaliCasovi, int brojSemestara) {
		this(id, naziv, espb, obavezan, brojPredavanja, brojVezbi, drugiObliciNastave, istrazivackiRad, ostaliCasovi, brojSemestara, null, null, null, null);
	}
	
	public PredmetDTO(Long id, String naziv, int espb, boolean obavezan, int brojPredavanja, int brojVezbi,
			int drugiObliciNastave, int istrazivackiRad, int ostaliCasovi, int brojSemestara, GodinaStudijaDTO godinaStudija) {
		this(id, naziv, espb, obavezan, brojPredavanja, brojVezbi, drugiObliciNastave, istrazivackiRad, ostaliCasovi, brojSemestara, godinaStudija, null, null, null);
	}

	public PredmetDTO(Long id, String naziv, int espb, boolean obavezan, int brojPredavanja, int brojVezbi,
			int drugiObliciNastave, int istrazivackiRad, int ostaliCasovi, int brojSemestara,
			GodinaStudijaDTO godinaStudija, ArrayList<PredmetDTO> preduslov, IshodDTO silabus,
			RealizacijaPredmetaDTO realizacijaPredmeta) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.espb = espb;
		this.obavezan = obavezan;
		this.brojPredavanja = brojPredavanja;
		this.brojVezbi = brojVezbi;
		this.drugiObliciNastave = drugiObliciNastave;
		this.istrazivackiRad = istrazivackiRad;
		this.ostaliCasovi = ostaliCasovi;
		this.brojSemestara = brojSemestara;
		this.godinaStudija = godinaStudija;
		this.preduslov = preduslov;
		this.silabus = silabus;
		this.realizacijaPredmeta = realizacijaPredmeta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getEspb() {
		return espb;
	}

	public void setEspb(int espb) {
		this.espb = espb;
	}

	public boolean isObavezan() {
		return obavezan;
	}

	public void setObavezan(boolean obavezan) {
		this.obavezan = obavezan;
	}

	public int getBrojPredavanja() {
		return brojPredavanja;
	}

	public void setBrojPredavanja(int brojPredavanja) {
		this.brojPredavanja = brojPredavanja;
	}

	public int getBrojVezbi() {
		return brojVezbi;
	}

	public void setBrojVezbi(int brojVezbi) {
		this.brojVezbi = brojVezbi;
	}

	public int getDrugiObliciNastave() {
		return drugiObliciNastave;
	}

	public void setDrugiObliciNastave(int drugiObliciNastave) {
		this.drugiObliciNastave = drugiObliciNastave;
	}

	public int getIstrazivackiRad() {
		return istrazivackiRad;
	}

	public void setIstrazivackiRad(int istrazivackiRad) {
		this.istrazivackiRad = istrazivackiRad;
	}

	public int getOstaliCasovi() {
		return ostaliCasovi;
	}

	public void setOstaliCasovi(int ostaliCasovi) {
		this.ostaliCasovi = ostaliCasovi;
	}

	public int getBrojSemestara() {
		return brojSemestara;
	}

	public void setBrojSemestara(int brojSemestara) {
		this.brojSemestara = brojSemestara;
	}

	public GodinaStudijaDTO getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(GodinaStudijaDTO godinaStudija) {
		this.godinaStudija = godinaStudija;
	}

	public ArrayList<PredmetDTO> getPreduslov() {
		return preduslov;
	}

	public void setPreduslov(ArrayList<PredmetDTO> preduslov) {
		this.preduslov = preduslov;
	}

	public IshodDTO getSilabus() {
		return silabus;
	}

	public void setSilabus(IshodDTO silabus) {
		this.silabus = silabus;
	}

	public RealizacijaPredmetaDTO getRealizacijaPredmeta() {
		return realizacijaPredmeta;
	}

	public void setRealizacijaPredmeta(RealizacijaPredmetaDTO realizacijaPredmeta) {
		this.realizacijaPredmeta = realizacijaPredmeta;
	}
	
	public static PredmetDTO fromPredmet(Predmet predmet) {
		return new PredmetDTO(
				predmet.getId(), 
				predmet.getNaziv(), 
				predmet.getEspb(),
				predmet.isObavezan(), 
				predmet.getBrojPredavanja(),
				predmet.getBrojVezbi(),
				predmet.getDrugiObliciNastave(),
				predmet.getIstrazivackiRad(),
				predmet.getOstaliCasovi(),
				predmet.getBrojSemestara());
	}
}
