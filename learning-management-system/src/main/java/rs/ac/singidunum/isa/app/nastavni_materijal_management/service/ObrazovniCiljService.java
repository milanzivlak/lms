package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.ObrazovniCilj;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.ObrazovniCiljRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class ObrazovniCiljService extends GenerateService<ObrazovniCiljRepository, ObrazovniCilj, Long>{

	public Page<ObrazovniCilj> findAll(Pageable pageable) {
		return ((ObrazovniCiljRepository) this.getRepository()).findAll(pageable);
	}

}
