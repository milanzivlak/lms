package rs.ac.singidunum.isa.app.nastavni_materijal_management.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import rs.ac.singidunum.isa.app.model.Ishod;

@Entity
public class ObrazovniCilj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String opis;
	
	@OneToMany(mappedBy = "obrazovniCilj")
	private Set<Ishod> ishodi = new HashSet<Ishod>();
	
	
	public Set<Ishod> getIshodi() {
		return ishodi;
	}

	public void setIshodi(Set<Ishod> ishodi) {
		this.ishodi = ishodi;
	}

	public ObrazovniCilj() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ObrazovniCilj(Long id, String opis) {
		super();
		this.id = id;
		this.opis = opis;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}
	
	
}
