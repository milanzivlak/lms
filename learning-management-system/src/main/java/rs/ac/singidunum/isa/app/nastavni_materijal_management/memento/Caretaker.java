package rs.ac.singidunum.isa.app.nastavni_materijal_management.memento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;



@Entity
public class Caretaker {

	@Id
	private Long id;
	
	@OneToMany(mappedBy = "caretaker", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Memento> mementos = new ArrayList<Memento>();
	
	@OneToOne(mappedBy = "caretaker")
	
	@JsonIgnore
	private NastavniMaterijal nastavniMaterijal;
	public Caretaker() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Caretaker(Long id) {
		super();
		this.id = id;
	}


	public Memento add(Memento state) {
		this.mementos.add(state);
		return state;
	}
	
	public Memento get(int index) {
		return this.mementos.get(index);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Memento> getMementos() {
		return mementos;
	}

	public void setMementos(List<Memento> mementos) {
		this.mementos = mementos;
	}

	public NastavniMaterijal getNastavniMaterijal() {
		return nastavniMaterijal;
	}

	public void setNastavniMaterijal(NastavniMaterijal nastavniMaterijal) {
		this.nastavniMaterijal = nastavniMaterijal;
	}
	
	
	
}
