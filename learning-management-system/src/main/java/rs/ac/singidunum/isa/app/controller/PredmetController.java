package rs.ac.singidunum.isa.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;
import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.IshodDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.RealizacijaPredmetaDTO;
import rs.ac.singidunum.isa.app.dto.RegistrovaniKorisnikDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Fakultet;
import rs.ac.singidunum.isa.app.model.GodinaStudija;
import rs.ac.singidunum.isa.app.model.Mesto;
import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.Predmet;
import rs.ac.singidunum.isa.app.model.StudijskiProgram;
import rs.ac.singidunum.isa.app.model.Univerzitet;
import rs.ac.singidunum.isa.app.parsers.PredmetParser;
import rs.ac.singidunum.isa.app.service.AdresaService;
import rs.ac.singidunum.isa.app.service.DrzavaService;
import rs.ac.singidunum.isa.app.service.FakultetService;
import rs.ac.singidunum.isa.app.service.GodinaStudijaService;
import rs.ac.singidunum.isa.app.service.MestoService;
import rs.ac.singidunum.isa.app.service.NastavnikService;
import rs.ac.singidunum.isa.app.service.PredmetService;
import rs.ac.singidunum.isa.app.service.RegistrovaniKorisnikService;
import rs.ac.singidunum.isa.app.service.StudijskiProgramService;
import rs.ac.singidunum.isa.app.service.UniverzitetService;

@Controller
@RequestMapping(path = "/api/predmeti")
@CrossOrigin
public class PredmetController {
	
	@Autowired
	private PredmetService predmetService;
	
	PredmetParser parser = new PredmetParser();
	@Autowired
	private GodinaStudijaService godinaStudijaService;
	@Autowired
	private StudijskiProgramService spService;
	@Autowired
	private FakultetService fakultetService;
	@Autowired
	private UniverzitetService univerzitetService;
	
	@Autowired
	private NastavnikService nastavnikService;
	
	@Autowired
	private RegistrovaniKorisnikService registrovaniKorisnikService;
	
	@Autowired
	private AdresaService adresaService;
	
	@Autowired
	private MestoService mestoService;
	
	@Autowired
	private DrzavaService drzavaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<PredmetDTO>> getAllPredmeti() {
		
		
		ArrayList<PredmetDTO> predmeti = new ArrayList<PredmetDTO>();
		
		for(Predmet predmet : predmetService.findAll()) {
			ArrayList<PredmetDTO> preduslov = new ArrayList<PredmetDTO>();
			
			for(Predmet pr : predmet.getPreduslov()) {
				preduslov.add(new PredmetDTO(pr.getId(), pr.getNaziv(), pr.getEspb(), pr.isObavezan(), pr.getBrojPredavanja(), pr.getBrojVezbi(), pr.getDrugiObliciNastave(), pr.getIstrazivackiRad(), pr.getOstaliCasovi(), pr.getBrojSemestara()));
			}
			
			GodinaStudijaDTO godinaStudija = new GodinaStudijaDTO(predmet.getGodinaStudija().getId(), predmet.getGodinaStudija().getGodina());
			//IshodDTO silabus = new IshodDTO(predmet.getSilabus().getId(), predmet.getSilabus().getOpis());
			//RealizacijaPredmetaDTO realizacijaPredmeta = new RealizacijaPredmetaDTO(predmet.getRealizacijaPredmeta().getId());
			
			predmeti.add(new PredmetDTO(predmet.getId(), predmet.getNaziv(), predmet.getEspb(), predmet.isObavezan(), predmet.getBrojPredavanja(), predmet.getBrojVezbi(), predmet.getDrugiObliciNastave(), predmet.getIstrazivackiRad(), predmet.getOstaliCasovi(), predmet.getBrojSemestara(), godinaStudija));
			//nastavnici.add(new NastavnikDTO(nastavnik.getId(), nastavnik.getIme(), nastavnik.getBiografija(), nastavnik.getJmbg(), zvanja));
		}
		
		return new ResponseEntity<Iterable<PredmetDTO>>(predmeti, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/export", method = RequestMethod.GET)
	public void predmetiExport() {
		System.out.println("Exporting...");
		ArrayList<PredmetDTO> predmeti = new ArrayList<PredmetDTO>();
		
		for(Predmet predmet : predmetService.findAll()) {
			PredmetDTO predmetDTO = PredmetDTO.fromPredmet(predmet);
			GodinaStudijaDTO godinaStudija = new GodinaStudijaDTO(predmet.getGodinaStudija().getId(), predmet.getGodinaStudija().getGodina());
			StudijskiProgramDTO studijskiProgram = new StudijskiProgramDTO(predmet.getGodinaStudija().getStudijskiProgram().getId(), predmet.getGodinaStudija().getStudijskiProgram().getNaziv());
			
			Fakultet fakultet = predmet.getGodinaStudija().getStudijskiProgram().getFakultet();
			FakultetDTO fakultetDTO = new FakultetDTO(fakultet.getId(), fakultet.getNaziv());
			AdresaDTO fakultetAdresa = new AdresaDTO(fakultet.getAdresa().getId(), fakultet.getAdresa().getUlica(), fakultet.getAdresa().getBroj());
			Mesto mesto = fakultet.getAdresa().getMesto();
			Drzava drzava = mesto.getDrzava();
			DrzavaDTO drzavaDTO = new DrzavaDTO(drzava.getId(), drzava.getNaziv());
			MestoDTO mestoDTO = new MestoDTO(mesto.getId(), mesto.getNaziv());
			mestoDTO.setDrzava(drzavaDTO);
			fakultetAdresa.setMesto(mestoDTO);
			
			
			
			fakultetDTO.setAdresa(fakultetAdresa);
			
			
			Univerzitet univerzitet = predmet.getGodinaStudija().getStudijskiProgram().getFakultet().getUniverzitet();
			UniverzitetDTO univerzitetDTO = new UniverzitetDTO(univerzitet.getId(),  univerzitet.getUlica(), univerzitet.getBroj());
			AdresaDTO univerzitetAdresa = new AdresaDTO(univerzitet.getId(), univerzitet.getUlica(), univerzitet.getBroj());
			univerzitetDTO.setAdresa(univerzitetAdresa);
			
			Nastavnik rektor = univerzitet.getRektor();
			NastavnikDTO rektorDTO = new NastavnikDTO(rektor.getId(), rektor.getIme(), rektor.getBiografija(), rektor.getJmbg());
			
			RegistrovaniKorisnik rk = rektor.getRegistrovaniKorisnik();
			RegistrovaniKorisnikDTO rkDTO = new RegistrovaniKorisnikDTO();
			rkDTO.setId(rk.getId());
			rkDTO.setEmail(rk.getEmail());
			rkDTO.setKorisnickoIme(rk.getKorisnickoIme());
			rkDTO.setLozinka(rk.getLozinka());
			rektorDTO.setRegistrovaniKorisnik(rkDTO);
			univerzitetDTO.setRektor(rektorDTO);
			
			fakultetDTO.setUniverzitet(univerzitetDTO);
			
			
			Nastavnik dekan = fakultet.getDekan();
			NastavnikDTO dekanDTO = new NastavnikDTO(dekan.getId(), dekan.getIme(), dekan.getBiografija(), dekan.getJmbg());
			
			RegistrovaniKorisnik rkDekan = dekan.getRegistrovaniKorisnik();
			RegistrovaniKorisnikDTO dkDTO = new RegistrovaniKorisnikDTO();
			dkDTO.setId(rkDekan.getId());
			dkDTO.setEmail(rkDekan.getEmail());
			dkDTO.setKorisnickoIme(rkDekan.getKorisnickoIme());
			dkDTO.setLozinka(rkDekan.getLozinka());
			dekanDTO.setRegistrovaniKorisnik(dkDTO);
			fakultetDTO.setDekan(dekanDTO);
			
			
			studijskiProgram.setFakultet(fakultetDTO);
			StudijskiProgram sp = predmet.getGodinaStudija().getStudijskiProgram();
			
			Nastavnik rukovodilac = sp.getRukovodilac();
			NastavnikDTO rukovodilacDTO = new NastavnikDTO(rukovodilac.getId(), rukovodilac.getIme(), rukovodilac.getBiografija(), rukovodilac.getJmbg());
			
			RegistrovaniKorisnik rkRukovodilac = rukovodilac.getRegistrovaniKorisnik();
			RegistrovaniKorisnikDTO ruDTO = new RegistrovaniKorisnikDTO();
			ruDTO.setId(rkRukovodilac.getId());
			ruDTO.setEmail(rkRukovodilac.getEmail());
			ruDTO.setKorisnickoIme(rkRukovodilac.getKorisnickoIme());
			ruDTO.setLozinka(rkRukovodilac.getLozinka());
			rukovodilacDTO.setRegistrovaniKorisnik(ruDTO);
			studijskiProgram.setRukovodilac(rukovodilacDTO);
			
			godinaStudija.setStudijskiProgram(studijskiProgram);
			predmetDTO.setGodinaStudija(godinaStudija);
			
			predmeti.add(predmetDTO);
		}
		
		this.parser.spisakPredmeta = predmeti;
		try {
			this.parser.export();
		} catch (IOException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@RequestMapping(path = "/import", method = RequestMethod.GET)
	@Transactional
	public ResponseEntity<Iterable<PredmetDTO>> predmetiImport(@Param(value = "url") String url) throws UnsupportedEncodingException {

		
		ArrayList<PredmetDTO> importovane = this.parser.importXml(url);
		
		for(PredmetDTO d : importovane) {
			Predmet predmet = new Predmet(d.getId(), d.getNaziv(), d.getEspb(), d.isObavezan(), d.getBrojPredavanja(), d.getBrojVezbi(), d.getDrugiObliciNastave(), d.getIstrazivackiRad(), d.getOstaliCasovi(), d.getBrojSemestara());
			;
			
			GodinaStudija godinaStudija = new GodinaStudija(d.getGodinaStudija().getId(), d.getGodinaStudija().getGodina());
			
			StudijskiProgram sp = new StudijskiProgram(d.getGodinaStudija().getStudijskiProgram().getId(), d.getGodinaStudija().getStudijskiProgram().getNaziv());
			
//			Set<StudijskiProgram> g = new HashSet<StudijskiProgram>();
//
//			Set <GodinaStudija> w = new HashSet<GodinaStudija>();
//			
//			Set <Predmet> q = new HashSet<Predmet>();
//			
//			q.add(predmet);
//			
//			godinaStudija.setPredmeti(q);
//			w.add(godinaStudija);
//			
//			sp.setGodineStudija(w);
//			
//			g.add(sp);
			
			FakultetDTO fakultetDTO = d.getGodinaStudija().getStudijskiProgram().getFakultet();
			Fakultet fakultet = new Fakultet(d.getGodinaStudija().getStudijskiProgram().getFakultet().getId(), d.getGodinaStudija().getStudijskiProgram().getFakultet().getNaziv());
			//fakultet.setStudijskiProgrami(g);
			
			
			AdresaDTO adresaDTO = fakultetDTO.getAdresa();
			MestoDTO mestoDTO = adresaDTO.getMesto();
			DrzavaDTO drzavaDTO = mestoDTO.getDrzava();
			
			
			
			Drzava drzava = new Drzava(drzavaDTO.getId(), drzavaDTO.getNaziv());
			
			Mesto mesto = new Mesto(mestoDTO.getId(), mestoDTO.getNaziv());
			mesto.setDrzava(drzava);
			
			
			Adresa adresa = new Adresa(adresaDTO.getId(), adresaDTO.getUlica(), adresaDTO.getBroj());
		//	Set<Adresa> lista = mesto.getAdrese();
			
			adresa.setMesto(mesto);
		//	lista.add(adresa);
			
			adresa.setFakultet(fakultet);
			
//			adresa = adresaService.save(adresa);
//			fakultet.setAdresa(adresa);
			
			
			
			
//			Set <StudijskiProgram> s = new HashSet<StudijskiProgram>();
//			s.add(sp);
//			fakultet.setStudijskiProgrami(s);
			
			Univerzitet univerzitet = new Univerzitet(d.getGodinaStudija().getStudijskiProgram().getFakultet().getUniverzitet().getId(),
					d.getGodinaStudija().getStudijskiProgram().getFakultet().getUniverzitet().getUlica(),
					d.getGodinaStudija().getStudijskiProgram().getFakultet().getUniverzitet().getBroj());
			
			adresa.setUniverzitet(univerzitet);
			//System.out.println("ID:" + univerzitet.getId());
			
			NastavnikDTO rektorDTO = d.getGodinaStudija().getStudijskiProgram().getFakultet().getUniverzitet().getRektor();
			RegistrovaniKorisnikDTO rektorKorisnikDTO = rektorDTO.getRegistrovaniKorisnik();
			
			RegistrovaniKorisnik rektorKorisnik = new RegistrovaniKorisnik(rektorKorisnikDTO.getId(), rektorKorisnikDTO.getEmail(), rektorKorisnikDTO.getKorisnickoIme(), rektorKorisnikDTO.getLozinka());
			registrovaniKorisnikService.save(rektorKorisnik);
			
			Nastavnik rektor = new Nastavnik(rektorDTO.getId(), rektorDTO.getIme(), rektorDTO.getBiografija(), rektorDTO.getJmbg());
			rektor.setRegistrovaniKorisnik(rektorKorisnik);
			
			nastavnikService.save(rektor);
			univerzitet.setRektor(rektor);
			univerzitet.setAdresa(adresa);
			univerzitetService.save(univerzitet);
			
			
			
			NastavnikDTO dekanDTO = d.getGodinaStudija().getStudijskiProgram().getFakultet().getDekan();
			RegistrovaniKorisnikDTO dekanKorisnikDTO = dekanDTO.getRegistrovaniKorisnik();
			
			RegistrovaniKorisnik dekanKorisnik = new RegistrovaniKorisnik(dekanKorisnikDTO.getId(), dekanKorisnikDTO.getEmail(), dekanKorisnikDTO.getKorisnickoIme(), dekanKorisnikDTO.getLozinka());
			registrovaniKorisnikService.save(dekanKorisnik);
			
			Nastavnik dekan = new Nastavnik(dekanDTO.getId(), dekanDTO.getIme(), dekanDTO.getBiografija(), dekanDTO.getJmbg());
			dekan.setRegistrovaniKorisnik(dekanKorisnik);
			
			nastavnikService.save(dekan);
			fakultet.setDekan(dekan);
			fakultet.setUniverzitet(univerzitet);
			fakultet.setAdresa(adresa);
			
			
			//fakultet = fakultetService.save(fakultet);
//			adresa.setFakultet(fakultet);
//			adresaService.save(adresa);

			
			
			//			Set <Fakultet> f = new HashSet<Fakultet>();
//			f.add(fakultet);
//			univerzitet.setFakulteti(f);
			
			NastavnikDTO rukovodilacDTO = d.getGodinaStudija().getStudijskiProgram().getRukovodilac();
			RegistrovaniKorisnikDTO rukovodilacKorisnikDTO = rukovodilacDTO.getRegistrovaniKorisnik();
			
			RegistrovaniKorisnik rukovodilacKorisnik = new RegistrovaniKorisnik(rukovodilacKorisnikDTO.getId(), rukovodilacKorisnikDTO.getEmail(), rukovodilacKorisnikDTO.getKorisnickoIme(), rukovodilacKorisnikDTO.getLozinka());
			registrovaniKorisnikService.save(rukovodilacKorisnik);
			
			Nastavnik rukovodilac = new Nastavnik(rukovodilacDTO.getId(), rukovodilacDTO.getIme(), rukovodilacDTO.getBiografija(), rukovodilacDTO.getJmbg());
			rukovodilac.setRegistrovaniKorisnik(rukovodilacKorisnik);
			sp.setRukovodilac(rukovodilac);
			sp.setFakultet(fakultet);
			godinaStudija.setStudijskiProgram(sp);
			predmet.setGodinaStudija(godinaStudija);
			
			drzavaService.save(drzava);
			mestoService.save(mesto);
			adresaService.save(adresa);
			fakultetService.save(fakultet);
			nastavnikService.save(rukovodilac);
			spService.save(sp);
			godinaStudijaService.save(godinaStudija);
			predmetService.save(predmet);
		}
		return new ResponseEntity<Iterable<PredmetDTO>>(this.parser.importXml(url), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{predmetId}", method = RequestMethod.GET)
	public ResponseEntity<PredmetDTO> getPredmet(@PathVariable("predmetId") Long predmetId) {
		
		Optional<Predmet> predmet = predmetService.findOne(predmetId);
		
		
		if(predmet.isPresent()) {
			
			PredmetDTO predmetDTO = new PredmetDTO(predmet.get().getId(), predmet.get().getNaziv(), predmet.get().getEspb(), predmet.get().isObavezan(), predmet.get().getBrojPredavanja(), predmet.get().getBrojVezbi(), predmet.get().getDrugiObliciNastave(), predmet.get().getIstrazivackiRad(), predmet.get().getOstaliCasovi(), predmet.get().getBrojSemestara());
			return new ResponseEntity<PredmetDTO>(predmetDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<PredmetDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Predmet> createPredmet(@RequestBody Predmet predmet){
		
		try {
			
			predmetService.save(predmet);
			
			return new ResponseEntity<Predmet>(predmet, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Predmet>(HttpStatus.BAD_REQUEST);
		
	}
	
	@RequestMapping(path = "/{predmetId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Predmet> updatePredmet(@PathVariable("predmetId") Long predmetId, @RequestBody Predmet izmenjeniPredmet) {
		
		Predmet predmet = predmetService.findOne(predmetId).orElse(null);
		
		if( predmet != null) {
			izmenjeniPredmet.setId(predmetId);
			
			predmetService.save(izmenjeniPredmet);
			return new ResponseEntity<Predmet>(izmenjeniPredmet, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Predmet>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{predmetId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Predmet> deletePredmet(@PathVariable("predmetId") Long predmetId) {
	
		if(predmetService.findOne(predmetId).isPresent()) {
			predmetService.delete(predmetId);
			return new ResponseEntity<Predmet>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Predmet>(HttpStatus.NOT_FOUND);
	}
	
}
