package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mysql.cj.x.protobuf.MysqlxDatatypes.Array;
import java.util.List;

import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.NaucnaOblastDTO;
import rs.ac.singidunum.isa.app.dto.TipZvanjaDTO;
import rs.ac.singidunum.isa.app.dto.ZvanjeDTO;
import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.NaucnaOblast;
import rs.ac.singidunum.isa.app.model.TipZvanja;
import rs.ac.singidunum.isa.app.model.Zvanje;
import rs.ac.singidunum.isa.app.service.ZvanjeService;

@Controller
@RequestMapping(path = "/api/zvanja")
@CrossOrigin
public class ZvanjeController {
	
	@Autowired
	private ZvanjeService zvanjeService;
	
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<ZvanjeDTO>> getAllZvanja() {
		
		
		ArrayList<ZvanjeDTO> zvanja = new ArrayList<ZvanjeDTO>();
		
		for(Zvanje zvanje : zvanjeService.findAll()) {
		
			Nastavnik nastavnik = zvanje.getNastavnik();
			NaucnaOblast naucnaOblast = zvanje.getNaucnaOblast();
			TipZvanja tipZvanja = zvanje.getTipZvanja();
			
			NastavnikDTO nastavnikDTO = new NastavnikDTO(nastavnik.getId(), nastavnik.getBiografija(), nastavnik.getIme(), nastavnik.getJmbg());
			NaucnaOblastDTO oblastDTO = new NaucnaOblastDTO(naucnaOblast.getId(), naucnaOblast.getNaziv());
			TipZvanjaDTO tipZvanjaDTO = new TipZvanjaDTO(tipZvanja.getId(), tipZvanja.getNaziv());
			
			ZvanjeDTO zvanjeDTO = new ZvanjeDTO(zvanje.getId(), zvanje.getDatumIzbora(), zvanje.getDatumPrestanka(), nastavnikDTO, tipZvanjaDTO, oblastDTO);
			
			zvanja.add(zvanjeDTO);
		
		

	}
		
		return new ResponseEntity<Iterable<ZvanjeDTO>>(zvanja, HttpStatus.OK);
}
	@RequestMapping(path = "/{zvanjeId}", method = RequestMethod.GET)
	public ResponseEntity<ZvanjeDTO> getZvanje(@PathVariable("zvanjeId") Long zvanjeId) {
		
		Optional<Zvanje> zvanje = zvanjeService.findOne(zvanjeId);
		
		if(zvanje.isPresent()) {
			ZvanjeDTO zvanjeDTO = new ZvanjeDTO(zvanje.get().getId(), zvanje.get().getDatumIzbora(), zvanje.get().getDatumPrestanka());
			return new ResponseEntity<ZvanjeDTO>(zvanjeDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<ZvanjeDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Zvanje> createZvanje(@RequestBody Zvanje zvanje){
		
		try {
			
			zvanjeService.save(zvanje);
			
			return new ResponseEntity<Zvanje>(zvanje, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Zvanje>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{zvanjeId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Zvanje> updateNastavnik(@PathVariable("studentlId") Long zvanjeId, @RequestBody Zvanje izmijeniZvanje) {
		
		Zvanje zvanje = zvanjeService.findOne(zvanjeId).orElse(null);
		
		if( zvanje != null) {
			izmijeniZvanje.setId(zvanjeId);
			
			zvanjeService.save(izmijeniZvanje);
			return new ResponseEntity<Zvanje>(izmijeniZvanje, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Zvanje>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{zvanjeId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Zvanje> deleteStudent(@PathVariable("zvanjeId") Long zvanjeId) {
	
		if(zvanjeService.findOne(zvanjeId).isPresent()) {
			zvanjeService.delete(zvanjeId);
			return new ResponseEntity<Zvanje>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Zvanje>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "/poZvanju", method = RequestMethod.GET)
	public ResponseEntity<Iterable<NastavnikDTO>> getAllNastavniciPoZvanju(@RequestParam(name = "tipZvanja", required = true) String tipZvanja) {
		
		List<Nastavnik> nastavnici = zvanjeService.findByNaziv(tipZvanja);
		
		ArrayList<NastavnikDTO> nastavniciDTO = new ArrayList<NastavnikDTO>();
		for(Nastavnik nastavnik : nastavnici) {
			nastavniciDTO.add(new NastavnikDTO(nastavnik.getId(), nastavnik.getBiografija(), nastavnik.getIme(), nastavnik.getJmbg()));
		}
		
		return new ResponseEntity<Iterable<NastavnikDTO>>(nastavniciDTO, HttpStatus.OK);
	}
}
