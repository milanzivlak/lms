package rs.ac.singidunum.isa.app.nastavni_materijal_management.controller;

import org.springframework.stereotype.Controller;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.FajlDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Fajl;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.FajlService;

@Controller
@RequestMapping(path = "/api/nastavni-materijal/fajl")
public class FajlController {
	@Autowired
	private FajlService fajlService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Page<FajlDTO>> getAllFajlovi(Pageable pageable) {
		return new ResponseEntity<Page<FajlDTO>>(fajlService.findAll(pageable).map(a -> new FajlDTO(a)), HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{fajlId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<FajlDTO> getFajl(@PathVariable("fajlId") Long fajlId) {
		
		Optional<Fajl> fajl = fajlService.findOne(fajlId);
		
		if(fajl.isPresent()) {
			FajlDTO FajlDTO = new FajlDTO(fajl.get());
			return new ResponseEntity<FajlDTO>(FajlDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<FajlDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Fajl> createFajl(@RequestBody Fajl fajl){
		
		try {
			
			fajlService.save(fajl);
			
			return new ResponseEntity<Fajl>(fajl, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Fajl>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{fajlId}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Fajl> updateFajl(@PathVariable("fajlId") Long fajlId, @RequestBody Fajl izmenjenFajl) {
		
		Fajl adresa = fajlService.findOne(fajlId).orElse(null);
		
		if( adresa != null) {
			izmenjenFajl.setId(fajlId);
			
			fajlService.save(izmenjenFajl);
			return new ResponseEntity<Fajl>(izmenjenFajl, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Fajl>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{fajlId}", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Fajl> deleteFajl(@PathVariable("fajlId") Long fajlId) {
	
		if(fajlService.findOne(fajlId).isPresent()) {
			fajlService.delete(fajlId);
			return new ResponseEntity<Fajl>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Fajl>(HttpStatus.NOT_FOUND);
	}


}
