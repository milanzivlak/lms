package rs.ac.singidunum.isa.app.nastavni_materijal_management.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Fajl {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String opis;
	@Lob
	private String url;
	
	@ManyToMany
	@JsonIgnore
	private Set<InstrumentEvaluacije> instrumentiEvaluacije = new HashSet<InstrumentEvaluacije>();
	
	@ManyToMany
	@JsonIgnore
	private Set<NastavniMaterijal> nastavniMaterijali = new HashSet<NastavniMaterijal>();
	
	
	
	public Set<NastavniMaterijal> getNastavniMaterijali() {
		return nastavniMaterijali;
	}
	public void setNastavniMaterijali(Set<NastavniMaterijal> nastavniMaterijali) {
		this.nastavniMaterijali = nastavniMaterijali;
	}
	public Set<InstrumentEvaluacije> getInstrumentiEvaluacije() {
		return instrumentiEvaluacije;
	}
	public void setInstrumentiEvaluacije(Set<InstrumentEvaluacije> instrumentiEvaluacije) {
		this.instrumentiEvaluacije = instrumentiEvaluacije;
	}
	public Fajl() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Fajl(Long id, String opis, String url) {
		super();
		this.id = id;
		this.opis = opis;
		this.url = url;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
