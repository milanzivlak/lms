package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import rs.ac.singidunum.isa.app.model.Nastavnik;




public class NastavnikDTO {
	

	private Long id;
	
	private String ime;
	
	private String biografija;
	private String jmbg;
	
	private List<ZvanjeDTO> zvanja = new ArrayList<ZvanjeDTO>();
	
	private FakultetDTO fakultet;
	private UniverzitetDTO univerzitet;
	private StudijskiProgramDTO studijskiProgram;
	private NastavnikNaRealizacijiDTO realizacija;
	private RegistrovaniKorisnikDTO registrovaniKorisnik;
	
	public NastavnikDTO() {
		super();
	}
	
	public NastavnikDTO(Long id, String ime, String biografija, String jmbg, ArrayList<ZvanjeDTO> zvanja, FakultetDTO fakultet, UniverzitetDTO univerzitet, StudijskiProgramDTO studijskiProgram, NastavnikNaRealizacijiDTO realizacija, RegistrovaniKorisnikDTO registrovaniKorisnik) {
		super();
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
		this.zvanja = zvanja;
		this.fakultet = fakultet;
		this.univerzitet = univerzitet;
		this.studijskiProgram = studijskiProgram;
		this.realizacija = realizacija;
		this.registrovaniKorisnik = registrovaniKorisnik;
	}
	
	public NastavnikDTO(Long id, String ime, String biografija, String jmbg) {
		this(id, ime, biografija, jmbg, null, null, null, null, null, null);
		
	}
	
	public RegistrovaniKorisnikDTO getRegistrovaniKorisnik() {
		return registrovaniKorisnik;
	}

	public void setRegistrovaniKorisnik(RegistrovaniKorisnikDTO registrovaniKorisnik) {
		this.registrovaniKorisnik = registrovaniKorisnik;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getBiografija() {
		return biografija;
	}
	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}
	public String getJmbg() {
		return jmbg;
	}
	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public List<ZvanjeDTO> getZvanja() {
		return zvanja;
	}

	public void setZvanja(List<ZvanjeDTO> zvanja) {
		this.zvanja = zvanja;
	}

	public FakultetDTO getFakultet() {
		return fakultet;
	}

	public void setFakultet(FakultetDTO fakultet) {
		this.fakultet = fakultet;
	}

	public UniverzitetDTO getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(UniverzitetDTO univerzitet) {
		this.univerzitet = univerzitet;
	}

	public StudijskiProgramDTO getStudijskiProgram() {
		return studijskiProgram;
	}

	public void setStudijskiProgram(StudijskiProgramDTO studijskiProgram) {
		this.studijskiProgram = studijskiProgram;
	}

	public NastavnikNaRealizacijiDTO getRealizacija() {
		return realizacija;
	}

	public void setRealizacija(NastavnikNaRealizacijiDTO realizacija) {
		this.realizacija = realizacija;
	}

	public static NastavnikDTO fromNastavnik(Nastavnik nastavnik){
		NastavnikDTO nastavnikDto = new NastavnikDTO();
		nastavnikDto.setId(nastavnik.getId());
		if(nastavnik.getFakultet() != null) {
			FakultetDTO faks = new FakultetDTO(nastavnik.getFakultet().getId(), nastavnik.getFakultet().getNaziv());
			nastavnikDto.setFakultet(faks);
		}
		
		//UniverzitetDTO uni = new UniverzitetDTO(nastavnik.getUniverzitet().getId(), nastavnik.getUniverzitet().getUlica(), nastavnik.getUniverzitet().getBroj());
		//nastavnikDto.setUniverzitet(uni);
		
		if(nastavnik.getStudijskiProgram() != null) {
			StudijskiProgramDTO sp = new StudijskiProgramDTO(nastavnik.getStudijskiProgram().getId(), nastavnik.getStudijskiProgram().getNaziv());
			nastavnikDto.setStudijskiProgram(sp);
		}
		
		if(nastavnik.getRegistrovaniKorisnik() != null) {
			RegistrovaniKorisnikDTO rk = new RegistrovaniKorisnikDTO();
			rk.setId(nastavnik.getRegistrovaniKorisnik().getId());
			rk.setEmail(nastavnik.getRegistrovaniKorisnik().getEmail());
			rk.setKorisnickoIme(nastavnik.getRegistrovaniKorisnik().getKorisnickoIme());
			rk.setLozinka(nastavnik.getRegistrovaniKorisnik().getLozinka());
			nastavnikDto.setRegistrovaniKorisnik(rk);
		}
		
		//NastavnikNaRealizacijiDTO nnr = new NastavnikNaRealizacijiDTO(nastavnik.getRealizacija().getId(), nastavnik.getRealizacija().getBrojCasova());
	//	nastavnikDto.setRealizacija(nnr);
		nastavnikDto.setIme(nastavnik.getIme());
		nastavnikDto.setBiografija(nastavnik.getBiografija());
		nastavnikDto.setJmbg(nastavnik.getJmbg());
		
		nastavnikDto.setZvanja(nastavnik.getZvanja()
				.stream()
				.map(z -> ZvanjeDTO.fromZvanje(z))
				.collect(Collectors.toList()));
	
		return nastavnikDto;
	}
	
	
}
