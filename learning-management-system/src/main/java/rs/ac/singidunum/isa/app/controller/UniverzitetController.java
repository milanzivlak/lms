package rs.ac.singidunum.isa.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Fakultet;
import rs.ac.singidunum.isa.app.model.Mesto;
import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.Predmet;
import rs.ac.singidunum.isa.app.model.Univerzitet;
import rs.ac.singidunum.isa.app.parsers.UniverzitetParser;
import rs.ac.singidunum.isa.app.service.AdresaService;
import rs.ac.singidunum.isa.app.service.DrzavaService;
import rs.ac.singidunum.isa.app.service.FakultetService;
import rs.ac.singidunum.isa.app.service.MestoService;
import rs.ac.singidunum.isa.app.service.NastavnikService;
import rs.ac.singidunum.isa.app.service.UniverzitetService;

@Controller
@RequestMapping(path = "/api/univerziteti")
@CrossOrigin
public class UniverzitetController {
	
	@Autowired
	private UniverzitetService univerzitetService;
	
	@Autowired
	private NastavnikService nastavnikService;
	
	@Autowired
	private AdresaService adresaService;
	
	@Autowired
	private MestoService mestoService;
	
	@Autowired
	private DrzavaService drzavaService;
	
	@Autowired
	private FakultetService fakultetService;
	
	UniverzitetParser parser = new UniverzitetParser();
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<UniverzitetDTO>> getAllUniverziteti(){
		
		ArrayList<UniverzitetDTO> univerziteti = new ArrayList<UniverzitetDTO>();
		
		for(Univerzitet uni : univerzitetService.findAll()) {
			ArrayList<FakultetDTO> fakulteti = new ArrayList<FakultetDTO>();
			
			for(Fakultet faks : uni.getFakulteti()) {
				fakulteti.add(new FakultetDTO(faks.getId(), faks.getNaziv()));
			}
			AdresaDTO adresa = new AdresaDTO(uni.getAdresa().getId(), uni.getAdresa().getUlica(), uni.getAdresa().getBroj());
			
			NastavnikDTO nastavnik = new NastavnikDTO();
			if(uni.getRektor() != null) {
				nastavnik.setId(uni.getRektor().getId());
				nastavnik.setIme(uni.getRektor().getIme());
				nastavnik.setBiografija(uni.getRektor().getBiografija());
				nastavnik.setJmbg(uni.getRektor().getJmbg());
			}
			else {
				nastavnik = null;
			}
			//NastavnikDTO nastavnik = new NastavnikDTO(uni.getRektor().getId(), uni.getRektor().getIme(), uni.getRektor().getBiografija(), uni.getRektor().getJmbg());
			
			univerziteti.add(new UniverzitetDTO(uni.getId(), uni.getUlica(), uni.getBroj(), adresa, nastavnik, fakulteti));
		}
		
		return new ResponseEntity<Iterable<UniverzitetDTO>>(univerziteti, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/import", method = RequestMethod.GET)
	public ResponseEntity<Iterable<UniverzitetDTO>> univerzitetImport(@Param(value = "url") String url) throws UnsupportedEncodingException {
		
		ArrayList<UniverzitetDTO> importovane = this.parser.importXml(url);
		
		for(UniverzitetDTO d : importovane) {
			Univerzitet uni = new Univerzitet(d.getId(), d.getUlica(), d.getBroj());
			
			Adresa adresa = new Adresa(d.getAdresa().getId(), d.getAdresa().getUlica(), d.getAdresa().getBroj());
			
			Mesto mesto = new Mesto(d.getAdresa().getMesto().getId(), d.getAdresa().getMesto().getNaziv());
			
			Drzava drzava = new Drzava(d.getAdresa().getMesto().getDrzava().getId(), d.getAdresa().getMesto().getDrzava().getNaziv());
			
			mesto.setDrzava(drzava);
			
			adresa.setMesto(mesto);
			
			
			Nastavnik rektor = new Nastavnik(d.getRektor().getId(), d.getRektor().getIme(), d.getRektor().getBiografija(), d.getRektor().getJmbg());
			
			//rektor ima i zvanja i bilo bi lepo kad bi proslo bez njih
			
			Fakultet fakultet = new Fakultet();
			
			Set<Fakultet> faks = new HashSet<Fakultet>();
			
			for(FakultetDTO f : d.getFakulteti()) {
				Fakultet fakultet1 = new Fakultet(f.getId(), f.getNaziv());
				//fakultet1.setUniverzitet(uni);
				faks.add(fakultet1);
				fakultetService.save(fakultet1);
				fakultet.setId(f.getId());
				fakultet.setNaziv(f.getNaziv());
			}
			uni.setAdresa(adresa);
			uni.setRektor(rektor);
			uni.setFakulteti(faks);
			
			drzavaService.save(drzava);
			mestoService.save(mesto);
			adresa.setUniverzitet(uni);
			adresa.setFakultet(fakultet);
			adresaService.save(adresa);
			nastavnikService.save(rektor);
			univerzitetService.save(uni);

			
		}
		return new ResponseEntity<Iterable<UniverzitetDTO>>(this.parser.importXml(url), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/export", method = RequestMethod.GET)
	public void predmetiExport() {
		System.out.println("Exporting...");
		ArrayList<UniverzitetDTO> univerziteti = new ArrayList<UniverzitetDTO>();
		
		for(Univerzitet uni : univerzitetService.findAll()) {
			UniverzitetDTO uniDTO = UniverzitetDTO.fromUniverzitet(uni);
			AdresaDTO adresaDTO = new AdresaDTO(uni.getAdresa().getId(), uni.getAdresa().getUlica(), uni.getAdresa().getBroj());
			NastavnikDTO rektorDTO = new NastavnikDTO(uni.getRektor().getId(), uni.getRektor().getIme(), uni.getRektor().getBiografija(), uni.getRektor().getJmbg());
			
			Mesto mesto = uni.getAdresa().getMesto();
			MestoDTO mestoDTO = new MestoDTO(mesto.getId(), mesto.getNaziv());
			
			Drzava drzava = uni.getAdresa().getMesto().getDrzava();
			DrzavaDTO drzavaDTO = new DrzavaDTO(drzava.getId(), drzava.getNaziv());
			
			mestoDTO.setDrzava(drzavaDTO);
			
			adresaDTO.setMesto(mestoDTO);
			
			ArrayList<FakultetDTO> faks = new ArrayList<FakultetDTO>();
			
			for(Fakultet f : uni.getFakulteti()) {
				FakultetDTO fDTO = new FakultetDTO(f.getId(), f.getNaziv());
				faks.add(fDTO);
			}
			
			uniDTO.setAdresa(adresaDTO);
			uniDTO.setRektor(rektorDTO);
			uniDTO.setFakulteti(faks);
			
			univerziteti.add(uniDTO);
			
		}
		
		this.parser.spisakUniverziteta = univerziteti;
		try {
			this.parser.export();
		} catch (IOException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(path = "/{univerzitetId}", method = RequestMethod.GET)
	public ResponseEntity<UniverzitetDTO> getUniverzitet(@PathVariable("univerzitetId") Long univerzitetId) {
		
		Optional<Univerzitet> univerzitet = univerzitetService.findOne(univerzitetId);
		
		
		if(univerzitet.isPresent()) {
			
			
			UniverzitetDTO univerzitetDTO = new UniverzitetDTO(univerzitet.get().getId(), univerzitet.get().getUlica(), univerzitet.get().getBroj());
			return new ResponseEntity<UniverzitetDTO>(univerzitetDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<UniverzitetDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Univerzitet> createUniverzitet(@RequestBody Univerzitet univerzitet){
		
		try {
			Adresa adresa = adresaService.findOne(univerzitet.getAdresa().getId()).get();
			adresa.setUniverzitet(univerzitet);
			
			univerzitet.setAdresa(adresa);
			Nastavnik nastavnik = nastavnikService.findOne(univerzitet.getRektor().getId()).get();
			nastavnik.setUniverzitet(univerzitet);
			univerzitet.setRektor(nastavnik);
			adresaService.save(adresa);
			
			univerzitetService.save(univerzitet);
			
			return new ResponseEntity<Univerzitet>(univerzitet, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Univerzitet>(HttpStatus.BAD_REQUEST);
		
	}
	
	@RequestMapping(path = "/{univerzitetId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Univerzitet> updateUniverzitet(@PathVariable("univerzitetId") Long univerzitetId, @RequestBody Univerzitet izmenjeniUniverzitet) {
		
		Univerzitet univerzitet = univerzitetService.findOne(univerzitetId).orElse(null);
		
		if( univerzitet != null) {
			izmenjeniUniverzitet.setId(univerzitetId);
			
			univerzitetService.save(izmenjeniUniverzitet);
			return new ResponseEntity<Univerzitet>(izmenjeniUniverzitet, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Univerzitet>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{univerzitetId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Univerzitet> deleteUniverzitet(@PathVariable("univerzitetId") Long univerzitetId) {
	
		if(univerzitetService.findOne(univerzitetId).isPresent()) {
			univerzitetService.delete(univerzitetId);
			return new ResponseEntity<Univerzitet>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Univerzitet>(HttpStatus.NOT_FOUND);
	}
	
}
