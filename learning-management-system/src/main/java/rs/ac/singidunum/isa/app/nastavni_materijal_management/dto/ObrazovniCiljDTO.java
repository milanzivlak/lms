package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.dto.IshodDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.ObrazovniCilj;

public class ObrazovniCiljDTO {
	
	private Long id;
	private String opis;
	
	private ArrayList<IshodDTO> ishodi = new ArrayList<IshodDTO>();

	public ObrazovniCiljDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ObrazovniCiljDTO(Long id, String opis) {
		this(id, opis, null);
	}

	public ObrazovniCiljDTO(Long id, String opis, ArrayList<IshodDTO> ishodi) {
		super();
		this.id = id;
		this.opis = opis;
		this.ishodi = ishodi;
	}

	public ObrazovniCiljDTO(ObrazovniCilj a) {
		this(a.getId(), a.getOpis());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public ArrayList<IshodDTO> getIshodi() {
		return ishodi;
	}

	public void setIshodi(ArrayList<IshodDTO> ishodi) {
		this.ishodi = ishodi;
	}
	
}
