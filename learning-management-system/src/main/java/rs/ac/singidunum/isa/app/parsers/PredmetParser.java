package rs.ac.singidunum.isa.app.parsers;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.LongAdder;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;

import org.w3c.dom.Document;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.RegistrovaniKorisnikDTO;
import rs.ac.singidunum.isa.app.dto.StudentDTO;
import rs.ac.singidunum.isa.app.dto.StudentNaGodiniDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;

import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class PredmetParser {
	//private static final String FILENAME = "C:\\Users\\Korisnik\\Downloads\\drzave.xml";
	private static final String SCHEMANAME = "predmet.xsd";
	public ArrayList<PredmetDTO> spisakPredmeta;
	public ArrayList<PredmetDTO> importovaniPredmeti;
	
	public PredmetParser(ArrayList<PredmetDTO> spisakPredmeta) {
		this.spisakPredmeta = spisakPredmeta;
	}
	
	public PredmetParser() {
		
	}
	public void export() throws IOException, ParserConfigurationException, TransformerException {
		//root
		
		DocumentBuilderFactory dbFactory =
		         DocumentBuilderFactory.newInstance();
		         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		         Document doc = (Document) dBuilder.newDocument();
		
        Element predmeti = doc.createElement("predmeti");
		doc.appendChild(predmeti);
		for(PredmetDTO p : this.spisakPredmeta) {
			Element predmet = doc.createElement("predmet");
			
			Attr attribute = doc.createAttribute("id");
			attribute.setValue(p.getId().toString());
			predmet.setAttributeNode(attribute);
			
			//Naziv
			Element naziv = doc.createElement("naziv");
			naziv.appendChild(doc.createTextNode(p.getNaziv()));
			predmet.appendChild(naziv);
			
			//Espb
			Element espb = doc.createElement("espb");
			espb.appendChild(doc.createTextNode(String.valueOf(p.getEspb())));
			predmet.appendChild(espb);
			
			//Obavezan
			Element obavezan = doc.createElement("obavezan");
			obavezan.appendChild(doc.createTextNode(String.valueOf(p.isObavezan())));
			predmet.appendChild(obavezan);
			
			//Broj predavanja
			Element brojPredavanja = doc.createElement("brojPredavanja");
			brojPredavanja.appendChild(doc.createTextNode(String.valueOf(p.getBrojPredavanja())));
			predmet.appendChild(brojPredavanja);
			
			//Broj vezbi
			Element brojVezbi = doc.createElement("brojVezbi");
			brojVezbi.appendChild(doc.createTextNode(String.valueOf(p.getBrojVezbi())));
			predmet.appendChild(brojVezbi);
			
			//Drugi oblici nastave
			Element drugiObliciNastave = doc.createElement("drugiObliciNastave");
			drugiObliciNastave.appendChild(doc.createTextNode(String.valueOf(p.getDrugiObliciNastave())));
			predmet.appendChild(drugiObliciNastave);
			
			//Istrazivacki rad
			Element istrazivackiRad = doc.createElement("istrazivackiRad");
			istrazivackiRad.appendChild(doc.createTextNode(String.valueOf(p.getIstrazivackiRad())));
			predmet.appendChild(istrazivackiRad);
			
			//Ostali casovi
			Element ostaliCasovi = doc.createElement("ostaliCasovi");
			ostaliCasovi.appendChild(doc.createTextNode(String.valueOf(p.getOstaliCasovi())));
			predmet.appendChild(ostaliCasovi);
			
			//Broj semestara
			Element brojSemestara = doc.createElement("brojSemestara");
			brojSemestara.appendChild(doc.createTextNode(String.valueOf(p.getBrojSemestara())));
			predmet.appendChild(brojSemestara);
			
			
			//-----------------------------------------------------------------
			
			//Godina studija
			Element godinaStudija = doc.createElement("godinaStudija");
			GodinaStudijaDTO godinaStudijaDTO = p.getGodinaStudija();
			Attr godinaStudijaAttr = doc.createAttribute("id");
			godinaStudijaAttr.setValue(godinaStudijaDTO.getId().toString());
			godinaStudija.setAttributeNode(godinaStudijaAttr);
			
			//Godina studija-godina
			Element godinaStudijaGodina = doc.createElement("godina");
			godinaStudijaGodina.appendChild(doc.createTextNode(String.valueOf(godinaStudijaDTO.getGodina())));
			godinaStudija.appendChild(godinaStudijaGodina);
			

			//Godina studija-studijski program
			Element studijskiProgram = doc.createElement("studijskiProgram");
			StudijskiProgramDTO spDTO = godinaStudijaDTO.getStudijskiProgram();
			
			Attr spID = doc.createAttribute("id");
			spID.setValue(spDTO.getId().toString());
			studijskiProgram.setAttributeNode(spID);
			
			//SP naziv
			Element spNaziv = doc.createElement("naziv");
			spNaziv.appendChild(doc.createTextNode(spDTO.getNaziv()));
			studijskiProgram.appendChild(spNaziv);
			
			//Fakultet
			Element fakultet = doc.createElement("fakultet");
			FakultetDTO fakultetDTO = spDTO.getFakultet();
			
			Attr fakultetID = doc.createAttribute("id");
			fakultetID.setValue(fakultetDTO.getId().toString());
			fakultet.setAttributeNode(fakultetID);
			
			//Fakultet naziv
			Element fakultetNaziv = doc.createElement("naziv");
			fakultetNaziv.appendChild(doc.createTextNode(fakultetDTO.getNaziv()));
			fakultet.appendChild(fakultetNaziv);
			
			//Fakultet adresa
			Element fakultetAdresa = doc.createElement("adresa");
			AdresaDTO adresaDTO = fakultetDTO.getAdresa();
			
			Attr adresaID = doc.createAttribute("id");
			adresaID.setValue(adresaDTO.getId().toString());
			fakultetAdresa.setAttributeNode(adresaID);
			//Adresa ulica
			Element adresaUlica = doc.createElement("ulica");
			adresaUlica.appendChild(doc.createTextNode(adresaDTO.getUlica()));
			fakultetAdresa.appendChild(adresaUlica);
			
			//Adresa broj
			Element adresaBroj = doc.createElement("broj");
			adresaBroj.appendChild(doc.createTextNode(adresaDTO.getBroj()));
			fakultetAdresa.appendChild(adresaBroj);
			
			
			//Adresa mesto
			Element adresaMesto = doc.createElement("mesto");
			MestoDTO mestoDTO = adresaDTO.getMesto();
			
			Attr mestoID = doc.createAttribute("id");
			mestoID.setValue(mestoDTO.getId().toString());
			adresaMesto.setAttributeNode(mestoID);
			
			Element mestoNaziv = doc.createElement("naziv");
			mestoNaziv.appendChild(doc.createTextNode(mestoDTO.getNaziv()));
			adresaMesto.appendChild(mestoNaziv);
			
			//Mesto drzava
			Element drzava = doc.createElement("drzava");
			DrzavaDTO drzavaDTO = mestoDTO.getDrzava();
			
			Attr drzavaID = doc.createAttribute("id");
			drzavaID.setValue(drzavaDTO.getId().toString());
			drzava.setAttributeNode(drzavaID);
			
			Element drzavaNaziv = doc.createElement("naziv");
			drzavaNaziv.appendChild(doc.createTextNode(drzavaDTO.getNaziv()));
			drzava.appendChild(drzavaNaziv);
			
			adresaMesto.appendChild(drzava);
			fakultetAdresa.appendChild(adresaMesto);
			fakultet.appendChild(fakultetAdresa);
			
			
			
			//Univerzitet
			Element univerzitet = doc.createElement("univerzitet");
			UniverzitetDTO univerzitetDTO = fakultetDTO.getUniverzitet();
			
			Attr univerzitetID = doc.createAttribute("id");
			univerzitetID.setValue(univerzitetDTO.getId().toString());
			univerzitet.setAttributeNode(univerzitetID);
			
			//Univerzitet ulica
			Element ulica = doc.createElement("ulica");
			ulica.appendChild(doc.createTextNode(univerzitetDTO.getUlica()));
			univerzitet.appendChild(ulica);
			
			//Univerzitet broj
			Element broj = doc.createElement("broj");
			broj.appendChild(doc.createTextNode(univerzitetDTO.getBroj()));
			univerzitet.appendChild(broj);
			
			//Univerzitet rektor
			Element univerzitetRektor = doc.createElement("rektor");
			Attr rektorID = doc.createAttribute("id");
			
			NastavnikDTO rektor = univerzitetDTO.getRektor();
			rektorID.setValue(rektor.getId().toString());
			univerzitetRektor.setAttributeNode(rektorID);
			
			//Rektor ime
			Element rektorIme = doc.createElement("ime");
			rektorIme.appendChild(doc.createTextNode(rektor.getIme()));
			univerzitetRektor.appendChild(rektorIme);
			
			//Rektor biografija
			Element rektorBiografija = doc.createElement("biografija");
			rektorBiografija.appendChild(doc.createTextNode(rektor.getBiografija()));
			univerzitetRektor.appendChild(rektorBiografija);
			
			//Rektor jmbg
			Element rektorJmbg = doc.createElement("jmbg");
			rektorJmbg.appendChild(doc.createTextNode(rektor.getJmbg()));
			univerzitetRektor.appendChild(rektorJmbg);
			
			
			
			//Rektor korisnik
			Element rektorKorisnik = doc.createElement("registrovaniKorisnik");
			Attr rkID = doc.createAttribute("id");
			
			RegistrovaniKorisnikDTO rkDTO = rektor.getRegistrovaniKorisnik();
			rkID.setValue(rkDTO.getId().toString());
			rektorKorisnik.setAttributeNode(rkID);
			
			//Rektor korisnik email
			Element rkEmail = doc.createElement("email");
			rkEmail.appendChild(doc.createTextNode(rkDTO.getEmail()));
			rektorKorisnik.appendChild(rkEmail);
			
			//Rektor korisnickoIme
			Element rkKorisnicko = doc.createElement("korisnickoIme");
			rkKorisnicko.appendChild(doc.createTextNode(rkDTO.getKorisnickoIme()));
			rektorKorisnik.appendChild(rkKorisnicko);
			
			//Rektor lozinka
			Element rkLozinka = doc.createElement("lozinka");
			rkLozinka.appendChild(doc.createTextNode(rkDTO.getLozinka()));
			rektorKorisnik.appendChild(rkLozinka);
			univerzitetRektor.appendChild(rektorKorisnik);
			
			
			univerzitet.appendChild(univerzitetRektor);
	
			fakultet.appendChild(univerzitet);
			
			
			//Fakultet dekan
			Element fakultetDekan = doc.createElement("dekan");
			Attr dekanId = doc.createAttribute("id");
			
			NastavnikDTO dekan = fakultetDTO.getDekan();
			dekanId.setValue(dekan.getId().toString());
			fakultetDekan.setAttributeNode(dekanId);
			
			//Dekan ime
			Element dekanIme = doc.createElement("ime");
			dekanIme.appendChild(doc.createTextNode(dekan.getIme()));
			fakultetDekan.appendChild(dekanIme);
			
			//Dekan biografija
			Element dekanBiografija = doc.createElement("biografija");
			dekanBiografija.appendChild(doc.createTextNode(dekan.getBiografija()));
			fakultetDekan.appendChild(dekanBiografija);
			
			//Dekan jmbg
			Element dekanJmbg = doc.createElement("jmbg");
			dekanJmbg.appendChild(doc.createTextNode(dekan.getJmbg()));
			fakultetDekan.appendChild(dekanJmbg);
			
			
			//Dekan korisnik
			Element dekanKorisnik = doc.createElement("registrovaniKorisnik");
			Attr dkID = doc.createAttribute("id");
			
			RegistrovaniKorisnikDTO dkDTO = dekan.getRegistrovaniKorisnik();
			dkID.setValue(dkDTO.getId().toString());
			dekanKorisnik.setAttributeNode(dkID);
			
			//Dekan korisnik email
			Element dkEmail = doc.createElement("email");
			dkEmail.appendChild(doc.createTextNode(dkDTO.getEmail()));
			dekanKorisnik.appendChild(dkEmail);
			
			//Dekan korisnickoIme
			Element dkKorisnicko = doc.createElement("korisnickoIme");
			dkKorisnicko.appendChild(doc.createTextNode(dkDTO.getKorisnickoIme()));
			dekanKorisnik.appendChild(dkKorisnicko);
			
			//Dekan lozinka
			Element dkLozinka = doc.createElement("lozinka");
			dkLozinka.appendChild(doc.createTextNode(dkDTO.getLozinka()));
			dekanKorisnik.appendChild(dkLozinka);
			
			fakultetDekan.appendChild(dekanKorisnik);
			fakultet.appendChild(fakultetDekan);
			
			studijskiProgram.appendChild(fakultet);
			
			//SP rukovodilac
			Element spRukovodilac = doc.createElement("rukovodilac");
			Attr rukovodilacID = doc.createAttribute("id");
			
			NastavnikDTO rukovodilac = spDTO.getRukovodilac();
			rukovodilacID.setValue(rukovodilac.getId().toString());
			spRukovodilac.setAttributeNode(rukovodilacID);
			
			//Rukovodilac ime
			Element rukovodilacIme = doc.createElement("ime");
			rukovodilacIme.appendChild(doc.createTextNode(rukovodilac.getIme()));
			spRukovodilac.appendChild(rukovodilacIme);
			
			//Rukovodilac biografija
			Element rukovodilacBiografija = doc.createElement("biografija");
			rukovodilacBiografija.appendChild(doc.createTextNode(rukovodilac.getBiografija()));
			spRukovodilac.appendChild(rukovodilacBiografija);
			
			//Rukovodilac jmbg
			Element rukovodilacJmbg = doc.createElement("jmbg");
			rukovodilacJmbg.appendChild(doc.createTextNode(rukovodilac.getJmbg()));
			spRukovodilac.appendChild(rukovodilacJmbg);
			
			//Rukovodioc korisnik
			Element rukovodiocKorisnik = doc.createElement("registrovaniKorisnik");
			Attr ruID = doc.createAttribute("id");
			
			RegistrovaniKorisnikDTO ruDTO = rukovodilac.getRegistrovaniKorisnik();
			ruID.setValue(ruDTO.getId().toString());
			rukovodiocKorisnik.setAttributeNode(ruID);
			
			//Rukovodilac korisnik email
			Element ruEmail = doc.createElement("email");
			ruEmail.appendChild(doc.createTextNode(ruDTO.getEmail()));
			rukovodiocKorisnik.appendChild(ruEmail);
			
			//Rukovodilac korisnickoIme
			Element ruKorisnicko = doc.createElement("korisnickoIme");
			ruKorisnicko.appendChild(doc.createTextNode(ruDTO.getKorisnickoIme()));
			rukovodiocKorisnik.appendChild(ruKorisnicko);
			
			//Rukovodilac lozinka
			Element ruLozinka = doc.createElement("lozinka");
			ruLozinka.appendChild(doc.createTextNode(ruDTO.getLozinka()));
			rukovodiocKorisnik.appendChild(ruLozinka);
			
			spRukovodilac.appendChild(rukovodiocKorisnik);
			
			studijskiProgram.appendChild(spRukovodilac);
			
			godinaStudija.appendChild(studijskiProgram);
			
			
			predmet.appendChild(godinaStudija);
			predmeti.appendChild(predmet);
		}
		
		
		
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Schema schema = loadSchema(SCHEMANAME);
		
		if(validateXml(schema, doc)) {
			DOMSource source = new DOMSource((Node) doc);
			String home = System.getProperty("user.home");
			File file = new File(home+"/Downloads/" + "predmeti" + ".xml");
			StreamResult result = new StreamResult(file);
			//StreamResult result =  new StreamResult(new File("C:\\Users\\Korisnik\\Desktop\\xml\\drzave.xml"));
			transformer.transform(source, result);
			StreamResult consoleResult = new StreamResult(System.out);
	        transformer.transform(source, consoleResult);
		}else {
			System.out.println("Neispravan format za export!");
		}
		
	}
	
	public ArrayList<PredmetDTO> importXml(String s) {
		
		this.importovaniPredmeti = new ArrayList<PredmetDTO>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			
			// procces XML securely, avoid attacks like XML External Entities
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			
			DocumentBuilder db = dbf.newDocumentBuilder();

			
			Document doc = convertStringToXMLDocument(s);
			Schema schema = loadSchema(SCHEMANAME);
			//if(validateXml(schema, doc)) {
				doc.getDocumentElement().normalize();
				
				System.out.println("Root element:" + doc.getDocumentElement().getNodeName());
				
				NodeList list = doc.getElementsByTagName("predmet");
				
				for(int temp = 0; temp < list.getLength(); temp++) {
					
					Node node = list.item(temp);
					
					if(node.getNodeType() == Node.ELEMENT_NODE) {
						
						Element predmet = (Element) node;
						
						String predmetID = predmet.getAttribute("id");
						String predmetNaziv = predmet.getElementsByTagName("naziv").item(0).getTextContent();
						String espb = predmet.getElementsByTagName("espb").item(0).getTextContent();
						String obavezan = predmet.getElementsByTagName("obavezan").item(0).getTextContent();
						String brojPredavanja = predmet.getElementsByTagName("brojPredavanja").item(0).getTextContent();
						String brojVezbi = predmet.getElementsByTagName("brojVezbi").item(0).getTextContent();
						String drugiObliciNastave = predmet.getElementsByTagName("drugiObliciNastave").item(0).getTextContent();
						String istrazivackiRad = predmet.getElementsByTagName("istrazivackiRad").item(0).getTextContent();
						String ostaliCasovi = predmet.getElementsByTagName("ostaliCasovi").item(0).getTextContent();
						String brojSemestara = predmet.getElementsByTagName("brojSemestara").item(0).getTextContent();
						
						
						PredmetDTO predmetDTO = new PredmetDTO();
						predmetDTO.setId(Long.parseLong(predmetID));
						predmetDTO.setNaziv(predmetNaziv);
						predmetDTO.setEspb(Integer.parseInt(espb));
						predmetDTO.setObavezan(Boolean.parseBoolean(obavezan));
						predmetDTO.setBrojPredavanja(Integer.parseInt(brojPredavanja));
						predmetDTO.setBrojVezbi(Integer.parseInt(brojVezbi));
						predmetDTO.setDrugiObliciNastave(Integer.parseInt(drugiObliciNastave));
						predmetDTO.setIstrazivackiRad(Integer.parseInt(istrazivackiRad));
						predmetDTO.setOstaliCasovi(Integer.parseInt(ostaliCasovi));
						predmetDTO.setBrojSemestara(Integer.parseInt(brojSemestara));
						
						
						Element godinaStudija = (Element)predmet.getElementsByTagName("godinaStudija").item(0);
						
						//NodeList listMesta = mesta.getChildNodes();
						
						System.out.println("Element: " + node.getNodeName());
						System.out.println("Drzava ID: " + predmetID);
						System.out.println("Naziv drzave: " + predmetNaziv);
						
						GodinaStudijaDTO godinaStudijaDTO = new GodinaStudijaDTO();
						
						String godinaStudijaID = godinaStudija.getAttribute("id");
						String godinaStudijaGodina = godinaStudija.getElementsByTagName("godina").item(0).getTextContent();
						
						
						Element studijskiProgram = (Element)godinaStudija.getElementsByTagName("studijskiProgram").item(0);
						
						String spID = studijskiProgram.getAttribute("id");
						String spNaziv = studijskiProgram.getElementsByTagName("naziv").item(0).getTextContent();
						
						Element fakultet = (Element)studijskiProgram.getElementsByTagName("fakultet").item(0);
						
						String fakultetID = fakultet.getAttribute("id");
						String fakultetNaziv = fakultet.getElementsByTagName("naziv").item(0).getTextContent();
						
						Element adresa = (Element)fakultet.getElementsByTagName("adresa").item(0);
						
						String adresaID = adresa.getAttribute("id");
						String adresaUlica = adresa.getElementsByTagName("ulica").item(0).getTextContent();
						String adresaBroj = adresa.getElementsByTagName("broj").item(0).getTextContent();
						
						Element mesto = (Element)adresa.getElementsByTagName("mesto").item(0);
						
						String mestoID = mesto.getAttribute("id");
						String mestoNaziv = mesto.getElementsByTagName("naziv").item(0).getTextContent();
						
						Element drzava = (Element)mesto.getElementsByTagName("drzava").item(0);
						
						String drzavaID = drzava.getAttribute("id");
						String drzavaNaziv = drzava.getElementsByTagName("naziv").item(0).getTextContent();
						
						DrzavaDTO drzavaDTO = new DrzavaDTO(Long.parseLong(drzavaID), drzavaNaziv);
						
						MestoDTO mestoDTO = new MestoDTO(Long.parseLong(mestoID), mestoNaziv);
						mestoDTO.setDrzava(drzavaDTO);
						
						AdresaDTO adresaDTO = new AdresaDTO(Long.parseLong(adresaID), adresaUlica, adresaBroj);
						adresaDTO.setMesto(mestoDTO);
						
						
						
						
						Element univerzitet = (Element)fakultet.getElementsByTagName("univerzitet").item(0);
						
						String univerzitetID = univerzitet.getAttribute("id");
						String univerzitetUlica = univerzitet.getElementsByTagName("ulica").item(0).getTextContent();
						String univerzitetBroj = univerzitet.getElementsByTagName("broj").item(0).getTextContent();
						
						
						Element rektor = (Element)univerzitet.getElementsByTagName("rektor").item(0);
						
						String rektorID = rektor.getAttribute("id");
						String rektorIme = rektor.getElementsByTagName("ime").item(0).getTextContent();
						String rektorBiografija = rektor.getElementsByTagName("biografija").item(0).getTextContent();
						String rektorJmbg = rektor.getElementsByTagName("jmbg").item(0).getTextContent();
						
						Element rektorKorisnik = (Element)rektor.getElementsByTagName("registrovaniKorisnik").item(0);
						
						String reKorisnikID = rektorKorisnik.getAttribute("id");
						String reKorisnikEmail = rektorKorisnik.getElementsByTagName("email").item(0).getTextContent();
						String reKorisnikKorisnicko = rektorKorisnik.getElementsByTagName("korisnickoIme").item(0).getTextContent();
						String reKorisnikLozinka = rektorKorisnik.getElementsByTagName("lozinka").item(0).getTextContent();
						
						RegistrovaniKorisnikDTO rektorKorisnikDTO = new RegistrovaniKorisnikDTO();
						rektorKorisnikDTO.setId(Long.parseLong(reKorisnikID));
						rektorKorisnikDTO.setEmail(reKorisnikEmail);
						rektorKorisnikDTO.setKorisnickoIme(reKorisnikKorisnicko);
						rektorKorisnikDTO.setLozinka(reKorisnikLozinka);
						NastavnikDTO rektorDTO = new NastavnikDTO(Long.parseLong(rektorID), rektorIme, rektorBiografija, rektorJmbg);
						rektorDTO.setRegistrovaniKorisnik(rektorKorisnikDTO);
						
						
						UniverzitetDTO univerzitetDTO = new UniverzitetDTO(Long.parseLong(univerzitetID), univerzitetUlica, univerzitetBroj);
						univerzitetDTO.setRektor(rektorDTO);
						
						
						FakultetDTO fakultetDTO = new FakultetDTO(Long.parseLong(fakultetID), fakultetNaziv);
						fakultetDTO.setAdresa(adresaDTO);
						fakultetDTO.setUniverzitet(univerzitetDTO);
						
						Element dekan = (Element)fakultet.getElementsByTagName("dekan").item(0);
						
						String dekanID = dekan.getAttribute("id");
						String dekanIme = dekan.getElementsByTagName("ime").item(0).getTextContent();
						String dekanBiografija = dekan.getElementsByTagName("biografija").item(0).getTextContent();
						String dekanJmbg = dekan.getElementsByTagName("jmbg").item(0).getTextContent();
						
						Element dekanKorisnik = (Element)dekan.getElementsByTagName("registrovaniKorisnik").item(0);
						
						String deKorisnikID = dekanKorisnik.getAttribute("id");
						String deKorisnikEmail = dekanKorisnik.getElementsByTagName("email").item(0).getTextContent();
						String deKorisnikKorisnicko = dekanKorisnik.getElementsByTagName("korisnickoIme").item(0).getTextContent();
						String deKorisnikLozinka = dekanKorisnik.getElementsByTagName("lozinka").item(0).getTextContent();
						
						RegistrovaniKorisnikDTO dekanKorisnikDTO = new RegistrovaniKorisnikDTO();
						dekanKorisnikDTO.setId(Long.parseLong(deKorisnikID));
						dekanKorisnikDTO.setEmail(deKorisnikEmail);
						dekanKorisnikDTO.setKorisnickoIme(deKorisnikKorisnicko);
						dekanKorisnikDTO.setLozinka(deKorisnikLozinka);
						
						NastavnikDTO dekanDTO = new NastavnikDTO(Long.parseLong(dekanID), dekanIme, dekanBiografija, dekanJmbg);
						dekanDTO.setRegistrovaniKorisnik(dekanKorisnikDTO);
						fakultetDTO.setDekan(dekanDTO);
						
						
						StudijskiProgramDTO spDTO = new StudijskiProgramDTO(Long.parseLong(spID), spNaziv);
						spDTO.setFakultet(fakultetDTO);
						
						Element rukovodilac = (Element)studijskiProgram.getElementsByTagName("rukovodilac").item(0);
						
						String rukovodilacID = rukovodilac.getAttribute("id");
						String rukovodilacIme = rukovodilac.getElementsByTagName("ime").item(0).getTextContent();
						String rukovodilacBiografija = rukovodilac.getElementsByTagName("biografija").item(0).getTextContent();
						String rukovodilacJmbg = rukovodilac.getElementsByTagName("jmbg").item(0).getTextContent();
						
						Element rukovodilacKorisnik = (Element)rukovodilac.getElementsByTagName("registrovaniKorisnik").item(0);
						
						String ruKorisnikID = rukovodilacKorisnik.getAttribute("id");
						String ruKorisnikEmail = rukovodilacKorisnik.getElementsByTagName("email").item(0).getTextContent();
						String ruKorisniKorisnicko = rukovodilacKorisnik.getElementsByTagName("korisnickoIme").item(0).getTextContent();
						String ruKorisnikLozinka = rukovodilacKorisnik.getElementsByTagName("lozinka").item(0).getTextContent();
						
						RegistrovaniKorisnikDTO rukovodilacKorisnikDTO = new RegistrovaniKorisnikDTO();
						rukovodilacKorisnikDTO.setId(Long.parseLong(ruKorisnikID));
						rukovodilacKorisnikDTO.setEmail(ruKorisnikEmail);
						rukovodilacKorisnikDTO.setKorisnickoIme(ruKorisniKorisnicko);
						rukovodilacKorisnikDTO.setLozinka(ruKorisnikLozinka);
						
						NastavnikDTO rukovodilacDTO = new NastavnikDTO(Long.parseLong(rukovodilacID), rukovodilacIme, rukovodilacBiografija, rukovodilacJmbg);
						rukovodilacDTO.setRegistrovaniKorisnik(rukovodilacKorisnikDTO);
						spDTO.setRukovodilac(rukovodilacDTO);
						
						godinaStudijaDTO.setId(Long.parseLong(godinaStudijaID));
						
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

						Date date = formatter.parse(godinaStudijaGodina);
						godinaStudijaDTO.setGodina(date);
						godinaStudijaDTO.setStudijskiProgram(spDTO);
						predmetDTO.setGodinaStudija(godinaStudijaDTO);
						
						this.importovaniPredmeti.add(predmetDTO);
					}
					
					
				}
				
				return this.importovaniPredmeti;
			//} else {
				//System.out.println("XML dokument nije ispravan!");
				
			//}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	public static Schema loadSchema(String schemaFileName) {
		Schema schema = null;
		try {
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			
			schema = factory.newSchema(new File(schemaFileName));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return schema;
	}
	
	public static Document parseXmlDom(String xmlName) {
		
		Document document = null;
		try {
		   DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		   DocumentBuilder builder = factory.newDocumentBuilder();
		   document = builder.parse(new File(xmlName));
		} catch (Exception e) {
		   System.out.println(e.toString());
		}
		return document;
	}
	
	public static boolean validateXml(Schema schema, Document document) {
		try {
			Validator validator = schema.newValidator();
			
			validator.validate(new DOMSource(document));
			System.out.println("Validacija uspjesna!");
			return true;
			
		} catch (Exception e) {
			System.out.println("Validacije neuspjesna!");
			return false;
		}
		
	}
	
	public static Document convertStringToXMLDocument(String xmlString) 
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
             
            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return null;
    }
}
