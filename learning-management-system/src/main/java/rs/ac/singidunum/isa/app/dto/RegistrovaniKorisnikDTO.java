package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.model.UserPermission;

public class RegistrovaniKorisnikDTO {
	
	
	private Long id;
	
	
	private String korisnickoIme;
	
	private String lozinka;
	
	private String email;
	
	// Nisam siguran da li ova veza izgleda ovako. Da li korisnik ima Administrator u sebi
	//private Administrator administrator;
	
	private ArrayList<UserPermissionDTO> userPermissions = new ArrayList<UserPermissionDTO>();
	
	public ArrayList<UserPermissionDTO> getUserPermissions() {
		return userPermissions;
	}
	public void setUserPermissions(ArrayList<UserPermissionDTO> userPermissions) {
		this.userPermissions = userPermissions;
	}
	
	
	public RegistrovaniKorisnikDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegistrovaniKorisnikDTO(Long id, String email, String korisnickoIme, String lozinka) {
		this(id, email, korisnickoIme, lozinka,  null);
	}
	
	public RegistrovaniKorisnikDTO(Long id, String korisnickoIme, String lozinka, String email, ArrayList<UserPermissionDTO> userPermissions) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.email = email;
		this.userPermissions = userPermissions;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
