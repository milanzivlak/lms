package rs.ac.singidunum.isa.app.utils;

import java.io.File;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class XmlUtil {
	public static Schema loadSchema(String schemaFileName) {
		Schema schema = null;
		try {
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			
			schema = factory.newSchema(new File(schemaFileName));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return schema;
	}
	
	public static Document parseXmlDom(String xmlName) {
		
		Document document = null;
		try {
		   DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		   DocumentBuilder builder = factory.newDocumentBuilder();
		   document = builder.parse(new File(xmlName));
		} catch (Exception e) {
		   System.out.println(e.toString());
		}
		return document;
	}
	
	public static boolean validateXml(Schema schema, Document document) {
		try {
			Validator validator = schema.newValidator();
			
			validator.validate(new DOMSource(document));
			System.out.println("Validacija uspesna!");
			return true;
			
		} catch (Exception e) {
			System.out.println("Validacije neuspesna!");
			return false;
		}
		
	}
	
	public static Document convertStringToXMLDocument(String xmlString) 
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try
        {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return null;
    }
}
