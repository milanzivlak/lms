package rs.ac.singidunum.isa.app.parsers;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;

public class MestoParser {
	
	private static final String SCHEMANAME = "mesto.xsd";
	public ArrayList<MestoDTO> spisakMesta;
	public ArrayList<MestoDTO> importovanaMesta;
	
	public MestoParser(ArrayList<MestoDTO> spisakMesta) {
		this.spisakMesta = spisakMesta;
	}
	
	public MestoParser() {
		
	}
	
	public void export() throws IOException, ParserConfigurationException, TransformerException {
		//root
		
		DocumentBuilderFactory dbFactory =
		         DocumentBuilderFactory.newInstance();
		         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		         Document doc = (Document) dBuilder.newDocument();
		 Element mesta = doc.createElement("mesta");
		doc.appendChild(mesta);
		for(MestoDTO d : this.spisakMesta) {
			Element mesto = doc.createElement("mesto");
			
			Attr attribute = doc.createAttribute("id");
			attribute.setValue(d.getId().toString());
			mesto.setAttributeNode(attribute);
			
			Element naziv = doc.createElement("naziv");
			naziv.appendChild(doc.createTextNode(d.getNaziv()));
			mesto.appendChild(naziv);
			
			Element drzava = doc.createElement("drzava");
			DrzavaDTO drzavaDTO = d.getDrzava();
			Attr drzavaAttr = doc.createAttribute("id");
			drzavaAttr.setValue(drzavaDTO.getId().toString());
			drzava.setAttributeNode(drzavaAttr);
			Element naziv_d = doc.createElement("naziv");
			naziv_d.appendChild(doc.createTextNode(drzavaDTO.getNaziv()));
			drzava.appendChild(naziv_d);
			mesto.appendChild(drzava);
			
//			Element drzava = doc.createElement("drzava");
//			drzava.appendChild(doc.createTextNode(d.getDrzava().getNaziv()));
//			mesto.appendChild(drzava);
			
			mesta.appendChild(mesto);
		}
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Schema schema = loadSchema(SCHEMANAME);
		
		if(validateXml(schema, doc)) {
			DOMSource source = new DOMSource((Node) doc);
			String home = System.getProperty("user.home");
			File file = new File(home+"/Downloads/" + "mesto" + ".xml");
			StreamResult result = new StreamResult(file);
			//StreamResult result =  new StreamResult(new File("C:\\Users\\Korisnik\\Desktop\\xml\\drzave.xml"));
			transformer.transform(source, result);
			StreamResult consoleResult = new StreamResult(System.out);
	        transformer.transform(source, consoleResult);
		}else {
			System.out.println("Neispravan format za export!");
		}
		
	}
	
	public ArrayList<MestoDTO> importXml(String s){
		
		this.importovanaMesta = new ArrayList<MestoDTO>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			Document doc = convertStringToXMLDocument(s);
			Schema schema = loadSchema(SCHEMANAME);
			if(validateXml(schema, doc)) {
				doc.getDocumentElement().normalize();
				
				System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
				
				NodeList list = doc.getElementsByTagName("mesto");
				
				for(int temp = 0; temp < list.getLength(); temp++) {
					Node node = list.item(temp);
					
					if(node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						
						String mestoId = element.getAttribute("id");
						String mestoNaziv = element.getElementsByTagName("naziv").item(0).getTextContent();
						
						MestoDTO dto = new MestoDTO();
						dto.setId(Long.parseLong(mestoId));
						dto.setNaziv(mestoNaziv);
						
						Element drzava = (Element)element.getElementsByTagName("drzava").item(0);
						
						String drzavaId = drzava.getAttribute("id");
						String drzavaNaziv = drzava.getElementsByTagName("naziv").item(0).getTextContent();
						
						System.out.println("Element: " + node.getNodeName());
						System.out.println("Drzava ID: " + drzavaId);
						System.out.println("Drzava naziv: " + drzavaNaziv);
						
						DrzavaDTO drz = new DrzavaDTO();		
						drz.setId(Long.parseLong(drzavaId));
						drz.setNaziv(drzavaNaziv);
						
						dto.setDrzava(drz);
						
						
						this.importovanaMesta.add(dto);
					}
				}
				//}
				return this.importovanaMesta;
			} else {
				System.out.println("XML dokument nije ispravan!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Schema loadSchema(String schemaFileName) {
		Schema schema = null;
		try {
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			
			schema = factory.newSchema(new File(schemaFileName));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return schema;
	}
	
	public static Document parseXmlDom(String xmlName) {
		
		Document document = null;
		try {
		   DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		   DocumentBuilder builder = factory.newDocumentBuilder();
		   document = builder.parse(new File(xmlName));
		} catch (Exception e) {
		   System.out.println(e.toString());
		}
		return document;
	}
	
	public static boolean validateXml(Schema schema, Document document) {
		try {
			Validator validator = schema.newValidator();
			
			validator.validate(new DOMSource(document));
			System.out.println("Validacija uspjesna!");
			return true;
			
		} catch (Exception e) {
			System.out.println("Validacije neuspjesna!");
			return false;
		}
		
	}
	
	public static Document convertStringToXMLDocument(String xmlString) 
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
             
            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return null;
    }
}
