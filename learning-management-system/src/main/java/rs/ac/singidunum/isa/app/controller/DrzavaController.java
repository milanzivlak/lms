package rs.ac.singidunum.isa.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mysql.cj.util.Base64Decoder;

import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;

import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Mesto;
import rs.ac.singidunum.isa.app.parsers.DrzavaParser;
import rs.ac.singidunum.isa.app.service.DrzavaService;
import rs.ac.singidunum.isa.app.service.MestoService;

@Controller
@RequestMapping(path = "/api/drzave")
@CrossOrigin
public class DrzavaController {
	
	@Autowired
	private DrzavaService drzavaService;
	
	@Autowired
	private MestoService mestoService;
	
	DrzavaParser parser = new DrzavaParser();
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<DrzavaDTO>> getAllDrzave() {
		ArrayList<DrzavaDTO> drzave = new ArrayList<DrzavaDTO>();
		
		for(Drzava drzava : drzavaService.findAll()) {
			drzave.add(DrzavaDTO.fromDrzava(drzava));
		}
		
//		this.parser.spisakDrzava = drzave;
//		try {
//			this.parser.export();
//		} catch (IOException | ParserConfigurationException | TransformerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return new ResponseEntity<Iterable<DrzavaDTO>>(drzave, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/import", method = RequestMethod.GET)
	public ResponseEntity<Iterable<DrzavaDTO>> drzaveImport(@Param(value = "url") String url) throws UnsupportedEncodingException {
//		String[] arrOfStr = url.split(":");
//		
//
//		String toDecode = arrOfStr[1];
//		
//		byte[] arr = Base64.getEncoder().encode(toDecode.getBytes(StandardCharsets.UTF_8)); 
//		arr = Base64.getDecoder().decode(arr);
		//byte[] arr = decoder.decode(toDecode.getBytes("UTF-8"));
		
		ArrayList<DrzavaDTO> importovane = this.parser.importXml(url);
		
		for(DrzavaDTO d : importovane) {
			Drzava drzava = new Drzava(d.getId(), d.getNaziv());
			drzavaService.save(drzava);
			
			for(MestoDTO m : d.getMesta()) {
				Mesto mesto = new Mesto(m.getId(), m.getNaziv());
				mesto.setDrzava(drzava);
				mestoService.save(mesto);
			}
			
		}
		return new ResponseEntity<Iterable<DrzavaDTO>>(this.parser.importXml(url), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/export", method = RequestMethod.GET)
	public void drzaveExport() {
		System.out.println("Exporting...");
		ArrayList<DrzavaDTO> drzave = new ArrayList<DrzavaDTO>();
		
		for(Drzava drzava : drzavaService.findAll()) {
			drzave.add(DrzavaDTO.fromDrzava(drzava));
		}
		
		this.parser.spisakDrzava = drzave;
		try {
			this.parser.export();
		} catch (IOException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@RequestMapping(path = "/{drzavaId}", method = RequestMethod.GET)
	public ResponseEntity<DrzavaDTO> getDrzava(@PathVariable("drzavaId") Long drzavaId) {
		
		Optional<Drzava> drzava = drzavaService.findOne(drzavaId);
		
		if(drzava.isPresent()) {
			DrzavaDTO drzavaDTO = new DrzavaDTO(drzava.get().getId(), drzava.get().getNaziv());
			return new ResponseEntity<DrzavaDTO>(drzavaDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<DrzavaDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Drzava> createDrzava(@RequestBody Drzava drzava){
		
		try {
			
			drzavaService.save(drzava);
			
			return new ResponseEntity<Drzava>(drzava, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Drzava>(HttpStatus.BAD_REQUEST);

		
	}
	
	@RequestMapping(path = "/{drzavaId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Drzava> updateDrzava(@PathVariable("drzavaId") Long drzavaId, @RequestBody Drzava izmenjenaDrzava) {
		
		Drzava drzava = drzavaService.findOne(drzavaId).orElse(null);
		
		if( drzava != null) {
			izmenjenaDrzava.setId(drzavaId);
			
			drzavaService.save(izmenjenaDrzava);
			return new ResponseEntity<Drzava>(izmenjenaDrzava, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Drzava>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{drzavaId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Drzava> deleteDrzava(@PathVariable("drzavaId") Long drzavaId) {
	
		if(drzavaService.findOne(drzavaId).isPresent()) {
			drzavaService.delete(drzavaId);
			return new ResponseEntity<Drzava>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Drzava>(HttpStatus.NOT_FOUND);
	}
}
