package rs.ac.singidunum.isa.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mysql.cj.protocol.x.Ok;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;
import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.RegistrovaniKorisnikDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.dto.ZvanjeDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Fakultet;
import rs.ac.singidunum.isa.app.model.GodinaStudija;
import rs.ac.singidunum.isa.app.model.Mesto;
import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.NastavnikNaRealizaciji;
import rs.ac.singidunum.isa.app.model.Predmet;
import rs.ac.singidunum.isa.app.model.StudijskiProgram;
import rs.ac.singidunum.isa.app.model.Univerzitet;
import rs.ac.singidunum.isa.app.model.Zvanje;
import rs.ac.singidunum.isa.app.parsers.NastavnikParser;
import rs.ac.singidunum.isa.app.service.FakultetService;
import rs.ac.singidunum.isa.app.service.NastavnikNaRealizacijiService;
import rs.ac.singidunum.isa.app.service.NastavnikService;
import rs.ac.singidunum.isa.app.service.StudijskiProgramService;
import rs.ac.singidunum.isa.app.service.UniverzitetService;




@Controller
@RequestMapping(path = "/api/nastavnici")
public class NastavnikController {
	
	@Autowired
	private NastavnikService nastavnikService;
	
	@Autowired
	private FakultetService fakultetService;
	
	@Autowired
	private UniverzitetService univerzitetService;
	
	@Autowired
	private StudijskiProgramService studijskiProgramService;
	
	@Autowired
	private NastavnikNaRealizacijiService nastavnikNaRealizacijiService;
	
	private NastavnikParser nastavnikParser = new NastavnikParser();
	
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<NastavnikDTO>> getAllNastavnici() {
		
		
		ArrayList<NastavnikDTO> nastavnici = new ArrayList<NastavnikDTO>();
		
		for(Nastavnik nastavnik : nastavnikService.findAll()) {
			nastavnici.add(NastavnikDTO.fromNastavnik(nastavnik));
		}
		
		return new ResponseEntity<Iterable<NastavnikDTO>>(nastavnici, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{nastavnikId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<NastavnikDTO> getNastavnik(@PathVariable("nastavnikId") Long nastavnikId) {
		
		Optional<Nastavnik> nastavnik = nastavnikService.findOne(nastavnikId);
		
		if(nastavnik.isPresent()) {
			NastavnikDTO nastavnikDTO = new NastavnikDTO(nastavnik.get().getId(), nastavnik.get().getIme(), nastavnik.get().getBiografija(), nastavnik.get().getJmbg());
			
			return new ResponseEntity<NastavnikDTO>(nastavnikDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<NastavnikDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Nastavnik> createNastavnik(@RequestBody Nastavnik nastavnik){
		
		try {
			//Fakultet fakultet = fakultetService.findOne(nastavnik.getFakultet().getId()).get();
			//fakultetService.save(fakultet);
			
		//	Univerzitet univerzitet = univerzitetService.findOne(nastavnik.getUniverzitet().getId()).get();
			//univerzitetService.save(univerzitet);
			
			StudijskiProgram studijskiProgram = studijskiProgramService.findOne(nastavnik.getStudijskiProgram().getId()).get();
			studijskiProgramService.save(studijskiProgram);
			
//			NastavnikNaRealizaciji nnr = nastavnikNaRealizacijiService.findOne(nastavnik.getRealizacija().getId()).get();
//			nnr.setNastavnik(nastavnik);
//			nastavnikNaRealizacijiService.save(nnr);
			
			
			
			nastavnikService.save(nastavnik);
			
			return new ResponseEntity<Nastavnik>(nastavnik, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Nastavnik>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{nastavnikId}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Nastavnik> updateNastavnik(@PathVariable("nastavnikId") Long nastavnikId, @RequestBody Nastavnik izmenjeniNastavnik) {
		
		Nastavnik nastavnik = nastavnikService.findOne(nastavnikId).orElse(null);
		
		if( nastavnik != null) {
//			Nastavnik rk = nastavnikService.findOne(izmenjeniNastavnik.getId()).get();
//			izmenjeniNastavnik.setRegistrovaniKorisnik(rk.getRegistrovaniKorisnik());
//			Nastavnik n = nastavnikService.findOne(nastavnikId).get();
//			NastavnikNaRealizaciji nnr = nastavnikNaRealizacijiService.findOne(n.getRealizacija().getId()).get();
//			nnr.setNastavnik(izmenjeniNastavnik);
//			nastavnikNaRealizacijiService.save(nnr);
//			
//			izmenjeniNastavnik.setId(nastavnikId);
			
			nastavnikService.save(izmenjeniNastavnik);
			return new ResponseEntity<Nastavnik>(izmenjeniNastavnik, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Nastavnik>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{nastavnikId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Nastavnik> deleteStudent(@PathVariable("nastavnikId") Long nastavnikId) {
	
		if(nastavnikService.findOne(nastavnikId).isPresent()) {
			nastavnikService.delete(nastavnikId);
			return new ResponseEntity<Nastavnik>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Nastavnik>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "/prethodnaZvanja", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Iterable<ZvanjeDTO>> getAllPrethodnaZvanja(@RequestParam(name = "naucnaOblast", required = true) String naucnaOblast) {
		
		List<Zvanje> prethodnaZvanja = nastavnikService.findByNaziv(naucnaOblast);
		
		ArrayList<ZvanjeDTO> zvanjaDTO = new ArrayList<ZvanjeDTO>();
		for(Zvanje zvanje : prethodnaZvanja) {
			zvanjaDTO.add(new ZvanjeDTO(zvanje.getId(), zvanje.getDatumIzbora(), zvanje.getDatumPrestanka()));
		}
		
		return new ResponseEntity<Iterable<ZvanjeDTO>>(zvanjaDTO, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/export", method = RequestMethod.GET)
	public void nastavnikExport() {
		System.out.println("Exporting...");
		ArrayList<NastavnikDTO> nastavnici = new ArrayList<NastavnikDTO>();
		
		for(Nastavnik nastavnik: nastavnikService.findAll()) {
			NastavnikDTO nastavnikDTO = NastavnikDTO.fromNastavnik(nastavnik);
			nastavnici.add(nastavnikDTO);
		}
		
		this.nastavnikParser.spisak = nastavnici;
		try {
			this.nastavnikParser.export();
		} catch (IOException | ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
		return;
	}
	
	
	@RequestMapping(path = "/import", method = RequestMethod.GET)
	@Transactional
	public ResponseEntity<Iterable<NastavnikDTO>> predmetiImport(@Param(value = "url") String url) throws UnsupportedEncodingException {

		
		ArrayList<NastavnikDTO> imports = this.nastavnikParser.importXml(url);
		
		for(NastavnikDTO nastavnikDTO: imports) {
			Nastavnik nastavnik = new Nastavnik(nastavnikDTO.getId(), nastavnikDTO.getIme(), nastavnikDTO.getBiografija(), nastavnikDTO.getJmbg());
			nastavnik.setZvanja(new HashSet<Zvanje>());
			for (ZvanjeDTO zvanjeDTO : nastavnikDTO.getZvanja()) {
				Zvanje zvanje = new Zvanje(zvanjeDTO.getId(), zvanjeDTO.getDatumIzbora(), zvanjeDTO.getDatumPrestanka());
				nastavnik.getZvanja().add(zvanje);
			}
			nastavnik.setFakultet(new Fakultet(nastavnikDTO.getFakultet().getId(), nastavnikDTO.getFakultet().getNaziv()));
			nastavnik.setUniverzitet(new Univerzitet(nastavnikDTO.getUniverzitet().getId(), nastavnikDTO.getUniverzitet().getBroj(), nastavnikDTO.getUniverzitet().getUlica()));
			nastavnikService.save(nastavnik);
		}
		return new ResponseEntity<Iterable<NastavnikDTO>>(this.nastavnikParser.importXml(url), HttpStatus.OK);
	}
}
