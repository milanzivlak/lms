package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.Fakultet;
import rs.ac.singidunum.isa.app.model.GodinaStudija;
import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.StudijskiProgram;
import rs.ac.singidunum.isa.app.service.AdresaService;
import rs.ac.singidunum.isa.app.service.FakultetService;
import rs.ac.singidunum.isa.app.service.NastavnikService;

@Controller
@RequestMapping(path = "/api/fakulteti")
@CrossOrigin
public class FakultetController {
	
	@Autowired
	private FakultetService fakultetService;
	
	@Autowired
	private AdresaService adresaService;
	
	@Autowired
	private NastavnikService nastavnikService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<FakultetDTO>> getAllFakulteti() {
		
		
		ArrayList<FakultetDTO> fakulteti = new ArrayList<FakultetDTO>();
		
		for(Fakultet fakultet : fakultetService.findAll()) {
			ArrayList<StudijskiProgramDTO> studijskiProgrami = new ArrayList<StudijskiProgramDTO>();
			
			for(StudijskiProgram sp : fakultet.getStudijskiProgrami()) {
				FakultetDTO faks = new FakultetDTO(sp.getFakultet().getId(), sp.getFakultet().getNaziv());
				
				NastavnikDTO rukovodilac = new NastavnikDTO(sp.getRukovodilac().getId(), sp.getRukovodilac().getIme(), sp.getRukovodilac().getBiografija(), sp.getRukovodilac().getJmbg());
				
				ArrayList<GodinaStudijaDTO> gs = new ArrayList<GodinaStudijaDTO>();
				
				for(GodinaStudija gss : sp.getGodineStudija()) {
					gs.add(new GodinaStudijaDTO(gss.getId(), gss.getGodina()));
				}
				
				studijskiProgrami.add(new StudijskiProgramDTO(sp.getId(), sp.getNaziv(), faks, rukovodilac, gs));
				
			}
			//UniverzitetDTO univerzitet = new UniverzitetDTO(fakultet.getUniverzitet().getId(), fakultet.getUniverzitet().getUlica(), fakultet.getUniverzitet().getBroj());
			
			UniverzitetDTO univerzitet = new UniverzitetDTO();
			
			if(fakultet.getUniverzitet() != null) {
				univerzitet.setId(fakultet.getUniverzitet().getId());
				univerzitet.setUlica(fakultet.getUniverzitet().getUlica());
				univerzitet.setBroj(fakultet.getUniverzitet().getBroj());
			}else {
				univerzitet = null;
			}
			
			//NastavnikDTO nastavnik = new NastavnikDTO(fakultet.getDekan().getId(), fakultet.getDekan().getIme(), fakultet.getDekan().getBiografija(), fakultet.getDekan().getJmbg());
			
			NastavnikDTO nastavnik = new NastavnikDTO();
			if(fakultet.getDekan() != null) {
				nastavnik.setId(fakultet.getDekan().getId());
				nastavnik.setIme(fakultet.getDekan().getIme());
				nastavnik.setBiografija(fakultet.getDekan().getBiografija());
				nastavnik.setJmbg(fakultet.getDekan().getJmbg());
			}
			else {
				nastavnik = null;
			}
			AdresaDTO adresa = new AdresaDTO(fakultet.getAdresa().getId(), fakultet.getAdresa().getUlica(), fakultet.getAdresa().getBroj());
			
			fakulteti.add(new FakultetDTO(fakultet.getId(), fakultet.getNaziv(), adresa, univerzitet, nastavnik, studijskiProgrami));
		}
		
		return new ResponseEntity<Iterable<FakultetDTO>>(fakulteti, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{fakultetId}", method = RequestMethod.GET)
	public ResponseEntity<FakultetDTO> getFakultet(@PathVariable("fakultetId") Long fakultetId) {
		
		Optional<Fakultet> fakultet = fakultetService.findOne(fakultetId);
		
		
		if(fakultet.isPresent()) {
			AdresaDTO adresa = new AdresaDTO(fakultet.get().getAdresa().getId(), fakultet.get().getAdresa().getUlica(), fakultet.get().getAdresa().getBroj());
			UniverzitetDTO univerzitet = new UniverzitetDTO(fakultet.get().getUniverzitet().getId(), fakultet.get().getUniverzitet().getUlica(), fakultet.get().getUniverzitet().getBroj());
			NastavnikDTO nastavnik = new NastavnikDTO(fakultet.get().getDekan().getId(), fakultet.get().getDekan().getIme(), fakultet.get().getDekan().getBiografija(), fakultet.get().getDekan().getJmbg());
			
			FakultetDTO fakultetDTO = new FakultetDTO(fakultet.get().getId(), fakultet.get().getNaziv(), adresa, univerzitet, nastavnik, null);
			return new ResponseEntity<FakultetDTO>(fakultetDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<FakultetDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Fakultet> createFakultet(@RequestBody Fakultet fakultet){
		
		try {
			Adresa adresa = adresaService.findOne(fakultet.getAdresa().getId()).get();
			adresa.setFakultet(fakultet);
			fakultet.setAdresa(adresa);
			
			Nastavnik nastavnik = nastavnikService.findOne(fakultet.getDekan().getId()).get();
			nastavnik.setFakultet(fakultet);
			fakultet.setDekan(nastavnik);
			
			nastavnikService.save(nastavnik);
			adresaService.save(adresa);
			fakultetService.save(fakultet);
			
			return new ResponseEntity<Fakultet>(fakultet, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Fakultet>(HttpStatus.BAD_REQUEST);
		
	}
	
	@RequestMapping(path = "/{fakultetId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Fakultet> updateFakultet(@PathVariable("fakultetId") Long fakultetId, @RequestBody Fakultet izmenjeniFakultet) {
		
		Fakultet fakultet = fakultetService.findOne(fakultetId).orElse(null);
		
		if( fakultet != null) {
			izmenjeniFakultet.setId(fakultetId);
			
			fakultetService.save(izmenjeniFakultet);
			return new ResponseEntity<Fakultet>(izmenjeniFakultet, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Fakultet>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{fakultetId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Fakultet> deleteFakultet(@PathVariable("fakultetId") Long fakultetId) {
	
		if(fakultetService.findOne(fakultetId).isPresent()) {
			fakultetService.delete(fakultetId);
			return new ResponseEntity<Fakultet>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Fakultet>(HttpStatus.NOT_FOUND);
	}
	
}
