package rs.ac.singidunum.isa.app.nastavni_materijal_management.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import antlr.collections.List;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.NastavniMaterijalDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Caretaker;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Memento;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Stanje;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.CaretakerService;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.MementoService;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.NastavniMaterijalService;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.OriginatorService;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.StanjeService;

@Controller
@RequestMapping(path = "/api/nastavni-materijal")
public class NastavniMaterijalController {
	@Autowired
	private NastavniMaterijalService nastavniMaterijalService;
	
	@Autowired
	private StanjeService stanjeService;
	
	@Autowired
	private CaretakerService caretakerService;
	
	@Autowired
	private MementoService mementoService;
	
	@Autowired
	private OriginatorService originatorService;
	
	
	
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Page<NastavniMaterijalDTO>> getAllNastavniMaterijali(Pageable pageable) {
		return new ResponseEntity<Page<NastavniMaterijalDTO>>(nastavniMaterijalService.findAll(pageable).map(a -> new NastavniMaterijalDTO(a)), HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{autorId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<NastavniMaterijalDTO> getAutor(@PathVariable("autorId") Long autorId) {
		
		Optional<NastavniMaterijal> autor = nastavniMaterijalService.findOne(autorId);
		
		if(autor.isPresent()) {
			NastavniMaterijalDTO NastavniMaterijalDTO = new NastavniMaterijalDTO(autor.get());
			return new ResponseEntity<NastavniMaterijalDTO>(NastavniMaterijalDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<NastavniMaterijalDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<NastavniMaterijal> createNastavniMaterijal(@RequestBody NastavniMaterijal nastavniMaterijal){
		
		try {
			
			//Stanje stanje = new Stanje(null, "Kreirana datoteka");
			Optional<Stanje> stanje = stanjeService.findOne((long) 2);
			Stanje s = new Stanje(stanje.get().getId(), stanje.get().getNaziv());
			nastavniMaterijal.setStanje(s);
			
			Memento m = new Memento();
			// Set-ovanje state-a originatoru
			if(nastavniMaterijal.getIstorija() != null) {
				nastavniMaterijal.getIstorija().setState(nastavniMaterijal);
				
				m = nastavniMaterijal.getCaretaker().add(nastavniMaterijal.getIstorija().saveStateToMemento());
				
				System.out.println("Current State: " + nastavniMaterijal.getIstorija().getState().getStanje().getNaziv());          
				//autor.getIstorija().getStateFromMemento(caretaker.get(0));  
				originatorService.save(nastavniMaterijal.getIstorija());
				//mementoService.save(m);
				caretakerService.save(nastavniMaterijal.getCaretaker());
				//nastavniMaterijalService.save(nastavniMaterijal);
			}
			
			
			
			return new ResponseEntity<NastavniMaterijal>(nastavniMaterijal, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<NastavniMaterijal>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{nastavniMaterijalId}", method = RequestMethod.PUT)
	public ResponseEntity<NastavniMaterijal> updateNastavniMaterijal(@PathVariable("nastavniMaterijalId") Long nastavniMaterijalId, @RequestBody NastavniMaterijal izmenjenNastavniMaterijal) {
		
		NastavniMaterijal adresa = nastavniMaterijalService.findOne(nastavniMaterijalId).orElse(null);
		
		if( adresa != null) {
			izmenjenNastavniMaterijal.setId(nastavniMaterijalId);
			
			
			
			Iterable<Stanje> stanja = stanjeService.findAll();
			for(Stanje stanje : stanja) {
				if(stanje.getNaziv().equals("Izmenjena datoteka")) {
					izmenjenNastavniMaterijal.setStanje(stanje);
				}
			}
			Memento m = new Memento();
			// Set-ovanje state-a originatoru
			if(izmenjenNastavniMaterijal.getIstorija() != null) {
				izmenjenNastavniMaterijal.getIstorija().setState(izmenjenNastavniMaterijal);
				izmenjenNastavniMaterijal.setCaretaker(adresa.getCaretaker());
				System.out.println("Previous State: " + izmenjenNastavniMaterijal.getIstorija().getState().getStanje().getNaziv());
				System.out.println(izmenjenNastavniMaterijal.getCaretaker().getMementos().get(0).getState().getNaziv());
				
				m = izmenjenNastavniMaterijal.getCaretaker().add(izmenjenNastavniMaterijal.getIstorija().saveStateToMemento());
				
				System.out.println("Current State: " + izmenjenNastavniMaterijal.getIstorija().getState().getStanje().getNaziv());           

				
			}
			//mementoService.save(m);
			//caretakerService.save(izmenjenNastavniMaterijal.getCaretaker());
			nastavniMaterijalService.save(izmenjenNastavniMaterijal);
			return new ResponseEntity<NastavniMaterijal>(izmenjenNastavniMaterijal, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<NastavniMaterijal>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{autorId}", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<NastavniMaterijal> deleteNastavniMaterijal(@PathVariable("autorId") Long autorId) {
	
		if(nastavniMaterijalService.findOne(autorId).isPresent()) {
			nastavniMaterijalService.delete(autorId);
			return new ResponseEntity<NastavniMaterijal>(HttpStatus.OK);
		}
		
		return new ResponseEntity<NastavniMaterijal>(HttpStatus.NOT_FOUND);
	}
}
