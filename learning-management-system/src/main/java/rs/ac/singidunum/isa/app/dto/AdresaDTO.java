package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.Student;

public class AdresaDTO {
	
	private Long id;
	private String ulica;
	private String broj;
	
	private MestoDTO mesto;
	private UniverzitetDTO univerzitet;
	private FakultetDTO fakultet;
	
	private ArrayList<StudentDTO> studenti = new ArrayList<StudentDTO>();

	public AdresaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public AdresaDTO(Long id, String ulica, String broj) {
		this(id, ulica, broj, null, null, null, null);
	}

	public AdresaDTO(Long id, String ulica, String broj, MestoDTO mesto, UniverzitetDTO univerzitet,
			FakultetDTO fakultet, ArrayList<StudentDTO> studenti) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
		this.mesto = mesto;
		this.univerzitet = univerzitet;
		this.fakultet = fakultet;
		this.studenti = studenti;
	}
	
	public static AdresaDTO fromAdresa(Adresa adresa) {
		ArrayList<StudentDTO> studenti = new ArrayList<StudentDTO>();
		
		for(Student student : adresa.getStudenti()) {
			studenti.add(new StudentDTO(student.getId(), student.getJmbg(), student.getJmbg()));
		}
		MestoDTO mesto = new MestoDTO(adresa.getMesto().getId(), adresa.getMesto().getNaziv());
		
		UniverzitetDTO univerzitet = new UniverzitetDTO();
		
		if(adresa.getUniverzitet() != null) {
			univerzitet.setId(adresa.getUniverzitet().getId());
			univerzitet.setUlica(adresa.getUniverzitet().getUlica());
			univerzitet.setBroj(adresa.getUniverzitet().getBroj());
		}else {
			univerzitet = null;
		}
		
		FakultetDTO faks = new FakultetDTO();
		
		if(adresa.getFakultet() != null) {
			faks.setId(adresa.getFakultet().getId());
			faks.setNaziv(adresa.getFakultet().getNaziv());
		}
		else {
			faks = null;
		}

		
		return new AdresaDTO(adresa.getId(), adresa.getUlica(), adresa.getBroj(), mesto, univerzitet, faks, studenti);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public MestoDTO getMesto() {
		return mesto;
	}

	public void setMesto(MestoDTO mesto) {
		this.mesto = mesto;
	}

	public UniverzitetDTO getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(UniverzitetDTO univerzitet) {
		this.univerzitet = univerzitet;
	}

	public FakultetDTO getFakultet() {
		return fakultet;
	}

	public void setFakultet(FakultetDTO fakultet) {
		this.fakultet = fakultet;
	}

	public ArrayList<StudentDTO> getStudenti() {
		return studenti;
	}

	public void setStudenti(ArrayList<StudentDTO> studenti) {
		this.studenti = studenti;
	}
}

