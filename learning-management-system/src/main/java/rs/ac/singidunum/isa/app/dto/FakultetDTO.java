package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class FakultetDTO {
	
	private Long id;
	private String naziv;
	
	private AdresaDTO adresa;
	private UniverzitetDTO univerzitet;
	private NastavnikDTO dekan;
	
	private ArrayList<StudijskiProgramDTO> studijskiProgrami = new ArrayList<StudijskiProgramDTO>();

	public FakultetDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public FakultetDTO(Long id, String naziv) {
		this(id, naziv, null, null, null, null);
	}

	public FakultetDTO(Long id, String naziv, AdresaDTO adresa, UniverzitetDTO univerzitet, NastavnikDTO dekan,
			ArrayList<StudijskiProgramDTO> studijskiProgrami) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.adresa = adresa;
		this.univerzitet = univerzitet;
		this.dekan = dekan;
		this.studijskiProgrami = studijskiProgrami;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public AdresaDTO getAdresa() {
		return adresa;
	}

	public void setAdresa(AdresaDTO adresa) {
		this.adresa = adresa;
	}

	public UniverzitetDTO getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(UniverzitetDTO univerzitet) {
		this.univerzitet = univerzitet;
	}

	public NastavnikDTO getDekan() {
		return dekan;
	}

	public void setDekan(NastavnikDTO dekan) {
		this.dekan = dekan;
	}

	public ArrayList<StudijskiProgramDTO> getStudijskiProgrami() {
		return studijskiProgrami;
	}

	public void setStudijskiProgrami(ArrayList<StudijskiProgramDTO> studijskiProgrami) {
		this.studijskiProgrami = studijskiProgrami;
	}
	
	
}
