package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.Date;

import rs.ac.singidunum.isa.app.dto.IshodDTO;
import rs.ac.singidunum.isa.app.dto.RealizacijaPredmetaDTO;
import rs.ac.singidunum.isa.app.dto.TipNastaveDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TerminNastave;

public class TerminNastaveDTO {
	
	private Long id;
	private Date vremePocetka;
	private Date vremeKraja;
	
	private IshodDTO ishod;
	private TipNastaveDTO tipNastave;
	private RealizacijaPredmetaDTO realizacijaPredmeta;
	
	public TerminNastaveDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TerminNastaveDTO(Long id, Date vremePocetka, Date vremeKraja) {
		this(id, vremePocetka, vremeKraja, null, null, null);
	}

	public TerminNastaveDTO(Long id, Date vremePocetka, Date vremeKraja, IshodDTO ishod, TipNastaveDTO tipNastave,
			RealizacijaPredmetaDTO realizacijaPredmeta) {
		super();
		this.id = id;
		this.vremePocetka = vremePocetka;
		this.vremeKraja = vremeKraja;
		this.ishod = ishod;
		this.tipNastave = tipNastave;
		this.realizacijaPredmeta = realizacijaPredmeta;
	}

	public TerminNastaveDTO(TerminNastave terminNastave) {
		this(terminNastave.getId(), terminNastave.getVremePocetka(), terminNastave.getVremeKraja());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getVremePocetka() {
		return vremePocetka;
	}

	public void setVremePocetka(Date vremePocetka) {
		this.vremePocetka = vremePocetka;
	}

	public Date getVremeKraja() {
		return vremeKraja;
	}

	public void setVremeKraja(Date vremeKraja) {
		this.vremeKraja = vremeKraja;
	}

	public IshodDTO getIshod() {
		return ishod;
	}

	public void setIshod(IshodDTO ishod) {
		this.ishod = ishod;
	}

	public TipNastaveDTO getTipNastave() {
		return tipNastave;
	}

	public void setTipNastave(TipNastaveDTO tipNastave) {
		this.tipNastave = tipNastave;
	}

	public RealizacijaPredmetaDTO getRealizacijaPredmeta() {
		return realizacijaPredmeta;
	}

	public void setRealizacijaPredmeta(RealizacijaPredmetaDTO realizacijaPredmeta) {
		this.realizacijaPredmeta = realizacijaPredmeta;
	}
	
}
