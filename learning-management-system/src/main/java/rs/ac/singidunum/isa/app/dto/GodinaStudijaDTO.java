package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.Date;

public class GodinaStudijaDTO {
	
	private Long id;
	private Date godina;
	
	private ArrayList<StudentNaGodiniDTO> studentNaGodini = new ArrayList<StudentNaGodiniDTO>();
	
	private ArrayList<PredmetDTO> predmeti = new ArrayList<PredmetDTO>();
	
	private StudijskiProgramDTO studijskiProgram;

	public GodinaStudijaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public GodinaStudijaDTO(Long id, Date godina) {
		this(id, godina, null, null, null);
	}

	public GodinaStudijaDTO(Long id, Date godina, ArrayList<StudentNaGodiniDTO> studentNaGodini,
			ArrayList<PredmetDTO> predmeti, StudijskiProgramDTO studijskiProgram) {
		super();
		this.id = id;
		this.godina = godina;
		this.studentNaGodini = studentNaGodini;
		this.predmeti = predmeti;
		this.studijskiProgram = studijskiProgram;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getGodina() {
		return godina;
	}

	public void setGodina(Date godina) {
		this.godina = godina;
	}

	public ArrayList<StudentNaGodiniDTO> getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(ArrayList<StudentNaGodiniDTO> studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}

	public ArrayList<PredmetDTO> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(ArrayList<PredmetDTO> predmeti) {
		this.predmeti = predmeti;
	}

	public StudijskiProgramDTO getStudijskiProgram() {
		return studijskiProgram;
	}

	public void setStudijskiProgram(StudijskiProgramDTO studijskiProgram) {
		this.studijskiProgram = studijskiProgram;
	}
	
	
	
}
