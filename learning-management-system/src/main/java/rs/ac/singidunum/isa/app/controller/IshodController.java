package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.IshodDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.model.Fakultet;
import rs.ac.singidunum.isa.app.model.Ishod;
import rs.ac.singidunum.isa.app.model.StudijskiProgram;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.EvaluacijaZnanjaDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.NastavniMaterijalDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.ObrazovniCiljDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.TerminNastaveDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.ObrazovniCilj;
import rs.ac.singidunum.isa.app.service.IshodService;

@Controller
@RequestMapping(path = "/api/ishodi")
@CrossOrigin
public class IshodController {
	
	@Autowired
	private IshodService ishodService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<IshodDTO>> getAllIshodi() {
		
		
		ArrayList<IshodDTO> ishodi = new ArrayList<IshodDTO>();
		for(Ishod ishod : ishodService.findAll()) {
			PredmetDTO predmet = new PredmetDTO(ishod.getPredmet().getId(), ishod.getPredmet().getNaziv(), ishod.getPredmet().getEspb(), ishod.getPredmet().isObavezan(), ishod.getPredmet().getBrojPredavanja(), ishod.getPredmet().getBrojVezbi(), ishod.getPredmet().getDrugiObliciNastave(), ishod.getPredmet().getIstrazivackiRad(), ishod.getPredmet().getOstaliCasovi(), ishod.getPredmet().getBrojSemestara());
			
			NastavniMaterijalDTO nmd = new NastavniMaterijalDTO(ishod.getNastavniMaterijal().getId(), ishod.getNastavniMaterijal().getNaziv(), ishod.getNastavniMaterijal().getGodinaIzdanja(), ishod.getNastavniMaterijal().getDatumAzuriranja(), ishod.getNastavniMaterijal().getDatumUklanjanja());
			ObrazovniCiljDTO ocd = new ObrazovniCiljDTO(ishod.getObrazovniCilj().getId(), ishod.getObrazovniCilj().getOpis());
			TerminNastaveDTO tnd = new TerminNastaveDTO(ishod.getTerminNastave().getId(), ishod.getTerminNastave().getVremePocetka(), ishod.getTerminNastave().getVremeKraja());
			EvaluacijaZnanjaDTO ezd = new EvaluacijaZnanjaDTO(ishod.getEvaluacijaZnanja().getId(), ishod.getEvaluacijaZnanja().getVremePocetka(), ishod.getEvaluacijaZnanja().getVremeZavrsetka(), ishod.getEvaluacijaZnanja().getBodovi());
			
			ishodi.add(new IshodDTO(ishod.getId(), ishod.getOpis(), predmet, nmd, ocd, tnd, ezd));
		}
		
		return new ResponseEntity<Iterable<IshodDTO>>(ishodi, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{ishodId}", method = RequestMethod.GET)
	public ResponseEntity<IshodDTO> getIshod(@PathVariable("ishodId") Long ishodId) {
		
		Optional<Ishod> ishod = ishodService.findOne(ishodId);
		
		
		if(ishod.isPresent()) {
			PredmetDTO predmet = new PredmetDTO(ishod.get().getPredmet().getId(), ishod.get().getPredmet().getNaziv(), ishod.get().getPredmet().getEspb(), ishod.get().getPredmet().isObavezan(), ishod.get().getPredmet().getBrojPredavanja(), ishod.get().getPredmet().getBrojVezbi(), ishod.get().getPredmet().getDrugiObliciNastave(), ishod.get().getPredmet().getIstrazivackiRad(), ishod.get().getPredmet().getOstaliCasovi(), ishod.get().getPredmet().getBrojSemestara());
			NastavniMaterijalDTO nmd = new NastavniMaterijalDTO(ishod.get().getNastavniMaterijal().getId(), ishod.get().getNastavniMaterijal().getNaziv(), ishod.get().getNastavniMaterijal().getGodinaIzdanja(), ishod.get().getNastavniMaterijal().getDatumAzuriranja(), ishod.get().getNastavniMaterijal().getDatumUklanjanja());
			ObrazovniCiljDTO ocd = new ObrazovniCiljDTO(ishod.get().getObrazovniCilj().getId(), ishod.get().getObrazovniCilj().getOpis());
			TerminNastaveDTO tnd = new TerminNastaveDTO(ishod.get().getTerminNastave().getId(), ishod.get().getTerminNastave().getVremePocetka(), ishod.get().getTerminNastave().getVremeKraja());
			EvaluacijaZnanjaDTO ezd = new EvaluacijaZnanjaDTO(ishod.get().getEvaluacijaZnanja().getId(), ishod.get().getEvaluacijaZnanja().getVremePocetka(), ishod.get().getEvaluacijaZnanja().getVremeZavrsetka(), ishod.get().getEvaluacijaZnanja().getBodovi());
			
			IshodDTO ishodDTO = new IshodDTO(ishod.get().getId(), ishod.get().getOpis(), predmet, nmd, ocd, tnd, ezd);
			return new ResponseEntity<IshodDTO>(ishodDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<IshodDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Ishod> createIshod(@RequestBody Ishod ishod){
		
		try {
			
			ishodService.save(ishod);
			
			return new ResponseEntity<Ishod>(ishod, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Ishod>(HttpStatus.BAD_REQUEST);
		
	}
	
	@RequestMapping(path = "/{ishodId}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Ishod> updateIshod(@PathVariable("ishodId") Long ishodId, @RequestBody Ishod izmenjeniIshod) {
		
		Ishod ishod = ishodService.findOne(ishodId).orElse(null);
		
		if( ishod != null) {
			izmenjeniIshod.setId(ishodId);
			
			ishodService.save(izmenjeniIshod);
			return new ResponseEntity<Ishod>(izmenjeniIshod, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Ishod>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{ishodId}", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Ishod> deleteIshod(@PathVariable("ishodId") Long ishodId) {
	
		if(ishodService.findOne(ishodId).isPresent()) {
			ishodService.delete(ishodId);
			return new ResponseEntity<Ishod>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Ishod>(HttpStatus.NOT_FOUND);
	}

}
