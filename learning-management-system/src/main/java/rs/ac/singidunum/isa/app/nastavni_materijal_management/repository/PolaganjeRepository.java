package rs.ac.singidunum.isa.app.nastavni_materijal_management.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.PolaganjeDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Polaganje;

@Repository
public interface PolaganjeRepository extends PagingAndSortingRepository<Polaganje, Long> {
}
