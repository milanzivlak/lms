package rs.ac.singidunum.isa.app.dto;

public class UserPermissionDTO {
	
	private Long id;
	private RegistrovaniKorisnikDTO user;
	private PermissionDTO permission;
	
	public UserPermissionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserPermissionDTO(Long id) {
		this(id, null, null);
	}
	
	public UserPermissionDTO(Long id, RegistrovaniKorisnikDTO user, PermissionDTO permission) {
		super();
		this.id = id;
		this.user = user;
		this.permission = permission;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RegistrovaniKorisnikDTO getUser() {
		return user;
	}

	public void setUser(RegistrovaniKorisnikDTO user) {
		this.user = user;
	}

	public PermissionDTO getPermission() {
		return permission;
	}

	public void setPermission(PermissionDTO permission) {
		this.permission = permission;
	}
	
}
