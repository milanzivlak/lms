package rs.ac.singidunum.isa.app.nastavni_materijal_management.memento;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;

@Entity
public class Stanje {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String naziv;
	
	@OneToMany(mappedBy = "stanje")
	@JsonIgnore
	private Set<NastavniMaterijal> nastavniMaterijali = new HashSet<NastavniMaterijal>();
	
	@OneToOne(mappedBy = "state")
	@JsonIgnore
	private Memento memento;
	
	
	public Stanje() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Stanje(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<NastavniMaterijal> getNastavniMaterijali() {
		return nastavniMaterijali;
	}

	public void setNastavniMaterijali(Set<NastavniMaterijal> nastavniMaterijali) {
		this.nastavniMaterijali = nastavniMaterijali;
	}
	
	
	
}
