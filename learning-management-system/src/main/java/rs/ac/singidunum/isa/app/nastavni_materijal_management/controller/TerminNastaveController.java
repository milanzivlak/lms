package rs.ac.singidunum.isa.app.nastavni_materijal_management.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.TerminNastaveDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TerminNastave;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.TerminNastaveService;

@Controller
@RequestMapping(path = "/api/nastavni-materijal/termin-nastave")
public class TerminNastaveController {
	@Autowired
	private TerminNastaveService terminNastaveService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<TerminNastaveDTO>> getAllAutori(Pageable pageable) {
		return new ResponseEntity<Page<TerminNastaveDTO>>(
				terminNastaveService.findAll(pageable).map(a -> new TerminNastaveDTO(a)), HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{terminNastaveId}", method = RequestMethod.GET)
	public ResponseEntity<TerminNastaveDTO> getAutor(@PathVariable("terminNastaveId") Long terminNastaveId) {
		
		Optional<TerminNastave> terminNastave = terminNastaveService.findOne(terminNastaveId);
		
		if(terminNastave.isPresent()) {
			TerminNastaveDTO TerminNastaveDTO = new TerminNastaveDTO(terminNastave.get());
			return new ResponseEntity<TerminNastaveDTO>(TerminNastaveDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<TerminNastaveDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<TerminNastave> createTerminNastave(@RequestBody TerminNastave terminNastave){
		
		try {
			
			terminNastaveService.save(terminNastave);
			
			return new ResponseEntity<TerminNastave>(terminNastave, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<TerminNastave>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{terminNastaveId}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<TerminNastave> updateAutor(@PathVariable("terminNastaveId") Long terminNastaveId, @RequestBody TerminNastave izmenjenAutor) {
		
		TerminNastave adresa = terminNastaveService.findOne(terminNastaveId).orElse(null);
		
		if( adresa != null) {
			izmenjenAutor.setId(terminNastaveId);
			
			terminNastaveService.save(izmenjenAutor);
			return new ResponseEntity<TerminNastave>(izmenjenAutor, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<TerminNastave>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{terminNastaveId}", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<TerminNastave> deleteAutor(@PathVariable("terminNastaveId") Long terminNastaveId) {
	
		if(terminNastaveService.findOne(terminNastaveId).isPresent()) {
			terminNastaveService.delete(terminNastaveId);
			return new ResponseEntity<TerminNastave>(HttpStatus.OK);
		}
		
		return new ResponseEntity<TerminNastave>(HttpStatus.NOT_FOUND);
	}

}
