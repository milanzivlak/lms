package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class StudijskiProgramDTO {
	
	private Long id;
	private String naziv;
	
	private FakultetDTO fakultet;
	private NastavnikDTO rukovodilac;
	
	private ArrayList<GodinaStudijaDTO> godinaStudija = new ArrayList<GodinaStudijaDTO>();
	
	
	public StudijskiProgramDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public StudijskiProgramDTO(Long id, String naziv) {
		this(id, naziv, null, null, null);
	}

	public StudijskiProgramDTO(Long id, String naziv, FakultetDTO fakultet, NastavnikDTO rukovodilac, ArrayList<GodinaStudijaDTO> godinaStudija) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.fakultet = fakultet;
		this.rukovodilac = rukovodilac;
		this.godinaStudija = godinaStudija;
	}
	
//	public StudijskiProgramDTO(Long id, String naziv, FakultetDTO fakultet, ArrayList<GodinaStudijaDTO> godinaStudija) {
//		super();
//		this.id = id;
//		this.naziv = naziv;
//		this.fakultet = fakultet;
//		this.godinaStudija = godinaStudija;
//	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public FakultetDTO getFakultet() {
		return fakultet;
	}

	public void setFakultet(FakultetDTO fakultet) {
		this.fakultet = fakultet;
	}

	public NastavnikDTO getRukovodilac() {
		return rukovodilac;
	}

	public void setRukovodilac(NastavnikDTO rukovodilac) {
		this.rukovodilac = rukovodilac;
	}

	public ArrayList<GodinaStudijaDTO> getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(ArrayList<GodinaStudijaDTO> godinaStudija) {
		this.godinaStudija = godinaStudija;
	}
}
