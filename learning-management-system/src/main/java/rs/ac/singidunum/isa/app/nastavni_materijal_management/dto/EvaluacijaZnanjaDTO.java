package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.Date;

import rs.ac.singidunum.isa.app.dto.IshodDTO;
import rs.ac.singidunum.isa.app.dto.RealizacijaPredmetaDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.EvaluacijaZnanja;

public class EvaluacijaZnanjaDTO {
	
	private Long id;
	private Date vremePocetka;
	private Date vremeZavrsetka;
	
	private int bodovi;
	
	private TipEvaluacijeDTO tipEvaluacije;
	private IshodDTO ishod;
	private RealizacijaPredmetaDTO realizacijaPredmeta;
	private PolaganjeDTO polaganje;
	
	public EvaluacijaZnanjaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public EvaluacijaZnanjaDTO(Long id, Date vremePocetka, Date vremeZavrsetka, int bodovi) {
		this(id, vremePocetka, vremeZavrsetka, bodovi, null, null, null, null);
	}

	public EvaluacijaZnanjaDTO(Long id, Date vremePocetka, Date vremeZavrsetka, int bodovi,
			TipEvaluacijeDTO tipEvaluacije, IshodDTO ishod, RealizacijaPredmetaDTO realizacijaPredmeta,
			PolaganjeDTO polaganje) {
		super();
		this.id = id;
		this.vremePocetka = vremePocetka;
		this.vremeZavrsetka = vremeZavrsetka;
		this.bodovi = bodovi;
		this.tipEvaluacije = tipEvaluacije;
		this.ishod = ishod;
		this.realizacijaPredmeta = realizacijaPredmeta;
		this.polaganje = polaganje;
	}

	public EvaluacijaZnanjaDTO(EvaluacijaZnanja evaluacijaZnanja) {
		this(evaluacijaZnanja.getId(), evaluacijaZnanja.getVremePocetka(), evaluacijaZnanja.getVremeZavrsetka(), evaluacijaZnanja.getBodovi());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getVremePocetka() {
		return vremePocetka;
	}

	public void setVremePocetka(Date vremePocetka) {
		this.vremePocetka = vremePocetka;
	}

	public Date getVremeZavrsetka() {
		return vremeZavrsetka;
	}

	public void setVremeZavrsetka(Date vremeZavrsetka) {
		this.vremeZavrsetka = vremeZavrsetka;
	}

	public int getBodovi() {
		return bodovi;
	}

	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}

	public TipEvaluacijeDTO getTipEvaluacije() {
		return tipEvaluacije;
	}

	public void setTipEvaluacije(TipEvaluacijeDTO tipEvaluacije) {
		this.tipEvaluacije = tipEvaluacije;
	}

	public IshodDTO getIshod() {
		return ishod;
	}

	public void setIshod(IshodDTO ishod) {
		this.ishod = ishod;
	}

	public RealizacijaPredmetaDTO getRealizacijaPredmeta() {
		return realizacijaPredmeta;
	}

	public void setRealizacijaPredmeta(RealizacijaPredmetaDTO realizacijaPredmeta) {
		this.realizacijaPredmeta = realizacijaPredmeta;
	}

	public PolaganjeDTO getPolaganje() {
		return polaganje;
	}

	public void setPolaganje(PolaganjeDTO polaganje) {
		this.polaganje = polaganje;
	}
	
}
