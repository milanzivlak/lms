package rs.ac.singidunum.isa.app.dto;

import java.util.List;
import java.util.stream.Collectors;

import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Ishod;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.EvaluacijaZnanjaDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.NastavniMaterijalDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.ObrazovniCiljDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.TerminNastaveDTO;

public class IshodDTO {
	
	private Long id;
	private String opis;
	
	private PredmetDTO predmet;
	
	private NastavniMaterijalDTO nastavniMaterijal;
	
	private ObrazovniCiljDTO obrazovniCilj;
	
	private TerminNastaveDTO terminNastave;
	
	private EvaluacijaZnanjaDTO evaluacijaZnanja;

	public IshodDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public IshodDTO(Long id, String opis) {
		this(id, opis, null, null, null, null, null);
	}

	public IshodDTO(Long id, String opis, PredmetDTO predmet, NastavniMaterijalDTO nastavniMaterijal, ObrazovniCiljDTO obrazovniCilj, TerminNastaveDTO terminNastave, EvaluacijaZnanjaDTO evaluacijaZnanja) {
		super();
		this.id = id;
		this.opis = opis;
		this.predmet = predmet;
		this.nastavniMaterijal = nastavniMaterijal;
		this.obrazovniCilj = obrazovniCilj;
		this.terminNastave = terminNastave;
		this.evaluacijaZnanja = evaluacijaZnanja;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public PredmetDTO getPredmet() {
		return predmet;
	}

	public void setPredmet(PredmetDTO predmet) {
		this.predmet = predmet;
	}

	public NastavniMaterijalDTO getNastavniMaterijal() {
		return nastavniMaterijal;
	}

	public void setNastavniMaterijal(NastavniMaterijalDTO nastavniMaterijal) {
		this.nastavniMaterijal = nastavniMaterijal;
	}

	public ObrazovniCiljDTO getObrazovniCilj() {
		return obrazovniCilj;
	}

	public void setObrazovniCilj(ObrazovniCiljDTO obrazovniCilj) {
		this.obrazovniCilj = obrazovniCilj;
	}

	public TerminNastaveDTO getTerminNastave() {
		return terminNastave;
	}

	public void setTerminNastave(TerminNastaveDTO terminNastave) {
		this.terminNastave = terminNastave;
	}

	public EvaluacijaZnanjaDTO getEvaluacijaZnanja() {
		return evaluacijaZnanja;
	}

	public void setEvaluacijaZnanja(EvaluacijaZnanjaDTO evaluacijaZnanja) {
		this.evaluacijaZnanja = evaluacijaZnanja;
	}
	
	
	public static IshodDTO fromIshod(Ishod ishod) {
		IshodDTO temp = new IshodDTO();
		temp.setId(ishod.getId());
		temp.setOpis(ishod.getOpis());
		temp.setPredmet(PredmetDTO.fromPredmet(ishod.getPredmet()));
		
		return temp;
	}
}
