package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.PohadjanjePredmetaDTO;
import rs.ac.singidunum.isa.app.dto.RealizacijaPredmetaDTO;
import rs.ac.singidunum.isa.app.dto.StudentDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.PohadjanjePredmeta;
import rs.ac.singidunum.isa.app.service.PohadjanjePredmetaService;

@Controller
@RequestMapping(path = "/api/pohadjanjePredmeta")
@PreAuthorize("hasAuthority('KORISNIK')")
@CrossOrigin
public class PohadjanjePredmetaController {
	
	@Autowired
	private PohadjanjePredmetaService pohadjanjePredmetaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Iterable<PohadjanjePredmetaDTO>> getAllPohadjanja() {
		
		
		ArrayList<PohadjanjePredmetaDTO> pohadjanjaPredmeta = new ArrayList<PohadjanjePredmetaDTO>();
		
		for(PohadjanjePredmeta pp : pohadjanjePredmetaService.findAll()) {
			
			RealizacijaPredmetaDTO realizacijaPredmeta = new RealizacijaPredmetaDTO(pp.getRealizacijaPredmeta().getId());
			
			StudentDTO student = new StudentDTO(pp.getStudent().getId(), pp.getStudent().getJmbg(), pp.getStudent().getIme());
			
			pohadjanjaPredmeta.add(new PohadjanjePredmetaDTO(pp.getId(), pp.getKonacnaOcena(), realizacijaPredmeta, student));
		}
		
		return new ResponseEntity<Iterable<PohadjanjePredmetaDTO>>(pohadjanjaPredmeta, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{pohadjanjePredmetaId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<PohadjanjePredmetaDTO> getPohadjanja(@PathVariable("pohadjanjePredmetaId") Long pohadjanjePredmetaId) {
		
		Optional<PohadjanjePredmeta> pohadjanje = pohadjanjePredmetaService.findOne(pohadjanjePredmetaId);
		
		if(pohadjanje.isPresent()) {
			PohadjanjePredmetaDTO pohadjanjeDTO = new PohadjanjePredmetaDTO(pohadjanje.get().getId(), pohadjanje.get().getKonacnaOcena());
			return new ResponseEntity<PohadjanjePredmetaDTO>(pohadjanjeDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<PohadjanjePredmetaDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<PohadjanjePredmeta> createPohadjanje(@RequestBody PohadjanjePredmeta pohadjanje){
		
		try {
			
			pohadjanjePredmetaService.save(pohadjanje);
			
			return new ResponseEntity<PohadjanjePredmeta>(pohadjanje, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<PohadjanjePredmeta>(HttpStatus.BAD_REQUEST);

	}
	
	@RequestMapping(path = "/{pohadjanjePredmetaId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<PohadjanjePredmeta> updatePohadjanje(@PathVariable("pohadjanjePredmetaId") Long pohadjanjePredmetaId, @RequestBody PohadjanjePredmeta izmenjenaPohadjanja) {
		
		PohadjanjePredmeta pohadjanje = pohadjanjePredmetaService.findOne(pohadjanjePredmetaId).orElse(null);
		
		if( pohadjanje != null) {
			izmenjenaPohadjanja.setId(pohadjanjePredmetaId);
			
			pohadjanjePredmetaService.save(izmenjenaPohadjanja);
			return new ResponseEntity<PohadjanjePredmeta>(izmenjenaPohadjanja, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<PohadjanjePredmeta>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{pohadjanjePredmetaId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<PohadjanjePredmeta> deletePohadjanje(@PathVariable("pohadjanjePredmetaId") Long pohadjanjePredmetaId) {
	
		if(pohadjanjePredmetaService.findOne(pohadjanjePredmetaId).isPresent()) {
			pohadjanjePredmetaService.delete(pohadjanjePredmetaId);
			return new ResponseEntity<PohadjanjePredmeta>(HttpStatus.OK);
		}
		
		return new ResponseEntity<PohadjanjePredmeta>(HttpStatus.NOT_FOUND);
	}
}
