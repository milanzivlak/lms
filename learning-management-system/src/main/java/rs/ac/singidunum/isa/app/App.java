package rs.ac.singidunum.isa.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;



@SpringBootApplication
public class App {
	
	public static void main(String [] args) {
		SpringApplication.run(App.class, args);
	}
}
