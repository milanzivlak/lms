package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Fajl;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.FajlRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class FajlService extends GenerateService<FajlRepository, Fajl, Long>{

	public Page<Fajl> findAll(Pageable pageable) {
		return ((FajlRepository) this.getRepository()).findAll(pageable);
	}

}
