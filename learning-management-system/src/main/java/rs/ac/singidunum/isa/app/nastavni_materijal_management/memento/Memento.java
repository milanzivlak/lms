package rs.ac.singidunum.isa.app.nastavni_materijal_management.memento;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;

@Entity
public class Memento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(cascade = CascadeType.ALL)
	private NastavniMaterijal state;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Caretaker caretaker;
	
	
	public Memento() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Memento(Long id, NastavniMaterijal state) {
		super();
		this.id = id;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NastavniMaterijal getState() {
		return state;
	}

	public void setState(NastavniMaterijal state) {
		this.state = state;
	}

	public Caretaker getCaretaker() {
		return caretaker;
	}

	public void setCaretaker(Caretaker caretaker) {
		this.caretaker = caretaker;
	}
	
	
	
	
}
