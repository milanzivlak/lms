package rs.ac.singidunum.isa.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PohadjanjePredmeta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// Mozda ograniciti od 5-10
	private int konacnaOcena;
	
	@ManyToOne(optional = false)
	private Student student;
	
	@OneToOne()
	private RealizacijaPredmeta realizacijaPredmeta;

	public PohadjanjePredmeta() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PohadjanjePredmeta(Long id, int konacnaOcena) {
		super();
		this.id = id;
		this.konacnaOcena = konacnaOcena;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getKonacnaOcena() {
		return konacnaOcena;
	}

	public void setKonacnaOcena(int konacnaOcena) {
		this.konacnaOcena = konacnaOcena;
	}

	public RealizacijaPredmeta getRealizacijaPredmeta() {
		return realizacijaPredmeta;
	}

	public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
		this.realizacijaPredmeta = realizacijaPredmeta;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	
	
}
