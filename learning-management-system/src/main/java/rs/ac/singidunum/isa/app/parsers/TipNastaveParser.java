package rs.ac.singidunum.isa.app.parsers;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import rs.ac.singidunum.isa.app.dto.TipNastaveDTO;
import rs.ac.singidunum.isa.app.dto.TipZvanjaDTO;


public class TipNastaveParser {
	
	private static final String SCHEMANAME = "tipNastave.xsd";
	public ArrayList<TipNastaveDTO> spisakNastava;
	public ArrayList<TipNastaveDTO> importovaneNastave;
	
	public TipNastaveParser(ArrayList<TipNastaveDTO> spisakNastava) {
		this.spisakNastava = spisakNastava;
	}
	
	public TipNastaveParser() {
		
	}
	
	public void export() throws IOException, ParserConfigurationException, TransformerException {
		//root
		
		DocumentBuilderFactory dbFactory =
		         DocumentBuilderFactory.newInstance();
		         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		         Document doc = (Document) dBuilder.newDocument();
		         
		 Element tipovi = doc.createElement("tipoviNastave");
		doc.appendChild(tipovi);
		for(TipNastaveDTO d : this.spisakNastava) {
			Element tip = doc.createElement("tipNastave");
			
			Attr attribute = doc.createAttribute("id");
			attribute.setValue(d.getId().toString());
			tip.setAttributeNode(attribute);
			
			Element naziv = doc.createElement("naziv");
			naziv.appendChild(doc.createTextNode(d.getNaziv()));
			tip.appendChild(naziv);
			
			tipovi.appendChild(tip);
		}
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Schema schema = loadSchema(SCHEMANAME);
		
		if(validateXml(schema, doc)) {
			DOMSource source = new DOMSource((Node) doc);
			String home = System.getProperty("user.home");
			File file = new File(home+"/Downloads/" + "tipNastave" + ".xml");
			StreamResult result = new StreamResult(file);
			//StreamResult result =  new StreamResult(new File("C:\\Users\\Korisnik\\Desktop\\xml\\drzave.xml"));
			transformer.transform(source, result);
			StreamResult consoleResult = new StreamResult(System.out);
	        transformer.transform(source, consoleResult);
		}else {
			System.out.println("Neispravan format za export!");
		}
	}
	
public ArrayList<TipNastaveDTO> importXml(String s) {
		
		this.importovaneNastave = new ArrayList<TipNastaveDTO>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			Document doc = convertStringToXMLDocument(s);
			Schema schema = loadSchema(SCHEMANAME);
			if(validateXml(schema, doc)) {
				doc.getDocumentElement().normalize();
				
				System.out.println("Root element:" + doc.getDocumentElement().getNodeName());
				
				NodeList list = doc.getElementsByTagName("tipNastave");
				
				for(int temp = 0; temp < list.getLength(); temp++) {
					Node node = list.item(temp);
					
					if(node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						
						String tipNastaveId = element.getAttribute("id");
						String tipNastaveNaziv = element.getElementsByTagName("naziv").item(0).getTextContent();
						
						TipNastaveDTO dto = new TipNastaveDTO();
						dto.setId(Long.parseLong(tipNastaveId));
						dto.setNaziv(tipNastaveNaziv);
						
						System.out.println("Element: " + node.getNodeName());
						System.out.println("Tip nastave ID: " + tipNastaveId);
						System.out.println("Naziv tip nastave: " + tipNastaveNaziv);
						
						this.importovaneNastave.add(dto);
					}
				}
				
				return importovaneNastave;
				
			} else {
				System.out.println("XML dokument nije ispravan!");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Schema loadSchema(String schemaFileName) {
		Schema schema = null;
		try {
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			
			schema = factory.newSchema(new File(schemaFileName));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return schema;
	}
	
	public static Document parseXmlDom(String xmlName) {
		
		Document document = null;
		try {
		   DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		   DocumentBuilder builder = factory.newDocumentBuilder();
		   document = builder.parse(new File(xmlName));
		} catch (Exception e) {
		   System.out.println(e.toString());
		}
		return document;
	}
	
	public static boolean validateXml(Schema schema, Document document) {
		try {
			Validator validator = schema.newValidator();
			
			validator.validate(new DOMSource(document));
			System.out.println("Validacija uspjesna!");
			return true;
			
		} catch (Exception e) {
			System.out.println("Validacije neuspjesna!");
			return false;
		}
		
	}
	
	public static Document convertStringToXMLDocument(String xmlString) 
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
             
            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return null;
    }

}
