package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Mesto;

public class DrzavaDTO {
	
	private Long id;
	private String naziv;
	
	private List<MestoDTO> mesta = new ArrayList<MestoDTO>();

	public DrzavaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DrzavaDTO(Long id, String naziv) {
		this(id, naziv, null);
	}

	public DrzavaDTO(Long id, String naziv, ArrayList<MestoDTO> mesta) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.mesta = mesta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<MestoDTO> getMesta() {
		return mesta;
	}

	public void setMesta(List<MestoDTO> tempMesta) {
		this.mesta = tempMesta;
	}
	
	public static DrzavaDTO fromDrzava(Drzava drzava) {
		DrzavaDTO temp = new DrzavaDTO();
		temp.setId(drzava.getId());
		temp.setNaziv(drzava.getNaziv());
		
//		List<MestoDTO> tempMesta = new ArrayList();
//		for(Mesto m: drzava.getMesta()) {
//			MestoDTO mDto = MestoDTO.fromMesto(m);
//			tempMesta.add(mDto);
//		} 
		//for petlja je zamenjena sa stream i map funkcijom
		
		List<MestoDTO> tempMesta = drzava.getMesta()
				.stream()
				.map(m -> MestoDTO.fromMesto(m))
				.collect(Collectors.toList());
		temp.setMesta(tempMesta);
		return temp;
	}
	
}
