package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Caretaker;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.CaretakerRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class CaretakerService extends GenerateService<CaretakerRepository, Caretaker, Long>{

}
