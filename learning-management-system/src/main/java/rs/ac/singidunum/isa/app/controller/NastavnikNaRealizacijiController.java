package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikNaRealizacijiDTO;
import rs.ac.singidunum.isa.app.dto.RealizacijaPredmetaDTO;
import rs.ac.singidunum.isa.app.dto.TipNastaveDTO;
import rs.ac.singidunum.isa.app.model.NastavnikNaRealizaciji;
import rs.ac.singidunum.isa.app.model.RealizacijaPredmeta;
import rs.ac.singidunum.isa.app.service.NastavnikNaRealizacijiService;

@Controller
@RequestMapping(path = "/api/nastavniknarealizaciji")
@PreAuthorize("hasAuthority('KORISNIK')")
@CrossOrigin
public class NastavnikNaRealizacijiController {
	
	@Autowired
	private NastavnikNaRealizacijiService nastavnikNaRealizacijiService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<NastavnikNaRealizacijiDTO>> getAllNastavnici() {
		
		
		ArrayList<NastavnikNaRealizacijiDTO> nastavnici = new ArrayList<NastavnikNaRealizacijiDTO>();
		
		for(NastavnikNaRealizaciji nnr : nastavnikNaRealizacijiService.findAll()) {
			NastavnikDTO nastavnik = new NastavnikDTO(nnr.getNastavnik().getId(), nnr.getNastavnik().getIme(), nnr.getNastavnik().getBiografija(), nnr.getNastavnik().getJmbg());
			TipNastaveDTO tipNastave = new TipNastaveDTO(nnr.getTipNastave().getId(), nnr.getTipNastave().getNaziv());
			
			ArrayList<RealizacijaPredmetaDTO> realizacijaPredmeta = new ArrayList<RealizacijaPredmetaDTO>();
			
			for(RealizacijaPredmeta rp : nnr.getRealizacijePredmeta()) {
				realizacijaPredmeta.add(new RealizacijaPredmetaDTO(rp.getId()));
			}
			
			nastavnici.add(new NastavnikNaRealizacijiDTO(nnr.getId(), nnr.getBrojCasova(), null, tipNastave, realizacijaPredmeta));
		}
		
		return new ResponseEntity<Iterable<NastavnikNaRealizacijiDTO>>(nastavnici, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{nastavnikNaRealizacijiId}", method = RequestMethod.GET)
	public ResponseEntity<NastavnikNaRealizacijiDTO> getNastavnik(@PathVariable("nastavnikNaRealizacijiId") Long nastavnikNaRealizacijiId) {
		
		Optional<NastavnikNaRealizaciji> nastavnikNaRealizaciji = nastavnikNaRealizacijiService.findOne(nastavnikNaRealizacijiId);
		
		if(nastavnikNaRealizaciji.isPresent()) {
			NastavnikNaRealizacijiDTO nastavnikNaRealizacijiDTO = new NastavnikNaRealizacijiDTO(nastavnikNaRealizaciji.get().getId(), nastavnikNaRealizaciji.get().getBrojCasova());
			return new ResponseEntity<NastavnikNaRealizacijiDTO>(nastavnikNaRealizacijiDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<NastavnikNaRealizacijiDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<NastavnikNaRealizaciji> createNastavnik(@RequestBody NastavnikNaRealizaciji nastavnikNaRealizaciji){
		
		try {
			
			nastavnikNaRealizacijiService.save(nastavnikNaRealizaciji);
			
			return new ResponseEntity<NastavnikNaRealizaciji>(nastavnikNaRealizaciji, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<NastavnikNaRealizaciji>(HttpStatus.BAD_REQUEST);

		
	}
	
	@RequestMapping(path = "/{nastavnikNaRealizacijiId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<NastavnikNaRealizaciji> updateNastavnik(@PathVariable("nastavnikNaRealizacijiId") Long nastavnikNaRealizacijiId, @RequestBody NastavnikNaRealizaciji izmenjeniNastavnik) {
		
		NastavnikNaRealizaciji nnr = nastavnikNaRealizacijiService.findOne(nastavnikNaRealizacijiId).orElse(null);
		
		if( nnr != null) {
			izmenjeniNastavnik.setId(nastavnikNaRealizacijiId);
			
			nastavnikNaRealizacijiService.save(izmenjeniNastavnik);
			return new ResponseEntity<NastavnikNaRealizaciji>(izmenjeniNastavnik, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<NastavnikNaRealizaciji>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{nastavnikNaRealizacijiId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<NastavnikNaRealizaciji> deleteNastavnik(@PathVariable("nastavnikNaRealizacijiId") Long nastavnikNaRealizacijiId) {
	
		if(nastavnikNaRealizacijiService.findOne(nastavnikNaRealizacijiId).isPresent()) {
			nastavnikNaRealizacijiService.delete(nastavnikNaRealizacijiId);
			return new ResponseEntity<NastavnikNaRealizaciji>(HttpStatus.OK);
		}
		
		return new ResponseEntity<NastavnikNaRealizaciji>(HttpStatus.NOT_FOUND);
	}
	
	
}
