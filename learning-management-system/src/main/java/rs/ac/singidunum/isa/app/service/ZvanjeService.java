package rs.ac.singidunum.isa.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.Zvanje;
import rs.ac.singidunum.isa.app.repository.ZvanjeRepository;

@Service
public class ZvanjeService extends GenerateService<ZvanjeRepository, Zvanje, Long>{
	
	public List<Nastavnik> findByNaziv(String tipZvanja){
		return this.repository.pronadjiPoZvanju(tipZvanja);
	}
}
