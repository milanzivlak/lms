package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.EvaluacijaZnanjaDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.TerminNastaveDTO;

public class RealizacijaPredmetaDTO {
	
	private Long id;
	
	private PohadjanjePredmetaDTO pohadjanjePredmeta;
	private PredmetDTO predmet;
	
	private ArrayList<EvaluacijaZnanjaDTO> evaluacijaZnanja = new ArrayList<EvaluacijaZnanjaDTO>();
	
	private NastavnikNaRealizacijiDTO nastavnikNaRealizaciji;
	
	private ArrayList<TerminNastaveDTO> terminNastave = new ArrayList<TerminNastaveDTO>();
	
	
	
	public RealizacijaPredmetaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public RealizacijaPredmetaDTO(Long id) {
		this(id, null, null, null, null, null);
	}

	public RealizacijaPredmetaDTO(Long id, PohadjanjePredmetaDTO pohadjanjePredmeta, PredmetDTO predmet, ArrayList<EvaluacijaZnanjaDTO> evaluacijaZnanja, NastavnikNaRealizacijiDTO nastavnikNaRealizaciji, ArrayList<TerminNastaveDTO> terminNastave) {
		super();
		this.id = id;
		this.pohadjanjePredmeta = pohadjanjePredmeta;
		this.predmet = predmet;
		this.evaluacijaZnanja = evaluacijaZnanja;
		this.nastavnikNaRealizaciji = nastavnikNaRealizaciji;
		this.terminNastave = terminNastave;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PohadjanjePredmetaDTO getPohadjanjePredmeta() {
		return pohadjanjePredmeta;
	}

	public void setPohadjanjePredmeta(PohadjanjePredmetaDTO pohadjanjePredmeta) {
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}

	public PredmetDTO getPredmet() {
		return predmet;
	}

	public void setPredmet(PredmetDTO predmet) {
		this.predmet = predmet;
	}

	public ArrayList<EvaluacijaZnanjaDTO> getEvaluacijaZnanja() {
		return evaluacijaZnanja;
	}

	public void setEvaluacijaZnanja(ArrayList<EvaluacijaZnanjaDTO> evaluacijaZnanja) {
		this.evaluacijaZnanja = evaluacijaZnanja;
	}

	public NastavnikNaRealizacijiDTO getNastavnikNaRealizaciji() {
		return nastavnikNaRealizaciji;
	}

	public void setNastavnikNaRealizaciji(NastavnikNaRealizacijiDTO nastavnikNaRealizaciji) {
		this.nastavnikNaRealizaciji = nastavnikNaRealizaciji;
	}

	public ArrayList<TerminNastaveDTO> getTerminNastave() {
		return terminNastave;
	}

	public void setTerminNastave(ArrayList<TerminNastaveDTO> terminNastave) {
		this.terminNastave = terminNastave;
	}
	
}
