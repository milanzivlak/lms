package rs.ac.singidunum.isa.app.nastavni_materijal_management.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.InstrumentEvaluacijeDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.InstrumentEvaluacije;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.InstrumentEvaluacijeService;

@Controller
@RequestMapping(path = "/api/nastavni-materijal/instrument-evaluacije")
public class InstrumentEvaluacijeController {
	@Autowired
	private InstrumentEvaluacijeService instrumentEvaluacijeService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Page<InstrumentEvaluacijeDTO>> getAllInstrumentEvaluacije(Pageable pageable) {
		return new ResponseEntity<Page<InstrumentEvaluacijeDTO>>(instrumentEvaluacijeService.findAll(pageable).map(a -> new InstrumentEvaluacijeDTO(a)), HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{InstrumentEvaluacijeId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<InstrumentEvaluacijeDTO> getAutor(@PathVariable("autorId") Long autorId) {
		
		Optional<InstrumentEvaluacije> instrumentEvaluacije = instrumentEvaluacijeService.findOne(autorId);
		
		if(instrumentEvaluacije.isPresent()) {
			InstrumentEvaluacijeDTO InstrumentEvaluacijeDTO = new InstrumentEvaluacijeDTO(instrumentEvaluacije.get());
			return new ResponseEntity<InstrumentEvaluacijeDTO>(InstrumentEvaluacijeDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<InstrumentEvaluacijeDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<InstrumentEvaluacije> createInstrumentEvaluacije(@RequestBody InstrumentEvaluacije InstrumentEvaluacije){
		
		try {
			
			instrumentEvaluacijeService.save(InstrumentEvaluacije);
			
			return new ResponseEntity<InstrumentEvaluacije>(InstrumentEvaluacije, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<InstrumentEvaluacije>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{InstrumentEvaluacijeId}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<InstrumentEvaluacije> updateInstrumentEvaluacije(@PathVariable("InstrumentEvaluacijeId") Long InstrumentEvaluacijeId, @RequestBody InstrumentEvaluacije izmenjenInstrumentEvaluacije) {
		
		InstrumentEvaluacije adresa = instrumentEvaluacijeService.findOne(InstrumentEvaluacijeId).orElse(null);
		
		if( adresa != null) {
			izmenjenInstrumentEvaluacije.setId(InstrumentEvaluacijeId);
			
			instrumentEvaluacijeService.save(izmenjenInstrumentEvaluacije);
			return new ResponseEntity<InstrumentEvaluacije>(izmenjenInstrumentEvaluacije, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<InstrumentEvaluacije>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{InstrumentEvaluacijeId}", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<InstrumentEvaluacije> deleteInstrumentEvaluacije(@PathVariable("InstrumentEvaluacijeId") Long InstrumentEvaluacijeId) {
	
		if(instrumentEvaluacijeService.findOne(InstrumentEvaluacijeId).isPresent()) {
			instrumentEvaluacijeService.delete(InstrumentEvaluacijeId);
			return new ResponseEntity<InstrumentEvaluacije>(HttpStatus.OK);
		}
		
		return new ResponseEntity<InstrumentEvaluacije>(HttpStatus.NOT_FOUND);
	}

}
