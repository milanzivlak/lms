package rs.ac.singidunum.isa.app.nastavni_materijal_management.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.ObrazovniCilj;

@Repository
public interface ObrazovniCiljRepository extends PagingAndSortingRepository<ObrazovniCilj, Long>{


}
