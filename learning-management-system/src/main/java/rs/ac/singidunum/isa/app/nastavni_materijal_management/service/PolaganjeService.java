package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.PolaganjeDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Polaganje;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.PolaganjeRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class PolaganjeService extends GenerateService<PolaganjeRepository, Polaganje, Long>{

	public Page<Polaganje> findAll(Pageable pageable) {
		return ((PolaganjeRepository) this.getRepository()).findAll(pageable);
	}

}
