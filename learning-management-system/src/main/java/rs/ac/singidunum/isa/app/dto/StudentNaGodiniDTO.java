package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.Date;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.PolaganjeDTO;

public class StudentNaGodiniDTO {
	
	private Long id;
	private Date datumUpisa;
	private String brojIndeksa;
	
	private StudentDTO student;
	private GodinaStudijaDTO godinaStudija;
	
	private ArrayList<PolaganjeDTO> polaganja = new ArrayList<PolaganjeDTO>();
	
	public StudentNaGodiniDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public StudentNaGodiniDTO(Long id, Date datumUpisa, String brojIndeksa) {
		this(id, datumUpisa, brojIndeksa, null, null, null);
	}

	public StudentNaGodiniDTO(Long id, Date datumUpisa, String brojIndeksa, StudentDTO student,
			GodinaStudijaDTO godinaStudija, ArrayList<PolaganjeDTO> polaganja) {
		super();
		this.id = id;
		this.datumUpisa = datumUpisa;
		this.brojIndeksa = brojIndeksa;
		this.student = student;
		this.godinaStudija = godinaStudija;
		this.polaganja = polaganja;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatumUpisa() {
		return datumUpisa;
	}

	public void setDatumUpisa(Date datumUpisa) {
		this.datumUpisa = datumUpisa;
	}

	public String getBrojIndeksa() {
		return brojIndeksa;
	}

	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public GodinaStudijaDTO getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(GodinaStudijaDTO godinaStudija) {
		this.godinaStudija = godinaStudija;
	}

	public ArrayList<PolaganjeDTO> getPolaganja() {
		return polaganja;
	}

	public void setPolaganja(ArrayList<PolaganjeDTO> polaganja) {
		this.polaganja = polaganja;
	}
	
}
