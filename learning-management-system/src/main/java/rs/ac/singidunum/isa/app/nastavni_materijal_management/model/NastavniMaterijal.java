package rs.ac.singidunum.isa.app.nastavni_materijal_management.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import rs.ac.singidunum.isa.app.model.Ishod;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Caretaker;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Originator;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Stanje;

@Entity
public class NastavniMaterijal {
	
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String naziv;
	
	@ManyToMany
	private Set<Autor> autori = new HashSet<Autor>();
	
	@Temporal(TemporalType.DATE)
	private Date godinaIzdanja;
	
	
	@ManyToMany
	private Set<Fajl> fajlovi = new HashSet<Fajl>();
	
	@OneToMany(mappedBy = "nastavniMaterijal")
	private Set<Ishod> ishodi = new HashSet<Ishod>();
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datumAzuriranja;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datumUklanjanja;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Stanje stanje;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Caretaker caretaker;
	
	@Version
	private Long version;

	
	@OneToOne(cascade = CascadeType.ALL)
	private Originator istorija;
	

	public NastavniMaterijal() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public NastavniMaterijal(Long id, String naziv, Date godinaIzdanja, Long version,
			Date datumAzuriranja, Date datumUklanjanja) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.godinaIzdanja = godinaIzdanja;
		this.version = version;
		this.datumAzuriranja = datumAzuriranja;
		this.datumUklanjanja = datumUklanjanja;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<Autor> getAutori() {
		return autori;
	}

	public void setAutori(Set<Autor> autori) {
		this.autori = autori;
	}

	public Date getGodinaIzdanja() {
		return godinaIzdanja;
	}

	public void setGodinaIzdanja(Date godinaIzdanja) {
		this.godinaIzdanja = godinaIzdanja;
	}

	public Set<Fajl> getFajlovi() {
		return fajlovi;
	}

	public void setFajlovi(Set<Fajl> fajlovi) {
		this.fajlovi = fajlovi;
	}

	public Set<Ishod> getIshodi() {
		return ishodi;
	}

	public void setIshodi(Set<Ishod> ishodi) {
		this.ishodi = ishodi;
	}
	
	public Originator getIstorija() {
		return istorija;
	}

	public void setIstorija(Originator istorija) {
		this.istorija = istorija;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getDatumAzuriranja() {
		return datumAzuriranja;
	}

	public void setDatumAzuriranja(Date datumAzuriranja) {
		this.datumAzuriranja = datumAzuriranja;
	}

	public Date getDatumUklanjanja() {
		return datumUklanjanja;
	}

	public void setDatumUklanjanja(Date datumUklanjanja) {
		this.datumUklanjanja = datumUklanjanja;
	}

	public Stanje getStanje() {
		return stanje;
	}

	public void setStanje(Stanje stanje) {
		this.stanje = stanje;
	}

	public Caretaker getCaretaker() {
		return caretaker;
	}

	public void setCaretaker(Caretaker caretaker) {
		this.caretaker = caretaker;
	}
	
	
	
	
	
	
	
	
	
}
