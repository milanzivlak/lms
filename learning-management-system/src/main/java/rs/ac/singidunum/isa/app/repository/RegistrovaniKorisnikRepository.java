package rs.ac.singidunum.isa.app.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;

@Repository
public interface RegistrovaniKorisnikRepository extends CrudRepository<RegistrovaniKorisnik, Long>{
	Optional<RegistrovaniKorisnik>findByKorisnickoIme(String korisnickoIme);
}
