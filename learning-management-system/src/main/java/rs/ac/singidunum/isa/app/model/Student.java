package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;

import javax.persistence.Id;

@Entity
public class Student {

	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String jmbg;
	
	@Lob
	private String ime;
	
	@ManyToOne(optional = false)
	private Adresa adresa;
	
	@ManyToOne(optional = false)
	private RegistrovaniKorisnik registrovaniKorisnik;

	@OneToMany(mappedBy = "student")
	private Set<StudentNaGodini> godine = new HashSet<StudentNaGodini>();
	
	@OneToMany(mappedBy = "student")
	private Set<PohadjanjePredmeta> pohadjanja = new HashSet<PohadjanjePredmeta>();
	
	
	
	public Set<StudentNaGodini> getGodine() {
		return godine;
	}

	public void setGodine(Set<StudentNaGodini> godine) {
		this.godine = godine;
	}
	

	public RegistrovaniKorisnik getRegistrovaniKorisnik() {
		return registrovaniKorisnik;
	}

	public void setRegistrovaniKorisnik(RegistrovaniKorisnik registrovaniKorisnik) {
		this.registrovaniKorisnik = registrovaniKorisnik;
	}

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(Long id, String jmbg, String ime) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}


	public Set<PohadjanjePredmeta> getPohadjanja() {
		return pohadjanja;
	}

	public void setPohadjanja(Set<PohadjanjePredmeta> pohadjanja) {
		this.pohadjanja = pohadjanja;
	}
	
	
	
}
