package rs.ac.singidunum.isa.app.repository;

import java.util.List;

import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.Zvanje;

@Primary
@Repository("NastavnikRepository")
public interface NastavnikRepository extends CrudRepository<Nastavnik, Long>{
	
	@Query("SELECT a FROM Zvanje a WHERE a.naucnaOblast.naziv = :naucnaOblast")
	List<Zvanje> pronadjiPrethodnaZvanja(String naucnaOblast);
}
