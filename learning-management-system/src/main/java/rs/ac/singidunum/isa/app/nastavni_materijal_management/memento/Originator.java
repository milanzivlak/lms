package rs.ac.singidunum.isa.app.nastavni_materijal_management.memento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;

@Entity
public class Originator {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(mappedBy = "istorija")
	@JsonIgnore
	private NastavniMaterijal state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NastavniMaterijal getState() {
		return state;
	}

	public void setState(NastavniMaterijal state) {
		this.state = state;
	}
	
	public Memento saveStateToMemento() {
		return new Memento(null, state);
	}
	
	public void getStateFromMemento(Memento memento) {
		this.state = memento.getState();
	}

	public Originator() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Originator(Long id) {
		super();
		this.id = id;
	}
	
	
	
	
}
