package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.model.TipNastave;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.TerminNastaveDTO;

public class TipNastaveDTO {
	
	private Long id;
	private String naziv;
	
	private ArrayList<NastavnikNaRealizacijiDTO> realizacije = new ArrayList<NastavnikNaRealizacijiDTO>();
	
	private ArrayList<TerminNastaveDTO> terminNastave = new ArrayList<TerminNastaveDTO>();

	public TipNastaveDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TipNastaveDTO(Long id, String naziv) {
		this(id, naziv, null, null);
	}

	public TipNastaveDTO(Long id, String naziv, ArrayList<NastavnikNaRealizacijiDTO> realizacije, ArrayList<TerminNastaveDTO> terminNastave) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.realizacije = realizacije;
		this.terminNastave = terminNastave;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public ArrayList<NastavnikNaRealizacijiDTO> getRealizacije() {
		return realizacije;
	}

	public void setRealizacije(ArrayList<NastavnikNaRealizacijiDTO> realizacije) {
		this.realizacije = realizacije;
	}

	public ArrayList<TerminNastaveDTO> getTerminNastave() {
		return terminNastave;
	}

	public void setTerminNastave(ArrayList<TerminNastaveDTO> terminNastave) {
		this.terminNastave = terminNastave;
	}
	
	public static TipNastaveDTO fromTipNastave(TipNastave tipNastave) {
		TipNastaveDTO temp = new TipNastaveDTO();
		temp.setId(tipNastave.getId());
		temp.setNaziv(tipNastave.getNaziv());
		
		return temp;
		
	}
}
