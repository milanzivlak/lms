package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Memento;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;

public class OriginatorDTO {
	
	
	private Long id;
	
	private NastavniMaterijalDTO state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NastavniMaterijalDTO getState() {
		return state;
	}

	public void setState(NastavniMaterijalDTO state) {
		this.state = state;
	}

	public OriginatorDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OriginatorDTO(Long id) {
		this(id, null);
	}

	public OriginatorDTO(Long id, NastavniMaterijalDTO state) {
		super();
		this.id = id;
		this.state = state;
	}
	
	
	
	
}
