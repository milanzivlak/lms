package rs.ac.singidunum.isa.app.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Polaganje;

import javax.persistence.Id;

@Entity
public class StudentNaGodini {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datumUpisa;
	
	@Lob
	private String brojIndeksa;
	
	
	@ManyToOne(optional = false)
	private GodinaStudija godinaStudija;
	
	@ManyToOne(optional = false)
	private Student student;
	
	@OneToMany(mappedBy = "studentNaGodini")
	private Set<Polaganje> polaganja = new HashSet<Polaganje>();
	
	
	public StudentNaGodini() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudentNaGodini(Long id, Date datumUpisa, String brojIndeksa) {
		super();
		this.id = id;
		this.datumUpisa = datumUpisa;
		this.brojIndeksa = brojIndeksa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatumUpisa() {
		return datumUpisa;
	}

	public void setDatumUpisa(Date datumUpisa) {
		this.datumUpisa = datumUpisa;
	}

	public String getBrojIndeksa() {
		return brojIndeksa;
	}

	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public GodinaStudija getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(GodinaStudija godinaStudija) {
		this.godinaStudija = godinaStudija;
	}

	public Set<Polaganje> getPolaganja() {
		return polaganja;
	}

	public void setPolaganja(Set<Polaganje> polaganja) {
		this.polaganja = polaganja;
	}
	
	
	
}
