package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Autor;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.AutorRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class AutorService extends GenerateService<AutorRepository, Autor, Long>{

	public Page<Autor> findAll(Pageable pageable) {
		return ((AutorRepository) this.getRepository()).findAll(pageable);
	}

}
