package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.StudentDTO;
import rs.ac.singidunum.isa.app.dto.StudentNaGodiniDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.StudentNaGodini;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.PolaganjeDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Polaganje;
import rs.ac.singidunum.isa.app.service.StudentNaGodiniService;

@Controller
@RequestMapping(path = "/api/studentNaGodini")
@CrossOrigin
public class StudentNaGodiniController {
	
	@Autowired
	private StudentNaGodiniService studentNaGodiniService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Iterable<StudentNaGodiniDTO>> getAllAdrese() {
		
		
		ArrayList<StudentNaGodiniDTO> studentiNaGodini = new ArrayList<StudentNaGodiniDTO>();
		
		for(StudentNaGodini studentNaGodini : studentNaGodiniService.findAll()) {
		ArrayList<PolaganjeDTO> polaganja = new ArrayList<PolaganjeDTO>();
		
		for (Polaganje polaganje : studentNaGodini.getPolaganja()) {
			polaganja.add(new PolaganjeDTO(polaganje.getId(), polaganje.getBodovi(), polaganje.getNapomena()));
		}
		
		StudentDTO student = new StudentDTO(studentNaGodini.getStudent().getId(), studentNaGodini.getStudent().getJmbg(), studentNaGodini.getStudent().getIme());
		GodinaStudijaDTO godinaStudija = new GodinaStudijaDTO(studentNaGodini.getGodinaStudija().getId(), studentNaGodini.getGodinaStudija().getGodina());
			
			studentiNaGodini.add(new StudentNaGodiniDTO(studentNaGodini.getId(), studentNaGodini.getDatumUpisa(), studentNaGodini.getBrojIndeksa(), student, godinaStudija, polaganja));
		}
		
		return new ResponseEntity<Iterable<StudentNaGodiniDTO>>(studentiNaGodini, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{studentNaGodiniId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<StudentNaGodiniDTO> getStudentNaGodini(@PathVariable("studentNaGodiniId") Long studentNaGodiniId) {
		
		Optional<StudentNaGodini> studentNaGodini = studentNaGodiniService.findOne(studentNaGodiniId);
		
		if(studentNaGodini.isPresent()) {
			StudentNaGodiniDTO studentNaGodiniDTO = new StudentNaGodiniDTO(studentNaGodini.get().getId(), studentNaGodini.get().getDatumUpisa(), studentNaGodini.get().getBrojIndeksa());
			return new ResponseEntity<StudentNaGodiniDTO>(studentNaGodiniDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<StudentNaGodiniDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<StudentNaGodini> createStudentNaGodini(@RequestBody StudentNaGodini studentNaGodini){
		
		try {
			
			studentNaGodiniService.save(studentNaGodini);
			
			return new ResponseEntity<StudentNaGodini>(studentNaGodini, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<StudentNaGodini>(HttpStatus.BAD_REQUEST);

		
	}
	
	@RequestMapping(path = "/{studentNaGodiniId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<StudentNaGodini> updateStudentNaGodini(@PathVariable("studentNaGodiniId") Long studentNaGodiniId, @RequestBody StudentNaGodini izmenjenStudent) {
		
		StudentNaGodini studentNaGodini = studentNaGodiniService.findOne(studentNaGodiniId).orElse(null);
		
		if( studentNaGodini != null) {
			izmenjenStudent.setId(studentNaGodiniId);
			
			studentNaGodiniService.save(izmenjenStudent);
			return new ResponseEntity<StudentNaGodini>(izmenjenStudent, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<StudentNaGodini>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{studentNaGodiniId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<StudentNaGodini> deleteStudentNaGodini(@PathVariable("studentNaGodiniId") Long studentNaGodiniId) {
	
		if(studentNaGodiniService.findOne(studentNaGodiniId).isPresent()) {
			studentNaGodiniService.delete(studentNaGodiniId);
			return new ResponseEntity<StudentNaGodini>(HttpStatus.OK);
		}
		
		return new ResponseEntity<StudentNaGodini>(HttpStatus.NOT_FOUND);
	}
	
}
