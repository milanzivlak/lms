package rs.ac.singidunum.isa.app.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;
import rs.ac.singidunum.isa.app.model.UserPermission;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	RegistrovaniKorisnikService registrovaniKorisnikService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<RegistrovaniKorisnik> korisnik = registrovaniKorisnikService.findByKorisnickoIme(username);
		
		if(korisnik.isPresent()) {
			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			
			for(UserPermission userPermission: korisnik.get().getUserPermissions()) {
				grantedAuthorities.add(new SimpleGrantedAuthority(userPermission.getPermission().getPermissionType().name()));
			}
			return new org.springframework.security.core.userdetails.User(korisnik.get().getKorisnickoIme(), korisnik.get().getLozinka(), grantedAuthorities);
		}
		
		return null;
	
	
	}
	
	

}
