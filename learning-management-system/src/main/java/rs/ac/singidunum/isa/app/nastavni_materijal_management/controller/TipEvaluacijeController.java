package rs.ac.singidunum.isa.app.nastavni_materijal_management.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.TipEvaluacijeDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TipEvaluacije;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.TipEvaluacijeService;

@Controller
@RequestMapping(path = "/api/nastavni-materijal/tip-evaluacije")
public class TipEvaluacijeController {
	@Autowired
	private TipEvaluacijeService tipEvaluacijeService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Page<TipEvaluacijeDTO>> getAllTipEvaluacijei(Pageable pageable) {
		return new ResponseEntity<Page<TipEvaluacijeDTO>>(
				tipEvaluacijeService.findAll(pageable).map(a -> new TipEvaluacijeDTO(a)), HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{tipEvaluacijeId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<TipEvaluacijeDTO> getTipEvaluacije(@PathVariable("tipEvaluacijeId") Long tipEvaluacijeId) {
		
		Optional<TipEvaluacije> tipEvaluacije = tipEvaluacijeService.findOne(tipEvaluacijeId);
		
		if(tipEvaluacije.isPresent()) {
			TipEvaluacijeDTO TipEvaluacijeDTO = new TipEvaluacijeDTO(tipEvaluacije.get());
			return new ResponseEntity<TipEvaluacijeDTO>(TipEvaluacijeDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<TipEvaluacijeDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<TipEvaluacije> createTipEvaluacije(@RequestBody TipEvaluacije tipEvaluacije){
		
		try {
			
			tipEvaluacijeService.save(tipEvaluacije);
			
			return new ResponseEntity<TipEvaluacije>(tipEvaluacije, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<TipEvaluacije>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{tipEvaluacijeId}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<TipEvaluacije> updateTipEvaluacije(@PathVariable("tipEvaluacijeId") Long tipEvaluacijeId, @RequestBody TipEvaluacije izmenjenTipEvaluacije) {
		
		TipEvaluacije tipEvaluacije = tipEvaluacijeService.findOne(tipEvaluacijeId).orElse(null);
		
		if( tipEvaluacije != null) {
			izmenjenTipEvaluacije.setId(tipEvaluacijeId);
			
			tipEvaluacijeService.save(izmenjenTipEvaluacije);
			return new ResponseEntity<TipEvaluacije>(izmenjenTipEvaluacije, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<TipEvaluacije>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{tipEvaluacijeId}", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<TipEvaluacije> deleteTipEvaluacije(@PathVariable("tipEvaluacijeId") Long tipEvaluacijeId) {
	
		if(tipEvaluacijeService.findOne(tipEvaluacijeId).isPresent()) {
			tipEvaluacijeService.delete(tipEvaluacijeId);
			return new ResponseEntity<TipEvaluacije>(HttpStatus.OK);
		}
		
		return new ResponseEntity<TipEvaluacije>(HttpStatus.NOT_FOUND);
	}

}
