package rs.ac.singidunum.isa.app.nastavni_materijal_management.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rs.ac.singidunum.isa.app.model.Ishod;
import rs.ac.singidunum.isa.app.model.RealizacijaPredmeta;

@Entity
public class EvaluacijaZnanja {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date vremePocetka;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date vremeZavrsetka;
	
	private int bodovi;
	
	@ManyToOne(optional = false)
	private TipEvaluacije tipEvaluacije;
	
	@OneToOne
	private Ishod ishod;
	
	@ManyToOne
	private RealizacijaPredmeta realizacijaPredmeta;
	
	@OneToOne
	private Polaganje polaganje;
	
	public Ishod getIshod() {
		return ishod;
	}

	public void setIshod(Ishod ishod) {
		this.ishod = ishod;
	}

	public TipEvaluacije getTipEvaluacije() {
		return tipEvaluacije;
	}

	public void setTipEvaluacije(TipEvaluacije tipEvaluacije) {
		this.tipEvaluacije = tipEvaluacije;
	}

	public EvaluacijaZnanja() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EvaluacijaZnanja(Long id, Date vremePocetka, Date vremeZavrsetka, int bodovi) {
		super();
		this.id = id;
		this.vremePocetka = vremePocetka;
		this.vremeZavrsetka = vremeZavrsetka;
		this.bodovi = bodovi;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getVremePocetka() {
		return vremePocetka;
	}

	public void setVremePocetka(Date vremePocetka) {
		this.vremePocetka = vremePocetka;
	}

	public Date getVremeZavrsetka() {
		return vremeZavrsetka;
	}

	public void setVremeZavrsetka(Date vremeZavrsetka) {
		this.vremeZavrsetka = vremeZavrsetka;
	}

	public int getBodovi() {
		return bodovi;
	}

	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}

	public RealizacijaPredmeta getRealizacijaPredmeta() {
		return realizacijaPredmeta;
	}

	public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
		this.realizacijaPredmeta = realizacijaPredmeta;
	}

	public Polaganje getPolaganje() {
		return polaganje;
	}

	public void setPolaganje(Polaganje polaganje) {
		this.polaganje = polaganje;
	}
	
	
	
	
}
