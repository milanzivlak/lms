package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.EvaluacijaZnanja;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TerminNastave;

import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class RealizacijaPredmeta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(mappedBy = "realizacijaPredmeta", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private PohadjanjePredmeta pohadjanjePredmeta;
	
	@OneToMany(mappedBy = "realizacijaPredmeta")
	private Set<EvaluacijaZnanja> evaluacijeZnanja = new HashSet<EvaluacijaZnanja>();
	
	@ManyToOne
	private NastavnikNaRealizaciji nastavnikNaRealizaciji;
	
	@OneToMany(mappedBy = "realizacijaPredmeta")
	private Set<TerminNastave> terminiNastave = new HashSet<TerminNastave>();
	
	
	public Set<EvaluacijaZnanja> getEvaluacijeZnanja() {
		return evaluacijeZnanja;
	}

	public void setEvaluacijeZnanja(Set<EvaluacijaZnanja> evaluacijeZnanja) {
		this.evaluacijeZnanja = evaluacijeZnanja;
	}

	@OneToOne
	private Predmet predmet;

	public RealizacijaPredmeta() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RealizacijaPredmeta(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PohadjanjePredmeta getPohadjanjePredmeta() {
		return pohadjanjePredmeta;
	}

	public void setPohadjanjePredmeta(PohadjanjePredmeta pohadjanjePredmeta) {
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}

	public Predmet getPredmet() {
		return predmet;
	}

	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}

	public NastavnikNaRealizaciji getNastavnikNaRealizaciji() {
		return nastavnikNaRealizaciji;
	}

	public void setNastavnikNaRealizaciji(NastavnikNaRealizaciji nastavnikNaRealizaciji) {
		this.nastavnikNaRealizaciji = nastavnikNaRealizaciji;
	}

	public Set<TerminNastave> getTerminiNastave() {
		return terminiNastave;
	}

	public void setTerminiNastave(Set<TerminNastave> terminiNastave) {
		this.terminiNastave = terminiNastave;
	}
	
	
	
	
	
	
}
