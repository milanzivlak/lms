package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;
import rs.ac.singidunum.isa.app.dto.IshodDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.RegistrovaniKorisnikDTO;
import rs.ac.singidunum.isa.app.dto.UserPermissionDTO;
import rs.ac.singidunum.isa.app.model.Ishod;
import rs.ac.singidunum.isa.app.model.UserPermission;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.EvaluacijaZnanjaDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.NastavniMaterijalDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.ObrazovniCiljDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.TerminNastaveDTO;
import rs.ac.singidunum.isa.app.service.RegistrovaniKorisnikService;

@Controller
@RequestMapping(path = "/api/registrovanikorisnici")
public class RegistrovaniKorisnikController {
	
	@Autowired
	private RegistrovaniKorisnikService registrovaniKorisnikService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<RegistrovaniKorisnikDTO>> getAllRegistrovaniKorisnici() {
		
		
		ArrayList<RegistrovaniKorisnikDTO> registrovani = new ArrayList<RegistrovaniKorisnikDTO>();
		for(RegistrovaniKorisnik rk : registrovaniKorisnikService.findAll()) {
			
			ArrayList<UserPermissionDTO> userPermissions = new ArrayList<UserPermissionDTO>();
			
			for(UserPermission up : rk.getUserPermissions()) {
				userPermissions.add(new UserPermissionDTO(up.getId()));
			}
			
			registrovani.add(new RegistrovaniKorisnikDTO(rk.getId(), rk.getKorisnickoIme(), rk.getLozinka(), rk.getEmail(), userPermissions));
		}
		return new ResponseEntity<Iterable<RegistrovaniKorisnikDTO>>(registrovani, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{regKorisnikId}", method = RequestMethod.GET)
	public ResponseEntity<RegistrovaniKorisnikDTO> getRegistrovan(@PathVariable("regKorisnikId") Long regKorisnikId) {
		
		Optional<RegistrovaniKorisnik> reg = registrovaniKorisnikService.findOne(regKorisnikId);
		
		
		if(reg.isPresent()) {
			
			RegistrovaniKorisnikDTO regKorisnikDTO = new RegistrovaniKorisnikDTO(reg.get().getId(), reg.get().getKorisnickoIme(), reg.get().getLozinka(), reg.get().getEmail());
			return new ResponseEntity<RegistrovaniKorisnikDTO>(regKorisnikDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<RegistrovaniKorisnikDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<RegistrovaniKorisnik> createRegKorisnik(@RequestBody RegistrovaniKorisnik registrovaniKorisnik){
		
		try {
			
			registrovaniKorisnikService.save(registrovaniKorisnik);
			
			return new ResponseEntity<RegistrovaniKorisnik>(registrovaniKorisnik, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<RegistrovaniKorisnik>(HttpStatus.BAD_REQUEST);
		
	}
	
	@RequestMapping(path = "/{regKorisnikId}", method = RequestMethod.PUT)
	public ResponseEntity<RegistrovaniKorisnik> updateRegKorisnik(@PathVariable("regKorisnikId") Long regKorisnikId, @RequestBody RegistrovaniKorisnik izmenjenReg) {
		
		RegistrovaniKorisnik regKorisnik = registrovaniKorisnikService.findOne(regKorisnikId).orElse(null);
		
		if( regKorisnik != null) {
			izmenjenReg.setId(regKorisnikId);
			
			registrovaniKorisnikService.save(izmenjenReg);
			return new ResponseEntity<RegistrovaniKorisnik>(izmenjenReg, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<RegistrovaniKorisnik>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{regKorisnikId}", method = RequestMethod.DELETE)
	public ResponseEntity<RegistrovaniKorisnik> deleteRegKorisnik(@PathVariable("regKorisnikId") Long regKorisnikId) {
	
		if(registrovaniKorisnikService.findOne(regKorisnikId).isPresent()) {
			registrovaniKorisnikService.delete(regKorisnikId);
			return new ResponseEntity<RegistrovaniKorisnik>(HttpStatus.OK);
		}
		
		return new ResponseEntity<RegistrovaniKorisnik>(HttpStatus.NOT_FOUND);
	}
	
}
