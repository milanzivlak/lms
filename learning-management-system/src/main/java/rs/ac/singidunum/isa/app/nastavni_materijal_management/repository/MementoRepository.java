package rs.ac.singidunum.isa.app.nastavni_materijal_management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Memento;

@Repository
public interface MementoRepository extends CrudRepository<Memento, Long>{

}
