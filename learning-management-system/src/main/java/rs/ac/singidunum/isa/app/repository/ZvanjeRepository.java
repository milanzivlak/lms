package rs.ac.singidunum.isa.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.Zvanje;

@Repository("ZvanjeRepository")
public interface ZvanjeRepository extends CrudRepository<Zvanje, Long> {
	
	@Query("SELECT a.nastavnik FROM Zvanje a WHERE a.tipZvanja.naziv = :tipZvanja")
	List<Nastavnik> pronadjiPoZvanju(String tipZvanja);
}
