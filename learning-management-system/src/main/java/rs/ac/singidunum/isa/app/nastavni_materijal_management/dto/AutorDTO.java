package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Autor;

public class AutorDTO {
	
	private Long id;
	private String ime;
	private String prezime;
	
	private ArrayList<NastavniMaterijalDTO> nastavniMaterijal = new ArrayList<NastavniMaterijalDTO>();

	public AutorDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public AutorDTO(Autor autor) {
		this(autor.getId(), autor.getIme(), autor.getPrezime());
	}
	
	public AutorDTO(Long id, String ime, String prezime) {
		this(id, ime, prezime, null);
	}

	public AutorDTO(Long id, String ime, String prezime, ArrayList<NastavniMaterijalDTO> nastavniMaterijal) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.nastavniMaterijal = nastavniMaterijal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public ArrayList<NastavniMaterijalDTO> getNastavniMaterijal() {
		return nastavniMaterijal;
	}

	public void setNastavniMaterijal(ArrayList<NastavniMaterijalDTO> nastavniMaterijal) {
		this.nastavniMaterijal = nastavniMaterijal;
	}

}
