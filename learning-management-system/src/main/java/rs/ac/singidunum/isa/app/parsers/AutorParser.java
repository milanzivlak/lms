package rs.ac.singidunum.isa.app.parsers;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;

import org.w3c.dom.Document;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.AutorDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Autor;

import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class AutorParser {
	
	private static final String SCHEMANAME = "autori.xsd";
	public ArrayList<AutorDTO> spisakAutora;
	public ArrayList<AutorDTO> importovaniAutori;
	
	public AutorParser(ArrayList<AutorDTO> spisakAutora) {
		this.spisakAutora = spisakAutora;
	}
	
	public AutorParser() {
		
	}
	public void export() throws IOException, ParserConfigurationException, TransformerException {
		//root
		
		DocumentBuilderFactory dbFactory =
		         DocumentBuilderFactory.newInstance();
		         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		         Document doc = (Document) dBuilder.newDocument();
		
        Element autori = doc.createElement("autori");
		doc.appendChild(autori);
		for(AutorDTO a : this.spisakAutora) {
			Element autor = doc.createElement("autor");
			
			Attr attribute = doc.createAttribute("id");
			attribute.setValue(a.getId().toString());
			autor.setAttributeNode(attribute);
			
			Element ime = doc.createElement("ime");
			ime.appendChild(doc.createTextNode(a.getIme()));
			autor.appendChild(ime);
			
			Element prezime = doc.createElement("prezime");
			prezime.appendChild(doc.createTextNode(a.getPrezime()));
			autor.appendChild(prezime);
			
			autori.appendChild(autor);
		}
		
		
		
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Schema schema = loadSchema(SCHEMANAME);
		
		if(validateXml(schema, doc)) {
			DOMSource source = new DOMSource((Node) doc);
			String home = System.getProperty("user.home");
			File file = new File(home+"/Downloads/" + "autori" + ".xml");
			StreamResult result = new StreamResult(file);
			//StreamResult result =  new StreamResult(new File("C:\\Users\\Korisnik\\Desktop\\xml\\drzave.xml"));
			transformer.transform(source, result);
			StreamResult consoleResult = new StreamResult(System.out);
	        transformer.transform(source, consoleResult);
		}else {
			System.out.println("Neispravan format za export!");
		}
		
	}
	
	public ArrayList<AutorDTO> importXml(String s) {
		
		this.importovaniAutori = new ArrayList<AutorDTO>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			
			// procces XML securely, avoid attacks like XML External Entities
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			
			DocumentBuilder db = dbf.newDocumentBuilder();

			
			Document doc = convertStringToXMLDocument(s);
			Schema schema = loadSchema(SCHEMANAME);
			if(validateXml(schema, doc)) {
				doc.getDocumentElement().normalize();
				
				System.out.println("Root element:" + doc.getDocumentElement().getNodeName());
				
				NodeList list = doc.getElementsByTagName("autor");
				
				for(int temp = 0; temp < list.getLength(); temp++) {
					
					Node node = list.item(temp);
					
					if(node.getNodeType() == Node.ELEMENT_NODE) {
						
						Element autor = (Element) node;
						
						String autorID = autor.getAttribute("id");
						String autorIme = autor.getElementsByTagName("ime").item(0).getTextContent();
						String autorPrezime = autor.getElementsByTagName("prezime").item(0).getTextContent();
						
						AutorDTO dto = new AutorDTO();
						dto.setId(Long.parseLong(autorID));
						dto.setIme(autorIme);
						dto.setPrezime(autorPrezime);
					
												
						this.importovaniAutori.add(dto);
					}
					
					
				}
				
				return importovaniAutori;
			} else {
				System.out.println("XML dokument nije ispravan!");
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	public static Schema loadSchema(String schemaFileName) {
		Schema schema = null;
		try {
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			
			schema = factory.newSchema(new File(schemaFileName));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return schema;
	}
	
	public static Document parseXmlDom(String xmlName) {
		
		Document document = null;
		try {
		   DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		   DocumentBuilder builder = factory.newDocumentBuilder();
		   document = builder.parse(new File(xmlName));
		} catch (Exception e) {
		   System.out.println(e.toString());
		}
		return document;
	}
	
	public static boolean validateXml(Schema schema, Document document) {
		try {
			Validator validator = schema.newValidator();
			
			validator.validate(new DOMSource(document));
			System.out.println("Validacija uspjesna!");
			return true;
			
		} catch (Exception e) {
			System.out.println("Validacije neuspjesna!");
			return false;
		}
		
	}
	
	public static Document convertStringToXMLDocument(String xmlString) 
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
             
            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return null;
    }
}
