package rs.ac.singidunum.isa.app.nastavni_materijal_management.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Autor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String ime;
	@Lob
	private String prezime;
	
	@ManyToMany
	@JsonIgnore
	private Set<NastavniMaterijal> nastavniMaterijali = new HashSet<NastavniMaterijal>();
	
	
	public Set<NastavniMaterijal> getNastavniMaterijali() {
		return nastavniMaterijali;
	}
	public void setNastavniMaterijali(Set<NastavniMaterijal> nastavniMaterijali) {
		this.nastavniMaterijali = nastavniMaterijali;
	}
	public Autor() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Autor(Long id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	
}

