package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Stanje;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.StanjeRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class StanjeService extends GenerateService<StanjeRepository, Stanje, Long>{

}
