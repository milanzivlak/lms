package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Mesto;

public class MestoDTO {
	
	private Long id;
	private String naziv;
	
	private DrzavaDTO drzava;
	
	private List<AdresaDTO> adrese = new ArrayList<AdresaDTO>();

	public MestoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public MestoDTO(Long id, String naziv) {
		this(id, naziv, null, null);
	}

	public MestoDTO(Long id, String naziv, DrzavaDTO drzava, List<AdresaDTO> adrese) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.drzava = drzava;
		this.adrese = adrese;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public DrzavaDTO getDrzava() {
		return drzava;
	}

	public void setDrzava(DrzavaDTO drzava) {
		this.drzava = drzava;
	}

	public List<AdresaDTO> getAdrese() {
		return adrese;
	}

	public void setAdrese(List<AdresaDTO> adrese) {
		this.adrese = adrese;
	}
	
	public static MestoDTO fromMesto(Mesto mesto) {
		return new MestoDTO(mesto.getId(), mesto.getNaziv());
	}
	
	public MestoDTO addAdressses(Set<Adresa> set) {
		List<AdresaDTO> adreseDto = set
				.stream()
				.map(a -> new AdresaDTO(a.getId(), a.getUlica(), a.getBroj()))
				.collect(Collectors.toList());
		
		this.setAdrese(adreseDto);
		return this;
	}
	
	public void addDrzava(Drzava drzava) {
		DrzavaDTO dto = new DrzavaDTO(drzava.getId(), drzava.getNaziv());
		this.setDrzava(dto);
	}
}
