package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class PermissionDTO {
	
	private Long id;
	private String naziv;
	
	private ArrayList<UserPermissionDTO> userPermissions = new ArrayList<UserPermissionDTO>();

	public PermissionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public PermissionDTO(Long id, String naziv) {
		this(id, naziv, null);
	}

	public PermissionDTO(Long id, String naziv, ArrayList<UserPermissionDTO> userPermissions) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.userPermissions = userPermissions;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public ArrayList<UserPermissionDTO> getUserPermissions() {
		return userPermissions;
	}

	public void setUserPermissions(ArrayList<UserPermissionDTO> userPermissions) {
		this.userPermissions = userPermissions;
	}
	
}
