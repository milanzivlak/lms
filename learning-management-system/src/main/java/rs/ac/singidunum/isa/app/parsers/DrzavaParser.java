package rs.ac.singidunum.isa.app.parsers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;

import org.w3c.dom.Document;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class DrzavaParser {
	
	private static final String FILENAME = "C:\\Users\\Korisnik\\Downloads\\drzave.xml";
	private static final String SCHEMANAME = "drzave.xsd";
	public ArrayList<DrzavaDTO> spisakDrzava;
	public ArrayList<DrzavaDTO> importovaneDrzave;
	
	public DrzavaParser(ArrayList<DrzavaDTO> spisakDrzava) {
		this.spisakDrzava = spisakDrzava;
	}
	
	public DrzavaParser() {
		
	}
	public void export() throws IOException, ParserConfigurationException, TransformerException {
		//root
		
		DocumentBuilderFactory dbFactory =
		         DocumentBuilderFactory.newInstance();
		         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		         Document doc = (Document) dBuilder.newDocument();
		
         Element drzave = doc.createElement("drzave");
		doc.appendChild(drzave);
		for(DrzavaDTO d : this.spisakDrzava) {
			Element drzava = doc.createElement("drzava");
			
			Attr attribute = doc.createAttribute("id");
			attribute.setValue(d.getId().toString());
			drzava.setAttributeNode(attribute);
			
			Element naziv = doc.createElement("naziv");
			naziv.appendChild(doc.createTextNode(d.getNaziv()));
			drzava.appendChild(naziv);
			
			Element mesta = doc.createElement("mesta");
			for(MestoDTO m : d.getMesta()) {
				
				Element mesto = doc.createElement("mesto");
				Attr mestoId = doc.createAttribute("id");
				mestoId.setValue(m.getId().toString());
				mesto.setAttributeNode(mestoId);
				
				Element mestoNaziv = doc.createElement("naziv");
				mestoNaziv.appendChild(doc.createTextNode(m.getNaziv()));
				mesto.appendChild(mestoNaziv);
				mesta.appendChild(mesto);
				
			}
			drzava.appendChild(mesta);
			drzave.appendChild(drzava);
		}
		
		
		
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Schema schema = loadSchema(SCHEMANAME);
		
		if(validateXml(schema, doc)) {
			DOMSource source = new DOMSource((Node) doc);
			String home = System.getProperty("user.home");
			File file = new File(home+"/Downloads/" + "drzave" + ".xml");
			StreamResult result = new StreamResult(file);
			//StreamResult result =  new StreamResult(new File("C:\\Users\\Korisnik\\Desktop\\xml\\drzave.xml"));
			transformer.transform(source, result);
			StreamResult consoleResult = new StreamResult(System.out);
	        transformer.transform(source, consoleResult);
		}else {
			System.out.println("Neispravan format za export!");
		}
		
	}
	
	public ArrayList<DrzavaDTO> importXml(String s) {
		
		this.importovaneDrzave = new ArrayList<DrzavaDTO>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			
			// procces XML securely, avoid attacks like XML External Entities
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			
			DocumentBuilder db = dbf.newDocumentBuilder();
//			String home = System.getProperty("user.home");
//			
//			File file = new File(home+"/Downloads/"+"drzaveImported.xml");
//			
//			FileOutputStream fos = null;
//			 
//	        try {
//	             
//	            fos = new FileOutputStream(file);
//	             
//	            // Writes bytes from the specified byte array to this file output stream 
//	            fos.write(arr);
//	 
//	        }
//	        catch (FileNotFoundException e) {
//	            System.out.println("File not found" + e);
//	        }
//	        catch (IOException ioe) {
//	            System.out.println("Exception while writing file " + ioe);
//	        }
//	        finally {
//	            // close the streams using close method
//	            try {
//	                if (fos != null) {
//	                    fos.close();
//	                }
//	            }
//	            catch (IOException ioe) {
//	                System.out.println("Error while closing stream: " + ioe);
//	            }
//	 
//	        }
			
			Document doc = convertStringToXMLDocument(s);
			Schema schema = loadSchema(SCHEMANAME);
			if(validateXml(schema, doc)) {
				doc.getDocumentElement().normalize();
				
				System.out.println("Root element:" + doc.getDocumentElement().getNodeName());
				
				NodeList list = doc.getElementsByTagName("drzava");
				
				for(int temp = 0; temp < list.getLength(); temp++) {
					
					Node node = list.item(temp);
					
					if(node.getNodeType() == Node.ELEMENT_NODE) {
						
						Element element = (Element) node;
						
						String drzavaId = element.getAttribute("id");
						String drzavaNaziv = element.getElementsByTagName("naziv").item(0).getTextContent();
						
						DrzavaDTO dto = new DrzavaDTO();
						dto.setId(Long.parseLong(drzavaId));
						dto.setNaziv(drzavaNaziv);
						
						Node mesta = element.getElementsByTagName("mesta").item(0);
						
						NodeList listMesta = mesta.getChildNodes();
						
						System.out.println("Element: " + node.getNodeName());
						System.out.println("Drzava ID: " + drzavaId);
						System.out.println("Naziv drzave: " + drzavaNaziv);
						
						ArrayList<MestoDTO> mestaDTO = new ArrayList<MestoDTO>();
						
						for(int m = 0; m < listMesta.getLength(); m++) {
							
							Node mNode = listMesta.item(m);
							
							if(mNode.getNodeType() == Node.ELEMENT_NODE) {
								
								Element mestoElement = (Element) mNode;
								
								String mestoId = mestoElement.getAttribute("id");
								String mestoNaziv = mestoElement.getElementsByTagName("naziv").item(0).getTextContent();
								
								MestoDTO mestoDTO = new MestoDTO();
								mestoDTO.setId(Long.parseLong(mestoId));
								mestoDTO.setNaziv(mestoNaziv);
								mestoDTO.setAdrese(null);
								mestaDTO.add(mestoDTO);
								
								System.out.println("Subelement: " + mNode.getNodeName());
								System.out.println("Mesto ID: " + mestoId);
								System.out.println("Naziv mesta: " + mestoNaziv);
								
							}
						}
						
						
						dto.setMesta(mestaDTO);
						
						this.importovaneDrzave.add(dto);
					}
					
					
				}
				
				return importovaneDrzave;
			} else {
				System.out.println("XML dokument nije ispravan!");
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	public static Schema loadSchema(String schemaFileName) {
		Schema schema = null;
		try {
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			
			schema = factory.newSchema(new File(schemaFileName));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return schema;
	}
	
	public static Document parseXmlDom(String xmlName) {
		
		Document document = null;
		try {
		   DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		   DocumentBuilder builder = factory.newDocumentBuilder();
		   document = builder.parse(new File(xmlName));
		} catch (Exception e) {
		   System.out.println(e.toString());
		}
		return document;
	}
	
	public static boolean validateXml(Schema schema, Document document) {
		try {
			Validator validator = schema.newValidator();
			
			validator.validate(new DOMSource(document));
			System.out.println("Validacija uspjesna!");
			return true;
			
		} catch (Exception e) {
			System.out.println("Validacije neuspjesna!");
			return false;
		}
		
	}
	
	public static Document convertStringToXMLDocument(String xmlString) 
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
             
            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return null;
    }
}
