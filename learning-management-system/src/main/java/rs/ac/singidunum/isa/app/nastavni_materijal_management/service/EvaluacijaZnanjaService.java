package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.EvaluacijaZnanja;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.EvaluacijaZnanjaRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class EvaluacijaZnanjaService extends GenerateService<EvaluacijaZnanjaRepository, EvaluacijaZnanja, Long>{

	public Page<EvaluacijaZnanja> findAll(Pageable pageable) {
		return ((EvaluacijaZnanjaRepository) this.getRepository()).findAll(pageable);
	}

}
