package rs.ac.singidunum.isa.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Autor;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.AutorRepository;
import rs.ac.singidunum.isa.app.repository.AdresaRepository;

@Service
public class AdresaService extends GenerateService<AdresaRepository, Adresa, Long>{

	public Page<Adresa> findAll(Pageable pageable) {
		return ((AdresaRepository) this.getRepository()).findAll(pageable);
	}
}
