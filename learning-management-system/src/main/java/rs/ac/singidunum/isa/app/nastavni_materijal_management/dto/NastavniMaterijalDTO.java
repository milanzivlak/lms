package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.ArrayList;
import java.util.Date;

import rs.ac.singidunum.isa.app.dto.IshodDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Originator;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;

public class NastavniMaterijalDTO {
	
	
	private Long id;
	private String naziv;
	private Date godinaIzdanja;
	
	private Long version;
	
	private Date datumAzuriranja;
	private Date datumUklanjanja;
	
	private ArrayList<AutorDTO> autori = new ArrayList<AutorDTO>();
	
	private ArrayList<FajlDTO> fajlovi = new ArrayList<FajlDTO>();
	
	private ArrayList<IshodDTO> ishodi = new ArrayList<IshodDTO>();
	
	private OriginatorDTO istorija;
	
	private CaretakerDTO caretaker;
	
	private StanjeDTO stanje;
	
	
	
	
	public NastavniMaterijalDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public NastavniMaterijalDTO(Long id, String naziv,  Date godinaIzdanja, Date datumAzuriranja, Date datumUklanjanja) {
		this(id, naziv, godinaIzdanja, datumAzuriranja, datumUklanjanja, null, null, null, null, null, null, null);
	}
	
	public NastavniMaterijalDTO(Long id, String naziv, Date godinaIzdanja, Long version, Date datumAzuriranja, Date datumUklanjanja, OriginatorDTO istorija, CaretakerDTO caretaker) {
		this(id, naziv, godinaIzdanja, datumAzuriranja, datumUklanjanja, istorija, caretaker, null, null, null, null, version);
	}

	public NastavniMaterijalDTO(Long id, String naziv, Date godinaIzdanja, Date datumAzuriranja, Date datumUklanjanja, OriginatorDTO istorija,CaretakerDTO caretaker, ArrayList<AutorDTO> autori,
			ArrayList<FajlDTO> fajlovi, ArrayList<IshodDTO> ishodi, StanjeDTO stanje, Long version) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.godinaIzdanja = godinaIzdanja;
		this.autori = autori;
		this.fajlovi = fajlovi;
		this.ishodi = ishodi;
		this.datumAzuriranja = datumAzuriranja;
		this.datumUklanjanja = datumUklanjanja;
		this.istorija = istorija;
		this.caretaker = caretaker;
		this.stanje = stanje;
		this.version = version;
	}

	public OriginatorDTO getIstorija() {
		return istorija;
	}

	public void setIstorija(OriginatorDTO istorija) {
		this.istorija = istorija;
	}

	public NastavniMaterijalDTO(NastavniMaterijal nastavniMaterijal) {
		this(nastavniMaterijal.getId(), nastavniMaterijal.getNaziv(), nastavniMaterijal.getGodinaIzdanja(), nastavniMaterijal.getDatumAzuriranja(), nastavniMaterijal.getDatumUklanjanja());
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Date getGodinaIzdanja() {
		return godinaIzdanja;
	}

	public void setGodinaIzdanja(Date godinaIzdanja) {
		this.godinaIzdanja = godinaIzdanja;
	}

	public ArrayList<AutorDTO> getAutori() {
		return autori;
	}

	public void setAutori(ArrayList<AutorDTO> autori) {
		this.autori = autori;
	}

	public ArrayList<FajlDTO> getFajlovi() {
		return fajlovi;
	}

	public void setFajlovi(ArrayList<FajlDTO> fajlovi) {
		this.fajlovi = fajlovi;
	}

	public ArrayList<IshodDTO> getIshodi() {
		return ishodi;
	}

	public void setIshodi(ArrayList<IshodDTO> ishodi) {
		this.ishodi = ishodi;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getDatumAzuriranja() {
		return datumAzuriranja;
	}

	public void setDatumAzuriranja(Date datumAzuriranja) {
		this.datumAzuriranja = datumAzuriranja;
	}

	public Date getDatumUklanjanja() {
		return datumUklanjanja;
	}

	public void setDatumUklanjanja(Date datumUklanjanja) {
		this.datumUklanjanja = datumUklanjanja;
	}

	public CaretakerDTO getCaretaker() {
		return caretaker;
	}

	public void setCaretaker(CaretakerDTO caretaker) {
		this.caretaker = caretaker;
	}

	public StanjeDTO getStanje() {
		return stanje;
	}

	public void setStanje(StanjeDTO stanje) {
		this.stanje = stanje;
	}
	
	
	
	
	
}
