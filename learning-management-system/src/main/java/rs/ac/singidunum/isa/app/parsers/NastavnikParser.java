package rs.ac.singidunum.isa.app.parsers;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.TipZvanjaDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.dto.ZvanjeDTO;
import rs.ac.singidunum.isa.app.model.Zvanje;
import rs.ac.singidunum.isa.app.utils.XmlUtil;

public class NastavnikParser {
	private static final String SCHEMANAME = "nastavnik.xsd";
	public ArrayList<NastavnikDTO> spisak;
	public ArrayList<NastavnikDTO> importovani;
	
	public NastavnikParser(ArrayList<NastavnikDTO> spisakTipova) {
		this.spisak = spisakTipova;
	}
	
	public NastavnikParser() {
		
	}
	
	public void export() throws IOException, ParserConfigurationException, TransformerException {
		//root
		
		DocumentBuilderFactory dbFactory =
		         DocumentBuilderFactory.newInstance();
		         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		         Document doc = (Document) dBuilder.newDocument();
		         
		Element tipovi = doc.createElement("nastavnici");
		doc.appendChild(tipovi);
		for(NastavnikDTO d : this.spisak) {
			Element tip = doc.createElement("nastavnik");
			
			Attr attribute = doc.createAttribute("id");
			attribute.setValue(d.getId().toString());
			tip.setAttributeNode(attribute);
			
			Element ime = doc.createElement("ime");
			ime.appendChild(doc.createTextNode(d.getIme()));
			tip.appendChild(ime);
			
			Element jmbg = doc.createElement("jmbg");
			jmbg.appendChild(doc.createTextNode(d.getJmbg()));
			tip.appendChild(jmbg);
			
			Element biografija = doc.createElement("biografija");
			biografija.appendChild(doc.createTextNode(d.getBiografija()));
			tip.appendChild(biografija);
			
			Element zvanja = doc.createElement("zvanja");
			for(ZvanjeDTO z: d.getZvanja()) {
				Element zvanje = doc.createElement("zvanje");

				Attr zvanjeAttribute = doc.createAttribute("id");
				zvanjeAttribute.setValue(z.getId().toString());
				zvanje.setAttributeNode(zvanjeAttribute);
				
				Element datumIzbora = doc.createElement("datumIzbora");
				datumIzbora.appendChild(doc.createTextNode(z.getDatumIzbora().toString()));
				zvanje.appendChild(datumIzbora);
				
				Element datumPrestanka = doc.createElement("datumPrestanka");
				datumPrestanka.appendChild(doc.createTextNode(z.getDatumPrestanka().toString()));
				zvanje.appendChild(datumPrestanka);
				
				
				Element tipZvanjaElement = doc.createElement("tipZvanja");

				Attr tipZvanjaAttribute = doc.createAttribute("id");
				tipZvanjaAttribute.setValue(z.getId().toString());
				tipZvanjaElement.setAttributeNode(tipZvanjaAttribute);
				
				Element naziv = doc.createElement("naziv");
				naziv.appendChild(doc.createTextNode(z.getTipZvanja().getNaziv()));
				tipZvanjaElement.appendChild(datumIzbora);

				zvanje.appendChild(tipZvanjaElement);
				zvanja.appendChild(zvanje);
			}
			tip.appendChild(zvanja);
			
			Element fakultet = doc.createElement("fakultet");
			
			Attr fakultetAttribute = doc.createAttribute("id");
			fakultetAttribute.setValue(d.getFakultet().getId().toString());
			fakultet.setAttributeNode(fakultetAttribute);
			
			Element naziv = doc.createElement("naziv");
			naziv.appendChild(doc.createTextNode(d.getFakultet().getNaziv()));
			fakultet.appendChild(naziv);

			tip.appendChild(fakultet);
			
			Element univerzitet = doc.createElement("univerzitet");
			
			Attr univerzitetAttribute = doc.createAttribute("id");
			univerzitetAttribute.setValue(d.getUniverzitet().getId().toString());
			univerzitet.setAttributeNode(univerzitetAttribute);
			
			Element ulica = doc.createElement("ulica");
			ulica.appendChild(doc.createTextNode(d.getUniverzitet().getUlica()));
			univerzitet.appendChild(ulica);
			
			Element broj = doc.createElement("broj");
			broj.appendChild(doc.createTextNode(d.getUniverzitet().getBroj()));
			univerzitet.appendChild(broj);

			tip.appendChild(univerzitet);
			
			tipovi.appendChild(tip);
		}
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Schema schema = XmlUtil.loadSchema(SCHEMANAME);
		
		if(XmlUtil.validateXml(schema, doc)) {
			DOMSource source = new DOMSource((Node) doc);
			String home = System.getProperty("user.home");
			File file = new File(home+"/Downloads/" + "nastavnik" + ".xml");
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result);
			StreamResult consoleResult = new StreamResult(System.out);
	        transformer.transform(source, consoleResult);
		}else {
			System.out.println("Neispravan format za export!");
		}
	}
	
	public ArrayList<NastavnikDTO> importXml(String s) {
		
		this.importovani = new ArrayList<NastavnikDTO>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			Document doc = XmlUtil.convertStringToXMLDocument(s);
			Schema schema = XmlUtil.loadSchema(SCHEMANAME);
			if(XmlUtil.validateXml(schema, doc)) {
				doc.getDocumentElement().normalize();
				
				System.out.println("Root element:" + doc.getDocumentElement().getNodeName());
				
				NodeList list = doc.getElementsByTagName("nastavnik");
				
				for(int temp = 0; temp < list.getLength(); temp++) {
					Node node = list.item(temp);
					
					if(node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						

						String nastavnikId = element.getAttribute("id");
						String ime = element.getElementsByTagName("ime").item(0).getTextContent();
						String jmbg = element.getElementsByTagName("jmbg").item(0).getTextContent();
						String biografija = element.getElementsByTagName("biografija").item(0).getTextContent();
						
						//zvanja 
					
						Element zvanja = (Element)element.getElementsByTagName("zvanja").item(0);
						NodeList lista_zvanja = doc.getElementsByTagName("zvanje");
						ArrayList<ZvanjeDTO> zvanja_lista = new ArrayList<ZvanjeDTO>();
						for(int temp1 = 0; temp1 < lista_zvanja.getLength(); temp1++) {
							Node node1 = lista_zvanja.item(temp1);
							
							if(node.getNodeType() == Node.ELEMENT_NODE) {
								Element zvanje = (Element) node1;
								
								String zvanjeId = zvanje.getAttribute("id");
								String zvanjeDatumIzbora = zvanje.getElementsByTagName("datumIzbora").item(0).getTextContent();
								String zvanjeDatumPrestanka = zvanje.getElementsByTagName("datumPrestanka").item(0).getTextContent();
								
								ZvanjeDTO zvanjeDto = new ZvanjeDTO(Long.parseLong(zvanjeId), new SimpleDateFormat("yyyy-MM-dd").parse(zvanjeDatumIzbora), new SimpleDateFormat("yyyy-MM-dd").parse(zvanjeDatumPrestanka));
								
								zvanja_lista.add(zvanjeDto);
						
							}
						}
						
						//fakultet
						Element fakultet = (Element)element.getElementsByTagName("fakultet").item(0);
						
						String fakultetID = fakultet.getAttribute("id");
						String fakultetNaziv = fakultet.getElementsByTagName("naziv").item(0).getTextContent();
						
						
						//univerzitet
						Element univerzitet = (Element)element.getElementsByTagName("univerzitet").item(0);
						
						String univerzitetID = univerzitet.getAttribute("id");
						String univerzitetUlica = univerzitet.getElementsByTagName("ulica").item(0).getTextContent();
						String univerzitetBroj = univerzitet.getElementsByTagName("broj").item(0).getTextContent();
						
						NastavnikDTO dto = new NastavnikDTO();
						dto.setId(Long.parseLong(nastavnikId));
						dto.setIme(ime);
						dto.setJmbg(jmbg);
						dto.setBiografija(biografija);
						
						dto.setFakultet(new FakultetDTO(Long.parseLong(fakultetID), fakultetNaziv));
						dto.setUniverzitet(new UniverzitetDTO(Long.parseLong(univerzitetID), univerzitetBroj, univerzitetUlica));
						
						this.importovani.add(dto);
					}
				}
				
				return importovani;
				
			} else {
				System.out.println("XML dokument nije ispravan!");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
