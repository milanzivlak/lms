package rs.ac.singidunum.isa.app.nastavni_materijal_management.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class InstrumentEvaluacije {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToMany(mappedBy = "instrumentEvaluacije")
	private Set<Polaganje> polaganja = new HashSet<Polaganje>();
	
	@ManyToMany
	private Set<Fajl> fajlovi = new HashSet<Fajl>();
	
	
	public Set<Fajl> getFajlovi() {
		return fajlovi;
	}

	public void setFajlovi(Set<Fajl> fajlovi) {
		this.fajlovi = fajlovi;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Polaganje> getPolaganja() {
		return polaganja;
	}

	public void setPolaganja(Set<Polaganje> polaganja) {
		this.polaganja = polaganja;
	}
	
	
}
