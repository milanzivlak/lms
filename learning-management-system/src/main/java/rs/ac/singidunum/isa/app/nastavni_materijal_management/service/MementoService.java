package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.repository.core.support.DefaultCrudMethods;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Memento;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.MementoRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class MementoService extends GenerateService<MementoRepository, Memento, Long>{

}
