package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.InstrumentEvaluacije;

public class InstrumentEvaluacijeDTO {
	
	private Long id;
	
	private ArrayList<PolaganjeDTO> polaganja = new ArrayList<PolaganjeDTO>();
	
	private ArrayList<FajlDTO> fajlovi = new ArrayList<FajlDTO>();

	public InstrumentEvaluacijeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public InstrumentEvaluacijeDTO(Long id) {
		this(id, null, null);
	}

	public InstrumentEvaluacijeDTO(Long id, ArrayList<PolaganjeDTO> polaganja, ArrayList<FajlDTO> fajlovi) {
		super();
		this.id = id;
		this.polaganja = polaganja;
		this.fajlovi = fajlovi;
	}


	public InstrumentEvaluacijeDTO(InstrumentEvaluacije instrumentEvaluacije) {
		this(instrumentEvaluacije.getId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArrayList<PolaganjeDTO> getPolaganja() {
		return polaganja;
	}

	public void setPolaganja(ArrayList<PolaganjeDTO> polaganja) {
		this.polaganja = polaganja;
	}

	public ArrayList<FajlDTO> getFajlovi() {
		return fajlovi;
	}

	public void setFajlovi(ArrayList<FajlDTO> fajlovi) {
		this.fajlovi = fajlovi;
	}
	
}
