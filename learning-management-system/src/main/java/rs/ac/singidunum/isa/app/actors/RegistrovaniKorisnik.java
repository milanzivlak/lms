package rs.ac.singidunum.isa.app.actors;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.Student;
import rs.ac.singidunum.isa.app.model.UserPermission;

@Entity
public class RegistrovaniKorisnik {
	
	// Eventualno dodati mesto, datum rodjenja...
	
	
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	@Column(unique = true)
	private String korisnickoIme;
	
	@Lob
	private String lozinka;
	
	@Lob
	private String email;
	
	@OneToOne(mappedBy = "registrovaniKorisnik")
	private Nastavnik nastavnik;
	
	// Nisam siguran da li ova veza izgleda ovako. Da li korisnik ima Administrator u sebi
	@OneToOne(mappedBy = "registrovaniKorisnik", cascade = CascadeType.REMOVE)
	private Administrator administrator;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private Set<UserPermission> userPermissions = new HashSet<UserPermission>();
	
	@OneToMany(mappedBy = "registrovaniKorisnik")
	private Set<Student> studenti = new HashSet<Student>();
	
	public Set<UserPermission> getUserPermissions() {
		return userPermissions;
	}
	public void setUserPermissions(Set<UserPermission> userPermissions) {
		this.userPermissions = userPermissions;
	}
	
	
	
	public Nastavnik getNastavnik() {
		return nastavnik;
	}
	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}
	public Administrator getAdministrator() {
		return administrator;
	}
	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}
	public Set<Student> getStudenti() {
		return studenti;
	}
	public void setStudenti(Set<Student> studenti) {
		this.studenti = studenti;
	}
	public RegistrovaniKorisnik() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegistrovaniKorisnik(Long id, String korisnickoIme, String lozinka, String email) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
