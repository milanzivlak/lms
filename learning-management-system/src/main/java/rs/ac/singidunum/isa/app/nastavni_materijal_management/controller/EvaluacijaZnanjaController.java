package rs.ac.singidunum.isa.app.nastavni_materijal_management.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.EvaluacijaZnanjaDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.EvaluacijaZnanja;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.EvaluacijaZnanjaService;

@Controller
@RequestMapping(path = "/api/nastavni-materijal/evaluacija-znanja")
public class EvaluacijaZnanjaController {
	@Autowired
	private EvaluacijaZnanjaService evaluacijaZnanjaService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Page<EvaluacijaZnanjaDTO>> getAllEvaluacijaZnanja(Pageable pageable) {
		return new ResponseEntity<Page<EvaluacijaZnanjaDTO>>(evaluacijaZnanjaService.findAll(pageable).map(a -> new EvaluacijaZnanjaDTO(a)),
				HttpStatus.OK);

	}

	@RequestMapping(path = "/{ezId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<EvaluacijaZnanjaDTO> getEvaluacijaZnanja(@PathVariable("ezId") Long ezId) {

		Optional<EvaluacijaZnanja> ez = evaluacijaZnanjaService.findOne(ezId);

		if (ez.isPresent()) {
			EvaluacijaZnanjaDTO EvaluacijaZnanjaDTO = new EvaluacijaZnanjaDTO(ez.get());
			return new ResponseEntity<EvaluacijaZnanjaDTO>(EvaluacijaZnanjaDTO, HttpStatus.OK);

		}

		return new ResponseEntity<EvaluacijaZnanjaDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<EvaluacijaZnanja> createEvaluacijaZnanja(@RequestBody EvaluacijaZnanja ez) {

		try {

			evaluacijaZnanjaService.save(ez);

			return new ResponseEntity<EvaluacijaZnanja>(ez, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();

		}

		return new ResponseEntity<EvaluacijaZnanja>(HttpStatus.BAD_REQUEST);

	}

	@RequestMapping(path = "/{ezId}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<EvaluacijaZnanja> updateEvaluacijaZnanja(@PathVariable("ezId") Long ezId, @RequestBody EvaluacijaZnanja izmenjenEvaluacijaZnanja) {

		EvaluacijaZnanja adresa = evaluacijaZnanjaService.findOne(ezId).orElse(null);

		if (adresa != null) {
			izmenjenEvaluacijaZnanja.setId(ezId);

			evaluacijaZnanjaService.save(izmenjenEvaluacijaZnanja);
			return new ResponseEntity<EvaluacijaZnanja>(izmenjenEvaluacijaZnanja, HttpStatus.OK);

		}

		return new ResponseEntity<EvaluacijaZnanja>(HttpStatus.NOT_FOUND);

	}

	@RequestMapping(path = "/{ezId}", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<EvaluacijaZnanja> deleteEvaluacijaZnanja(@PathVariable("ezId") Long ezId) {

		if (evaluacijaZnanjaService.findOne(ezId).isPresent()) {
			evaluacijaZnanjaService.delete(ezId);
			return new ResponseEntity<EvaluacijaZnanja>(HttpStatus.OK);
		}

		return new ResponseEntity<EvaluacijaZnanja>(HttpStatus.NOT_FOUND);
	}

}