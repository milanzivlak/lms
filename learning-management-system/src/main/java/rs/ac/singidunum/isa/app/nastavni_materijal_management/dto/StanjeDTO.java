package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Memento;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;

public class StanjeDTO {
	
	
	private Long id;
	
	private String naziv;
	
	private ArrayList<NastavniMaterijalDTO> nastavniMaterijali = new ArrayList<NastavniMaterijalDTO>();
	
	

	
	public StanjeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public StanjeDTO(Long id, String naziv, ArrayList<NastavniMaterijalDTO> nastavniMaterijali) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.nastavniMaterijali = nastavniMaterijali;
	}



	public StanjeDTO(Long id, String naziv) {
		this(id, naziv, null);
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public ArrayList<NastavniMaterijalDTO> getNastavniMaterijali() {
		return nastavniMaterijali;
	}

	public void setNastavniMaterijali(ArrayList<NastavniMaterijalDTO> nastavniMaterijali) {
		this.nastavniMaterijali = nastavniMaterijali;
	}
}
