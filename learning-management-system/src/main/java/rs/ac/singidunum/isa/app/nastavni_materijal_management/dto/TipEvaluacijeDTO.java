package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TipEvaluacije;

public class TipEvaluacijeDTO {
	
	private Long id;
	private String naziv;
	
	private ArrayList<EvaluacijaZnanjaDTO> evaluacijaZnanja = new ArrayList<EvaluacijaZnanjaDTO>();

	public TipEvaluacijeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TipEvaluacijeDTO(Long id, String naziv) {
		this(id, naziv, null);
	}

	public TipEvaluacijeDTO(Long id, String naziv, ArrayList<EvaluacijaZnanjaDTO> evaluacijaZnanja) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.evaluacijaZnanja = evaluacijaZnanja;
	}

	public TipEvaluacijeDTO(TipEvaluacije tipEvaluacije) {
		this(tipEvaluacije.getId(), tipEvaluacije.getNaziv());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public ArrayList<EvaluacijaZnanjaDTO> getEvaluacijaZnanja() {
		return evaluacijaZnanja;
	}

	public void setEvaluacijaZnanja(ArrayList<EvaluacijaZnanjaDTO> evaluacijaZnanja) {
		this.evaluacijaZnanja = evaluacijaZnanja;
	}
	
}
