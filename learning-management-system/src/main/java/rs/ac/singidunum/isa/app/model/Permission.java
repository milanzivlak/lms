package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import rs.ac.singidunum.isa.app.actors.PermissionType;

@Entity
public class Permission {

	public Set<UserPermission> getUserPermissions() {
		return userPermissions;
	}

	public void setUserPermissions(Set<UserPermission> userPermissions) {
		this.userPermissions = userPermissions;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private PermissionType permissionType;

	
	@OneToMany(mappedBy = "permission")
	private Set<UserPermission> userPermissions = new HashSet<UserPermission>();
	
	public Permission(Long id, PermissionType permissionType) {
		super();
		this.id = id;
		this.permissionType = permissionType;
	}

	public Permission() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PermissionType getPermissionType() {
		return permissionType;
	}

	public void setPermissionType(PermissionType naziv) {
		this.permissionType = naziv;
	}
	
	
}
