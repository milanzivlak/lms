package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikNaRealizacijiDTO;
import rs.ac.singidunum.isa.app.dto.PohadjanjePredmetaDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.RealizacijaPredmetaDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.RealizacijaPredmeta;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.EvaluacijaZnanjaDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.TerminNastaveDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.EvaluacijaZnanja;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TerminNastave;
import rs.ac.singidunum.isa.app.service.RealizacijaPredmetaService;

@Controller
@RequestMapping(path = "/api/realizacijaPredmeta")
@PreAuthorize("hasAuthority('KORISNIK')")
@CrossOrigin
public class RealizacijaPredmetaController {
	
	@Autowired
	private RealizacijaPredmetaService realizacijaPredmetaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<Iterable<RealizacijaPredmetaDTO>> getAllRealizacije() {
		
		
		ArrayList<RealizacijaPredmetaDTO> realizacijaPredmeta = new ArrayList<RealizacijaPredmetaDTO>();
		
		for(RealizacijaPredmeta rp : realizacijaPredmetaService.findAll()) {
			ArrayList<TerminNastaveDTO> terminNastave = new ArrayList<TerminNastaveDTO>();
			ArrayList<EvaluacijaZnanjaDTO> evaluacijaZnanja = new ArrayList<EvaluacijaZnanjaDTO>();
			
			for(TerminNastave tn : rp.getTerminiNastave()) {
				terminNastave.add(new TerminNastaveDTO(tn.getId(), tn.getVremePocetka(), tn.getVremeKraja()));
			}
			
			for(EvaluacijaZnanja ez : rp.getEvaluacijeZnanja()) {
				evaluacijaZnanja.add(new EvaluacijaZnanjaDTO(ez.getId(), ez.getVremePocetka(), ez.getVremeZavrsetka(), ez.getBodovi()));
			}
			
			PohadjanjePredmetaDTO pohadjanjePredmeta = new PohadjanjePredmetaDTO(rp.getPohadjanjePredmeta().getId(), rp.getPohadjanjePredmeta().getKonacnaOcena());
			
			PredmetDTO predmet = new PredmetDTO(rp.getPredmet().getId(), rp.getPredmet().getNaziv(), rp.getPredmet().getEspb(), rp.getPredmet().isObavezan(), rp.getPredmet().getBrojPredavanja(), rp.getPredmet().getBrojVezbi(), rp.getPredmet().getDrugiObliciNastave(), rp.getPredmet().getIstrazivackiRad(), rp.getPredmet().getOstaliCasovi(), rp.getPredmet().getBrojSemestara());
			
			NastavnikNaRealizacijiDTO nastavnikNaRealizaciji = new NastavnikNaRealizacijiDTO(rp.getNastavnikNaRealizaciji().getId(), rp.getNastavnikNaRealizaciji().getBrojCasova());
			
			realizacijaPredmeta.add(new RealizacijaPredmetaDTO(rp.getId(), pohadjanjePredmeta, predmet, evaluacijaZnanja, nastavnikNaRealizaciji, terminNastave));
		}
		
		return new ResponseEntity<Iterable<RealizacijaPredmetaDTO>>(realizacijaPredmeta, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{realizacijaPredmetaId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<RealizacijaPredmetaDTO> getRealizacijaPredmeta(@PathVariable("realizacijaPredmetaId") Long realizacijaPredmetaId) {
		
		Optional<RealizacijaPredmeta> realizacija = realizacijaPredmetaService.findOne(realizacijaPredmetaId);
		
		if(realizacija.isPresent()) {
			RealizacijaPredmetaDTO realizacijaDTO = new RealizacijaPredmetaDTO(realizacija.get().getId());
			return new ResponseEntity<RealizacijaPredmetaDTO>(realizacijaDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<RealizacijaPredmetaDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<RealizacijaPredmeta> createRealizacija(@RequestBody RealizacijaPredmeta realizacijaPredmeta){
		
		try {
			
			realizacijaPredmetaService.save(realizacijaPredmeta);
			
			return new ResponseEntity<RealizacijaPredmeta>(realizacijaPredmeta, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<RealizacijaPredmeta>(HttpStatus.BAD_REQUEST);

		
	}
	
	@RequestMapping(path = "/{realizacijaPredmetaId}", method = RequestMethod.PUT)
	//@Secured("ROLE_ADMIN")
	public ResponseEntity<RealizacijaPredmeta> updateRealizacija(@PathVariable("realizacijaPredmetaId") Long realizacijaPredmetaId, @RequestBody RealizacijaPredmeta izmenjenaRealizacija) {
		
		RealizacijaPredmeta adresa = realizacijaPredmetaService.findOne(realizacijaPredmetaId).orElse(null);
		
		if( adresa != null) {
			izmenjenaRealizacija.setId(realizacijaPredmetaId);
			
			realizacijaPredmetaService.save(izmenjenaRealizacija);
			return new ResponseEntity<RealizacijaPredmeta>(izmenjenaRealizacija, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<RealizacijaPredmeta>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{realizacijaPredmetaId}", method = RequestMethod.DELETE)
	//@Secured("ROLE_ADMIN")
	public ResponseEntity<RealizacijaPredmeta> deleteRealizacija(@PathVariable("realizacijaPredmetaId") Long realizacijaPredmetaId) {
	
		if(realizacijaPredmetaService.findOne(realizacijaPredmetaId).isPresent()) {
			realizacijaPredmetaService.delete(realizacijaPredmetaId);
			return new ResponseEntity<RealizacijaPredmeta>(HttpStatus.OK);
		}
		
		return new ResponseEntity<RealizacijaPredmeta>(HttpStatus.NOT_FOUND);
	}

}
