package rs.ac.singidunum.isa.app.actors;

public enum PermissionType {

	ROLE_ADMINISTRATOR,
	ROLE_KORISNIK,
	ROLE_STUDENT,
	ROLE_NASTAVNIK
}
