package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import rs.ac.singidunum.isa.app.dto.StudentNaGodiniDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Polaganje;

public class PolaganjeDTO {
	
	private Long id;
	private int bodovi;
	private String napomena;
	
	private InstrumentEvaluacijeDTO instrumentEvaluacije;
	private StudentNaGodiniDTO studentNaGodini;
	private EvaluacijaZnanjaDTO evaluacijaZnanja;
	
	public PolaganjeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public PolaganjeDTO(Long id, int bodovi, String napomena) {
		this(id, bodovi, napomena, null, null, null);
	}

	public PolaganjeDTO(Long id, int bodovi, String napomena, InstrumentEvaluacijeDTO instrumentEvaluacije,
			StudentNaGodiniDTO studentNaGodini, EvaluacijaZnanjaDTO evaluacijaZnanja) {
		super();
		this.id = id;
		this.bodovi = bodovi;
		this.napomena = napomena;
		this.instrumentEvaluacije = instrumentEvaluacije;
		this.studentNaGodini = studentNaGodini;
		this.evaluacijaZnanja = evaluacijaZnanja;
	}

	public PolaganjeDTO(Polaganje polaganje) {
		this(polaganje.getId(), polaganje.getBodovi(), polaganje.getNapomena());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBodovi() {
		return bodovi;
	}

	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}

	public String getNapomena() {
		return napomena;
	}

	public void setNapomena(String napomena) {
		this.napomena = napomena;
	}

	public InstrumentEvaluacijeDTO getInstrumentEvaluacije() {
		return instrumentEvaluacije;
	}

	public void setInstrumentEvaluacije(InstrumentEvaluacijeDTO instrumentEvaluacije) {
		this.instrumentEvaluacije = instrumentEvaluacije;
	}

	public StudentNaGodiniDTO getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(StudentNaGodiniDTO studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}

	public EvaluacijaZnanjaDTO getEvaluacijaZnanja() {
		return evaluacijaZnanja;
	}

	public void setEvaluacijaZnanja(EvaluacijaZnanjaDTO evaluacijaZnanja) {
		this.evaluacijaZnanja = evaluacijaZnanja;
	}
	
}
