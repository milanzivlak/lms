package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Fajl;

public class FajlDTO {
	
	private Long id;
	private String opis;
	private String url;
	
	private ArrayList<InstrumentEvaluacijeDTO> instrumentEvaluacije = new ArrayList<InstrumentEvaluacijeDTO>();
	
	private ArrayList<NastavniMaterijalDTO> nastavniMaterijal = new ArrayList<NastavniMaterijalDTO>();

	public FajlDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public FajlDTO(Long id, String opis, String url) {
		this(id, opis, url, null, null);
	}

	public FajlDTO(Long id, String opis, String url, ArrayList<InstrumentEvaluacijeDTO> instrumentEvaluacije,
			ArrayList<NastavniMaterijalDTO> nastavniMaterijal) {
		super();
		this.id = id;
		this.opis = opis;
		this.url = url;
		this.instrumentEvaluacije = instrumentEvaluacije;
		this.nastavniMaterijal = nastavniMaterijal;
		
	}
	
	public FajlDTO(Fajl fajl) {
		this(fajl.getId(), fajl.getOpis(), fajl.getUrl());
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ArrayList<InstrumentEvaluacijeDTO> getInstrumentEvaluacije() {
		return instrumentEvaluacije;
	}

	public void setInstrumentEvaluacije(ArrayList<InstrumentEvaluacijeDTO> instrumentEvaluacije) {
		this.instrumentEvaluacije = instrumentEvaluacije;
	}

	public ArrayList<NastavniMaterijalDTO> getNastavniMaterijal() {
		return nastavniMaterijal;
	}

	public void setNastavniMaterijal(ArrayList<NastavniMaterijalDTO> nastavniMaterijal) {
		this.nastavniMaterijal = nastavniMaterijal;
	}
	
}
