package rs.ac.singidunum.isa.app.nastavni_materijal_management.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Memento;

public class CaretakerDTO {
	
	private Long id;
	
	private ArrayList<MementoDTO> mementos = new ArrayList<MementoDTO>();
	
	
	private NastavniMaterijalDTO nastavniMaterijal;
	
	public CaretakerDTO(Long id) {
		this(id, null, null);
	}
	
	
	public CaretakerDTO(Long id, ArrayList<MementoDTO> mementos, NastavniMaterijalDTO nastavniMaterijal) {
		super();
		this.id = id;
		this.mementos = mementos;
		this.nastavniMaterijal = nastavniMaterijal;
	}


	public void add(MementoDTO state) {
		this.mementos.add(state);
	}
	
	public MementoDTO get(int index) {
		return this.mementos.get(index);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArrayList<MementoDTO> getMementos() {
		return mementos;
	}

	public void setMementos(ArrayList<MementoDTO> mementos) {
		this.mementos = mementos;
	}

	public NastavniMaterijalDTO getNastavniMaterijal() {
		return nastavniMaterijal;
	}

	public void setNastavniMaterijal(NastavniMaterijalDTO nastavniMaterijal) {
		this.nastavniMaterijal = nastavniMaterijal;
	}
}
