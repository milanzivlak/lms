package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.model.Fakultet;
import rs.ac.singidunum.isa.app.model.GodinaStudija;
import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.StudijskiProgram;
import rs.ac.singidunum.isa.app.service.NastavnikService;
import rs.ac.singidunum.isa.app.service.StudijskiProgramService;

@Controller
@RequestMapping(path = "/api/studijskiprogrami")
@CrossOrigin
public class StudijskiProgramController {
	
	@Autowired
	private StudijskiProgramService studijskiProgramService;
	
	@Autowired
	private NastavnikService nastavnikService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<StudijskiProgramDTO>> getAllStudijskiProgrami() {
		
		
		ArrayList<StudijskiProgramDTO> studijskiProgrami = new ArrayList<StudijskiProgramDTO>();
		
		for(StudijskiProgram sp : studijskiProgramService.findAll()) {
			
			FakultetDTO fakultet = new FakultetDTO(sp.getFakultet().getId(), sp.getFakultet().getNaziv());
			NastavnikDTO rukovodilac = new NastavnikDTO(sp.getRukovodilac().getId(), sp.getRukovodilac().getIme(), sp.getRukovodilac().getBiografija(), sp.getRukovodilac().getJmbg());
			
			ArrayList<GodinaStudijaDTO> godinaStudija = new ArrayList<GodinaStudijaDTO>();
			
			for(GodinaStudija gs : sp.getGodineStudija()) {
				GodinaStudijaDTO godstudija = new GodinaStudijaDTO(gs.getId(), gs.getGodina());
			}
			
			studijskiProgrami.add(new StudijskiProgramDTO(sp.getId(), sp.getNaziv(), fakultet, rukovodilac, godinaStudija));
			//nastavnici.add(new NastavnikDTO(nastavnik.getId(), nastavnik.getIme(), nastavnik.getBiografija(), nastavnik.getJmbg(), zvanja));
		}
		
		return new ResponseEntity<Iterable<StudijskiProgramDTO>>(studijskiProgrami, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{studijskiProgramId}", method = RequestMethod.GET)
	public ResponseEntity<StudijskiProgramDTO> getStudijskiProgram(@PathVariable("studijskiProgramId") Long studijskiProgramId) {
		
		Optional<StudijskiProgram> studijskiProgram = studijskiProgramService.findOne(studijskiProgramId);
		
		
		if(studijskiProgram.isPresent()) {
			
			FakultetDTO fakultet = new FakultetDTO(studijskiProgram.get().getFakultet().getId(), studijskiProgram.get().getFakultet().getNaziv());
			NastavnikDTO rukovodilac = new NastavnikDTO(studijskiProgram.get().getRukovodilac().getId(), studijskiProgram.get().getRukovodilac().getIme(), studijskiProgram.get().getRukovodilac().getBiografija(), studijskiProgram.get().getRukovodilac().getJmbg());
			
			StudijskiProgramDTO studijskiProgramDTO = new StudijskiProgramDTO(studijskiProgram.get().getId(), studijskiProgram.get().getNaziv(), fakultet, rukovodilac, null);
			
			return new ResponseEntity<StudijskiProgramDTO>(studijskiProgramDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<StudijskiProgramDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<StudijskiProgram> createStudijskiProgram(@RequestBody StudijskiProgram studijskiProgram){
		
		try {
			Optional<Nastavnik> n = nastavnikService.findOne(studijskiProgram.getRukovodilac().getId());
			n.get().setStudijskiProgram(studijskiProgram);
			studijskiProgramService.save(studijskiProgram);
			nastavnikService.save(n.get());
			
			return new ResponseEntity<StudijskiProgram>(studijskiProgram, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<StudijskiProgram>(HttpStatus.BAD_REQUEST);
		
	}
	
	@RequestMapping(path = "/{studijskiProgramId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<StudijskiProgram> updateStudijskiProgram(@PathVariable("studijskiProgramId") Long studijskiProgramId, @RequestBody StudijskiProgram izmenjenStudijskiProgram) {
		
		StudijskiProgram studijskiProgram = studijskiProgramService.findOne(studijskiProgramId).orElse(null);
		
		if( studijskiProgram != null) {
			izmenjenStudijskiProgram.setId(studijskiProgramId);
			
			studijskiProgramService.save(izmenjenStudijskiProgram);
			return new ResponseEntity<StudijskiProgram>(izmenjenStudijskiProgram, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<StudijskiProgram>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{studijskiProgramId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<StudijskiProgram> deleteStudijskiProgram(@PathVariable("studijskiProgramId") Long studijskiProgramId) {
	
		if(studijskiProgramService.findOne(studijskiProgramId).isPresent()) {
			studijskiProgramService.delete(studijskiProgramId);
			return new ResponseEntity<StudijskiProgram>(HttpStatus.OK);
		}
		
		return new ResponseEntity<StudijskiProgram>(HttpStatus.NOT_FOUND);
	}

}
