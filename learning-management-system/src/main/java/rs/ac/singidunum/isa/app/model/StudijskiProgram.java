package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity
public class StudijskiProgram {
	
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String naziv;
	
	@ManyToOne
	@JoinColumn(name = "fakultet_id")
	private Fakultet fakultet;
	
	@OneToOne(mappedBy = "studijskiProgram", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private Nastavnik rukovodilac;
	
	@OneToMany(mappedBy = "studijskiProgram")
	private Set<GodinaStudija> godineStudija = new HashSet<GodinaStudija>();
	
	
	public Set<GodinaStudija> getGodineStudija() {
		return godineStudija;
	}

	public void setGodineStudija(Set<GodinaStudija> godineStudija) {
		this.godineStudija = godineStudija;
	}

	public StudijskiProgram() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudijskiProgram(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Fakultet getFakultet() {
		return fakultet;
	}

	public void setFakultet(Fakultet fakultet) {
		this.fakultet = fakultet;
	}

	public Nastavnik getRukovodilac() {
		return rukovodilac;
	}

	public void setRukovodilac(Nastavnik rukovodilac) {
		this.rukovodilac = rukovodilac;
	}
	
}
