package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;
import rs.ac.singidunum.isa.app.repository.RegistrovaniKorisnikRepository;

@Service
public class RegistrovaniKorisnikService extends GenerateService<RegistrovaniKorisnikRepository, RegistrovaniKorisnik, Long>{
	
	public Optional<RegistrovaniKorisnik> findByKorisnickoIme(String korisnickoIme) {
		return this.repository.findByKorisnickoIme(korisnickoIme);
	}
	
}
