package rs.ac.singidunum.isa.app.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.PohadjanjePredmeta;
import rs.ac.singidunum.isa.app.repository.PohadjanjePredmetaRepository;

@Service
public class PohadjanjePredmetaService extends GenerateService<PohadjanjePredmetaRepository, PohadjanjePredmeta, Long>{

}
