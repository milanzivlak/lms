package rs.ac.singidunum.isa.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.NastavnikNaRealizacijiDTO;
import rs.ac.singidunum.isa.app.dto.TipNastaveDTO;
import rs.ac.singidunum.isa.app.dto.TipZvanjaDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.NastavnikNaRealizaciji;
import rs.ac.singidunum.isa.app.model.TipNastave;
import rs.ac.singidunum.isa.app.model.TipZvanja;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.TerminNastaveDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TerminNastave;
import rs.ac.singidunum.isa.app.parsers.TipNastaveParser;
import rs.ac.singidunum.isa.app.parsers.TipZvanjaParser;
import rs.ac.singidunum.isa.app.service.TipNastaveService;

@Controller
@RequestMapping(path = "/api/tipnastave")
@CrossOrigin
public class TipNastaveController {
	
	@Autowired
	private TipNastaveService tipNastaveService;
	
	TipNastaveParser parser = new TipNastaveParser();
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<TipNastaveDTO>> getAllTipNastave() {
		
		
		ArrayList<TipNastaveDTO> tipoviNastave = new ArrayList<TipNastaveDTO>();
		
		for(TipNastave tipNastave : tipNastaveService.findAll()) {
			ArrayList<NastavnikNaRealizacijiDTO> realizacije = new ArrayList<NastavnikNaRealizacijiDTO>();
			ArrayList<TerminNastaveDTO> terminNastave = new ArrayList<TerminNastaveDTO>();
			
			for(NastavnikNaRealizaciji nnr : tipNastave.getRealizacije()) {
				realizacije.add(new NastavnikNaRealizacijiDTO(nnr.getId(), nnr.getBrojCasova()));
			}
			
			for(TerminNastave tn : tipNastave.getTerminiNastave()) {
				terminNastave.add(new TerminNastaveDTO(tn.getId(), tn.getVremePocetka(), tn.getVremeKraja()));
			}
			
			tipoviNastave.add(new TipNastaveDTO(tipNastave.getId(), tipNastave.getNaziv(), realizacije, terminNastave));
		}
		
		return new ResponseEntity<Iterable<TipNastaveDTO>>(tipoviNastave, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/import", method = RequestMethod.GET)
	public ResponseEntity<Iterable<TipNastaveDTO>> tipNastaveImport(@Param(value = "url") String url) throws UnsupportedEncodingException {

		
		ArrayList<TipNastaveDTO> importovane = this.parser.importXml(url);
		
		for(TipNastaveDTO d : importovane) {
			TipNastave tipNastave = new TipNastave(d.getId(), d.getNaziv());
			tipNastaveService.save(tipNastave);
			
		}
		return new ResponseEntity<Iterable<TipNastaveDTO>>(this.parser.importXml(url), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/export", method = RequestMethod.GET)
	public void tipZvanjaExport() {
		System.out.println("Exporting...");
		ArrayList<TipNastaveDTO> tipovi = new ArrayList<TipNastaveDTO>();
		
		for(TipNastave tipNastave : tipNastaveService.findAll()) {
			tipovi.add(TipNastaveDTO.fromTipNastave(tipNastave));
		}
		
		this.parser.spisakNastava = tipovi;
		try {
			this.parser.export();
		} catch (IOException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(path = "/{tipNastaveId}", method = RequestMethod.GET)
	public ResponseEntity<TipNastaveDTO> getTipNastave(@PathVariable("tipNastaveId") Long tipNastaveId) {
		
		Optional<TipNastave> tipNastave = tipNastaveService.findOne(tipNastaveId);
		
		if(tipNastave.isPresent()) {
			TipNastaveDTO tipNastaveDTO = new TipNastaveDTO(tipNastave.get().getId(), tipNastave.get().getNaziv());
			return new ResponseEntity<TipNastaveDTO>(tipNastaveDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<TipNastaveDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	public ResponseEntity<TipNastave> createTipNastave(@RequestBody TipNastave tipNastave){
		
		try {
			
			tipNastaveService.save(tipNastave);
			
			return new ResponseEntity<TipNastave>(tipNastave, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<TipNastave>(HttpStatus.BAD_REQUEST);

		
	}
	
	@RequestMapping(path = "/{tipNastaveId}", method = RequestMethod.PUT)
	//@Secured("ROLE_ADMIN")
	public ResponseEntity<TipNastave> updateTipNastave(@PathVariable("tipNastaveId") Long tipNastaveId, @RequestBody TipNastave izmenjenTipNastave) {
		
		TipNastave tipNastave = tipNastaveService.findOne(tipNastaveId).orElse(null);
		
		if( tipNastave != null) {
			izmenjenTipNastave.setId(tipNastaveId);
			
			tipNastaveService.save(izmenjenTipNastave);
			return new ResponseEntity<TipNastave>(izmenjenTipNastave, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<TipNastave>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{tipNastaveId}", method = RequestMethod.DELETE)
	//@Secured("ROLE_ADMIN")
	public ResponseEntity<TipNastave> deleteTipNastave(@PathVariable("tipNastaveId") Long tipNastaveId) {
	
		if(tipNastaveService.findOne(tipNastaveId).isPresent()) {
			tipNastaveService.delete(tipNastaveId);
			return new ResponseEntity<TipNastave>(HttpStatus.OK);
		}
		
		return new ResponseEntity<TipNastave>(HttpStatus.NOT_FOUND);
	}
}
