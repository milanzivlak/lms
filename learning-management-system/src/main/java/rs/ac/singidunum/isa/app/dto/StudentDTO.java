package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class StudentDTO {
	
	private Long id;
	private String jmbg;
	private String ime;
	
	private AdresaDTO adresa;
	
	
	private ArrayList<StudentNaGodiniDTO> godine = new ArrayList<StudentNaGodiniDTO>();
	
	private ArrayList<PohadjanjePredmetaDTO> pohadjanje = new ArrayList<PohadjanjePredmetaDTO>();
	
	private RegistrovaniKorisnikDTO registrovaniKorisnik;
	
	
	public StudentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public StudentDTO(Long id, String jmbg, String ime) {
		this(id, jmbg, ime, null, null, null, null);
	}
	
	public StudentDTO(Long id, String jmbg, String ime, AdresaDTO adresa, ArrayList<StudentNaGodiniDTO> godine, ArrayList<PohadjanjePredmetaDTO> pohadjanje, RegistrovaniKorisnikDTO registrovaniKorisnik) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
		this.godine = godine;
		this.pohadjanje = pohadjanje;
		this.registrovaniKorisnik = registrovaniKorisnik;
		
	}

	public RegistrovaniKorisnikDTO getRegistrovaniKorisnik() {
		return registrovaniKorisnik;
	}

	public void setRegistrovaniKorisnik(RegistrovaniKorisnikDTO registrovaniKorisnik) {
		this.registrovaniKorisnik = registrovaniKorisnik;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public AdresaDTO getAdresa() {
		return adresa;
	}

	public void setAdresa(AdresaDTO adresa) {
		this.adresa = adresa;
	}

	public ArrayList<StudentNaGodiniDTO> getGodine() {
		return godine;
	}

	public void setGodine(ArrayList<StudentNaGodiniDTO> godine) {
		this.godine = godine;
	}

	public ArrayList<PohadjanjePredmetaDTO> getPohadjanje() {
		return pohadjanje;
	}

	public void setPohadjanje(ArrayList<PohadjanjePredmetaDTO> pohadjanje) {
		this.pohadjanje = pohadjanje;
	}
	
	
}
