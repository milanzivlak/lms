package rs.ac.singidunum.isa.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
//<<<<<<< HEAD
//=======
import org.springframework.security.access.prepost.PreAuthorize;
//>>>>>>> a7906396c2fd4778e0252a23614b9f1ffef93107
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.actors.RegistrovaniKorisnik;
import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.GodinaStudijaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.dto.PohadjanjePredmetaDTO;
import rs.ac.singidunum.isa.app.dto.PredmetDTO;
import rs.ac.singidunum.isa.app.dto.RegistrovaniKorisnikDTO;
import rs.ac.singidunum.isa.app.dto.StudentDTO;
import rs.ac.singidunum.isa.app.dto.StudentNaGodiniDTO;
import rs.ac.singidunum.isa.app.dto.StudijskiProgramDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Fakultet;
import rs.ac.singidunum.isa.app.model.GodinaStudija;
import rs.ac.singidunum.isa.app.model.Mesto;
import rs.ac.singidunum.isa.app.model.PohadjanjePredmeta;
import rs.ac.singidunum.isa.app.model.Predmet;
import rs.ac.singidunum.isa.app.model.Student;
import rs.ac.singidunum.isa.app.model.StudentNaGodini;
import rs.ac.singidunum.isa.app.model.StudijskiProgram;
import rs.ac.singidunum.isa.app.model.Univerzitet;
import rs.ac.singidunum.isa.app.parsers.StudentParser;
import rs.ac.singidunum.isa.app.service.AdresaService;
import rs.ac.singidunum.isa.app.service.DrzavaService;
import rs.ac.singidunum.isa.app.service.MestoService;
import rs.ac.singidunum.isa.app.service.RegistrovaniKorisnikService;
import rs.ac.singidunum.isa.app.service.StudentService;

@Controller
@RequestMapping(path = "/api/studenti")
@PreAuthorize("hasAuthority('KORISNIK')")
@CrossOrigin
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private DrzavaService drzavaService;
	
	@Autowired
	private MestoService mestoService;
	
	@Autowired
	private AdresaService adresaService;
	
	@Autowired
	private RegistrovaniKorisnikService registrovaniKorisnikService;
	
	StudentParser parser = new StudentParser();
	
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<StudentDTO>> getAllStudente() {
		
		
		ArrayList<StudentDTO> studenti = new ArrayList<StudentDTO>();
		
		for(Student student : studentService.findAll()) {
			ArrayList<StudentNaGodiniDTO> studentNaGodini = new ArrayList<StudentNaGodiniDTO>();
			ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta = new ArrayList<PohadjanjePredmetaDTO>();
			
			for(StudentNaGodini sng : student.getGodine()) {
				
				studentNaGodini.add(new StudentNaGodiniDTO(sng.getId(), sng.getDatumUpisa(), sng.getBrojIndeksa()));
			}
			
			for(PohadjanjePredmeta pp : student.getPohadjanja()) {
				pohadjanjePredmeta.add(new PohadjanjePredmetaDTO(pp.getId(), pp.getKonacnaOcena()));
			}
			
			AdresaDTO adresa = new AdresaDTO(student.getAdresa().getId(), student.getAdresa().getUlica(), student.getAdresa().getBroj());
			RegistrovaniKorisnikDTO rk = new RegistrovaniKorisnikDTO();
			rk.setId(student.getRegistrovaniKorisnik().getId());
			rk.setEmail(student.getRegistrovaniKorisnik().getEmail());
			rk.setKorisnickoIme(student.getRegistrovaniKorisnik().getKorisnickoIme());
			rk.setLozinka(student.getRegistrovaniKorisnik().getLozinka());
			studenti.add(new StudentDTO(student.getId(), student.getJmbg(), student.getIme(), adresa, studentNaGodini, pohadjanjePredmeta, rk));
		}
		
		return new ResponseEntity<Iterable<StudentDTO>>(studenti, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/export", method = RequestMethod.GET)
	public void studentiExport() {
		System.out.println("Exporting...");
		ArrayList<StudentDTO> studenti = new ArrayList<StudentDTO>();
		
		for(Student student : studentService.findAll()) {
			StudentDTO studentDTO = new StudentDTO(student.getId(), student.getJmbg(), student.getIme());
			
			AdresaDTO adresaDTO = new AdresaDTO(student.getAdresa().getId(), student.getAdresa().getUlica(), student.getAdresa().getBroj());
			
			Mesto mesto = student.getAdresa().getMesto();
			MestoDTO mestoDTO = MestoDTO.fromMesto(mesto);
			
			Drzava drzava = mesto.getDrzava();
			DrzavaDTO drzavaDTO = DrzavaDTO.fromDrzava(drzava);
			mestoDTO.setDrzava(drzavaDTO);
			adresaDTO.setMesto(mestoDTO);
			studentDTO.setAdresa(adresaDTO);
			
			RegistrovaniKorisnikDTO rkDTO = new RegistrovaniKorisnikDTO();
			rkDTO.setId(student.getRegistrovaniKorisnik().getId());
			rkDTO.setEmail(student.getRegistrovaniKorisnik().getEmail());
			rkDTO.setKorisnickoIme(student.getRegistrovaniKorisnik().getKorisnickoIme());
			rkDTO.setLozinka(student.getRegistrovaniKorisnik().getLozinka());
			studentDTO.setRegistrovaniKorisnik(rkDTO);
			System.out.println(rkDTO.getEmail());
			studenti.add(studentDTO);
		}
		
		this.parser.spisakStudenata = studenti;
		try {
			this.parser.export();
		} catch (IOException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(path = "/import", method = RequestMethod.GET)
	public ResponseEntity<Iterable<StudentDTO>> studentiImport(@Param(value = "url") String url) throws UnsupportedEncodingException {

		
		ArrayList<StudentDTO> importovane = this.parser.importXml(url);
		
		for(StudentDTO d : importovane) {
			Student student = new Student(d.getId(), d.getJmbg(), d.getIme());
			
			Drzava drzava = new Drzava(d.getAdresa().getMesto().getDrzava().getId(), d.getAdresa().getMesto().getDrzava().getNaziv());
			
			Mesto mesto = new Mesto(d.getAdresa().getMesto().getId(), d.getAdresa().getMesto().getNaziv());
			
			Adresa adresa = new Adresa(d.getAdresa().getId(), d.getAdresa().getUlica(), d.getAdresa().getBroj());
			
			RegistrovaniKorisnik reg_kor = new RegistrovaniKorisnik();
			reg_kor.setId(d.getRegistrovaniKorisnik().getId());
			reg_kor.setEmail(d.getRegistrovaniKorisnik().getEmail());
			reg_kor.setKorisnickoIme(d.getRegistrovaniKorisnik().getKorisnickoIme());
			reg_kor.setLozinka(d.getRegistrovaniKorisnik().getLozinka());
			
			mesto.setDrzava(drzava);
			adresa.setMesto(mesto);
			student.setAdresa(adresa);
			student.setRegistrovaniKorisnik(reg_kor);
			
			drzavaService.save(drzava);
			mestoService.save(mesto);
			adresaService.save(adresa);
			registrovaniKorisnikService.save(reg_kor);
			studentService.save(student);
		}
		
		
		return new ResponseEntity<Iterable<StudentDTO>>(this.parser.importXml(url), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	@RequestMapping(path = "/{studentId}", method = RequestMethod.GET)
	public ResponseEntity<StudentDTO> getStudent(@PathVariable("studentId") Long studentId) {
		
		Optional<Student> student = studentService.findOne(studentId);
		
		if(student.isPresent()) {
			StudentDTO studentDTO = new StudentDTO(student.get().getId(), student.get().getJmbg(), student.get().getIme());
			return new ResponseEntity<StudentDTO>(studentDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<StudentDTO>(HttpStatus.NOT_FOUND);
	}
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Student> createStudent(@RequestBody Student student){
		
		try {
			
			studentService.save(student);
			
			return new ResponseEntity<Student>(student, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Student>(HttpStatus.BAD_REQUEST);

	}
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	@RequestMapping(path = "/{studentId}", method = RequestMethod.PUT)
	public ResponseEntity<Student> updateStudent(@PathVariable("studentId") Long studentId, @RequestBody Student izmenjenStudent) {
		
		Student student = studentService.findOne(studentId).orElse(null);
		
		if( student != null) {
			izmenjenStudent.setId(studentId);
			
			studentService.save(izmenjenStudent);
			return new ResponseEntity<Student>(izmenjenStudent, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Student>(HttpStatus.NOT_FOUND);

	}
	@Secured({"ROLE_ADMINISTRATOR", "ROLE_NASTAVNIK"})
	@RequestMapping(path = "/{studentId}", method = RequestMethod.DELETE)
	public ResponseEntity<Student> deleteStudent(@PathVariable("studentId") Long studentId) {
	
		if(studentService.findOne(studentId).isPresent()) {
			studentService.delete(studentId);
			return new ResponseEntity<Student>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Student>(HttpStatus.NOT_FOUND);
	}

}
