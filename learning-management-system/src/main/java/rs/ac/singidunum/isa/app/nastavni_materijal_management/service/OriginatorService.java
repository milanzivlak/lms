package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.memento.Originator;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.OriginatorRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class OriginatorService extends GenerateService<OriginatorRepository, Originator, Long>{

}
