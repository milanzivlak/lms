package rs.ac.singidunum.isa.app.model;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.EvaluacijaZnanja;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.NastavniMaterijal;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.ObrazovniCilj;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TerminNastave;

import javax.persistence.Id;

@Entity
public class Ishod {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String opis;
	
	@OneToOne
	@JsonIgnore
	private Predmet predmet;
	
	@ManyToOne
	@JsonIgnore
	private NastavniMaterijal nastavniMaterijal;
	
	@ManyToOne
	@JsonIgnore
	private ObrazovniCilj obrazovniCilj;
	
	
	@OneToOne(mappedBy = "ishod", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnore
	private TerminNastave terminNastave;
	
	@OneToOne(mappedBy = "ishod")
	@JsonIgnore
	private EvaluacijaZnanja evaluacijaZnanja;
	

	
	
	public EvaluacijaZnanja getEvaluacijaZnanja() {
		return evaluacijaZnanja;
	}

	public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
		this.evaluacijaZnanja = evaluacijaZnanja;
	}

	public ObrazovniCilj getObrazovniCilj() {
		return obrazovniCilj;
	}

	public void setObrazovniCilj(ObrazovniCilj obrazovniCilj) {
		this.obrazovniCilj = obrazovniCilj;
	}

	public TerminNastave getTerminNastave() {
		return terminNastave;
	}

	public void setTerminNastave(TerminNastave terminNastave) {
		this.terminNastave = terminNastave;
	}

	public NastavniMaterijal getNastavniMaterijal() {
		return nastavniMaterijal;
	}

	public void setNastavniMaterijal(NastavniMaterijal nastavniMaterijal) {
		this.nastavniMaterijal = nastavniMaterijal;
	}

	public Ishod() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Ishod(Long id, String opis) {
		super();
		this.id = id;
		this.opis = opis;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Predmet getPredmet() {
		return predmet;
	}

	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}
	
}
