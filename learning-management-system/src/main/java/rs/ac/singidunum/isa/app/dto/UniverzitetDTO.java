package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.model.Univerzitet;

public class UniverzitetDTO {
	
	private Long id;
	private String ulica;
	private String broj;
	
	private AdresaDTO adresa;
	private NastavnikDTO rektor;
	
	private ArrayList<FakultetDTO> fakulteti = new ArrayList<FakultetDTO>();

	public UniverzitetDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public UniverzitetDTO(Long id, String ulica, String broj) {
		this(id, ulica, broj, null, null, null);
	}

	public UniverzitetDTO(Long id, String ulica, String broj, AdresaDTO adresa, NastavnikDTO rektor,
			ArrayList<FakultetDTO> fakulteti) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
		this.adresa = adresa;
		this.rektor = rektor;
		this.fakulteti = fakulteti;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public AdresaDTO getAdresa() {
		return adresa;
	}

	public void setAdresa(AdresaDTO adresa) {
		this.adresa = adresa;
	}

	public NastavnikDTO getRektor() {
		return rektor;
	}

	public void setRektor(NastavnikDTO rektor) {
		this.rektor = rektor;
	}

	public ArrayList<FakultetDTO> getFakulteti() {
		return fakulteti;
	}

	public void setFakulteti(ArrayList<FakultetDTO> fakulteti) {
		this.fakulteti = fakulteti;
	}
	
	

	public static UniverzitetDTO fromUniverzitet(Univerzitet uni) {
		return new UniverzitetDTO(
				uni.getId(),
				uni.getUlica(),
				uni.getBroj()
				
				);
	}
}
