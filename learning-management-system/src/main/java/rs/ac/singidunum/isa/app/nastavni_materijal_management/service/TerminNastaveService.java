package rs.ac.singidunum.isa.app.nastavni_materijal_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.TerminNastave;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.repository.TerminNastaveRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class TerminNastaveService extends GenerateService<TerminNastaveRepository, TerminNastave, Long>{
	public Page<TerminNastave> findAll(Pageable pageable) {
		return ((TerminNastaveRepository) this.getRepository()).findAll(pageable);
	}

}
