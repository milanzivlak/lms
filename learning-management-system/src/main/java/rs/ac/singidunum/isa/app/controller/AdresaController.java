package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.AdresaDTO;
import rs.ac.singidunum.isa.app.dto.FakultetDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.dto.StudentDTO;
import rs.ac.singidunum.isa.app.dto.UniverzitetDTO;
import rs.ac.singidunum.isa.app.model.Adresa;
import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Fakultet;
import rs.ac.singidunum.isa.app.model.Mesto;
import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.Student;
import rs.ac.singidunum.isa.app.model.Univerzitet;
import rs.ac.singidunum.isa.app.service.AdresaService;
import rs.ac.singidunum.isa.app.service.DrzavaService;
import rs.ac.singidunum.isa.app.service.FakultetService;
import rs.ac.singidunum.isa.app.service.MestoService;
import rs.ac.singidunum.isa.app.service.UniverzitetService;



@Controller
@RequestMapping(path = "/api/adresa")
@CrossOrigin
public class AdresaController {
	
	@Autowired
	private AdresaService adresaService;
	
	@Autowired
	private MestoService mestoService;
	
	@Autowired
	private FakultetService fakultetService;
	
	@Autowired
	private UniverzitetService univerzitetService;
	
	@Autowired
	private DrzavaService drzavaService;
	
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<AdresaDTO>> getAllAdrese(Pageable pageable) {
		return new ResponseEntity<Page<AdresaDTO>>(
				adresaService.findAll(pageable).map(a -> AdresaDTO.fromAdresa(a)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{adresaId}", method = RequestMethod.GET)
	@Transactional
	public ResponseEntity<AdresaDTO> getAdresa(@PathVariable("adresaId") Long adresaId) {
		
		Optional<Adresa> adresa = adresaService.findOne(adresaId);
		
		if(adresa.isPresent()) {
			AdresaDTO adresaDTO = new AdresaDTO(adresa.get().getId(), adresa.get().getUlica(), adresa.get().getBroj());
			return new ResponseEntity<AdresaDTO>(adresaDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<AdresaDTO>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Adresa> createAdresa(@RequestBody Adresa adresa){
		
		try {

			//Fakultet fakultet = fakultetService.findOne(adresa.getFakultet().getId()).get();
			

		//	Univerzitet uni = univerzitetService.findOne(fakultet.getUniverzitet().getId()).get();
			
		//	adresa.setUniverzitet(uni);
		//	adresa.setFakultet(fakultet);
			//univerzitetService.save(uni);
			//fakultetService.save(fakultet);
			adresaService.save(adresa);
			
			return new ResponseEntity<Adresa>(adresa, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Adresa>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{adresaId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Adresa> updateAdresa(@PathVariable("adresaId") Long adresaId, @RequestBody Adresa izmenjenaAdresa) {
		
		Adresa adresa = adresaService.findOne(adresaId).orElse(null);
		
		if( adresa != null) {
			izmenjenaAdresa.setId(adresaId);
			
			adresaService.save(izmenjenaAdresa);
			return new ResponseEntity<Adresa>(izmenjenaAdresa, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Adresa>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{adresaId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Adresa> deleteAdresa(@PathVariable("adresaId") Long adresaId) {
	
		if(adresaService.findOne(adresaId).isPresent()) {
			adresaService.delete(adresaId);
			return new ResponseEntity<Adresa>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Adresa>(HttpStatus.NOT_FOUND);
	}
	
	
}
