package rs.ac.singidunum.isa.app.nastavni_materijal_management.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.DrzavaDTO;
import rs.ac.singidunum.isa.app.dto.MestoDTO;
import rs.ac.singidunum.isa.app.model.Drzava;
import rs.ac.singidunum.isa.app.model.Mesto;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.dto.AutorDTO;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.model.Autor;
import rs.ac.singidunum.isa.app.nastavni_materijal_management.service.AutorService;
import rs.ac.singidunum.isa.app.parsers.AutorParser;

@Controller
@RequestMapping(path = "/api/nastavni-materijal/autor")
public class AutorController {
	@Autowired
	private AutorService autorService;
	
	AutorParser parser = new AutorParser();
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<AutorDTO>> getAllAutori(Pageable pageable) {
		return new ResponseEntity<Page<AutorDTO>>(
				autorService.findAll(pageable).map(a -> new AutorDTO(a)), HttpStatus.OK);

	}
	
	@RequestMapping(path = "/{autorId}", method = RequestMethod.GET)
	public ResponseEntity<AutorDTO> getAutor(@PathVariable("autorId") Long autorId) {
		
		Optional<Autor> autor = autorService.findOne(autorId);
		
		if(autor.isPresent()) {
			AutorDTO AutorDTO = new AutorDTO(autor.get());
			return new ResponseEntity<AutorDTO>(AutorDTO, HttpStatus.OK);

		}
		
		return new ResponseEntity<AutorDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "/export", method = RequestMethod.GET)
	public void autoriExport() {
		System.out.println("Exporting...");
		ArrayList<AutorDTO> autori = new ArrayList<AutorDTO>();
		
		for(Autor autor : autorService.findAll()) {
			AutorDTO dto = new AutorDTO(autor.getId(), autor.getIme(), autor.getPrezime());
			autori.add(dto);
		}
		
		this.parser.spisakAutora = autori;
		try {
			this.parser.export();
		} catch (IOException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(path = "/import", method = RequestMethod.GET)
	public ResponseEntity<Iterable<AutorDTO>> autoriImport(@Param(value = "url") String url) throws UnsupportedEncodingException {

		
		ArrayList<AutorDTO> importovani = this.parser.importXml(url);
		
		for(AutorDTO a : importovani) {
			Autor autor = new Autor(a.getId(), a.getIme(), a.getPrezime());
			autorService.save(autor);
			
			
		}
		return new ResponseEntity<Iterable<AutorDTO>>(this.parser.importXml(url), HttpStatus.OK);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Autor> createAutor(@RequestBody Autor autor){
		
		try {
			
			autorService.save(autor);
			
			return new ResponseEntity<Autor>(autor, HttpStatus.CREATED);
		
		}catch(Exception e) {
			e.printStackTrace();
			 
		}			
		
		return new ResponseEntity<Autor>(HttpStatus.BAD_REQUEST);

		
	}
	
	
	@RequestMapping(path = "/{autorId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Autor> updateAutor(@PathVariable("autorId") Long autorId, @RequestBody Autor izmenjenAutor) {
		
		Autor adresa = autorService.findOne(autorId).orElse(null);
		
		if( adresa != null) {
			izmenjenAutor.setId(autorId);
			
			autorService.save(izmenjenAutor);
			return new ResponseEntity<Autor>(izmenjenAutor, HttpStatus.OK);
			
		}
		
		return new ResponseEntity<Autor>(HttpStatus.NOT_FOUND);

	}
	
	@RequestMapping(path = "/{autorId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMINISTRATOR")
	public ResponseEntity<Autor> deleteAutor(@PathVariable("autorId") Long autorId) {
	
		if(autorService.findOne(autorId).isPresent()) {
			autorService.delete(autorId);
			return new ResponseEntity<Autor>(HttpStatus.OK);
		}
		
		return new ResponseEntity<Autor>(HttpStatus.NOT_FOUND);
	}

}
