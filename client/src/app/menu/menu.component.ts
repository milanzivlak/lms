import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  @Input() user: any;


  isNastavnik = false
  isStudent = false
  isAdmin = false
  isKorisnik = false

  constructor(public loginService: LoginService) { }

  ngOnInit(): void {
    this.checkRoles();
  }

  checkRoles() {
    if(this.loginService.hasRole("ROLE_NASTAVNIK")) {
      this.isNastavnik = true;
    }

    if(this.loginService.hasRole("ROLE_STUDENT")) {
      this.isStudent = true;
    }

    if(this.loginService.hasRole("ROLE_ADMINISTRATOR")) {
      this.isAdmin = true;
    }

    if(this.loginService.hasRole("ROLE_KORISNIK")) {
      this.isKorisnik = true;
    }

  }

  logout() {
    this.loginService.logout();
  }
}
