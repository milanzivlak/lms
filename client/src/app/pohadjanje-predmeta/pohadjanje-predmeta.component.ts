import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { PohadjanjePredmeta } from '../model/pohadjanje-predmeta';
import { PohadjanjePredmetaService } from '../services/pohadjanje-predmeta.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-pohadjanje-predmeta',
  templateUrl: './pohadjanje-predmeta.component.html',
  styleUrls: ['./pohadjanje-predmeta.component.css']
})
export class PohadjanjePredmetaComponent extends BaseComponent<PohadjanjePredmeta, number> implements OnInit {

  constructor(public pohadjanjePredmetaService : PohadjanjePredmetaService, public loginService:LoginService) { 
    super(pohadjanjePredmetaService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'realizacijaPredmeta', 'student'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    if(this.loginService.hasRole("ROLE_NASTAVNIK")) {
      this.findAllAsync();
    }
    
  }

  findAllAsync(){
    this.pohadjanjePredmetaService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:PohadjanjePredmeta){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

}
