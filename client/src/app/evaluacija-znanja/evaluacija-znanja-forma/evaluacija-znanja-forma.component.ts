import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { EvaluacijaZnanjaService } from 'src/app/services/evaluacija-znanja.service';

@Component({
  selector: 'app-evaluacija-znanja-forma',
  templateUrl: './evaluacija-znanja-forma.component.html',
  styleUrls: ['./evaluacija-znanja-forma.component.css']
})
export class EvaluacijaZnanjaFormaComponent implements OnInit {

  form = new FormGroup({
    id: new FormControl(),
    evaluacijaZnanja: new FormControl()
  });

  constructor(private evaluacijaZnanjaService : EvaluacijaZnanjaService) { }

  ngOnInit(): void {
  }
  
  submit(){
    this.evaluacijaZnanjaService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error) => {
      console.log(error);
    })
  }

}
