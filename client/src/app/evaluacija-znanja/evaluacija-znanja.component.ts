import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from '../model/base-component';
import { EvaluacijaZnanja } from '../model/evaluacija-znanja'
import { EvaluacijaZnanjaService } from '../services/evaluacija-znanja.service';

@Component({
  selector: 'app-evaluacija-znanja',
  templateUrl: './evaluacija-znanja.component.html',
  styleUrls: ['./evaluacija-znanja.component.css']
})
export class EvaluacijaZnanjaComponent extends BaseComponent<EvaluacijaZnanja, number> implements OnInit {

  constructor(public evaluacijaZnanjaService : EvaluacijaZnanjaService) {
    super(evaluacijaZnanjaService);
   }

   pageable:any;
  displayedColumns: string[] = ['vremePocetka', 'vremeZavrsetka', 'bodovi'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator
  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.evaluacijaZnanjaService.findAll().subscribe((value) =>{
      this.pageable = value;
      console.log(this.pageable.content);
      this._elements = this.pageable.content;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:EvaluacijaZnanja){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

}
