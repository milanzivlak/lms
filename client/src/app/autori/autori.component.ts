import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from 'src/environments/environment';
import { Autor } from '../model/autor';
import { BaseComponent } from '../model/base-component';
import { AdresaService } from '../services/adresa.service';
import { AutorService } from '../services/autor.service';

@Component({
  selector: 'app-autori',
  templateUrl: './autori.component.html',
  styleUrls: ['./autori.component.css']
})
export class AutoriComponent extends BaseComponent<Autor,number> implements OnInit {

  constructor(public autorService:AutorService) { 
    super(autorService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'ime', 'prezime', 'akcije'];
  dataSource = new MatTableDataSource(this._elements);
  finalResult:string;
  
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator
  
  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.autorService.findAll().subscribe((value) =>{
      this.pageable = value;
      this._elements = this.pageable.content;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  deleteRow(element:Autor){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  export(){
    this.autorService.exportAll();
  }

  import(importUrl:string){
    this.autorService._http.get<Autor[]>(`${environment.api.baseUrl}/nastavni-materijal/autor/import`, {params: {"url":importUrl}}).subscribe((value) =>{
      this._elements = value;
      this.dataSource.data = this._elements;
      this.dataSource._updatePaginator;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, (error) => {
      console.log(error);
    });
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any) : void {
    var file:File = inputValue.files[0]; 
    
    
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.finalResult = myReader.result as string;
      console.log("FINAL RESULT", this.finalResult)
      this.import(this.finalResult);
   }
  
    if (file) {
      myReader.readAsText(file);
    }
  
  }
}
