import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdreseFormaComponent } from './adrese/adrese-forma/adrese-forma.component';
import { AdreseComponent } from './adrese/adrese.component';
import { DrzaveFormaComponent } from './drzave/drzave-forma/drzave-forma.component';
import { DrzaveComponent } from './drzave/drzave.component';
import { FakultetiComponent } from './fakulteti/fakulteti.component';
import { GodinaStudijaFormaComponent } from './godina-studija/godina-studija-forma/godina-studija-forma.component';
import { GodinaStudijaComponent } from './godina-studija/godina-studija.component';
import { IshodComponent } from './ishod/ishod.component';
import { LoginComponent } from './login/login.component';
import { MestaFormaComponent } from './mesta/mesta-forma/mesta-forma.component';
import { MestaComponent } from './mesta/mesta.component';
import { NastavniMaterijalComponent } from './nastavni-materijal/nastavni-materijal.component';
import { NastavnikNaRealizacijiComponent } from './nastavnik-na-realizaciji/nastavnik-na-realizaciji.component';
import { NastavnikComponent } from './nastavnik/nastavnik.component';
import { NaucnaOblastComponent } from './naucna-oblast/naucna-oblast.component';
import { PohadjanjePredmetaComponent } from './pohadjanje-predmeta/pohadjanje-predmeta.component';
import { PredmetComponent } from './predmet/predmet.component';
import { RealizacijaPredmetaComponent } from './realizacija-predmeta/realizacija-predmeta.component';
import { StudentNaGodiniComponent } from './student-na-godini/student-na-godini.component';
import { StudentComponent } from './student/student.component';
import { StudijskiProgramiComponent } from './studijski-programi/studijski-programi.component';
import { TipNastaveComponent } from './tip-nastave/tip-nastave.component';
import { TipZvanjaComponent } from './tip-zvanja/tip-zvanja.component';
import { UniverzitetiComponent } from './univerziteti/univerziteti.component';
import { ZvanjeComponent } from './zvanje/zvanje.component';
import { EvaluacijaZnanjaComponent } from './evaluacija-znanja/evaluacija-znanja.component';
import { TerminNastaveComponent } from './termin-nastave/termin-nastave.component';
import { AuthGuard } from './guards/auth.guard';
import { MestaEditComponent } from './mesta/mesta-edit/mesta-edit.component';
import { AutoriComponent } from './autori/autori.component';
import { StudentFormaComponent } from './student/student-forma/student-forma.component';
import { PredmetFormaComponent } from './predmet/predmet-forma/predmet-forma.component';
import { UniverzitetiFormaComponent } from './univerziteti/univerziteti-forma/univerziteti-forma.component';
import { NastavniMaterijalFormaComponent } from './nastavni-materijal/nastavni-materijal-forma/nastavni-materijal-forma.component';
import { NastavnikFormaComponent } from './nastavnik/nastavnik-forma/nastavnik-forma.component';
import { FakultetiFormaComponent } from './fakulteti/fakulteti-forma/fakulteti-forma.component';
import { RegistracijaComponent } from './registracija/registracija.component';
import { DrzaveEditComponent } from './drzave/drzave-edit/drzave-edit.component';
import { FakultetiEditComponent } from './fakulteti/fakulteti-edit/fakulteti-edit.component';
import { AdreseEditComponent } from './adrese/adrese-edit/adrese-edit.component';
import { StudijskiProgramFormaComponent } from './studijski-programi/studijski-program-forma/studijski-program-forma.component';
import { UniverzitetiEditComponent } from './univerziteti/univerziteti-edit/univerziteti-edit.component';
import { NastavniMaterijalEditComponent } from './nastavni-materijal/nastavni-materijal-edit/nastavni-materijal-edit.component';
import { NastavnikEditComponent } from './nastavnik/nastavnik-edit/nastavnik-edit.component';

const routes: Routes = [
  {path: "login", component: LoginComponent},
  {path: "registracija", component:RegistracijaComponent},
  // {path: "drzave", component: DrzaveComponent, canActivate: [AuthGuard], data: {
  //   allowedRoles: ["ROLE_NASTAVNIK", "ROLE_CUSTOMER"]
  // }},
  {path: "drzave", component: DrzaveComponent},
  {path: "drzave-forma", component: DrzaveFormaComponent},
  {path: "drzave/:id", component:DrzaveEditComponent},
  {path: "mesta", component: MestaComponent},
  {path: "mesta-forma", component: MestaFormaComponent},
  {path: "mesta/:id", component: MestaEditComponent},
  {path: "adrese", component: AdreseComponent},
  {path: "adrese-forma", component: AdreseFormaComponent},
  {path: "adrese/:id", component: AdreseEditComponent},
  {path: "univerziteti", component: UniverzitetiComponent},
  {path: "univerziteti-forma", component: UniverzitetiFormaComponent},
  {path: "univerziteti/:id", component: UniverzitetiEditComponent},
  {path: "fakulteti", component: FakultetiComponent},
  {path: "fakulteti-forma", component: FakultetiFormaComponent},
  {path: "fakulteti/:id", component: FakultetiEditComponent},
  {path: "studijskiProgrami", component: StudijskiProgramiComponent},
  {path: "studijskiProgrami-forma", component: StudijskiProgramFormaComponent},
  {path: "godinaStudija", component: GodinaStudijaComponent},
  {path: "godinaStudija-forma", component: GodinaStudijaFormaComponent},
  {path: "studentNaGodini", component: StudentNaGodiniComponent},
  {path: "studenti", component: StudentComponent},
  {path: "studenti-forma", component: StudentFormaComponent},
  {path: "predmeti", component: PredmetComponent},
  {path: "predmeti-forma", component:PredmetFormaComponent},
  {path: "ishodi", component: IshodComponent},
  {path: "nastavnici", component: NastavnikComponent},
  {path: "nastavnici-forma", component: NastavnikFormaComponent},
  {path: "nastavnici/:id", component: NastavnikEditComponent},
  {path: "naucneOblasti", component: NaucnaOblastComponent},
  {path: "zvanja", component: ZvanjeComponent},
  {path: "nastavni-materijal", component: NastavniMaterijalComponent},
  {path: "nastavni-materijal-forma", component: NastavniMaterijalFormaComponent},
  {path: "nastavni-materijal/:id", component: NastavniMaterijalEditComponent},
  {path: "evaluacija-znanja", component: EvaluacijaZnanjaComponent},
  {path: "termin-nastave", component: TerminNastaveComponent},
  {path: "tipoviZvanja", component: TipZvanjaComponent},
  {path: "tipnastave", component: TipNastaveComponent},
  {path: "nastavniknarealizaciji", component: NastavnikNaRealizacijiComponent},
  {path: "realizacijaPredmeta", component: RealizacijaPredmetaComponent},
  {path: "pohadjanjePredmeta", component: PohadjanjePredmetaComponent},
  {path: "autori", component: AutoriComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
