import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TerminNastaveService } from 'src/app/services/termin-nastave.service';

@Component({
  selector: 'app-termin-nastave-forma',
  templateUrl: './termin-nastave-forma.component.html',
  styleUrls: ['./termin-nastave-forma.component.css']
})
export class TerminNastaveFormaComponent implements OnInit {
  
  form = new FormGroup({
    id: new FormControl(),
    terminNastave: new FormControl()
  });

  constructor(private terminNastaveService : TerminNastaveService) { }

  ngOnInit(): void {
  }

  submit(){
    this.terminNastaveService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error) => {
      console.log(error);
    })
  }

}
