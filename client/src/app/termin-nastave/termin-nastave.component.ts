import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from '../model/base-component';
import { TerminNastave } from '../model/termin-nastave'
import { TerminNastaveService } from '../services/termin-nastave.service';

@Component({
  selector: 'app-termin-nastave',
  templateUrl: './termin-nastave.component.html',
  styleUrls: ['./termin-nastave.component.css']
})
export class TerminNastaveComponent extends BaseComponent<TerminNastave, number> implements OnInit {

  constructor(public terminNastaveService : TerminNastaveService) {
    super(terminNastaveService);
   }

   pageable:any;
  displayedColumns: string[] = ['vremePocetka', 'vremeKraja'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.terminNastaveService.findAll().subscribe((value) =>{
      this.pageable = value;
      console.log(this.pageable.content);
      this._elements = this.pageable.content;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:TerminNastave){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

}
