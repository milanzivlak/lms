import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { TipZvanja } from '../model/tip-zvanja';
import { TipZvanjaService } from '../services/tip-zvanja.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-tip-zvanja',
  templateUrl: './tip-zvanja.component.html',
  styleUrls: ['./tip-zvanja.component.css']
})
export class TipZvanjaComponent extends BaseComponent<TipZvanja, number> implements OnInit {

  constructor(public tipZvanjaService : TipZvanjaService) { 
    super(tipZvanjaService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'naziv'];
  dataSource = new MatTableDataSource(this._elements);
  finalResult:string;

  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync()
  }

  findAllAsync(){
    this.tipZvanjaService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:TipZvanja){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

  export() {
    this.tipZvanjaService.exportAll();
  }

  import(importUrl:string){
    this.tipZvanjaService._http.get<TipZvanja[]>(`${environment.api.baseUrl}/tipoviZvanja/import`, {params: {"url":importUrl}}).subscribe((value) =>{
      this._elements = value;
      this.dataSource.data = this._elements;
      this.dataSource._updatePaginator;
      this.dataSource.sort = this.sort;
    }, (error)=> {
      console.log(error);
    });
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any) : void {
    var file:File = inputValue.files[0]; 
    
    
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.finalResult = myReader.result as string;
      console.log("FINAL RESULT", this.finalResult)
      this.import(this.finalResult);
   }
  
    if (file) {
      myReader.readAsText(file);
    }
  
  }

}
