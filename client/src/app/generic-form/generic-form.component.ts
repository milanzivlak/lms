import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-generic-form',
  templateUrl: './generic-form.component.html',
  styleUrls: ['./generic-form.component.css']
})
export class GenericFormComponent implements OnInit, OnChanges {

  @Input()
  element:any|null = null;
  @Input()
  config:any[] = [];

  
  
  @Output()
  public createEvent:EventEmitter<any> = new EventEmitter<any>();

  form:FormGroup = new FormGroup({});
  constructor(private fb: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    for(let c of this.config){
      if(c.type != "button"){
        this.form.get(c.name)?.setValue(this.element?.[c.name]);
      }
    }
    
  }

  ngOnInit(): void {
    this.form = this.createGroup();

    for(let c of this.config){
      if(c.type != "button"){
        this.form.get(c.name)?.setValue(this.element?.[c.name]);
      }
    }
    
  }

  createGroup(){
    const group = this.fb.group({});
    
    for(let c of this.config){
      if(c.name != ""){
        group.addControl(c.name, this.fb.control({}));
      }
      
    }
    return group;

  }

  create(){
    this.createEvent.emit(this.form.value);
  }

}
