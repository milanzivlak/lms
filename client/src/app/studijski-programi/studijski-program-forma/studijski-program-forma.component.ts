import { Input } from '@angular/core';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Fakultet } from 'src/app/model/fakultet';
import { Nastavnik } from 'src/app/model/nastavnik';
import { StudijskiProgram } from 'src/app/model/studijski-program';
import { FakultetService } from 'src/app/services/fakultet.service';
import { NastavnikService } from 'src/app/services/nastavnik.service';
import { StudijskiProgramService } from 'src/app/services/studijski-program.service';

@Component({
  selector: 'app-studijski-program-forma',
  templateUrl: './studijski-program-forma.component.html',
  styleUrls: ['./studijski-program-forma.component.css']
})
export class StudijskiProgramFormaComponent implements OnInit, OnChanges {

  @Input()
  studijskiProgram:StudijskiProgram|null = null;
  fakulteti:Fakultet[]|null = null;
  nastavnici:Nastavnik[]|null = null;
  form = new FormGroup({
    id: new FormControl("", Validators.required),
    naziv: new FormControl("", Validators.required),
    fakultet: new FormControl("", Validators.required),
    rukovodilac: new FormControl("", Validators.required)
  })
  constructor(public studijskiProgramiService:StudijskiProgramService, public fakultetService:FakultetService, public nastavnikService:NastavnikService) { }

  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
    this.findAllFakulteti();
    this.findAllNastavnici();
  }

  submit(){
    this.studijskiProgramiService.create(this.form.value).subscribe((value) => {
      console.log(this.form.value);
    }, (error) => {
      console.log(error);
    });
  }

   findAllFakulteti(){
    this.fakultetService.findAll().subscribe((value) =>{
      this.fakulteti = value;
      
    }, (error) => {
      console.log(error);
    });
  }

  findAllNastavnici(){
    this.nastavnikService.findAll().subscribe((value) =>{
      this.nastavnici = value;
      
    }, (error) => {
      console.log(error);
    });
  }
}
