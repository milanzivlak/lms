import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from '../model/base-component';
import { StudijskiProgram } from '../model/studijski-program';
import { StudijskiProgramService } from '../services/studijski-program.service';

@Component({
  selector: 'app-studijski-programi',
  templateUrl: './studijski-programi.component.html',
  styleUrls: ['./studijski-programi.component.css']
})
export class StudijskiProgramiComponent extends BaseComponent<StudijskiProgram,number> implements OnInit {

  constructor(public studijskiProgramService:StudijskiProgramService) { 
    super(studijskiProgramService);
  }

  displayedColumns: string[] = ['id', 'naziv','fakultet', 'akcije'];
  dataSource = new MatTableDataSource(this._elements);

  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator
  

  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.studijskiProgramService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:StudijskiProgram){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

}
