import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { NaucnaOblast } from '../model/naucna-oblast';
import { NaucnaOblastService } from '../services/naucna-oblast.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-naucna-oblast',
  templateUrl: './naucna-oblast.component.html',
  styleUrls: ['./naucna-oblast.component.css']
})
export class NaucnaOblastComponent extends BaseComponent<NaucnaOblast, number> implements OnInit {

  constructor(public naucnaOblastService : NaucnaOblastService) { 
    super(naucnaOblastService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'naziv'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync()
  }

  findAllAsync(){
    this.naucnaOblastService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:NaucnaOblast){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

}
