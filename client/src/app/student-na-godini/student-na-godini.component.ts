import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from '../model/base-component';
import { StudentNaGodini } from '../model/student-na-godini';
import { StudentNaGodiniService } from '../services/student-na-godini.service';

@Component({
  selector: 'app-student-na-godini',
  templateUrl: './student-na-godini.component.html',
  styleUrls: ['./student-na-godini.component.css']
})
export class StudentNaGodiniComponent extends BaseComponent<StudentNaGodini, number> implements OnInit {

  constructor(public studentNaGodiniService : StudentNaGodiniService) { 
    super(studentNaGodiniService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'datumUpisa', 'brojIndeksa'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.studentNaGodiniService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:StudentNaGodini){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

}
