import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { Ishod } from '../model/ishod';
import { IshodService } from '../services/ishod.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-ishod',
  templateUrl: './ishod.component.html',
  styleUrls: ['./ishod.component.css']
})
export class IshodComponent extends BaseComponent<Ishod, number> implements OnInit {

  constructor(public ishodService : IshodService) {
    super(ishodService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'opis'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.ishodService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:Ishod){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }


}
