import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { TipNastave } from '../model/tip-nastave';
import { TipZvanjaService } from '../services/tip-zvanja.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TipNastaveService } from '../services/tip-nastave.service';
import { LoginService } from '../services/login.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-tip-nastave',
  templateUrl: './tip-nastave.component.html',
  styleUrls: ['./tip-nastave.component.css']
})
export class TipNastaveComponent extends BaseComponent<TipNastave, number> implements OnInit {

  constructor(public tipNastaveService : TipNastaveService, public loginService:LoginService) { 
    super(tipNastaveService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'naziv'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator
  finalResult:string;

  ngOnInit(): void {
    // if(this.loginService.hasRole("ROLE_NASTAVNIK")) {
    //   this.findAllAsync();
    // }
    this.findAllAsync();
  }

  findAllAsync(){
    this.tipNastaveService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:TipNastave){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

  export(){
    this.tipNastaveService.exportAll();
  }

  import(importUrl:string){
    this.tipNastaveService._http.get<TipNastave[]>(`${environment.api.baseUrl}/tipnastave/import`, {params: {"url":importUrl}}).subscribe((value) =>{
      this._elements = value;
      this.dataSource.data = this._elements;
      this.dataSource._updatePaginator;
      this.dataSource.sort = this.sort;
    }, (error) => {
      console.log(error);
    });
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any) : void {
    var file:File = inputValue.files[0]; 
    
    
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.finalResult = myReader.result as string;
      console.log("FINAL RESULT", this.finalResult)
      this.import(this.finalResult);
   }
  
    if (file) {
      myReader.readAsText(file);
    }
  
  }
}
