import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie-service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenericFormComponent } from './generic-form/generic-form.component';
import { DrzaveComponent } from './drzave/drzave.component';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator'
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'
import {MatSortModule} from '@angular/material/sort'
import {MatTableModule} from '@angular/material/table'
import {MatMenuModule} from '@angular/material/menu'
import {MatButtonModule} from '@angular/material/button'
import { MatFormFieldControl, MatFormFieldModule } from '@angular/material/form-field';
import { DrzaveFormaComponent } from './drzave/drzave-forma/drzave-forma.component';
import { MestaComponent } from './mesta/mesta.component';
import { MestaFormaComponent } from './mesta/mesta-forma/mesta-forma.component';
import {  MatSelectModule } from '@angular/material/select';
import { AdreseComponent } from './adrese/adrese.component';
import { AdreseFormaComponent } from './adrese/adrese-forma/adrese-forma.component';
import { UniverzitetiComponent } from './univerziteti/univerziteti.component';
import { FakultetiComponent } from './fakulteti/fakulteti.component';
import { StudijskiProgramiComponent } from './studijski-programi/studijski-programi.component';
import { GodinaStudijaComponent } from './godina-studija/godina-studija.component';
import { GodinaStudijaFormaComponent } from './godina-studija/godina-studija-forma/godina-studija-forma.component';
import { StudentNaGodiniComponent } from './student-na-godini/student-na-godini.component';
import { StudentComponent } from './student/student.component';
import { PredmetComponent } from './predmet/predmet.component';
import { IshodComponent } from './ishod/ishod.component';
import { NastavnikComponent } from './nastavnik/nastavnik.component';
import { NaucnaOblastComponent } from './naucna-oblast/naucna-oblast.component';
import { ZvanjeComponent } from './zvanje/zvanje.component';
import { NastavniMaterijalComponent } from './nastavni-materijal/nastavni-materijal.component';
import { NastavniMaterijalFormaComponent } from './nastavni-materijal/nastavni-materijal-forma/nastavni-materijal-forma.component';
import { EvaluacijaZnanjaComponent } from './evaluacija-znanja/evaluacija-znanja.component';
import { EvaluacijaZnanjaFormaComponent } from './evaluacija-znanja/evaluacija-znanja-forma/evaluacija-znanja-forma.component';
import { TerminNastaveComponent } from './termin-nastave/termin-nastave.component';
import { TerminNastaveFormaComponent } from './termin-nastave/termin-nastave-forma/termin-nastave-forma.component';
import { TipZvanjaComponent } from './tip-zvanja/tip-zvanja.component';
import { NastavnikNaRealizacijiComponent } from './nastavnik-na-realizaciji/nastavnik-na-realizaciji.component';
import { TipNastaveComponent } from './tip-nastave/tip-nastave.component';
import { RealizacijaPredmetaComponent } from './realizacija-predmeta/realizacija-predmeta.component';
import { PohadjanjePredmetaComponent } from './pohadjanje-predmeta/pohadjanje-predmeta.component';
import { MenuComponent } from './menu/menu.component';
import { MestaEditComponent } from './mesta/mesta-edit/mesta-edit.component';
import { AdminMenuComponent } from './menu/admin-menu/admin-menu.component';
import { UserMenuComponent } from './menu/user-menu/user-menu.component';
import { NastavnikMenuComponent } from './menu/nastavnik-menu/nastavnik-menu.component';
import { AutoriComponent } from './autori/autori.component';
import { StudentFormaComponent } from './student/student-forma/student-forma.component';
import { PredmetFormaComponent } from './predmet/predmet-forma/predmet-forma.component';
import { UniverzitetiFormaComponent } from './univerziteti/univerziteti-forma/univerziteti-forma.component';
import { NastavnikFormaComponent } from './nastavnik/nastavnik-forma/nastavnik-forma.component';
import { FakultetiFormaComponent } from './fakulteti/fakulteti-forma/fakulteti-forma.component';
import { RegistracijaComponent } from './registracija/registracija.component';
import { DrzaveEditComponent } from './drzave/drzave-edit/drzave-edit.component';
import { FakultetiEditComponent } from './fakulteti/fakulteti-edit/fakulteti-edit.component';
import { AdreseEditComponent } from './adrese/adrese-edit/adrese-edit.component';
import { StudijskiProgramFormaComponent } from './studijski-programi/studijski-program-forma/studijski-program-forma.component';
import { UniverzitetiEditComponent } from './univerziteti/univerziteti-edit/univerziteti-edit.component';
import { NastavniMaterijalEditComponent } from './nastavni-materijal/nastavni-materijal-edit/nastavni-materijal-edit.component';
import { DatePipe } from '@angular/common';
import { NastavnikEditComponent } from './nastavnik/nastavnik-edit/nastavnik-edit.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    GenericFormComponent,
    DrzaveComponent,
    DrzaveFormaComponent,
    MestaComponent,
    MestaFormaComponent,
    AdreseComponent,
    AdreseFormaComponent,
    UniverzitetiComponent,
    FakultetiComponent,
    StudijskiProgramiComponent,
    GodinaStudijaComponent,
    GodinaStudijaFormaComponent,
    StudentNaGodiniComponent,
    StudentComponent,
    PredmetComponent,
    IshodComponent,
    NastavnikComponent,
    NaucnaOblastComponent,
    ZvanjeComponent,
    NastavniMaterijalComponent,
    NastavniMaterijalFormaComponent,
    EvaluacijaZnanjaComponent,
    EvaluacijaZnanjaFormaComponent,
    TerminNastaveComponent,
    TerminNastaveFormaComponent,
    TipZvanjaComponent,
    NastavnikNaRealizacijiComponent,
    TipNastaveComponent,
    RealizacijaPredmetaComponent,
    PohadjanjePredmetaComponent,
    MenuComponent,
    MestaEditComponent,
    AdminMenuComponent,
    UserMenuComponent,
    NastavnikMenuComponent,
    AutoriComponent,
    StudentFormaComponent,
    PredmetFormaComponent,
    UniverzitetiFormaComponent,
    NastavnikFormaComponent,
    FakultetiFormaComponent,
    RegistracijaComponent,
    DrzaveEditComponent,
    FakultetiEditComponent,
    AdreseEditComponent,
    StudijskiProgramFormaComponent,
    UniverzitetiEditComponent,
    NastavniMaterijalEditComponent,
    NastavnikEditComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule
    
  ],
  
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    CookieService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
