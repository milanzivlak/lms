import { Base } from "./base";

export interface Autor extends Base {

    ime: string,
    prezime: string

}

