import { Base } from "./base";

export interface StudentNaGodini extends Base{
    datumUpisa: Date,
    brojIndeksa: String
}
