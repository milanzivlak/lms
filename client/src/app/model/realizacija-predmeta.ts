import { Base } from "./base";
import { NastavnikNaRealizaciji } from "./nastavnik-na-realizaciji";
import { Predmet } from "./predmet";

export interface RealizacijaPredmeta extends Base{
    nastavnikNaRealizaciji : NastavnikNaRealizaciji,
    predmet : Predmet
}
