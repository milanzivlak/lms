import { Base } from "./base";
import { Fakultet } from "./fakultet";
import { Mesto } from "./mesto";
import { Univerzitet } from "./univerzitet";

export interface Adresa extends Base{
    ulica: string,
    broj: string,
    fakultet: Fakultet|null,
    mesto: Mesto,
    univerzitet: Univerzitet|null
}
