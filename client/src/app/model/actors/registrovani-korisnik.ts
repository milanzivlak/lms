import { Base } from "../base";

export interface RegistrovaniKorisnik extends Base{
    korisnickoIme: string,
    lozinka: string,
    email: string
}
