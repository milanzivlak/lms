import { Base } from "./base";
import { RealizacijaPredmeta } from "./realizacija-predmeta";
import { Student } from "./student";

export interface PohadjanjePredmeta extends Base {
    konacnaOcena : number,
    realizacijaPredmeta: RealizacijaPredmeta,
    student : Student
}
