import { Adresa } from "./adresa";
import { Base } from "./base";
import { Nastavnik } from "./nastavnik";

export interface Univerzitet extends Base{
    ulica: string,
    broj: string,
    adresa: Adresa,
    rektor: Nastavnik
}
