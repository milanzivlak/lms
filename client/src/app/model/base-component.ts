import { CrudService } from "../services/crud-service";
import { Base } from "./base";

export class BaseComponent<T extends Base, ID>{


 
  public _elements: T[] = [];
  public _element:T|null = null;

  constructor(protected _service:CrudService<T,ID>){}

    
  findAll(){
    this._service.findAll().subscribe((value) =>{
      this._elements = value;
    }, (error) => {
      console.log(error);
    });
    
  }


  create(element:T){
    this._service.create(element).subscribe((value) => {
      this.findAll();
    }, (error) => {
      console.log(error);
    });
  }
  delete(id:ID){
    this._service.delete(id).subscribe((value) => {
      this.findAll();
    }, (error) => {
      console.log(error);
    })
  }
  setUpdate(element:T){
    this._element = {...element};
  }


  update(element:T){
    
    if(element && element.id){
      this._service.update(element.id, element).subscribe((value) => {
        this._element = null;
        this.findAll();
      }, (error) => {
        console.log(error);
      });
    }
  }
}
