import { Base } from "./base";

export interface NastavniMaterijal extends Base {
    naziv: String,
    godinaIzdanja: Date,
    version: Number,
    datumAzuriranja: Date,
    datumUklanjanja: Date

}
