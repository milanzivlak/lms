import { Base } from "./base";
import { Nastavnik } from "./nastavnik";
import { TipNastave } from "./tip-nastave";

export interface NastavnikNaRealizaciji extends Base {
    brojCasova: number,
    nastavnik: Nastavnik,
    tipNastave: TipNastave
}
