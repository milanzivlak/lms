import { Base } from "./base";
import { StudijskiProgram } from "./studijski-program";

export interface GodinaStudija extends Base{
    godina: Date
    studijskiProgram: StudijskiProgram
}

// TODO: Treba dodati predmet, student na godini, etc.. kada se i oni kreiraju
