import { Base } from "./base";
import { Fakultet } from "./fakultet";
import { Nastavnik } from "./nastavnik";

export interface StudijskiProgram extends Base{
    naziv: string,
    fakultet: Fakultet,
    rukovodilac: Nastavnik|null
}
