import { RegistrovaniKorisnik } from "./actors/registrovani-korisnik";
import { Base } from "./base";
import { Fakultet } from "./fakultet";
import { NastavnikNaRealizaciji } from "./nastavnik-na-realizaciji";
import { StudijskiProgram } from "./studijski-program";
import { Univerzitet } from "./univerzitet";

export interface Nastavnik extends Base{
    ime: string,
    biografija: string,
    jmbg: string,
    fakultet : Fakultet | null,
    univerzitet : Univerzitet | null,
    studijskiProgram  : StudijskiProgram,
    nastavnikNaRealizaciji : NastavnikNaRealizaciji|null,
    registrovaniKorisnik : RegistrovaniKorisnik
}
