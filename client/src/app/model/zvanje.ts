import { Base } from "./base";

export interface Zvanje extends Base{
    datumIzbora: Date,
    datumPrestanka: Date
}
