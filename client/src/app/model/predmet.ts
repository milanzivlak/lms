import { Base } from "./base";

export interface Predmet extends Base {
    naziv: String,
    espb: number,
    obavezan: boolean,
    brojPredavanja: number,
    brojVezbi: number,
    drugiObliciNastave: number,
    istrazivackiRad: number,
    ostaliCasovi: number
}
