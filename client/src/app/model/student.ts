import { RegistrovaniKorisnik } from "./actors/registrovani-korisnik";
import { Adresa } from "./adresa";
import { Base } from "./base";

export interface Student extends Base{
    jmbg: string,
    ime: string,
    adresa : Adresa,
    registrovaniKorisnik : RegistrovaniKorisnik
}
