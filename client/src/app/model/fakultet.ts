import { Adresa } from "./adresa";
import { Base } from "./base";
import { Nastavnik } from "./nastavnik";
import { Univerzitet } from "./univerzitet";

export interface Fakultet extends Base{
    naziv: string,
    adresa: Adresa,
    univerzitet: Univerzitet|null,
    dekan: Nastavnik|null
}
