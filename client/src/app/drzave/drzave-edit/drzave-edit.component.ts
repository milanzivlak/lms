import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Drzava } from 'src/app/model/drzava';
import { DrzavaService } from 'src/app/services/drzava.service';

@Component({
  selector: 'app-drzave-edit',
  templateUrl: './drzave-edit.component.html',
  styleUrls: ['./drzave-edit.component.css']
})
export class DrzaveEditComponent implements OnInit {

  drzava:Drzava|null = null;

  constructor(public drzavaService:DrzavaService, private activatedRoute : ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.drzavaService.findOne(this.activatedRoute.snapshot.params['id']).subscribe(r=>{
      this.drzava = r;
    });
  }

  update(drzava : Drzava){
    this.drzavaService.update(this.activatedRoute.snapshot.params['id'], drzava).subscribe(r=>{
      console.log(r);
      this.router.navigate(["/drzave"]);
    })
  }
}
