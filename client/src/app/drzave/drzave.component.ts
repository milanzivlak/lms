import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from '../model/base-component';
import { Drzava } from '../model/drzava';
import { Mesto } from '../model/mesto';
import { DrzavaService } from '../services/drzava.service';
import { MestoService } from '../services/mesto.service';
import {environment} from '../../environments/environment'
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-drzave',
  templateUrl: './drzave.component.html',
  styleUrls: ['./drzave.component.css']
})
export class DrzaveComponent extends BaseComponent<Drzava, number> implements OnInit{

  constructor(public drzavaService:DrzavaService, public mestoService:MestoService) {
    super(drzavaService);
  }
  
 
  displayedColumns: string[] = ['id', 'naziv', 'akcije'];
  displayedColumnsMesta: string[] = ['id', 'naziv'];
  dataSource = new MatTableDataSource(this._elements);
  finalResult:string;
  izabrana:boolean = false;
  
  mestaDrzave:Mesto[] = [];
  izabranaDrzava:Drzava = null;
  dataSourceMesta = new MatTableDataSource(this.mestaDrzave);
  temp:any = [];


  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    
    this.findAllAsync();
    
  }
  
  findAllAsync(){
    this.drzavaService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.dataSourceMesta.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  findAllMesta(){
    this.mestaDrzave = [];
    this.mestoService.findAll().subscribe((value) =>{

      this.temp = value;
      for(let i = 0; i < this.temp.length; i++){
        if(this.temp[i].drzava.id == this.izabranaDrzava.id){
          this.mestaDrzave.push(this.temp[i]);
        }
      }
      console.log(this.mestaDrzave);
      this.dataSourceMesta = new MatTableDataSource(this.mestaDrzave);
      this.dataSourceMesta.paginator = this.paginator;
    }, (error) => {
      console.log(error);
    });
    
  }

  selected(d:Drzava){
    this.izabrana = true;
    this.izabranaDrzava = d;
    console.log(this.izabranaDrzava);
    this.findAllMesta();
  }

  deleteRow(element:Drzava){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

  export(){
    this.drzavaService.exportAll();
  }

  import(importUrl:string){
    this.drzavaService._http.get<Drzava[]>(`${environment.api.baseUrl}/drzave/import`, {params: {"url":importUrl}}).subscribe((value) =>{
      this._elements = value;
      this.dataSource.data = this._elements;
      this.dataSource._updatePaginator;
      this.dataSource.sort = this.sort;
    }, (error) => {
      console.log(error);
    });
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any) : void {
    var file:File = inputValue.files[0]; 
    
    
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.finalResult = myReader.result as string;
      console.log("FINAL RESULT", this.finalResult)
      this.import(this.finalResult);
   }
  
    if (file) {
      myReader.readAsText(file);
    }
  
  }

}
