import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrzaveFormaComponent } from './drzave-forma.component';

describe('DrzaveFormaComponent', () => {
  let component: DrzaveFormaComponent;
  let fixture: ComponentFixture<DrzaveFormaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrzaveFormaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrzaveFormaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
