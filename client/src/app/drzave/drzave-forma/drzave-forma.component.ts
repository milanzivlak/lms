import { OnChanges, SimpleChanges } from '@angular/core';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Drzava } from 'src/app/model/drzava';
import { DrzavaService } from 'src/app/services/drzava.service';

@Component({
  selector: 'app-drzave-forma',
  templateUrl: './drzave-forma.component.html',
  styleUrls: ['./drzave-forma.component.css']
})
export class DrzaveFormaComponent implements OnInit, OnChanges {

  drzave: Drzava[]=[];

  form = new FormGroup({
    id: new FormControl("", [this.idValidator()]),
    naziv: new FormControl("", Validators.required),
  });

  @Input()
  drzava:Drzava|null = null;
  constructor(private drzavaService: DrzavaService) { }

  
  found:boolean = false;

  ngOnInit(): void {
    this.findAllDrzave();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes["drzava"].currentValue != null){
      this.form.setValue({id: this.drzava.id, naziv: this.drzava.naziv});
    }
  }

  submit(){
    this.drzavaService.create(this.form.value).subscribe((value) => {
      console.log(this.form.value);
    }, (error) => {
      console.log(error);
    });
  }

  findAllDrzave(){
    this.drzavaService.findAll().subscribe((value)=>{
      this.drzave = value;
    }, (error)=>{
      console.log(error);
    })
  }

  idValidator(): ValidatorFn {
   
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      this.found = false;
      this.drzavaService.findAll().subscribe((value) =>{
        this.drzave = value;
        
      }, (error) => {
        console.log(error);
      });
      
      if (control.value !== undefined) {

        for(let i =0; i < this.drzave.length; i++){
          
          if(this.drzave[i].id == control.value){
            this.found = true;
          }
        }
        
        if(this.found == true){
          return { 'id': true };
        }
          
      }
      return null;
    };
  }
}
