import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Univerzitet } from 'src/app/model/univerzitet';
import { UniverzitetService } from 'src/app/services/univerzitet.service';

@Component({
  selector: 'app-univerziteti-edit',
  templateUrl: './univerziteti-edit.component.html',
  styleUrls: ['./univerziteti-edit.component.css']
})
export class UniverzitetiEditComponent implements OnInit {

  univerzitet : Univerzitet | null =null;

  constructor(private univerzitetService : UniverzitetService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.univerzitetService.findOne(this.activatedRoute.snapshot.params['id']).subscribe(r=>{
      this.univerzitet = r;
    })
  }

  update(univerzitet : Univerzitet) {
    this.univerzitetService.update(this.activatedRoute.snapshot.params['id'], univerzitet).subscribe(r=>{
      this.router.navigate(["/univerziteti"])
    })
  }

}
