import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UniverzitetiEditComponent } from './univerziteti-edit.component';

describe('UniverzitetiEditComponent', () => {
  let component: UniverzitetiEditComponent;
  let fixture: ComponentFixture<UniverzitetiEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UniverzitetiEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UniverzitetiEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
