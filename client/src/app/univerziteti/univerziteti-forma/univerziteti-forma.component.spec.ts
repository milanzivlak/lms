import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UniverzitetiFormaComponent } from './univerziteti-forma.component';

describe('UniverzitetiFormaComponent', () => {
  let component: UniverzitetiFormaComponent;
  let fixture: ComponentFixture<UniverzitetiFormaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UniverzitetiFormaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UniverzitetiFormaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
