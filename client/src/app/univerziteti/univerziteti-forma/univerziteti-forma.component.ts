import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Adresa } from 'src/app/model/adresa';
import { Nastavnik } from 'src/app/model/nastavnik';
import { Univerzitet } from 'src/app/model/univerzitet';
import { AdresaService } from 'src/app/services/adresa.service';
import { NastavnikService } from 'src/app/services/nastavnik.service';
import { UniverzitetService } from 'src/app/services/univerzitet.service';

@Component({
  selector: 'app-univerziteti-forma',
  templateUrl: './univerziteti-forma.component.html',
  styleUrls: ['./univerziteti-forma.component.css']
})
export class UniverzitetiFormaComponent implements OnInit, OnChanges {

  @Input()
  univerzitet : Univerzitet | null = null;

  form = new FormGroup({
    id: new FormControl(),
    ulica: new FormControl(),
    broj: new FormControl(),
    adresa: new FormControl(),
    rektor: new FormControl()
  })

  adrese_test: Adresa[]=[];
  adrese: Adresa[] = [];
  rektori: Nastavnik[] = [];

  pageable:any;

  constructor(public univerzitetService : UniverzitetService, public adresaService : AdresaService, public nastavnikService : NastavnikService) { }
 
  ngOnChanges(changes: SimpleChanges): void {
    if(changes["univerzitet"].currentValue != null){
      this.form.setValue({id: this.univerzitet.id, ulica: this.univerzitet.ulica, broj: this.univerzitet.broj, adresa: this.univerzitet.adresa, rektor: this.univerzitet.rektor})
      console.log(this.univerzitet);
    }
  }

  ngOnInit(): void {
    this.findAllAdrese();
    this.findAllNastavnici();
  }

  submit(){
    this.univerzitetService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error) =>{
      console.log(error);
    })
  }

  findAllAdrese(){
    this.adresaService.findAll().subscribe((value)=>{
      this.pageable = value;
      this.adrese_test = this.pageable.content;
      for(let a of this.adrese_test){
        if(a.univerzitet == null){
          this.adrese.push(a);
        }
      }
    }, (error) =>{
      console.log(error);
    })
  }

  findAllNastavnici(){
    this.nastavnikService.findAll().subscribe((value)=>{
      this.rektori = value;
    }, (error)=>{
      console.log(error);
    })
  }

}
