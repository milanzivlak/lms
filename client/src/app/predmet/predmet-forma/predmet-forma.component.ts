import { SimpleChanges } from '@angular/core';
import { Input, OnChanges } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { GodinaStudija } from 'src/app/model/godina-studija';
import { Predmet } from 'src/app/model/predmet';
import { GodinaStudijaService } from 'src/app/services/godina-studija.service';
import { PredmetService } from 'src/app/services/predmet.service';

@Component({
  selector: 'app-predmet-forma',
  templateUrl: './predmet-forma.component.html',
  styleUrls: ['./predmet-forma.component.css']
})
export class PredmetFormaComponent implements OnInit, OnChanges {

  @Input()
  predmet : Predmet | null = null;

  form = new FormGroup({
    id: new FormControl(),
    naziv: new FormControl(),
    espb: new FormControl(),
    obavezan: new FormControl(),
    brojPredavanja: new FormControl(),
    brojVezbi: new FormControl(),
    drugiObliciNastave: new FormControl(),
    istrazivackiRad: new FormControl(),
    ostaliCasovi: new FormControl(),
    godinaStudija: new FormControl()
  });

  godineStudija: GodinaStudija[] = [];

  constructor(public predmetService : PredmetService, public godinaStudijaService: GodinaStudijaService) { }

  pageable:any;

  ngOnChanges(changes : SimpleChanges): void {
    if(changes["predmet"].currentValue != null){
      this.form.setValue({id: this.predmet.id, naziv: this.predmet.naziv, espb : this.predmet.espb, obavezan: this.predmet.obavezan,
      brojPredavanja: this.predmet.brojPredavanja, brojVezbi: this.predmet.brojVezbi, drugiObliciNastave: this.predmet.drugiObliciNastave, istrazivackiRad: this.predmet.istrazivackiRad,
      ostaliCasovi: this.predmet.ostaliCasovi});
    }
  }

  ngOnInit(): void {
    // this.findAllAdrese();
    this.findAllGodineStudija();
  }

  submit(){
    this.predmetService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error) =>{
      console.log(error);
    })
  }

  // findAllAdrese(){
  //   this.adresaService.findAll().subscribe((value)=>{
  //     this.pageable = value;
  //     this.adrese = this.pageable.content;
  //   }, (error) =>{
  //     console.log(error);
  //   })
  // }

  findAllGodineStudija(){
    this.godinaStudijaService.findAll().subscribe((value)=>{
      this.godineStudija = value;
    }, (error) =>{
      console.log(error);
    })
  }

}
