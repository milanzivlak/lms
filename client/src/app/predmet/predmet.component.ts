import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { Predmet } from '../model/predmet';
import { PredmetService } from '../services/predmet.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {environment} from '../../environments/environment'

@Component({
  selector: 'app-predmet',
  templateUrl: './predmet.component.html',
  styleUrls: ['./predmet.component.css']
})
export class PredmetComponent extends BaseComponent<Predmet, number> implements OnInit {

  constructor(public predmetService : PredmetService) {
    super(predmetService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'naziv', 'espb', 'obavezan', 'brojPredavanja', 'brojVezbi', 'drugiObliciNastave', 'istrazivackiRad', 'ostaliCasovi'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator
  finalResult:string;

  ngOnInit(): void {
    this.findAllAsync()
  }


  findAllAsync(){
    this.predmetService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:Predmet){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

  export(){
    this.predmetService.exportAll();
  }

  import(importUrl:string){
    this.predmetService._http.get<Predmet[]>(`${environment.api.baseUrl}/predmeti/import`, {params: {"url":importUrl}}).subscribe((value) =>{
      this._elements = value;
      this.dataSource.data = this._elements;
      this.dataSource._updatePaginator;
      this.dataSource.sort = this.sort;
    }, (error) => {
      console.log(error);
    });
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any) : void {
    var file:File = inputValue.files[0]; 
    
    
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.finalResult = myReader.result as string;
      console.log("FINAL RESULT", this.finalResult)
      this.import(this.finalResult);
   }
  
    if (file) {
      myReader.readAsText(file);
    }
  
  }
}
