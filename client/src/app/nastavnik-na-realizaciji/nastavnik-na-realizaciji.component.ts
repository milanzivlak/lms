import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { NastavnikNaRealizaciji } from '../model/nastavnik-na-realizaciji';
import { NastavnikNaRealizacijiService } from '../services/nastavnik-na-realizaciji.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-nastavnik-na-realizaciji',
  templateUrl: './nastavnik-na-realizaciji.component.html',
  styleUrls: ['./nastavnik-na-realizaciji.component.css']
  
})
export class NastavnikNaRealizacijiComponent extends BaseComponent<NastavnikNaRealizaciji, number> implements OnInit {

  constructor(public nastavnikNaRealizacijiService : NastavnikNaRealizacijiService) { 
    super(nastavnikNaRealizacijiService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'brojCasova', 'nastavnik', 'tipNastave', "akcije"];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync()
  }

  findAllAsync(){
    this.nastavnikNaRealizacijiService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:NastavnikNaRealizaciji){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }
}
