import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { RegistrovaniKorisnik } from 'src/app/model/actors/registrovani-korisnik';
import { Adresa } from 'src/app/model/adresa';
import { Student } from 'src/app/model/student';
import { AdresaService } from 'src/app/services/adresa.service';
import { RegistrovaniKorisnikService } from 'src/app/services/registrovani-korisnik.service';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-student-forma',
  templateUrl: './student-forma.component.html',
  styleUrls: ['./student-forma.component.css']
})
export class StudentFormaComponent implements OnInit, OnChanges {

  @Input()
  student : Student | null = null;

  form = new FormGroup({
    id : new FormControl(),
    jmbg : new FormControl(),
    ime : new FormControl(),
    adresa : new FormControl(),
    registrovaniKorisnik : new FormControl()
  });

  adrese: Adresa[] = [];
  registrovaniKorisnici: RegistrovaniKorisnik[] = [];

  constructor(public studentService : StudentService, public adresaService : AdresaService, public registrovaniKorisnikService : RegistrovaniKorisnikService) { }

  pageable:any;

  ngOnChanges(changes : SimpleChanges): void {
    if(changes["student"].currentValue != null){
      this.form.setValue({id: this.student.id, jmbg: this.student.jmbg, adresa : this.student.adresa, registrovaniKorisnik: this.student.registrovaniKorisnik});
    }
  }

  ngOnInit(): void {
    this.findAllAdrese();
    this.findAllKorisnici();
  }

  submit(){
    this.studentService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error) =>{
      console.log(error);
    })
  }

  findAllAdrese(){
    this.adresaService.findAll().subscribe((value)=>{
      this.pageable = value;
      this.adrese = this.pageable.content;
    }, (error) =>{
      console.log(error);
    })
  }

  findAllKorisnici(){
    this.registrovaniKorisnikService.findAll().subscribe((value)=>{
      
      this.registrovaniKorisnici = value;
    }, (error) =>{
      console.log(error);
    })
  }

}
