import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { Student } from '../model/student';
import { StudentService } from '../services/student.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {environment} from '../../environments/environment'

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent extends BaseComponent<Student, number> implements OnInit {

  constructor(public studentService : StudentService) { 
    super(studentService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'ime', 'jmbg', 'adresa', 'registrovaniKorisnik'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator
  finalResult:string;

  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.studentService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:Student){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

  export(){
    this.studentService.exportAll();
  }

  import(importUrl:string){
    this.studentService._http.get<Student[]>(`${environment.api.baseUrl}/studenti/import`, {params: {"url":importUrl}}).subscribe((value) =>{
      this._elements = value;
      this.dataSource.data = this._elements;
      this.dataSource._updatePaginator;
      this.dataSource.sort = this.sort;
    }, (error) => {
      console.log(error);
    });
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any) : void {
    var file:File = inputValue.files[0]; 
    
    
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.finalResult = myReader.result as string;
      console.log("FINAL RESULT", this.finalResult)
      this.import(this.finalResult);
   }
  
    if (file) {
      myReader.readAsText(file);
    }
  
  }
}
