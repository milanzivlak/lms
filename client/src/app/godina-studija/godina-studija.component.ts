import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from '../model/base-component';
import { GodinaStudija } from '../model/godina-studija'
import { GodinaStudijaService } from '../services/godina-studija.service';

@Component({
  selector: 'app-godina-studija',
  templateUrl: './godina-studija.component.html',
  styleUrls: ['./godina-studija.component.css']
})
export class GodinaStudijaComponent extends BaseComponent<GodinaStudija, number> implements OnInit {

  constructor(public godinaStudijaService : GodinaStudijaService) {
    super(godinaStudijaService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'godina', 'akcije'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.godinaStudijaService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:GodinaStudija){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

}
