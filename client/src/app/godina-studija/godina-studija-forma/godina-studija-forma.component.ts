import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { GodinaStudija } from 'src/app/model/godina-studija';
import { StudijskiProgram } from 'src/app/model/studijski-program';
import { GodinaStudijaService } from 'src/app/services/godina-studija.service';
import { StudijskiProgramService } from 'src/app/services/studijski-program.service';

@Component({
  selector: 'app-godina-studija-forma',
  templateUrl: './godina-studija-forma.component.html',
  styleUrls: ['./godina-studija-forma.component.css']
})
export class GodinaStudijaFormaComponent implements OnInit {

  form = new FormGroup({
    id: new FormControl("", [this.idValidator()]),
    godinaStudija: new FormControl("", Validators.required),
    studijskiProgram: new FormControl("", Validators.required)
  });

  studijskiProgrami: StudijskiProgram[];

  constructor(private godinaStudijaService : GodinaStudijaService, public studijskiProgramService:StudijskiProgramService) { }


  godineStudija: GodinaStudija[]=[];
  found:boolean = false;

  ngOnInit(): void {
    this.findAllStudijskiProgrami();
    this.findAllGodineStudija();
  }

  submit(){

    this.form.value.godinaStudija = new Date(this.form.value.godinaStudija);
    this.godinaStudijaService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error) => {
      console.log(error);
    })
  }

  findAllStudijskiProgrami(){
    this.studijskiProgramService.findAll().subscribe((value)=>{
      this.studijskiProgrami = value;
    }, (error) =>{
      console.log(error);
    })
  }

  findAllGodineStudija(){
    this.godinaStudijaService.findAll().subscribe((value)=>{
      this.godineStudija = value;
    }, (error) =>{
      console.log(error);
    })
  }

  idValidator(): ValidatorFn {
   
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      this.found = false;
      this.godinaStudijaService.findAll().subscribe((value) =>{
        this.godineStudija = value;
        
      }, (error) => {
        console.log(error);
      });
      
      if (control.value !== undefined) {

        for(let i =0; i < this.godineStudija.length; i++){
          
          if(this.godineStudija[i].id == control.value){
            this.found = true;
          }
        }
        
        if(this.found == true){
          return { 'id': true };
        }
          
      }
      return null;
    };
  }

}
