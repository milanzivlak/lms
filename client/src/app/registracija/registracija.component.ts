import { Component, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, FormGroupDirective, NgForm, ValidatorFn, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

import { RegistrovaniKorisnik } from '../model/actors/registrovani-korisnik';
import { LoginService } from '../services/login.service';
import { RegistrovaniKorisnikService } from '../services/registrovani-korisnik.service';
import { fromEvent, interval } from 'rxjs';
import { scan, debounce } from 'rxjs/operators';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-registracija',
  templateUrl: './registracija.component.html',
  styleUrls: ['./registracija.component.css']
})
export class RegistracijaComponent implements OnInit {

  registrovaniKorisnici:RegistrovaniKorisnik[] = [];

  form = new FormGroup({
    id: new FormControl("", [this.idValidator()]),
    korisnickoIme: new FormControl("", Validators.required),
    lozinka: new FormControl("", Validators.required),
    email: new FormControl("", Validators.email)
  })

  matcher = new MyErrorStateMatcher();

  
  found:boolean = false;
  constructor(public loginService:LoginService, public registrovaniKorisnikService:RegistrovaniKorisnikService) { }

  


  ngOnInit(): void {
    this.findAllKorisnici();
  }

  submit(){

    if(!this.form.get('id').hasError('id')){
      this.loginService.register(this.form.value);
    }else{
      console.log("Uneseni ID vec postoji!");
    }
    
    
  }

  findAllKorisnici(){
    this.registrovaniKorisnikService.findAll().subscribe((value) =>{
      this.registrovaniKorisnici = value;
      
    }, (error) => {
      console.log(error);
    });
  }

  idValidator(): ValidatorFn {
   
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      this.found = false;
      this.registrovaniKorisnikService.findAll().subscribe((value) =>{
        this.registrovaniKorisnici = value;
        
      }, (error) => {
        console.log(error);
      });
      
      if (control.value !== undefined) {

        for(let i =0; i < this.registrovaniKorisnici.length; i++){
          
          if(this.registrovaniKorisnici[i].id == control.value){
            this.found = true;
          }
        }
        
        if(this.found == true){
          return { 'id': true };
        }
          
      }
      return null;
    };
  }

  checkId(){
    for(let i =0; i < this.registrovaniKorisnici.length; i++){
      if(this.registrovaniKorisnici[i].id == this.form.value.id){
        this.found = true;
      }
    }

    return this.found;
  }
}


