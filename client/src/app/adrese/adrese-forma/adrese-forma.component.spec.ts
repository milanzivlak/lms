import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdreseFormaComponent } from './adrese-forma.component';

describe('AdreseFormaComponent', () => {
  let component: AdreseFormaComponent;
  let fixture: ComponentFixture<AdreseFormaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdreseFormaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdreseFormaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
