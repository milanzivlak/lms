import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Adresa } from 'src/app/model/adresa';
import { Fakultet } from 'src/app/model/fakultet';
import { Mesto } from 'src/app/model/mesto';
import { Univerzitet } from 'src/app/model/univerzitet';
import { AdresaService } from 'src/app/services/adresa.service';
import { FakultetService } from 'src/app/services/fakultet.service';
import { MestoService } from 'src/app/services/mesto.service';
import { UniverzitetService } from 'src/app/services/univerzitet.service';

@Component({
  selector: 'app-adrese-forma',
  templateUrl: './adrese-forma.component.html',
  styleUrls: ['./adrese-forma.component.css']
})
export class AdreseFormaComponent implements OnInit, OnChanges {


  @Input()
  adresa : Adresa | null = null;

  adrese: Adresa[]=[];

  form = new FormGroup({
    id: new FormControl("", [this.idValidator()]),
    broj : new FormControl("", Validators.required),
    ulica : new FormControl("", Validators.required),
    fakultet : new FormControl("", Validators.required),
    mesto : new FormControl("", Validators.required),
    univerzitet : new FormControl("", Validators.required)
  });

  fakulteti: Fakultet[] = [];
  mesta: Mesto[] = [];
  univerziteti: Univerzitet[] = [];

  
  found:boolean = false;

  constructor(public adresaService : AdresaService, public fakultetService : FakultetService, public mestoService : MestoService, public univerzitetService : UniverzitetService) { }

  pageable:any;

  ngOnChanges(changes : SimpleChanges): void {
    if(changes["adresa"].currentValue != null){
      this.form.setValue({id: this.adresa.id, broj: this.adresa.broj, ulica : this.adresa.ulica, fakultet: this.adresa.fakultet, mesto: this.adresa.mesto, univerzitet : this.adresa.univerzitet});
    }
  }

  ngOnInit(): void {
    this.findAllFakulteti();
    this.findAllMesta();
    this.findAllUniverziteti();
    this.findAllAdrese();
  }

  submit(){
    this.adresaService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error)=> {
      console.log(error);
    })
  }

  findAllFakulteti(){
    this.fakultetService.findAll().subscribe((value)=>{
      this.fakulteti = value;
    }, (error)=>{
      console.log(error);
    })
  }

  findAllMesta(){
    this.mestoService.findAll().subscribe((value)=>{
      this.mesta = value;
    }, (error)=>{
      console.log(error);
    })
  }

  findAllUniverziteti(){
    this.univerzitetService.findAll().subscribe((value)=>{
      this.univerziteti = value;
    }, (error)=>{
      console.log(error);
    })
  }

  findAllAdrese(){
    this.adresaService.findAll().subscribe((value)=>{
      this.adrese = value;
    }, (error)=>{
      console.log(error);
    })
  }
  
  idValidator(): ValidatorFn {
   
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      this.found = false;
      this.adresaService.findAll().subscribe((value) =>{
        this.adrese = value;
        
      }, (error) => {
        console.log(error);
      });
      
      if (control.value !== undefined) {

        for(let i =0; i < this.adrese.length; i++){
          
          if(this.adrese[i].id == control.value){
            this.found = true;
          }
        }
        
        if(this.found == true){
          return { 'id': true };
        }
          
      }
      return null;
    };
  }
}
