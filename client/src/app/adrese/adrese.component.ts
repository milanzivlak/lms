import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Adresa } from '../model/adresa';
import { BaseComponent } from '../model/base-component';
import { AdresaService } from '../services/adresa.service';

@Component({
  selector: 'app-adrese',
  templateUrl: './adrese.component.html',
  styleUrls: ['./adrese.component.css']
})
export class AdreseComponent extends BaseComponent<Adresa,number> implements OnInit {

  constructor(public adresaService:AdresaService) {
    super(adresaService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'ulica', 'broj', 'mesto', 'akcije'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.adresaService.findAll().subscribe((value) =>{
      this.pageable = value;
      console.log(this.pageable.content);
      this._elements = this.pageable.content;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:Adresa){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

}
