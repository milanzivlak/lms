import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Adresa } from 'src/app/model/adresa';
import { AdresaService } from 'src/app/services/adresa.service';

@Component({
  selector: 'app-adrese-edit',
  templateUrl: './adrese-edit.component.html',
  styleUrls: ['./adrese-edit.component.css']
})
export class AdreseEditComponent implements OnInit {

  adresa:Adresa|null = null;
  constructor(public adresaService:AdresaService, private activatedRoute : ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.adresaService.findOne(this.activatedRoute.snapshot.params['id']).subscribe(r=>{
      this.adresa = r;
    });
  }

  update(adresa : Adresa){
    this.adresaService.update(this.activatedRoute.snapshot.params['id'], adresa).subscribe(r=>{
      console.log(r);
      this.router.navigate(["/adrese"]);
    })
  }

}
