import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdreseEditComponent } from './adrese-edit.component';

describe('AdreseEditComponent', () => {
  let component: AdreseEditComponent;
  let fixture: ComponentFixture<AdreseEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdreseEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdreseEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
