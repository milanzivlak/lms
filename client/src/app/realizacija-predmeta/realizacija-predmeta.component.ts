import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { RealizacijaPredmeta } from '../model/realizacija-predmeta';
import { RealizacijaPredmetaService } from '../services/realizacija-predmeta.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-realizacija-predmeta',
  templateUrl: './realizacija-predmeta.component.html',
  styleUrls: ['./realizacija-predmeta.component.css']
})
export class RealizacijaPredmetaComponent extends BaseComponent<RealizacijaPredmeta, number> implements OnInit {

  constructor(public realizacijaPredmetaService : RealizacijaPredmetaService) { 
    super(realizacijaPredmetaService);
  }

  pageable:any;
  displayedColumns: string[] = ['id', 'nastavnikNaRealizaciji', 'predmet'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.realizacijaPredmetaService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:RealizacijaPredmeta){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

}
