import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../model/base-component';
import { Nastavnik } from '../model/nastavnik';
import { NastavnikService } from '../services/nastavnik.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-nastavnik',
  templateUrl: './nastavnik.component.html',
  styleUrls: ['./nastavnik.component.css']
})
export class NastavnikComponent extends BaseComponent<Nastavnik, number> implements OnInit {

  constructor(public nastavnikService : NastavnikService) { 
    super(nastavnikService);
  }

  izmena_bool: boolean;

  pageable:any;
  // flai nastavnikNaRealizaciji 
  displayedColumns: string[] = ['id', 'ime', 'biografija', 'jmbg', "studijskiProgram", "akcije"];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator
  finalResult:string;

  ngOnInit(): void {
    this.findAllAsync()
  }

  findAllAsync(){
    this.nastavnikService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:Nastavnik){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }

  export() {
    this.nastavnikService.exportAll();
  }

  import(importUrl:string){
    this.nastavnikService._http.get<Nastavnik[]>(`${environment.api.baseUrl}/nastavnici/import`, {params: {"url":importUrl}}).subscribe((value) =>{
      this._elements = value;
      this.dataSource.data = this._elements;
      this.dataSource._updatePaginator;
      this.dataSource.sort = this.sort;
    }, (error)=> {
      console.log(error);
    });
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any) : void {
    var file:File = inputValue.files[0]; 
    
    
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.finalResult = myReader.result as string;
      console.log("FINAL RESULT", this.finalResult)
      this.import(this.finalResult);
   }
  
    if (file) {
      myReader.readAsText(file);
    }
  
  }

  izmeni(nastavnik: Nastavnik){

  }
}
