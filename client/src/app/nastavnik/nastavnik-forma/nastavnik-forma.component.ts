import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { RegistrovaniKorisnik } from 'src/app/model/actors/registrovani-korisnik';
import { Fakultet } from 'src/app/model/fakultet';
import { Nastavnik } from 'src/app/model/nastavnik';
import { NastavnikNaRealizaciji } from 'src/app/model/nastavnik-na-realizaciji';
import { Student } from 'src/app/model/student';
import { StudijskiProgram } from 'src/app/model/studijski-program';
import { Univerzitet } from 'src/app/model/univerzitet';
import { FakultetService } from 'src/app/services/fakultet.service';
import { NastavnikNaRealizacijiService } from 'src/app/services/nastavnik-na-realizaciji.service';
import { NastavnikService } from 'src/app/services/nastavnik.service';
import { RegistrovaniKorisnikService } from 'src/app/services/registrovani-korisnik.service';
import { StudentService } from 'src/app/services/student.service';
import { StudijskiProgramService } from 'src/app/services/studijski-program.service';
import { UniverzitetService } from 'src/app/services/univerzitet.service';

@Component({
  selector: 'app-nastavnik-forma',
  templateUrl: './nastavnik-forma.component.html',
  styleUrls: ['./nastavnik-forma.component.css']
})
export class NastavnikFormaComponent implements OnInit, OnChanges {

  @Input()
  nastavnik: Nastavnik | null = null;

  

  form = new FormGroup({
    id: new FormControl(),
    ime: new FormControl(),
    biografija: new FormControl(),
    jmbg: new FormControl(),
    registrovaniKorisnik: new FormControl(),
  //  fakultet: new FormControl(),
  //  univerzitet: new FormControl(),
    studijskiProgram: new FormControl(),
   // nastavnikNaRealizaciji: new FormControl()

  })
  pageable:any;

  fakulteti: Fakultet[] = [];
  univerziteti: Univerzitet[] = [];
  studijskiProgrami: StudijskiProgram[] = [];
  nastavniciNaRealizaciji: NastavnikNaRealizaciji[] = [];
  registrovaniKorisnici: RegistrovaniKorisnik[] = [];
  nastavnici:Nastavnik[] = [];
  studenti:Student[] = [];
  setStudenti:Set<number> = new Set();
  setNastavnici:Set<number> = new Set();
  temp:RegistrovaniKorisnik[] = [];

  constructor(public nastavnikService : NastavnikService, public fakultetService : FakultetService, public univerzitetService: UniverzitetService, public studijskiProgramService : StudijskiProgramService, public nastavnikNaRealizacijiService: NastavnikNaRealizacijiService,
    public registrovaniKorisnikService:RegistrovaniKorisnikService, public studentService:StudentService) { }
  
    ngOnChanges(changes: SimpleChanges): void {
    if(changes["nastavnik"].currentValue != null){
      this.form.setValue({id: this.nastavnik.id, biografija: this.nastavnik.biografija, ime: this.nastavnik.ime, jmbg: this.nastavnik.jmbg, registrovaniKorisnik: this.nastavnik.registrovaniKorisnik, studijskiProgram: this.nastavnik.studijskiProgram})
    }
  }

  ngOnInit(): void {
    this.findAllFakulteti();
    this.findAllUniverziteti();
    this.findAllStudijskiProgrami();
    this.findAllNastavnikNaRealizaciji();
    this.findAllRegistrovaniKorisnici();
  }

  submit(){
    this.nastavnikService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error) =>{
      console.log(error);
    })
  }

  findAllFakulteti(){
    this.fakultetService.findAll().subscribe((value)=>{
      //this.pageable = value;
     // this.adrese = this.pageable.content;
     this.fakulteti = value;
    }, (error) =>{
      console.log(error);
    })
  }

  findAllStudenti(){
    this.studentService.findAll().subscribe((value)=>{

      this.studenti = value;
      for (let i =0; i < this.studenti.length; i++){
        if(this.studenti[i].registrovaniKorisnik != null){
          this.setStudenti.add(this.studenti[i].registrovaniKorisnik.id);
        }
      }
    }, (error) =>{
      console.log(error);
    })
  }

  
  findAllNastavnici(){
    this.nastavnikService.findAll().subscribe((value)=>{
     this.nastavnici = value;
     for (let i =0; i < this.nastavnici.length; i++){
      if(this.nastavnici[i].registrovaniKorisnik != null){
        this.setNastavnici.add(this.nastavnici[i].registrovaniKorisnik.id);
      }
    }
    }, (error) =>{
      console.log(error);
    })
  }

  findAllRegistrovaniKorisnici(){
    this.findAllStudenti();
    this.findAllNastavnici();
    


    this.registrovaniKorisnikService.findAll().subscribe((value)=>{
      console.log("Studenti: ", this.setStudenti);
      console.log("Nastavnici: ", this.setNastavnici);
      this.temp = value;
      for(let i = 0; i < this.temp.length; i++){
        if(!this.setStudenti.has(this.temp[i].id) && !this.setNastavnici.has(this.temp[i].id)){
          this.registrovaniKorisnici.push(this.temp[i]);
        }
        
      }
     //this.registrovaniKorisnici = value;
     //console.log(this.registrovaniKorisnici);
    }, (error) =>{
      console.log(error);
    })
  }

  findAllUniverziteti(){
    this.univerzitetService.findAll().subscribe((value)=>{
   // this.pageable = value;
    //this.univerziteti = this.pageable.content;
    this.univerziteti = value;
    }, (error) =>{
      console.log(error);
    })
  }

  findAllStudijskiProgrami(){
    this.studijskiProgramService.findAll().subscribe((value)=>{
      //this.pageable = value;
     // this.adrese = this.pageable.content;
     this.studijskiProgrami = value;
    }, (error) =>{
      console.log(error);
    })
  }

  findAllNastavnikNaRealizaciji(){
    this.nastavnikNaRealizacijiService.findAll().subscribe((value)=>{
      //this.pageable = value;
     // this.adrese = this.pageable.content;
     this.nastavniciNaRealizaciji = value;
    }, (error) =>{
      console.log(error);
    })
  }

}
