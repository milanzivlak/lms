import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Nastavnik } from 'src/app/model/nastavnik';
import { StudijskiProgram } from 'src/app/model/studijski-program';
import { NastavnikService } from 'src/app/services/nastavnik.service';
import { StudijskiProgramService } from 'src/app/services/studijski-program.service';

@Component({
  selector: 'app-nastavnik-edit',
  templateUrl: './nastavnik-edit.component.html',
  styleUrls: ['./nastavnik-edit.component.css']
})
export class NastavnikEditComponent implements OnInit, OnChanges {

  form = new FormGroup({
    id: new FormControl(),
    biografija: new FormControl(),
    ime: new FormControl(),
    jmbg: new FormControl(),
    studijskiProgram: new FormControl()

  })

  studijskiProgrami: StudijskiProgram[] = [];

  nastavnik: Nastavnik|null = null;
  temp:any|null = null;
  constructor(public nastavnikService:NastavnikService, public studijskiProgramService : StudijskiProgramService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.findAllStudijskiProgrami();
    this.nastavnikService.findOne(this.activatedRoute.snapshot.params['id']).subscribe(r=>{
      this.temp = r;
      console.log("TEMP", this.temp);
      this.nastavnik = this.temp;
      this.form.setValue({id: this.nastavnik.id, ime: this.nastavnik.ime, biografija: this.nastavnik.biografija, jmbg: this.nastavnik.jmbg,
        studijskiProgram:this.nastavnik.studijskiProgram});
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes["nastavnik"].currentValue != null){
      this.form.setValue({id: this.nastavnik.id, ime: this.nastavnik.ime, biografija: this.nastavnik.biografija, jmbg: this.nastavnik.jmbg, 
        studijskiProgram:this.nastavnik.studijskiProgram});
    }
  }

  update(){
    
    this.nastavnikService.update(this.activatedRoute.snapshot.params['id'], this.form.value).subscribe(r=>{
      console.log(this.form.value);
      this.router.navigate(["/nastavnici"]);
    })
  }

  findAllStudijskiProgrami(){
    this.studijskiProgramService.findAll().subscribe((value)=>{
      //this.pageable = value;
     // this.adrese = this.pageable.content;
     this.studijskiProgrami = value;
    }, (error) =>{
      console.log(error);
    })
  }

}
