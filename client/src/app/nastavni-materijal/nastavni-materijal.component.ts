
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NastavniMaterijal } from '../model/nastavni-materijal';
import { BaseComponent } from '../model/base-component';
import { NastavniMaterijalService } from '../services/nastavni-materijal.service';

@Component({
  selector: 'app-nastavni-materijal',
  templateUrl: './nastavni-materijal.component.html',
  styleUrls: ['./nastavni-materijal.component.css']
})
export class NastavniMaterijalComponent extends BaseComponent<NastavniMaterijal, number> implements OnInit {


  constructor(public nastavniMaterijalService : NastavniMaterijalService) {
    super(nastavniMaterijalService);
  }

  pageable:any;
  displayedColumns: string[] = ['naziv', 'godinaIzdanja', 'datumAzuriranja', 'datumUklanjanja', 'akcije'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator

  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.nastavniMaterijalService.findAll().subscribe((value) =>{
      this.pageable = value;
      this._elements = this.pageable.content;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:NastavniMaterijal){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }


}
