import { DatePipe } from '@angular/common';
import { SimpleChanges } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NastavniMaterijal } from 'src/app/model/nastavni-materijal';
import { NastavniMaterijalService } from 'src/app/services/nastavni-materijal.service';

@Component({
  selector: 'app-nastavni-materijal-edit',
  templateUrl: './nastavni-materijal-edit.component.html',
  styleUrls: ['./nastavni-materijal-edit.component.css']
})
export class NastavniMaterijalEditComponent implements OnInit {

  form = new FormGroup({
    id: new FormControl(),
    datumAzuriranja: new FormControl(),
    datumUklanjanja: new FormControl(),
    godinaIzdanja: new FormControl(),
    naziv: new FormControl()

  });

  nastavniMaterijal:NastavniMaterijal|null = null;
  temp:any|null = null;
  constructor(private nastavniMaterijalService : NastavniMaterijalService, private datePipe:DatePipe, private activatedRoute : ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.nastavniMaterijalService.findOne(this.activatedRoute.snapshot.params['id']).subscribe(r=>{
      this.temp = r;
      console.log("TEMP:",this.temp);
      this.nastavniMaterijal = this.temp;
      this.form.setValue({id: this.nastavniMaterijal.id, datumAzuriranja:this.datePipe.transform(this.nastavniMaterijal.datumUklanjanja,"yyyy-MM-dd"),
      datumUklanjanja: this.datePipe.transform(this.nastavniMaterijal.datumUklanjanja,"yyyy-MM-dd"), godinaIzdanja:this.nastavniMaterijal.godinaIzdanja, naziv:this.nastavniMaterijal.naziv});
    })
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes["nastavniMaterijal"].currentValue != null){
      this.form.setValue({id: this.nastavniMaterijal.id, datumAzuriranja:this.datePipe.transform(this.nastavniMaterijal.datumUklanjanja,"yyyy-MM-dd"),
      datumUklanjanja: this.datePipe.transform(this.nastavniMaterijal.datumUklanjanja,"yyyy-MM-dd"), godinaIzdanja:this.nastavniMaterijal.godinaIzdanja, naziv:this.nastavniMaterijal.naziv});
    }
  }
  
  update(){
    
    this.nastavniMaterijalService.update(this.activatedRoute.snapshot.params['id'], this.nastavniMaterijal).subscribe(r=>{
      console.log(r);
      this.router.navigate(["/nastavni-materijal"]);
    })
  }



}
