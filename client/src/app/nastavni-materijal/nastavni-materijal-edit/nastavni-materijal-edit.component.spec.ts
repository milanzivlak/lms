import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavniMaterijalEditComponent } from './nastavni-materijal-edit.component';

describe('NastavniMaterijalEditComponent', () => {
  let component: NastavniMaterijalEditComponent;
  let fixture: ComponentFixture<NastavniMaterijalEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NastavniMaterijalEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NastavniMaterijalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
