import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavniMaterijalFormaComponent } from './nastavni-materijal-forma.component';

describe('NastavniMaterijalFormaComponent', () => {
  let component: NastavniMaterijalFormaComponent;
  let fixture: ComponentFixture<NastavniMaterijalFormaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NastavniMaterijalFormaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NastavniMaterijalFormaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
