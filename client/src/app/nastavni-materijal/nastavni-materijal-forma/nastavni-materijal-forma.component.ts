import { SimpleChanges } from '@angular/core';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NastavniMaterijal } from 'src/app/model/nastavni-materijal';
import { NastavniMaterijalService } from 'src/app/services/nastavni-materijal.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-nastavni-materijal-forma',
  templateUrl: './nastavni-materijal-forma.component.html',
  styleUrls: ['./nastavni-materijal-forma.component.css']
})
export class NastavniMaterijalFormaComponent implements OnInit, OnChanges {

  form = new FormGroup({
    id: new FormControl(),
    datumAzuriranja: new FormControl(),
    datumUklanjanja: new FormControl(),
    godinaIzdanja: new FormControl(),
    naziv: new FormControl()

  });

  @Input()
  nastavniMaterijal:NastavniMaterijal|null = null;
  constructor(private nastavniMaterijalService : NastavniMaterijalService, private datePipe:DatePipe) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes["nastavniMaterijal"].currentValue != null){
      this.form.setValue({id: this.nastavniMaterijal.id, datumAzuriranja:this.datePipe.transform(this.nastavniMaterijal.datumUklanjanja,"yyyy-MM-dd"),
      datumUklanjanja: this.datePipe.transform(this.nastavniMaterijal.datumUklanjanja,"yyyy-MM-dd"), godinaIzdanja:this.nastavniMaterijal.godinaIzdanja, naziv:this.nastavniMaterijal.naziv});
    }
  }
  submit(){
    this.nastavniMaterijalService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error) => {
      console.log(error);
    })
  }

}