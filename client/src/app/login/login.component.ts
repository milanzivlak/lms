import { Component, OnInit,  EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatFormFieldControl, MatFormFieldModule } from '@angular/material/form-field';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [{provide: MatFormFieldControl, useExisting: LoginComponent}] 
  
})
export class LoginComponent implements OnInit {

  form =  new FormGroup({
    korisnickoIme: new FormControl(),
    lozinka: new FormControl()
  });

  
  @Output()
  userLoginEvent = new EventEmitter<any>();
  constructor(public loginService: LoginService) { }

  ngOnInit(): void {
  }

  submit(){
    this.loginService.login(this.form.value).subscribe(r=>{
      this.userLoginEvent.emit(r);
      console.log(r);
    });
  }
}
