import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import { NastavnikNaRealizaciji } from '../model/nastavnik-na-realizaciji';
import { CrudService } from './crud-service';

@Injectable({
  providedIn: 'root'
})
export class NastavnikNaRealizacijiService extends CrudService<NastavnikNaRealizaciji, number>{

  constructor(public _http : HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/nastavniknarealizaciji`, _cookieService)
  }
}
