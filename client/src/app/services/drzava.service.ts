import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Drzava } from '../model/drzava';
import { CrudService } from './crud-service';
import {environment} from '../../environments/environment'
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class DrzavaService extends CrudService<Drzava, number>{


  constructor(public _http: HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/drzave`, _cookieService);
  }

  _imported:Drzava[] = [];
  exportAll(){
    this._http.get<Drzava[]>(`${environment.api.baseUrl}/drzave/export`).subscribe((value) =>{
      console.log(value);
    }, (error) => {
      console.log(error);
    });
  }

  importAll():any{
    this._http.get<Drzava[]>(`${environment.api.baseUrl}/drzave/import`).subscribe((value) =>{
      this._imported = value;
      console.log("IMPORTED", this._imported);
    }, (error) => {
      console.log(error);
    });
  }
}
