import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { NaucnaOblast } from '../model/naucna-oblast';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class NaucnaOblastService extends CrudService<NaucnaOblast, number> {

  constructor(public _http : HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/naucneOblasti`, _cookieService)
  }
}
