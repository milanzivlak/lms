import { Injectable } from '@angular/core';

import jwtDecode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor() { }


  decodeJwt(jwt: string): string {
    try {
      return jwtDecode(jwt)
    } catch (Error) {
      console.log(Error)
      return null;
    }
  }

  getRoles(jwt: string): string[]{
    let decoded = this.decodeJwt(jwt)
    if(decoded != null) 
        return decoded["roles"];
    return null;
  }


  hasRole(role: string, jwt:string): boolean {
    let roles = this.getRoles(jwt); 
    if(roles != null) {
      for(let i = 0; i < roles.length; i++) {
        if (roles[i]["authority"] == role) return true;
      }
    }
    return false;
  }


}
