import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Univerzitet } from '../model/univerzitet';
import { CrudService } from './crud-service';
import {environment} from '../../environments/environment'
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class UniverzitetService extends CrudService<Univerzitet,number>{

  constructor(public _http:HttpClient, public _cookieService: CookieService) {
    super(_http, `${environment.api.baseUrl}/univerziteti`, _cookieService);
  }

  _imported:Univerzitet[] = [];
  exportAll(){
    this._http.get<Univerzitet[]>(`${environment.api.baseUrl}/univerziteti/export`).subscribe((value)=>{
      console.log(value);
    }, (error)=>{
      console.log(error);
    });
  }

  importAll():any{
    this._http.get<Univerzitet[]>(`${environment.api.baseUrl}/univerziteti/import`).subscribe((value)=>{
      this._imported = value;
      console.log("IMPORTED", this._imported);
    }, (error)=> {
      console.log(error);
    })
  }
}
