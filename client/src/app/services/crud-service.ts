import { Observable } from "rxjs";
import { CrudOperations } from "../model/crud-operations";
import { HttpClient } from '@angular/common/http'
import { CookieService } from "ngx-cookie-service";
export abstract class CrudService<T,ID> implements CrudOperations<T,ID> {
    
    constructor(
        protected _http: HttpClient,
        protected _base: string,
        protected cookieService: CookieService,
    ){}
    
    create(t: T): Observable<T> {
        return this._http.post<T>(this._base, t, {headers: {"Authorization": this.cookieService.get("jwt")}});
    }
    update(id: ID, t: T): Observable<T> {
        return this._http.put<T>(this._base + "/" + id, t, {headers: {"Authorization": this.cookieService.get("jwt")}});
    }
    findOne(id: ID): Observable<T> {
        return this._http.get<T>(this._base + "/" + id, {headers: {"Authorization": this.cookieService.get("jwt")}});
    }
    findAll(): Observable<T[]> {
        return this._http.get<T[]>(this._base, {headers: {"Authorization": this.cookieService.get("jwt")}});
    }
    delete(id: ID): Observable<any> {
        return this._http.delete<T>(this._base + '/' + id, {headers: {"Authorization": this.cookieService.get("jwt")}});
    }

}
