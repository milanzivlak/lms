import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Ishod } from '../model/ishod';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class IshodService extends CrudService<Ishod, number> {

  constructor(public _http: HttpClient, public _cookieService: CookieService) {
    super(_http, `${environment.api.baseUrl}/ishodi`, _cookieService)
   }
}
