import { Injectable } from '@angular/core';
import { StudijskiProgram } from '../model/studijski-program';
import { CrudService } from './crud-service';
import {environment} from '../../environments/environment'
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class StudijskiProgramService extends CrudService<StudijskiProgram, number>{

  constructor(public _http:HttpClient, public _cookieService: CookieService) {
    super(_http, `${environment.api.baseUrl}/studijskiprogrami`, _cookieService);
  }
}
