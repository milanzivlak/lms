import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RegistrovaniKorisnik } from '../model/actors/registrovani-korisnik';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class RegistrovaniKorisnikService extends CrudService<RegistrovaniKorisnik, number> {

  constructor(public _http: HttpClient, public _cookieService: CookieService) {
    super(_http, `${environment.api.baseUrl}/registrovanikorisnici`, _cookieService)
   }
}
