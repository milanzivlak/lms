import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Mesto } from '../model/mesto';
import { CrudService } from './crud-service';
import {environment} from '../../environments/environment'
import { env } from 'process';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class MestoService extends CrudService<Mesto, number>{

  constructor(public _http:HttpClient, public _cookieService: CookieService) {
    super(_http, `${environment.api.baseUrl}/mesta`, _cookieService);
  }

  _imported:Mesto[] = [];
  exportAll(){
    this._http.get<Mesto[]>(`${environment.api.baseUrl}/mesta/export`).subscribe((value)=>{
      console.log(value);
    }, (error) =>{
      console.log(error);
    });
  }

  importAll():any{
    this._http.get<Mesto[]>(`${environment.api.baseUrl}/mesta/import`).subscribe((value)=>{
      this._imported = value;
      console.log("IMPORTED", this._imported);
    }, (error) => {
      console.log(error);
    });
  }
}
