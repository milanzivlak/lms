import { Injectable } from '@angular/core';
import { CrudService } from './crud-service';
import { GodinaStudija } from '../model/godina-studija'
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class GodinaStudijaService extends CrudService<GodinaStudija, number> {

  constructor(public _http: HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/godinastudija`, _cookieService)
  }
}
