import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators'
import { JwtService } from './jwt.service';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  token:any|null = null;
  user:any|null = null;
  newUser:any|null = null;

  constructor(
    private httpClient:HttpClient, 
    private jwtService: JwtService,
    private cookieService: CookieService,
    ) { }

  login(user:any){
    console.log(user);
    return this.httpClient.post<any>("http://localhost:8080/api/login", user).pipe(
      tap(r => {
        this.token = r["token"];
        this.user = JSON.parse(atob(r["token"].split(".")[1]));
        console.log(this.user);
        this.cookieService.set("jwt", this.token);
      })
    );
  }

  hasRole(role:string) {
    let token = this.cookieService.get("jwt")
    if(token) {
      return this.jwtService.hasRole(role, token);
    }
    return false;
  }

  register(user:any){
    return this.httpClient.post<any>("http://localhost:8080/api/register", user).subscribe((value)=>{
      console.log(this.user);
    }, (error)=>{
      console.log(error);
    })
     
  }


  validateRoles(roles:any){
    if(this.user){
      for(let r of roles){
        if(this.hasRole(r)){
          return true;
        }
      }
    }
    return false;
  }

  isLoggedIn() {
    let test = this.cookieService.get("jwt") ? true : false
    return test;
  }

  logout() {
    this.user = null;
    this.token = null;
    this.cookieService.delete("jwt")
  }

}
