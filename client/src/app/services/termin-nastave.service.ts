import { Injectable } from '@angular/core';
import { TerminNastave } from '../model/termin-nastave';
import { CrudService } from './crud-service';
import {environment} from '../../environments/environment'
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class TerminNastaveService extends CrudService<TerminNastave, number> {

  constructor(public _http:HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/nastavni-materijal/termin-nastave`, _cookieService);
  }
}
