import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Student } from '../model/student';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class StudentService extends CrudService<Student, number> {

  constructor(public _http: HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/studenti`, _cookieService)
  }

  exportAll(){
    this._http.get<Student[]>(`${environment.api.baseUrl}/studenti/export`).subscribe((value) =>{
      console.log(value);
    }, (error) => {
      console.log(error);
    });
  }
}
