import { Injectable } from '@angular/core';
import { Adresa } from '../model/adresa';
import { CrudService } from './crud-service';
import {environment} from '../../environments/environment'
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AdresaService extends CrudService<Adresa,number> {

  constructor(public _http: HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/adresa`, _cookieService);
  }
}
