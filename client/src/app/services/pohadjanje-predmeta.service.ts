import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PohadjanjePredmeta } from '../model/pohadjanje-predmeta';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class PohadjanjePredmetaService extends CrudService<PohadjanjePredmeta, number> {

  constructor(public _http : HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/pohadjanjePredmeta`, _cookieService)
  }
}
