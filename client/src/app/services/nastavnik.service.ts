import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Nastavnik } from '../model/nastavnik';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class NastavnikService extends CrudService<Nastavnik, number> {

  constructor(public _http : HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/nastavnici`, _cookieService)
  }

  _imported:Nastavnik[] = [];
  exportAll(){
    this._http.get<Nastavnik[]>(`${environment.api.baseUrl}/nastavnici/export`).subscribe((value)=>{
      console.log(value);
    }, (error)=> {
      console.log(error);
    });
  }

  importAll():any{
    this._http.get<Nastavnik[]>(`${environment.api.baseUrl}/nastavnici/import`).subscribe((value)=>{
      this._imported = value;
      console.log("IMPORTED", this._imported);
    }, (error) => {
      console.log(error);
    })
  }
}
