import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment'
import { Fakultet } from '../model/fakultet';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class FakultetService extends CrudService<Fakultet, number>{

  constructor(public _http: HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/fakulteti`, _cookieService);
  }
}
