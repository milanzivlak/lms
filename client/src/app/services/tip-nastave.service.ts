import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TipNastave } from '../model/tip-nastave';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class TipNastaveService extends CrudService<TipNastave, number> {

  constructor(public _http : HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/tipnastave`, _cookieService)
  }

  _imported:TipNastave[] = [];
  exportAll(){
    this._http.get<TipNastave[]>(`${environment.api.baseUrl}/tipnastave/export`).subscribe((value)=>{
      console.log(value);
    }, (error)=> {
      console.log(error);
    });
  }

  importAll():any{
    this._http.get<TipNastave[]>(`${environment.api.baseUrl}/tipnastave/import`).subscribe((value)=>{
      this._imported = value;
      console.log("IMPORTED", this._imported);
    }, (error) => {
      console.log(error);
    })
  }
}
