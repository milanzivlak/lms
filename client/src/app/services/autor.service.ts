import { Injectable } from '@angular/core';
import { Autor } from '../model/autor';
import { CrudService } from './crud-service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AutorService extends CrudService<Autor,number>{

  constructor(public _http: HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/nastavni-materijal/autor`, _cookieService)
  }

  exportAll(){
    this._http.get<Autor[]>(`${environment.api.baseUrl}/nastavni-materijal/autor/export`).subscribe((value) =>{
      console.log(value);
    }, (error) => {
      console.log(error);
    });
  }
}
