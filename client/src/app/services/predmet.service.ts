import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Predmet } from '../model/predmet';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class PredmetService extends CrudService<Predmet, number> {

  constructor(public _http : HttpClient, public _cookieService: CookieService) {
    super(_http, `${environment.api.baseUrl}/predmeti`, _cookieService)
   }

   exportAll(){
    this._http.get<Predmet[]>(`${environment.api.baseUrl}/predmeti/export`).subscribe((value) =>{
      console.log(value);
    }, (error) => {
      console.log(error);
    });
  }
}
