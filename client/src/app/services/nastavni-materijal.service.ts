import { Injectable } from '@angular/core';
import { NastavniMaterijal } from '../model/nastavni-materijal';
import { CrudService } from './crud-service';
import {environment} from '../../environments/environment'
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class NastavniMaterijalService extends CrudService<NastavniMaterijal, number> {

  constructor(public _http:HttpClient, public _cookieService: CookieService) {
    super(_http, `${environment.api.baseUrl}/nastavni-materijal`, _cookieService);
  }
}
