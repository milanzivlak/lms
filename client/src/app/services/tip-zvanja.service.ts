import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TipZvanja } from '../model/tip-zvanja';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class TipZvanjaService extends CrudService<TipZvanja, number> {

  constructor(public _http : HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/tipoviZvanja`, _cookieService)
  }

  _imported:TipZvanja[] = [];
  exportAll(){
    this._http.get<TipZvanja[]>(`${environment.api.baseUrl}/tipoviZvanja/export`).subscribe((value)=>{
      console.log(value);
    }, (error)=> {
      console.log(error);
    });
  }

  importAll():any{
    this._http.get<TipZvanja[]>(`${environment.api.baseUrl}/tipoviZvanja/import`).subscribe((value)=>{
      this._imported = value;
      console.log("IMPORTED", this._imported);
    }, (error) => {
      console.log(error);
    })
  }
}
