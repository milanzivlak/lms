import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RealizacijaPredmeta } from '../model/realizacija-predmeta';
import { CrudService } from './crud-service';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
  providedIn: 'root'
})
export class RealizacijaPredmetaService extends CrudService<RealizacijaPredmeta, number> {

  constructor(public _http : HttpClient, public _cookieService: CookieService) { 
    super(_http, `${environment.api.baseUrl}/realizacijaPredmeta`, _cookieService)
  }
}
