import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MestaEditComponent } from './mesta-edit.component';

describe('MestaEditComponent', () => {
  let component: MestaEditComponent;
  let fixture: ComponentFixture<MestaEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MestaEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MestaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
