import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Mesto } from 'src/app/model/mesto';
import { MestoService } from 'src/app/services/mesto.service';

@Component({
  selector: 'app-mesta-edit',
  templateUrl: './mesta-edit.component.html',
  styleUrls: ['./mesta-edit.component.css']
})
export class MestaEditComponent implements OnInit {
 
  mesto : Mesto | null = null;

  constructor(private mestoService : MestoService, private activatedRoute : ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.mestoService.findOne(this.activatedRoute.snapshot.params['id']).subscribe(r=>{
      this.mesto = r;
    });
  }

  update(mesto : Mesto){
    this.mestoService.update(this.activatedRoute.snapshot.params['id'], mesto).subscribe(r=>{
      this.router.navigate(["/mesta"]);
      
    })
  }

}
