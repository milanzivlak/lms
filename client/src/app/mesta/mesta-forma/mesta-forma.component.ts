import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Drzava } from 'src/app/model/drzava';
import { Mesto } from 'src/app/model/mesto';
import { DrzavaService } from 'src/app/services/drzava.service';
import { MestoService } from 'src/app/services/mesto.service';

@Component({
  selector: 'app-mesta-forma',
  templateUrl: './mesta-forma.component.html',
  styleUrls: ['./mesta-forma.component.css']
})
export class MestaFormaComponent implements OnInit, OnChanges {

  @Input()
  mesto : Mesto | null = null;

  form = new FormGroup({
    id: new FormControl(),
    naziv: new FormControl(),
    drzava: new FormControl()
  });
  
  drzave:Drzava[] = [];
  drzava:any|null = null;
  constructor(public mestoService:MestoService, public drzavaService:DrzavaService) { }
  
  ngOnChanges(changes: SimpleChanges): void {
    if(changes["mesto"].currentValue != null){
      this.form.setValue({id: this.mesto.id, naziv: this.mesto.naziv, drzava: this.mesto.drzava});
      this.drzava = this.mesto.drzava;
      console.log(this.drzava);
    }
  }

  ngOnInit(): void {
    this.findAllDrzave();
  }

  submit(){
    this.mestoService.create(this.form.value).subscribe((value) => {
      console.log(this.form.value);
      console.log("dasdsadsa")
    }, (error) => {
      console.log(error);
    });
  }

  findAllDrzave(){
    this.drzavaService.findAll().subscribe((value) =>{
      this.drzave = value;
      
    }, (error) => {
      console.log(error);
    });
  }
}
