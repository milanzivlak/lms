import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MestaFormaComponent } from './mesta-forma.component';

describe('MestaFormaComponent', () => {
  let component: MestaFormaComponent;
  let fixture: ComponentFixture<MestaFormaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MestaFormaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MestaFormaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
