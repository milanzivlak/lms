import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from '../model/base-component';
import { Fakultet } from '../model/fakultet';
import { StudijskiProgram } from '../model/studijski-program';
import { FakultetService } from '../services/fakultet.service';
import { StudijskiProgramService } from '../services/studijski-program.service';

@Component({
  selector: 'app-fakulteti',
  templateUrl: './fakulteti.component.html',
  styleUrls: ['./fakulteti.component.css']
})
export class FakultetiComponent extends BaseComponent<Fakultet,number> implements OnInit {

  constructor(public fakultetService:FakultetService, public studijskiProgramiService:StudijskiProgramService) { 
    super(fakultetService);
  }
  displayedColumns: string[] = ['id', 'naziv', 'studijskiProgram', 'akcije'];
  displayedColumnsSP: string[] = ['id', 'naziv'];
  dataSource = new MatTableDataSource(this._elements);
  @ViewChild(MatSort) sort : MatSort;
  @ViewChild(MatPaginator) paginator : MatPaginator
  izabran:boolean = false;
  spFakulteta:StudijskiProgram[] = [];
  izabraniFakultet:Fakultet = null;
  dataSourceSP = new MatTableDataSource(this.spFakulteta);
  temp:any = [];


  ngOnInit(): void {
    this.findAllAsync();
  }

  findAllAsync(){
    this.fakultetService.findAll().subscribe((value) =>{
      this._elements = value;
      this.dataSource = new MatTableDataSource(this._elements);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      
    }, (error) => {
      console.log(error);
    });
    
  }

  findAllStudijskiProgrami(){
    this.spFakulteta = [];
    this.studijskiProgramiService.findAll().subscribe((value) =>{

      this.temp = value;
      for(let i = 0; i < this.temp.length; i++){
        if(this.temp[i].fakultet.id == this.izabraniFakultet.id){
          this.spFakulteta.push(this.temp[i]);
        }
      }
      this.dataSourceSP = new MatTableDataSource(this.spFakulteta);
      this.dataSourceSP.paginator = this.paginator;
    }, (error) => {
      console.log(error);
    });
  }
  selected(f:Fakultet){
    this.izabran = true;
    this.izabraniFakultet = f;
    this.findAllStudijskiProgrami();
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteRow(element:Fakultet){
    console.log(element);
    this.delete(element.id);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.dataSource.sort = this.sort;
  }
}
