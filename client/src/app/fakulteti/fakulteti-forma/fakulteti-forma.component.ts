import { Input, OnChanges, SimpleChanges } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Adresa } from 'src/app/model/adresa';
import { Fakultet } from 'src/app/model/fakultet';
import { Nastavnik } from 'src/app/model/nastavnik';
import { Univerzitet } from 'src/app/model/univerzitet';
import { AdresaService } from 'src/app/services/adresa.service';
import { FakultetService } from 'src/app/services/fakultet.service';
import { NastavnikService } from 'src/app/services/nastavnik.service';
import { UniverzitetService } from 'src/app/services/univerzitet.service';

@Component({
  selector: 'app-fakulteti-forma',
  templateUrl: './fakulteti-forma.component.html',
  styleUrls: ['./fakulteti-forma.component.css']
})
export class FakultetiFormaComponent implements OnInit, OnChanges {

  fakulteti: Fakultet[] = [];

  form = new FormGroup({
    id: new FormControl("", [this.idValidator()]),
    naziv: new FormControl("", Validators.required),
    adresa: new FormControl("", Validators.required),
    univerzitet: new FormControl("", Validators.required),
    dekan: new FormControl("", Validators.required)
  })

  @Input()
  fakultet:Fakultet|null = null;
  adrese: Adresa[] = [];
  univerziteti: Univerzitet[] = [];
  dekani: Nastavnik[] = [];
  pageable:any;
  
  @Input()
  selected:Adresa|null = null;
  constructor(public fakultetService : FakultetService, public adresaService : AdresaService, public univerzitetService : UniverzitetService, public nastavnikService : NastavnikService) { }

  found:boolean = false;

  ngOnChanges(changes: SimpleChanges): void {
    if(changes["fakultet"].currentValue != null){
      this.form.setValue({id: this.fakultet.id, naziv: this.fakultet.naziv, adresa: this.fakultet.adresa, univerzitet: this.fakultet.univerzitet, dekan:this.fakultet.dekan});
    }
  }
  
  ngOnInit(): void {
    this.findAllAdrese();
    this.findAllUniverziteti();
    this.findAllNastavnici();
    this.findAllFakulteti();
  }

  submit(){
    this.fakultetService.create(this.form.value).subscribe((value)=> {
      console.log(this.form.value);
    }, (error)=> {
      console.log(error);
    })
  }

  findAllFakulteti(){
    this.fakultetService.findAll().subscribe((value)=>{
      this.fakulteti = value;
    }, (error)=>{
      console.log(error);
    })
  }

  findAllUniverziteti(){
    this.univerzitetService.findAll().subscribe((value)=>{
      this.univerziteti = value;
    }, (error)=>{
      console.log(error);
    })
  }

  findAllAdrese(){
    this.adresaService.findAll().subscribe((value)=>{
      this.pageable = value;
      this.adrese = this.pageable.content;
    }, (error)=>{
      console.log(error);
    })
  }

  findAllNastavnici(){
    this.nastavnikService.findAll().subscribe((value)=>{
      this.dekani = value;
    }, (error)=>{
      console.log(error);
    })
  }

  idValidator(): ValidatorFn {
   
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      this.found = false;
      this.fakultetService.findAll().subscribe((value) =>{
        this.fakulteti = value;
        
      }, (error) => {
        console.log(error);
      });
      
      if (control.value !== undefined) {

        for(let i =0; i < this.fakulteti.length; i++){
          
          if(this.fakulteti[i].id == control.value){
            this.found = true;
          }
        }
        
        if(this.found == true){
          return { 'id': true };
        }
          
      }
      return null;
    };
  }
}
