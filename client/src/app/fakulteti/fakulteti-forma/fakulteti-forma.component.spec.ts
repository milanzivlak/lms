import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FakultetiFormaComponent } from './fakulteti-forma.component';

describe('FakultetiFormaComponent', () => {
  let component: FakultetiFormaComponent;
  let fixture: ComponentFixture<FakultetiFormaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FakultetiFormaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FakultetiFormaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
