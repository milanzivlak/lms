import { NonNullAssert } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Adresa } from 'src/app/model/adresa';
import { Fakultet } from 'src/app/model/fakultet';
import { FakultetService } from 'src/app/services/fakultet.service';

@Component({
  selector: 'app-fakulteti-edit',
  templateUrl: './fakulteti-edit.component.html',
  styleUrls: ['./fakulteti-edit.component.css']
})
export class FakultetiEditComponent implements OnInit {

  fakultet:Fakultet|null = null;
  adresaFakulteta:Adresa|null = null;
  constructor(public fakultetService:FakultetService, private activatedRoute : ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.fakultetService.findOne(this.activatedRoute.snapshot.params['id']).subscribe(r=>{
      this.fakultet = r;
    });
  }

  update(fakultet : Fakultet){
    console.log("Adresa", fakultet.adresa);
    // this.fakultetService.update(this.activatedRoute.snapshot.params['id'], fakultet).subscribe(r=>{
    //   console.log(r);
    //   this.router.navigate(["/fakulteti"]);
    // })
  }

}
