import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FakultetiEditComponent } from './fakulteti-edit.component';

describe('FakultetiEditComponent', () => {
  let component: FakultetiEditComponent;
  let fixture: ComponentFixture<FakultetiEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FakultetiEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FakultetiEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
